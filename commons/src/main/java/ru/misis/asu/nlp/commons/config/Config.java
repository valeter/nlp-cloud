package ru.misis.asu.nlp.commons.config;

import ru.misis.asu.nlp.commons.Pair;

import java.io.PrintStream;
import java.lang.reflect.Modifier;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Optional;

/**
 * @author valter
 *         24.06.17
 */
public class Config {
    public static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    public static final Pair<String, String> PORT = new Pair<>("Port", "8080");

    public static final Pair<String, String> LANGUAGE = new Pair<>("InputLanguage", "ru");

    public static final Pair<String, String> DESCRIPTOR_FILE = new Pair<>("DescriptorFile",
            "uima_xml/" + getProperty(LANGUAGE) + "-spellcheck-ae.xml");

    public static final Pair<String, String> DESCRIPTOR_FILE_INTERACTIVE = new Pair<>("DescriptorFileInteractive",
            "uima_xml/" + getProperty(LANGUAGE) + "-spellcheck-interactive-ae.xml");

    public static final Pair<String, String> INPUT_DIRECTORY = new Pair<>("InputDirectory",
            System.getProperty("user.home") + "/tmp/in_" + getProperty(LANGUAGE));

    public static final Pair<String, String> INPUT_SEARCH_SUBDIRECTORIES = new Pair<>("SearchSubdirectories", "true");

    public static final Pair<String, String> OUTPUT_FILE = new Pair<>("OutputFile",
            System.getProperty("user.home") + "/tmp/out/out_" + getProperty(LANGUAGE) + "_" + DATE_TIME_FORMAT.format(LocalDateTime.now())  + ".txt");

    public static final Pair<String, String> OUTPUT_XMI = new Pair<>("OutputXmi", "true");

    public static final Pair<String, String> OUTPUT_XMI_FILE = new Pair<>("OutputXmiFile",
            System.getProperty("user.home") + "/tmp/out/out_" + getProperty(LANGUAGE) + "_" + DATE_TIME_FORMAT.format(LocalDateTime.now())  + ".xmi");

    public static final Pair<String, String> LINE_SEPARATOR = new Pair<>("LineSeparator", System.getProperty("line.separator"));


    public static String getProperty(Pair<String, String> propertyNameAdDefaultValue) {
        return System.getProperty(propertyNameAdDefaultValue.getFirst(), propertyNameAdDefaultValue.getSecond());
    }

    public static void printUsage(PrintStream out) {
        out.println("Usage info ");
        out.println();

        out.println("Available params: ");
        Arrays.stream(Config.class.getDeclaredFields())
                .filter(f -> Modifier.isStatic(f.getModifiers()))
                .filter(f -> f.getType() == Pair.class)
                .filter(f -> !"LINE_SEPARATOR".equals(f.getName()))
                .forEach(f -> {
            try {
                Pair p = (Pair)f.get(Config.class);
                out.printf("\t-D%-30s-\tcurrent value: %-50s\n", p.getFirst(), getProperty(p));
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        });
    }
}
