package ru.misis.asu.nlp.commons.ngram;

/**
 * @author valter
 *         19.03.17
 */
public interface PosNgramDictionary extends NgramDictionary {
    int getFrequency(String word1, String pos1, String word2, String pos2);
}
