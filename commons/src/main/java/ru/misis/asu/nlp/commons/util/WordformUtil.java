package ru.misis.asu.nlp.commons.util;

/**
 * @author valter
 *         06.04.17
 */
public class WordformUtil {
    public static String fixWordformString(String wordformString) {
        return "[" + wordformString.replaceAll("sofa[^\n\r]*", "")
            .replaceAll("begin[^\n\r]*", "")
            .replaceAll("end[^\n\r]*", "")
            .replaceAll("lemmaId[^\n\r]*", "")
            .replaceAll("grammemBits[^\n\r]*", "")
            .replaceAll("Array length[^\n\r]*", "")
            .replaceAll("Array elements: \\[[0-9,\\s]+\\]", "")
            .replaceAll("StringArray", "")
            .replaceAll("Array elements:", "")
            .replaceAll("Wordform", "")
            .replaceAll("[\n\r\\s]+", " ") + "]";
    }
}
