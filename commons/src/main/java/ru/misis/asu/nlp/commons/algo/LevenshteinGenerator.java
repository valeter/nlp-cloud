package ru.misis.asu.nlp.commons.algo;

import com.github.liblevenshtein.transducer.Algorithm;
import com.github.liblevenshtein.transducer.Candidate;
import com.github.liblevenshtein.transducer.ITransducer;
import com.github.liblevenshtein.transducer.factory.TransducerBuilder;

import java.util.*;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         07.02.2016
 */
public class LevenshteinGenerator {
    private ITransducer<Candidate> transducer;

    public LevenshteinGenerator(int distance, Collection<String> dictionary) {
        transducer = new TransducerBuilder()
                .algorithm(Algorithm.TRANSPOSITION)
                .defaultMaxDistance(distance)
                .dictionary(dictionary)
                .build();
    }

    public Collection<String> generate(String word) {
        Set<String> results = new HashSet<>();
        for (final Candidate candidate : transducer.transduce(word)) {
            results.add(candidate.term());
        }
        results.addAll(removeRepeats(word));
        return results;
    }

    private List<String> removeRepeats(String text) {
        List<String> result = new ArrayList<>();
        for (int i = 0; i < text.length(); i++) {
            int to = i;
            while (to + 1 < text.length() && text.charAt(to + 1) == text.charAt(i)) {
                to++;
            }

            if (to > i) {
                StringBuilder builder = new StringBuilder();
                if (i > 0) {
                    builder.append(text.substring(0, i));
                }
                builder.append(text.charAt(i));
                if (to + 1 < text.length()) {
                    builder.append(text.substring(to + 1, text.length()));
                }
                result.add(builder.toString());
            }
        }
        return result;
    }
}
