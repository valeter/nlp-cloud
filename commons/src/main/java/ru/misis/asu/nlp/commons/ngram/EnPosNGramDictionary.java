package ru.misis.asu.nlp.commons.ngram;

import ru.misis.asu.nlp.commons.config.Config;
import ru.misis.asu.nlp.commons.ngram.PosNgramDictionary;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author valter
 * @date 08.04.2016.
 */
public class EnPosNGramDictionary implements PosNgramDictionary {
    private Map<String, Set<String>> POS_TAG_MAPPINGS = new HashMap<String, Set<String>>() {{
        put("ccb", new HashSet<>(Arrays.asList("cc")));
        put("uh32", new HashSet<>(Arrays.asList("cc")));
        put("cst", new HashSet<>(Arrays.asList("cc", "in")));
        put("uh33", new HashSet<>(Arrays.asList("uh")));
        put("csw", new HashSet<>(Arrays.asList("cc", "in")));
        put("uh31", new HashSet<>(Arrays.asList("uh")));
        put("vhz", new HashSet<>(Arrays.asList("vb", "vbn", "vbp", "vbz")));
        put("rt41", new HashSet<>(Arrays.asList("rb", "rbr", "rbs")));
        put("vbdr", new HashSet<>(Arrays.asList("vb", "vbd", "vbn")));
        put("nn33", new HashSet<>(Arrays.asList("nns")));
        put("rt32", new HashSet<>(Arrays.asList("rb")));
        put("rt31", new HashSet<>(Arrays.asList("in")));
        put("ppis2", new HashSet<>(Arrays.asList("prp")));
        put("rt33", new HashSet<>(Arrays.asList("in")));
        put("ppis1", new HashSet<>(Arrays.asList("prp")));
        put("uh21", new HashSet<>(Arrays.asList("nn", "nnp")));
        put("uh22", new HashSet<>(Arrays.asList("nn", "nnp")));
        put("rl32", new HashSet<>(Arrays.asList("cc")));
        put("nn32", new HashSet<>(Arrays.asList("nns")));
        put("nn31", new HashSet<>(Arrays.asList("fw")));
        put("rl31", new HashSet<>(Arrays.asList("to")));
        put("nnu2", new HashSet<>(Arrays.asList("nn", "nnp", "nns")));
        put("nnu1", new HashSet<>(Arrays.asList("nn", "nnp")));
        put("ra", new HashSet<>(Arrays.asList("rb")));
        put("rl33", new HashSet<>(Arrays.asList("nn")));
        put("nn121", new HashSet<>(Arrays.asList("nn", "nnp", "nns")));
        put("rrq", new HashSet<>(Arrays.asList("rb")));
        put("nn122", new HashSet<>(Arrays.asList("nn", "nnp", "nns")));
        put("rrr", new HashSet<>(Arrays.asList("rb", "rbr")));
        put("at", new HashSet<>(Arrays.asList("dt", "nnp", "rb")));
        put("nn22", new HashSet<>(Arrays.asList("nn", "nnp")));
        put("rt21", new HashSet<>(Arrays.asList("rb")));
        put("rrt", new HashSet<>(Arrays.asList("rb", "rbs")));
        put("rg", new HashSet<>(Arrays.asList("rb", "rp")));
        put("rt22", new HashSet<>(Arrays.asList("rb")));
        put("rl", new HashSet<>(Arrays.asList("rb", "rp")));
        put("rp", new HashSet<>(Arrays.asList("rp")));
        put("rr", new HashSet<>(Arrays.asList("rb")));
        put("rt", new HashSet<>(Arrays.asList("rb")));
        put("rr21", new HashSet<>(Arrays.asList("rb", "rp")));
        put("vvnk", new HashSet<>(Arrays.asList("vbn")));
        put("rrqv", new HashSet<>(Arrays.asList("jjr", "nnp", "rb", "vb", "wrb")));
        put("ppho1", new HashSet<>(Arrays.asList("prp")));
        put("ppho2", new HashSet<>(Arrays.asList("prp")));
        put("bcl22", new HashSet<>(Arrays.asList("in")));
        put("bcl21", new HashSet<>(Arrays.asList("in")));
        put("at1", new HashSet<>(Arrays.asList("dt", "nn", "nnp", "to")));
        put("cc", new HashSet<>(Arrays.asList("cc", "in", "nn", "nnp")));
        put("pnqs33", new HashSet<>(Arrays.asList("wp")));
        put("f", new HashSet<>(Arrays.asList("in", "nn", "nnp", "nns")));
        put("j", new HashSet<>(Arrays.asList("nn", "nnp", "nns", "rb", "vb", "vbg")));
        put("m", new HashSet<>(Arrays.asList("cd", "nn", "nnp", "nns")));
        put("n", new HashSet<>(Arrays.asList("nn", "nns")));
        put("nn1", new HashSet<>(Arrays.asList("nn", "nnp")));
        put("cs", new HashSet<>(Arrays.asList("in")));
        put("p", new HashSet<>(Arrays.asList("nn", "prp", "vb")));
        put("nn221", new HashSet<>(Arrays.asList("nn", "nns", "vb", "vbp")));
        put("nn222", new HashSet<>(Arrays.asList("nns", "vbp")));
        put("nn2", new HashSet<>(Arrays.asList("nnps", "nns")));
        put("rt43", new HashSet<>(Arrays.asList("nn")));
        put("vbdz", new HashSet<>(Arrays.asList("vb", "vbd")));
        put("rt42", new HashSet<>(Arrays.asList("dt")));
        put("rt44", new HashSet<>(Arrays.asList("vbg")));
        put("ppy", new HashSet<>(Arrays.asList("nn", "nnp", "nns", "prp", "vb")));
        put("to", new HashSet<>(Arrays.asList("dt", "in", "nn", "nnp", "rb", "to")));
        put("da", new HashSet<>(Arrays.asList("nn", "nnp", "pdt", "vb")));
        put("db", new HashSet<>(Arrays.asList("dt", "nn", "nnp", "pdt", "rb")));
        put("dd", new HashSet<>(Arrays.asList("dt", "nn", "nnp", "rb", "vb")));
        put("rrqv32", new HashSet<>(Arrays.asList("nn", "vb")));
        put("cs21", new HashSet<>(Arrays.asList("in", "rb", "vb", "vbg", "vbn")));
        put("rrqv33", new HashSet<>(Arrays.asList("wrb")));
        put("rrqv31", new HashSet<>(Arrays.asList("dt")));
        put("rr41", new HashSet<>(Arrays.asList("dt", "in", "rb")));
        put("pn21", new HashSet<>(Arrays.asList("nn")));
        put("rr43", new HashSet<>(Arrays.asList("cc", "dt", "in", "jjs", "to")));
        put("pn22", new HashSet<>(Arrays.asList()));
        put("ddqv", new HashSet<>(Arrays.asList("jjr", "rb", "wdt")));
        put("rr42", new HashSet<>(Arrays.asList("cc", "dt", "in", "nn")));
        put("ppx1", new HashSet<>(Arrays.asList("nn", "nnp", "prp")));
        put("ppx2", new HashSet<>(Arrays.asList("nns", "prp", "vbz")));
        put("rgqv32", new HashSet<>(Arrays.asList("nn", "vb")));
        put("rgqv31", new HashSet<>(Arrays.asList("dt")));
        put("uh", new HashSet<>(Arrays.asList("uh")));
        put("rgqv33", new HashSet<>(Arrays.asList("wrb")));
        put("pnqs", new HashSet<>(Arrays.asList("wp")));
        put("npm2", new HashSet<>(Arrays.asList("nnp")));
        put("npm1", new HashSet<>(Arrays.asList("nn", "nnp")));
        put("rr33", new HashSet<>(Arrays.asList("rb")));
        put("vmk", new HashSet<>(Arrays.asList("md", "vbd", "vbn", "vbp")));
        put("pnqv", new HashSet<>(Arrays.asList("nn", "vb", "wp")));
        put("ra22", new HashSet<>(Arrays.asList("nn", "nnp", "vbg")));
        put("ra21", new HashSet<>(Arrays.asList("cc", "in", "nnp", "rb")));
        put("cs22", new HashSet<>(Arrays.asList("dt", "in", "nn", "nnp", "wdt", "wrb")));
        put("nna", new HashSet<>(Arrays.asList("nn", "nnp")));
        put("nnb", new HashSet<>(Arrays.asList("nn", "nnp", "vb")));
        put("rex", new HashSet<>(Arrays.asList("rb")));
        put("pnqo", new HashSet<>(Arrays.asList("wp")));
        put("rr32", new HashSet<>(Arrays.asList("rb")));
        put("rr31", new HashSet<>(Arrays.asList("rb")));
        put("np1", new HashSet<>(Arrays.asList("nn", "nnp", "nnps", "nns")));
        put("nno", new HashSet<>(Arrays.asList("nn", "nnp")));
        put("vd", new HashSet<>(Arrays.asList("nnp", "vb", "vbp", "vbz")));
        put("np2", new HashSet<>(Arrays.asList("nn", "nnp", "nnps", "nns", "vbz")));
        put("ex", new HashSet<>(Arrays.asList("ex", "nn", "nnp", "nns", "rb")));
        put("nnu", new HashSet<>(Arrays.asList("dt", "in", "nn", "nnp", "nns", "rb", "vb", "vbp")));
        put("rr22", new HashSet<>(Arrays.asList("rb", "rbr")));
        put("mc221", new HashSet<>(Arrays.asList("nn", "nnp")));
        put("vm", new HashSet<>(Arrays.asList("md", "nn", "nnp", "nns", "vb", "vbd", "vbn", "vbp")));
        put("cs42", new HashSet<>(Arrays.asList("in", "rb")));
        put("cs43", new HashSet<>(Arrays.asList("rb")));
        put("cs41", new HashSet<>(Arrays.asList("in")));
        put("fo", new HashSet<>(Arrays.asList("cd", "nn", "nnp", "vb")));
        put("fu", new HashSet<>(Arrays.asList("dt", "in", "nn", "nnp", "nns", "rb", "to", "vb", "vbd", "vbp")));
        put("nn131", new HashSet<>(Arrays.asList("nn", "nnp")));
        put("nn132", new HashSet<>(Arrays.asList("in", "nn", "nnp")));
        put("fw", new HashSet<>(Arrays.asList("fw")));
        put("nn133", new HashSet<>(Arrays.asList("nn", "nnp", "nns")));
        put("rr55", new HashSet<>(Arrays.asList("nn", "rb")));
        put("pphs1", new HashSet<>(Arrays.asList("nn", "nnp", "prp")));
        put("pphs2", new HashSet<>(Arrays.asList("nn", "nnp", "nns", "prp")));
        put("rgr", new HashSet<>(Arrays.asList("jjr", "rbr")));
        put("rgq", new HashSet<>(Arrays.asList("nn", "nnp", "wrb")));
        put("rgt", new HashSet<>(Arrays.asList("jjs", "nn", "nnp", "rbs")));
        put("cs44", new HashSet<>(Arrays.asList("in")));
        put("cs31", new HashSet<>(Arrays.asList("in", "nnp", "rb")));
        put("cs32", new HashSet<>(Arrays.asList("nnp", "rb")));
        put("nnt131", new HashSet<>(Arrays.asList("fw")));
        put("nnt133", new HashSet<>(Arrays.asList("fw")));
        put("rr52", new HashSet<>(Arrays.asList("nn")));
        put("rr51", new HashSet<>(Arrays.asList("dt")));
        put("rr54", new HashSet<>(Arrays.asList("dt")));
        put("ppge", new HashSet<>(Arrays.asList("prp", "prp$")));
        put("rr53", new HashSet<>(Arrays.asList("in")));
        put("nn141", new HashSet<>(Arrays.asList("nn")));
        put("nn142", new HashSet<>(Arrays.asList("fw")));
        put("nn143", new HashSet<>(Arrays.asList("fw")));
        put("nn144", new HashSet<>(Arrays.asList("fw")));
        put("rr44", new HashSet<>(Arrays.asList("dt", "nn", "pdt", "vb")));
        put("npx", new HashSet<>(Arrays.asList("md", "nn", "nnp", "nnps", "nns", "vb", "vbg")));
        put("pph1", new HashSet<>(Arrays.asList("nn", "nnp", "prp")));
        put("cs33", new HashSet<>(Arrays.asList("in", "nnp")));
        put("xx", new HashSet<>(Arrays.asList("dt", "nn", "nnp", "nns", "rb", "vb", "vbz")));
        put("rg22", new HashSet<>(Arrays.asList("in", "rb", "to")));
        put("rg21", new HashSet<>(Arrays.asList("in", "nn", "nns", "rb", "rp")));
        put("vm21", new HashSet<>(Arrays.asList("vb", "vbd", "vbn")));
        put("pnx1", new HashSet<>(Arrays.asList("nn", "rb", "vb")));
        put("vb0", new HashSet<>(Arrays.asList("vb", "vbp")));
        put("csw31", new HashSet<>(Arrays.asList("in", "nnp")));
        put("csw33", new HashSet<>(Arrays.asList("rb")));
        put("csw32", new HashSet<>(Arrays.asList("cc")));
        put("if", new HashSet<>(Arrays.asList("in")));
        put("ii", new HashSet<>(Arrays.asList("in")));
        put("io", new HashSet<>(Arrays.asList("in")));
        put("iw", new HashSet<>(Arrays.asList("in", "nn", "nnp")));
        put("pn122", new HashSet<>(Arrays.asList("cd", "nn", "prp")));
        put("pn121", new HashSet<>(Arrays.asList("dt", "nnp", "rb")));
        put("rgqv", new HashSet<>(Arrays.asList("rb")));
        put("jjr", new HashSet<>(Arrays.asList("jjr")));
        put("appge", new HashSet<>(Arrays.asList("nn", "nnp", "prp", "prp$")));
        put("jjt", new HashSet<>(Arrays.asList("jjs")));
        put("nnl2", new HashSet<>(Arrays.asList("nnp", "nnps", "nns")));
        put("nnl1", new HashSet<>(Arrays.asList("nn", "nnp", "vb")));
        put("ppx121", new HashSet<>(Arrays.asList("cd")));
        put("ppx122", new HashSet<>(Arrays.asList("dt", "nn")));
        put("nd1", new HashSet<>(Arrays.asList("nn", "nnp", "nns", "rb", "vb", "vbp")));
        put("vbg", new HashSet<>(Arrays.asList("vbg")));
        put("vbi", new HashSet<>(Arrays.asList("vb", "vbp")));
        put("ddqge", new HashSet<>(Arrays.asList("wp$")));
        put("jj", new HashSet<>(Arrays.asList("jj")));
        put("vbm", new HashSet<>(Arrays.asList("nnp", "rb", "vb", "vbp")));
        put("rg44", new HashSet<>(Arrays.asList("in")));
        put("jk", new HashSet<>(Arrays.asList("nn")));
        put("rg43", new HashSet<>(Arrays.asList("nn")));
        put("vbn", new HashSet<>(Arrays.asList("nn", "nnp", "nns", "vbd", "vbn")));
        put("vd0", new HashSet<>(Arrays.asList("in", "nn", "nnp", "vb", "vbp")));
        put("rg42", new HashSet<>(Arrays.asList("dt")));
        put("vbr", new HashSet<>(Arrays.asList("vb", "vbp")));
        put("rg41", new HashSet<>(Arrays.asList("in")));
        put("jj21", new HashSet<>(Arrays.asList("dt", "in", "nn", "nnp", "rb", "vb", "vbn")));
        put("ii32", new HashSet<>(Arrays.asList("dt", "nn", "nnp", "nns", "rb", "vb", "vbg", "vbn")));
        put("vbz", new HashSet<>(Arrays.asList("nn", "nnp", "nns", "vbz")));
        put("ii31", new HashSet<>(Arrays.asList("in", "rb", "rp")));
        put("nno2", new HashSet<>(Arrays.asList("nns")));
        put("ii22", new HashSet<>(Arrays.asList("dt", "in", "nn", "nnp", "rb", "to", "vbz")));
        put("da2", new HashSet<>(Arrays.asList("nn", "nnp", "vb", "vbp")));
        put("da1", new HashSet<>(Arrays.asList("nn", "nnp", "rb")));
        put("mc1", new HashSet<>(Arrays.asList("cd", "ls", "nn", "nnp", "prp")));
        put("mc2", new HashSet<>(Arrays.asList("cd", "nnp", "nnps", "nns", "rb")));
        put("m1", new HashSet<>(Arrays.asList("cd", "nn", "nnp")));
        put("ii21", new HashSet<>(Arrays.asList("in")));
        put("vv0", new HashSet<>(Arrays.asList("vb")));
        put("db2", new HashSet<>(Arrays.asList("cc", "dt")));
        put("vdd", new HashSet<>(Arrays.asList("nn", "nnp", "vb", "vbd", "vbn")));
        put("vdg", new HashSet<>(Arrays.asList("vbg")));
        put("xxy", new HashSet<>(Arrays.asList("nnp")));
        put("vdi", new HashSet<>(Arrays.asList("in", "nn", "nnp", "rp", "vb", "vbp")));
        put("vdn", new HashSet<>(Arrays.asList("nn", "nnp", "rb", "vb", "vbd", "vbn")));
        put("jj41", new HashSet<>(Arrays.asList("in")));
        put("vdz", new HashSet<>(Arrays.asList("nn", "nnp", "nns", "vb", "vbz")));
        put("jj43", new HashSet<>(Arrays.asList("dt")));
        put("jj42", new HashSet<>(Arrays.asList("to")));
        put("jj33", new HashSet<>(Arrays.asList("nn", "vb")));
        put("ii44", new HashSet<>(Arrays.asList("in", "to")));
        put("ppio2", new HashSet<>(Arrays.asList("nnp", "prp")));
        put("rex43", new HashSet<>(Arrays.asList("to")));
        put("vvgk", new HashSet<>(Arrays.asList("nn", "nnp", "vb", "vbg", "vbp")));
        put("rex44", new HashSet<>(Arrays.asList("vb")));
        put("dar", new HashSet<>(Arrays.asList("jjr", "nn", "nnp", "rbr")));
        put("rex41", new HashSet<>(Arrays.asList("dt")));
        put("ppio1", new HashSet<>(Arrays.asList("in", "nn", "nnp", "prp", "rb")));
        put("rex42", new HashSet<>(Arrays.asList("vbz")));
        put("mc", new HashSet<>(Arrays.asList("cd", "nn", "nnp", "nns", "rb", "sym", "vb", "vbn")));
        put("dat", new HashSet<>(Arrays.asList("jjs", "nn", "nnp", "rbs")));
        put("md", new HashSet<>(Arrays.asList("cd", "in", "nn", "nnp", "nns", "rb", "vb", "vbp")));
        put("mf", new HashSet<>(Arrays.asList("cd", "nn", "nns", "rb")));
        put("ddqv31", new HashSet<>(Arrays.asList("dt")));
        put("ddqv32", new HashSet<>(Arrays.asList("nn", "vb")));
        put("pnqs32", new HashSet<>(Arrays.asList("nn")));
        put("ddqv33", new HashSet<>(Arrays.asList("wdt", "wp")));
        put("pnqs31", new HashSet<>(Arrays.asList("dt")));
        put("npd2", new HashSet<>(Arrays.asList("nnp", "nnps", "nns")));
        put("vvd", new HashSet<>(Arrays.asList("vb", "vbd", "vbg", "vbn", "vbp", "vbz")));
        put("vvg", new HashSet<>(Arrays.asList("vb", "vbg", "vbn", "vbp")));
        put("vvi", new HashSet<>(Arrays.asList("vb")));
        put("ii41", new HashSet<>(Arrays.asList("in", "rp")));
        put("jj32", new HashSet<>(Arrays.asList("in", "nnp", "to")));
        put("ii43", new HashSet<>(Arrays.asList("nn", "nnp")));
        put("vvn", new HashSet<>(Arrays.asList("vb", "vbd", "vbg", "vbn", "vbp")));
        put("jj31", new HashSet<>(Arrays.asList("dt", "in", "nn", "rp")));
        put("ii42", new HashSet<>(Arrays.asList("dt", "in")));
        put("dd1", new HashSet<>(Arrays.asList("cc", "dt", "in", "nn", "nnp", "nns", "rb", "vb", "wdt")));
        put("jj22", new HashSet<>(Arrays.asList("in", "nn", "nnp", "nns", "rb", "rp", "vbn")));
        put("ii33", new HashSet<>(Arrays.asList("in", "nnp", "rb", "to")));
        put("dd2", new HashSet<>(Arrays.asList("dt", "nn", "nns")));
        put("vvz", new HashSet<>(Arrays.asList("nn", "nnp", "nnps", "nns", "vb", "vbn", "vbz")));
        put("npd1", new HashSet<>(Arrays.asList("nn", "nnp")));
        put("vh0", new HashSet<>(Arrays.asList("nn", "nnp", "vb", "vbn", "vbp", "vbz")));
        put("nn", new HashSet<>(Arrays.asList("nn")));
        put("np", new HashSet<>(Arrays.asList("nnp")));
        put("nn21", new HashSet<>(Arrays.asList("nn", "nnp")));
        put("rl21", new HashSet<>(Arrays.asList("in", "nn", "rb", "rp", "vb")));
        put("nnt2", new HashSet<>(Arrays.asList("nn", "nnp", "nnps", "nns", "vb", "vbz")));
        put("nnt1", new HashSet<>(Arrays.asList("in", "nn", "nnp", "nns", "rb", "vb", "vbg")));
        put("rl22", new HashSet<>(Arrays.asList("in", "nn", "nnp", "rb", "rp")));
        put("ddqge31", new HashSet<>(Arrays.asList("dt")));
        put("rex21", new HashSet<>(Arrays.asList("dt", "in")));
        put("ddqge32", new HashSet<>(Arrays.asList("nn")));
        put("rex22", new HashSet<>(Arrays.asList("nn", "vbz")));
        put("ddqge33", new HashSet<>(Arrays.asList("wp$")));
        put("rpk", new HashSet<>(Arrays.asList("in", "nn", "rb")));
        put("ddq", new HashSet<>(Arrays.asList("wdt", "wp")));
        put("csa", new HashSet<>(Arrays.asList("in", "nnp", "rb")));
        put("pn1", new HashSet<>(Arrays.asList("cd", "nn", "nnp", "prp", "vb", "vbg", "vbp")));
        put("vhd", new HashSet<>(Arrays.asList("in", "nn", "nnp", "vbd", "vbn")));
        put("vhg", new HashSet<>(Arrays.asList("nn", "vbg")));
        put("nnu22", new HashSet<>(Arrays.asList("nn")));
        put("nnu21", new HashSet<>(Arrays.asList("in")));
        put("vhi", new HashSet<>(Arrays.asList("nn", "nnp", "vb", "vbp")));
        put("ppx221", new HashSet<>(Arrays.asList("dt", "nnp")));
        put("ppx222", new HashSet<>(Arrays.asList("nn", "nnp")));
        put("vhn", new HashSet<>(Arrays.asList("vbd", "vbn")));
        put("pn", new HashSet<>(Arrays.asList("nn", "nnp", "nns", "rb")));
        put("csn", new HashSet<>(Arrays.asList("in", "nn", "nnp")));
    }};
    
    private Map<String, Integer> posFrequencies = new ConcurrentHashMap<>();
    private Map<String, Integer> frequencies = new ConcurrentHashMap<>();

    {
        if ("en".equals(Config.getProperty(Config.LANGUAGE))) {
            long start = System.currentTimeMillis();
            try (Scanner in = new Scanner(this.getClass().getClassLoader().getResourceAsStream("pos.ngram.txt"), "UTF-8")) {
                while (in.hasNext()) {
                    int freq = in.nextInt();
                    String w1 = in.next().toLowerCase();
                    String w2 = in.next().toLowerCase();
                    String p1 = in.next();
                    String p2 = in.next();
                    for (String p1m : POS_TAG_MAPPINGS.get(p1)) {
                        for (String p2m : POS_TAG_MAPPINGS.get(p2)) {
                            posFrequencies.put(p1m.toLowerCase() + "_" + w1 + " " + p2m.toLowerCase() + "_" + w2, freq);

                            String fkey = w1 + " " + w2;
                            frequencies.putIfAbsent(fkey, 0);
                            frequencies.put(fkey, frequencies.get(fkey) + freq);
                        }
                    }
                }
            }
            System.out.println("Finish loading english pos tagged ngram dictionary in " + (System.currentTimeMillis() - start) + " ms");
        }
    }

    EnPosNGramDictionary() {
    }

    @Override
    public int getFrequency(String word1, String pos1, String word2, String pos2) {
        String key = pos1.toLowerCase() + "_" + word1.toLowerCase()
                + " "
                + pos2.toLowerCase() + "_" + word2.toLowerCase();
        return posFrequencies.getOrDefault(key, 0);
    }

    @Override
    public int getFrequency(String word1, String word2) {
        String key = word1.toLowerCase()
                + " "
                + word2.toLowerCase();
        return frequencies.getOrDefault(key, 0);
    }
}
