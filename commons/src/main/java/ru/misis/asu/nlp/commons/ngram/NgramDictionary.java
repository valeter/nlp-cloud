package ru.misis.asu.nlp.commons.ngram;

/**
 * @author valter
 *         19.03.17
 */
public interface NgramDictionary {
    int getFrequency(String word1, String word2);

    enum Instance {
        RU(new RuNgramDictionary()),
        EN(new EnPosNGramDictionary());

        private NgramDictionary dictionary;

        Instance(NgramDictionary dictionary) {
            this.dictionary = dictionary;
        }

        public <T extends NgramDictionary> T get() {
            return (T)dictionary;
        }
    }
}
