package ru.misis.asu.nlp.commons.ngram;

import ru.misis.asu.nlp.commons.config.Config;

import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author valter
 *         19.03.17
 */
public class RuNgramDictionary implements NgramDictionary {
    private Map<String, Integer> frequencies = new ConcurrentHashMap<>();

    {
        if ("ru".equals(Config.getProperty(Config.LANGUAGE))) {
            long start = System.currentTimeMillis();
            try (Scanner in = new Scanner(RuNgramDictionary.class.getClassLoader().getResourceAsStream("bigrams.cyrB.lc"), "UTF-8")) {
                while (in.hasNext()) {
                    String w1 = in.next().toLowerCase();
                    String w2 = in.next().toLowerCase();
                    int freq = in.nextInt();
                    in.nextInt();
                    frequencies.putIfAbsent(w1 + " " + w2, freq);
                }
            }
            System.out.println("Finish loading russian ngram dictionary in " + (System.currentTimeMillis() - start) + " ms");
        }
    }

    RuNgramDictionary() {
    }

    @Override
    public int getFrequency(String word1, String word2) {
        String key = word1.toLowerCase()
                + " "
                + word2.toLowerCase();
        return frequencies.getOrDefault(key, 0);
    }
}
