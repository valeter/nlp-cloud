/* First created by JCasGen Sat Nov 25 11:26:01 MSK 2017 */
package ru.misis.asu.nlp.chunking.types;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;
import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Sat Nov 25 11:26:01 MSK 2017
 * XML source: /home/valter/projects/projects/nlp-cloud/chunking/target/jcasgen/typesystem.xml
 * @generated */
public class TextCorrected extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(TextCorrected.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected TextCorrected() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public TextCorrected(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public TextCorrected(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public TextCorrected(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: correction

  /** getter for correction - gets 
   * @generated
   * @return value of the feature 
   */
  public String getCorrection() {
    if (TextCorrected_Type.featOkTst && ((TextCorrected_Type)jcasType).casFeat_correction == null)
      jcasType.jcas.throwFeatMissing("correction", "ru.misis.asu.nlp.chunking.types.TextCorrected");
    return jcasType.ll_cas.ll_getStringValue(addr, ((TextCorrected_Type)jcasType).casFeatCode_correction);}
    
  /** setter for correction - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setCorrection(String v) {
    if (TextCorrected_Type.featOkTst && ((TextCorrected_Type)jcasType).casFeat_correction == null)
      jcasType.jcas.throwFeatMissing("correction", "ru.misis.asu.nlp.chunking.types.TextCorrected");
    jcasType.ll_cas.ll_setStringValue(addr, ((TextCorrected_Type)jcasType).casFeatCode_correction, v);}    
  }

    