
/* First created by JCasGen Fri Jul 15 09:23:17 MSK 2016 */
package ru.misis.asu.nlp.chunking.types;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Fri Jul 15 09:23:17 MSK 2016
 * @generated */
public class ChunkCorrectedEn_Type extends ChunkCorrected_Type {
  /** @generated 
   * @return the generator for this type
   */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (ChunkCorrectedEn_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = ChunkCorrectedEn_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new ChunkCorrectedEn(addr, ChunkCorrectedEn_Type.this);
  			   ChunkCorrectedEn_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new ChunkCorrectedEn(addr, ChunkCorrectedEn_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = ChunkCorrectedEn.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
 
  /** @generated */
  final Feature casFeat_mainWord;
  /** @generated */
  final int     casFeatCode_mainWord;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getMainWord(int addr) {
        if (featOkTst && casFeat_mainWord == null)
      jcas.throwFeatMissing("mainWord", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    return ll_cas.ll_getRefValue(addr, casFeatCode_mainWord);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setMainWord(int addr, int v) {
        if (featOkTst && casFeat_mainWord == null)
      jcas.throwFeatMissing("mainWord", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    ll_cas.ll_setRefValue(addr, casFeatCode_mainWord, v);}
    
  
 
  /** @generated */
  final Feature casFeat_mainWordPersonalPronounCorrection;
  /** @generated */
  final int     casFeatCode_mainWordPersonalPronounCorrection;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getMainWordPersonalPronounCorrection(int addr) {
        if (featOkTst && casFeat_mainWordPersonalPronounCorrection == null)
      jcas.throwFeatMissing("mainWordPersonalPronounCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    return ll_cas.ll_getStringValue(addr, casFeatCode_mainWordPersonalPronounCorrection);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setMainWordPersonalPronounCorrection(int addr, String v) {
        if (featOkTst && casFeat_mainWordPersonalPronounCorrection == null)
      jcas.throwFeatMissing("mainWordPersonalPronounCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    ll_cas.ll_setStringValue(addr, casFeatCode_mainWordPersonalPronounCorrection, v);}
    
  
 
  /** @generated */
  final Feature casFeat_mainWordPossessivePronounCorrection;
  /** @generated */
  final int     casFeatCode_mainWordPossessivePronounCorrection;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getMainWordPossessivePronounCorrection(int addr) {
        if (featOkTst && casFeat_mainWordPossessivePronounCorrection == null)
      jcas.throwFeatMissing("mainWordPossessivePronounCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    return ll_cas.ll_getStringValue(addr, casFeatCode_mainWordPossessivePronounCorrection);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setMainWordPossessivePronounCorrection(int addr, String v) {
        if (featOkTst && casFeat_mainWordPossessivePronounCorrection == null)
      jcas.throwFeatMissing("mainWordPossessivePronounCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    ll_cas.ll_setStringValue(addr, casFeatCode_mainWordPossessivePronounCorrection, v);}
    
  
 
  /** @generated */
  final Feature casFeat_mainWordPossessiveEndingCorrection;
  /** @generated */
  final int     casFeatCode_mainWordPossessiveEndingCorrection;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getMainWordPossessiveEndingCorrection(int addr) {
        if (featOkTst && casFeat_mainWordPossessiveEndingCorrection == null)
      jcas.throwFeatMissing("mainWordPossessiveEndingCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    return ll_cas.ll_getStringValue(addr, casFeatCode_mainWordPossessiveEndingCorrection);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setMainWordPossessiveEndingCorrection(int addr, String v) {
        if (featOkTst && casFeat_mainWordPossessiveEndingCorrection == null)
      jcas.throwFeatMissing("mainWordPossessiveEndingCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    ll_cas.ll_setStringValue(addr, casFeatCode_mainWordPossessiveEndingCorrection, v);}
    
  
 
  /** @generated */
  final Feature casFeat_dependentWord;
  /** @generated */
  final int     casFeatCode_dependentWord;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getDependentWord(int addr) {
        if (featOkTst && casFeat_dependentWord == null)
      jcas.throwFeatMissing("dependentWord", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    return ll_cas.ll_getRefValue(addr, casFeatCode_dependentWord);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setDependentWord(int addr, int v) {
        if (featOkTst && casFeat_dependentWord == null)
      jcas.throwFeatMissing("dependentWord", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    ll_cas.ll_setRefValue(addr, casFeatCode_dependentWord, v);}
    
  
 
  /** @generated */
  final Feature casFeat_dependentWordPersonalPronounCorrection;
  /** @generated */
  final int     casFeatCode_dependentWordPersonalPronounCorrection;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getDependentWordPersonalPronounCorrection(int addr) {
        if (featOkTst && casFeat_dependentWordPersonalPronounCorrection == null)
      jcas.throwFeatMissing("dependentWordPersonalPronounCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    return ll_cas.ll_getStringValue(addr, casFeatCode_dependentWordPersonalPronounCorrection);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setDependentWordPersonalPronounCorrection(int addr, String v) {
        if (featOkTst && casFeat_dependentWordPersonalPronounCorrection == null)
      jcas.throwFeatMissing("dependentWordPersonalPronounCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    ll_cas.ll_setStringValue(addr, casFeatCode_dependentWordPersonalPronounCorrection, v);}
    
  
 
  /** @generated */
  final Feature casFeat_dependentWordPossessivePronounCorrection;
  /** @generated */
  final int     casFeatCode_dependentWordPossessivePronounCorrection;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getDependentWordPossessivePronounCorrection(int addr) {
        if (featOkTst && casFeat_dependentWordPossessivePronounCorrection == null)
      jcas.throwFeatMissing("dependentWordPossessivePronounCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    return ll_cas.ll_getStringValue(addr, casFeatCode_dependentWordPossessivePronounCorrection);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setDependentWordPossessivePronounCorrection(int addr, String v) {
        if (featOkTst && casFeat_dependentWordPossessivePronounCorrection == null)
      jcas.throwFeatMissing("dependentWordPossessivePronounCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    ll_cas.ll_setStringValue(addr, casFeatCode_dependentWordPossessivePronounCorrection, v);}
    
  
 
  /** @generated */
  final Feature casFeat_dependentWordPossessiveEndingCorrection;
  /** @generated */
  final int     casFeatCode_dependentWordPossessiveEndingCorrection;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getDependentWordPossessiveEndingCorrection(int addr) {
        if (featOkTst && casFeat_dependentWordPossessiveEndingCorrection == null)
      jcas.throwFeatMissing("dependentWordPossessiveEndingCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    return ll_cas.ll_getStringValue(addr, casFeatCode_dependentWordPossessiveEndingCorrection);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setDependentWordPossessiveEndingCorrection(int addr, String v) {
        if (featOkTst && casFeat_dependentWordPossessiveEndingCorrection == null)
      jcas.throwFeatMissing("dependentWordPossessiveEndingCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    ll_cas.ll_setStringValue(addr, casFeatCode_dependentWordPossessiveEndingCorrection, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	 * @generated
	 * @param jcas JCas
	 * @param casType Type 
	 */
  public ChunkCorrectedEn_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_mainWord = jcas.getRequiredFeatureDE(casType, "mainWord", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn", featOkTst);
    casFeatCode_mainWord  = (null == casFeat_mainWord) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_mainWord).getCode();

 
    casFeat_mainWordPersonalPronounCorrection = jcas.getRequiredFeatureDE(casType, "mainWordPersonalPronounCorrection", "uima.cas.String", featOkTst);
    casFeatCode_mainWordPersonalPronounCorrection  = (null == casFeat_mainWordPersonalPronounCorrection) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_mainWordPersonalPronounCorrection).getCode();

 
    casFeat_mainWordPossessivePronounCorrection = jcas.getRequiredFeatureDE(casType, "mainWordPossessivePronounCorrection", "uima.cas.String", featOkTst);
    casFeatCode_mainWordPossessivePronounCorrection  = (null == casFeat_mainWordPossessivePronounCorrection) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_mainWordPossessivePronounCorrection).getCode();

 
    casFeat_mainWordPossessiveEndingCorrection = jcas.getRequiredFeatureDE(casType, "mainWordPossessiveEndingCorrection", "uima.cas.String", featOkTst);
    casFeatCode_mainWordPossessiveEndingCorrection  = (null == casFeat_mainWordPossessiveEndingCorrection) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_mainWordPossessiveEndingCorrection).getCode();

 
    casFeat_dependentWord = jcas.getRequiredFeatureDE(casType, "dependentWord", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn", featOkTst);
    casFeatCode_dependentWord  = (null == casFeat_dependentWord) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_dependentWord).getCode();

 
    casFeat_dependentWordPersonalPronounCorrection = jcas.getRequiredFeatureDE(casType, "dependentWordPersonalPronounCorrection", "uima.cas.String", featOkTst);
    casFeatCode_dependentWordPersonalPronounCorrection  = (null == casFeat_dependentWordPersonalPronounCorrection) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_dependentWordPersonalPronounCorrection).getCode();

 
    casFeat_dependentWordPossessivePronounCorrection = jcas.getRequiredFeatureDE(casType, "dependentWordPossessivePronounCorrection", "uima.cas.String", featOkTst);
    casFeatCode_dependentWordPossessivePronounCorrection  = (null == casFeat_dependentWordPossessivePronounCorrection) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_dependentWordPossessivePronounCorrection).getCode();

 
    casFeat_dependentWordPossessiveEndingCorrection = jcas.getRequiredFeatureDE(casType, "dependentWordPossessiveEndingCorrection", "uima.cas.String", featOkTst);
    casFeatCode_dependentWordPossessiveEndingCorrection  = (null == casFeat_dependentWordPossessiveEndingCorrection) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_dependentWordPossessiveEndingCorrection).getCode();

  }
}



    