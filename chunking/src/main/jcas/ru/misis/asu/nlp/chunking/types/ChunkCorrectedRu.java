

/* First created by JCasGen Fri Jul 15 09:23:17 MSK 2016 */
package ru.misis.asu.nlp.chunking.types;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import ru.misis.asu.nlp.extended.types.ru.ExtendedWordRu;


/** 
 * Updated by JCasGen Fri Jul 15 09:23:17 MSK 2016
 * XML source: /home/valter/projects/nlp-cloud/chunking/src/main/resources/uima_xml/chunking-ts.uima_xml
 * @generated */
public class ChunkCorrectedRu extends ChunkCorrected {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(ChunkCorrectedRu.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected ChunkCorrectedRu() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public ChunkCorrectedRu(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public ChunkCorrectedRu(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public ChunkCorrectedRu(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: mainWord

  /** getter for mainWord - gets 
   * @generated
   * @return value of the feature 
   */
  public ExtendedWordRu getMainWord() {
    if (ChunkCorrectedRu_Type.featOkTst && ((ChunkCorrectedRu_Type)jcasType).casFeat_mainWord == null)
      jcasType.jcas.throwFeatMissing("mainWord", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedRu");
    return (ExtendedWordRu)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((ChunkCorrectedRu_Type)jcasType).casFeatCode_mainWord)));}
    
  /** setter for mainWord - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setMainWord(ExtendedWordRu v) {
    if (ChunkCorrectedRu_Type.featOkTst && ((ChunkCorrectedRu_Type)jcasType).casFeat_mainWord == null)
      jcasType.jcas.throwFeatMissing("mainWord", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedRu");
    jcasType.ll_cas.ll_setRefValue(addr, ((ChunkCorrectedRu_Type)jcasType).casFeatCode_mainWord, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: mainWordInclinationCorrection

  /** getter for mainWordInclinationCorrection - gets 
   * @generated
   * @return value of the feature 
   */
  public String getMainWordInclinationCorrection() {
    if (ChunkCorrectedRu_Type.featOkTst && ((ChunkCorrectedRu_Type)jcasType).casFeat_mainWordInclinationCorrection == null)
      jcasType.jcas.throwFeatMissing("mainWordInclinationCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedRu");
    return jcasType.ll_cas.ll_getStringValue(addr, ((ChunkCorrectedRu_Type)jcasType).casFeatCode_mainWordInclinationCorrection);}
    
  /** setter for mainWordInclinationCorrection - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setMainWordInclinationCorrection(String v) {
    if (ChunkCorrectedRu_Type.featOkTst && ((ChunkCorrectedRu_Type)jcasType).casFeat_mainWordInclinationCorrection == null)
      jcasType.jcas.throwFeatMissing("mainWordInclinationCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedRu");
    jcasType.ll_cas.ll_setStringValue(addr, ((ChunkCorrectedRu_Type)jcasType).casFeatCode_mainWordInclinationCorrection, v);}    
   
    
  //*--------------*
  //* Feature: mainWordPrepositionCorrection

  /** getter for mainWordPrepositionCorrection - gets 
   * @generated
   * @return value of the feature 
   */
  public String getMainWordPrepositionCorrection() {
    if (ChunkCorrectedRu_Type.featOkTst && ((ChunkCorrectedRu_Type)jcasType).casFeat_mainWordPrepositionCorrection == null)
      jcasType.jcas.throwFeatMissing("mainWordPrepositionCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedRu");
    return jcasType.ll_cas.ll_getStringValue(addr, ((ChunkCorrectedRu_Type)jcasType).casFeatCode_mainWordPrepositionCorrection);}
    
  /** setter for mainWordPrepositionCorrection - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setMainWordPrepositionCorrection(String v) {
    if (ChunkCorrectedRu_Type.featOkTst && ((ChunkCorrectedRu_Type)jcasType).casFeat_mainWordPrepositionCorrection == null)
      jcasType.jcas.throwFeatMissing("mainWordPrepositionCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedRu");
    jcasType.ll_cas.ll_setStringValue(addr, ((ChunkCorrectedRu_Type)jcasType).casFeatCode_mainWordPrepositionCorrection, v);}    
   
    
  //*--------------*
  //* Feature: dependentWord

  /** getter for dependentWord - gets 
   * @generated
   * @return value of the feature 
   */
  public ExtendedWordRu getDependentWord() {
    if (ChunkCorrectedRu_Type.featOkTst && ((ChunkCorrectedRu_Type)jcasType).casFeat_dependentWord == null)
      jcasType.jcas.throwFeatMissing("dependentWord", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedRu");
    return (ExtendedWordRu)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((ChunkCorrectedRu_Type)jcasType).casFeatCode_dependentWord)));}
    
  /** setter for dependentWord - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setDependentWord(ExtendedWordRu v) {
    if (ChunkCorrectedRu_Type.featOkTst && ((ChunkCorrectedRu_Type)jcasType).casFeat_dependentWord == null)
      jcasType.jcas.throwFeatMissing("dependentWord", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedRu");
    jcasType.ll_cas.ll_setRefValue(addr, ((ChunkCorrectedRu_Type)jcasType).casFeatCode_dependentWord, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: dependentWordInclinationCorrection

  /** getter for dependentWordInclinationCorrection - gets 
   * @generated
   * @return value of the feature 
   */
  public String getDependentWordInclinationCorrection() {
    if (ChunkCorrectedRu_Type.featOkTst && ((ChunkCorrectedRu_Type)jcasType).casFeat_dependentWordInclinationCorrection == null)
      jcasType.jcas.throwFeatMissing("dependentWordInclinationCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedRu");
    return jcasType.ll_cas.ll_getStringValue(addr, ((ChunkCorrectedRu_Type)jcasType).casFeatCode_dependentWordInclinationCorrection);}
    
  /** setter for dependentWordInclinationCorrection - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setDependentWordInclinationCorrection(String v) {
    if (ChunkCorrectedRu_Type.featOkTst && ((ChunkCorrectedRu_Type)jcasType).casFeat_dependentWordInclinationCorrection == null)
      jcasType.jcas.throwFeatMissing("dependentWordInclinationCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedRu");
    jcasType.ll_cas.ll_setStringValue(addr, ((ChunkCorrectedRu_Type)jcasType).casFeatCode_dependentWordInclinationCorrection, v);}    
   
    
  //*--------------*
  //* Feature: dependentWordPrepositionCorrection

  /** getter for dependentWordPrepositionCorrection - gets 
   * @generated
   * @return value of the feature 
   */
  public String getDependentWordPrepositionCorrection() {
    if (ChunkCorrectedRu_Type.featOkTst && ((ChunkCorrectedRu_Type)jcasType).casFeat_dependentWordPrepositionCorrection == null)
      jcasType.jcas.throwFeatMissing("dependentWordPrepositionCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedRu");
    return jcasType.ll_cas.ll_getStringValue(addr, ((ChunkCorrectedRu_Type)jcasType).casFeatCode_dependentWordPrepositionCorrection);}
    
  /** setter for dependentWordPrepositionCorrection - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setDependentWordPrepositionCorrection(String v) {
    if (ChunkCorrectedRu_Type.featOkTst && ((ChunkCorrectedRu_Type)jcasType).casFeat_dependentWordPrepositionCorrection == null)
      jcasType.jcas.throwFeatMissing("dependentWordPrepositionCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedRu");
    jcasType.ll_cas.ll_setStringValue(addr, ((ChunkCorrectedRu_Type)jcasType).casFeatCode_dependentWordPrepositionCorrection, v);}    
  }

    