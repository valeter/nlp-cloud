

/* First created by JCasGen Fri Jul 15 09:23:17 MSK 2016 */
package ru.misis.asu.nlp.chunking.types;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Fri Jul 15 09:23:17 MSK 2016
 * XML source: /home/valter/projects/nlp-cloud/chunking/src/main/resources/uima_xml/chunking-ts.uima_xml
 * @generated */
public class ChunkCorrectedTree extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(ChunkCorrectedTree.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected ChunkCorrectedTree() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public ChunkCorrectedTree(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public ChunkCorrectedTree(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public ChunkCorrectedTree(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: headChunk

  /** getter for headChunk - gets 
   * @generated
   * @return value of the feature 
   */
  public ChunkCorrected getHeadChunk() {
    if (ChunkCorrectedTree_Type.featOkTst && ((ChunkCorrectedTree_Type)jcasType).casFeat_headChunk == null)
      jcasType.jcas.throwFeatMissing("headChunk", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedTree");
    return (ChunkCorrected)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((ChunkCorrectedTree_Type)jcasType).casFeatCode_headChunk)));}
    
  /** setter for headChunk - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setHeadChunk(ChunkCorrected v) {
    if (ChunkCorrectedTree_Type.featOkTst && ((ChunkCorrectedTree_Type)jcasType).casFeat_headChunk == null)
      jcasType.jcas.throwFeatMissing("headChunk", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedTree");
    jcasType.ll_cas.ll_setRefValue(addr, ((ChunkCorrectedTree_Type)jcasType).casFeatCode_headChunk, jcasType.ll_cas.ll_getFSRef(v));}    
  }

    