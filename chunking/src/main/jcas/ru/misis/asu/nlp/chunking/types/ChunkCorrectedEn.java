

/* First created by JCasGen Fri Jul 15 09:23:17 MSK 2016 */
package ru.misis.asu.nlp.chunking.types;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import ru.misis.asu.nlp.extended.types.en.ExtendedWordEn;


/** 
 * Updated by JCasGen Fri Jul 15 09:23:17 MSK 2016
 * XML source: /home/valter/projects/nlp-cloud/chunking/src/main/resources/uima_xml/chunking-ts.uima_xml
 * @generated */
public class ChunkCorrectedEn extends ChunkCorrected {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(ChunkCorrectedEn.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected ChunkCorrectedEn() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public ChunkCorrectedEn(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public ChunkCorrectedEn(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public ChunkCorrectedEn(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: mainWord

  /** getter for mainWord - gets 
   * @generated
   * @return value of the feature 
   */
  public ExtendedWordEn getMainWord() {
    if (ChunkCorrectedEn_Type.featOkTst && ((ChunkCorrectedEn_Type)jcasType).casFeat_mainWord == null)
      jcasType.jcas.throwFeatMissing("mainWord", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    return (ExtendedWordEn)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((ChunkCorrectedEn_Type)jcasType).casFeatCode_mainWord)));}
    
  /** setter for mainWord - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setMainWord(ExtendedWordEn v) {
    if (ChunkCorrectedEn_Type.featOkTst && ((ChunkCorrectedEn_Type)jcasType).casFeat_mainWord == null)
      jcasType.jcas.throwFeatMissing("mainWord", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    jcasType.ll_cas.ll_setRefValue(addr, ((ChunkCorrectedEn_Type)jcasType).casFeatCode_mainWord, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: mainWordPersonalPronounCorrection

  /** getter for mainWordPersonalPronounCorrection - gets 
   * @generated
   * @return value of the feature 
   */
  public String getMainWordPersonalPronounCorrection() {
    if (ChunkCorrectedEn_Type.featOkTst && ((ChunkCorrectedEn_Type)jcasType).casFeat_mainWordPersonalPronounCorrection == null)
      jcasType.jcas.throwFeatMissing("mainWordPersonalPronounCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    return jcasType.ll_cas.ll_getStringValue(addr, ((ChunkCorrectedEn_Type)jcasType).casFeatCode_mainWordPersonalPronounCorrection);}
    
  /** setter for mainWordPersonalPronounCorrection - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setMainWordPersonalPronounCorrection(String v) {
    if (ChunkCorrectedEn_Type.featOkTst && ((ChunkCorrectedEn_Type)jcasType).casFeat_mainWordPersonalPronounCorrection == null)
      jcasType.jcas.throwFeatMissing("mainWordPersonalPronounCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    jcasType.ll_cas.ll_setStringValue(addr, ((ChunkCorrectedEn_Type)jcasType).casFeatCode_mainWordPersonalPronounCorrection, v);}    
   
    
  //*--------------*
  //* Feature: mainWordPossessivePronounCorrection

  /** getter for mainWordPossessivePronounCorrection - gets 
   * @generated
   * @return value of the feature 
   */
  public String getMainWordPossessivePronounCorrection() {
    if (ChunkCorrectedEn_Type.featOkTst && ((ChunkCorrectedEn_Type)jcasType).casFeat_mainWordPossessivePronounCorrection == null)
      jcasType.jcas.throwFeatMissing("mainWordPossessivePronounCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    return jcasType.ll_cas.ll_getStringValue(addr, ((ChunkCorrectedEn_Type)jcasType).casFeatCode_mainWordPossessivePronounCorrection);}
    
  /** setter for mainWordPossessivePronounCorrection - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setMainWordPossessivePronounCorrection(String v) {
    if (ChunkCorrectedEn_Type.featOkTst && ((ChunkCorrectedEn_Type)jcasType).casFeat_mainWordPossessivePronounCorrection == null)
      jcasType.jcas.throwFeatMissing("mainWordPossessivePronounCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    jcasType.ll_cas.ll_setStringValue(addr, ((ChunkCorrectedEn_Type)jcasType).casFeatCode_mainWordPossessivePronounCorrection, v);}    
   
    
  //*--------------*
  //* Feature: mainWordPossessiveEndingCorrection

  /** getter for mainWordPossessiveEndingCorrection - gets 
   * @generated
   * @return value of the feature 
   */
  public String getMainWordPossessiveEndingCorrection() {
    if (ChunkCorrectedEn_Type.featOkTst && ((ChunkCorrectedEn_Type)jcasType).casFeat_mainWordPossessiveEndingCorrection == null)
      jcasType.jcas.throwFeatMissing("mainWordPossessiveEndingCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    return jcasType.ll_cas.ll_getStringValue(addr, ((ChunkCorrectedEn_Type)jcasType).casFeatCode_mainWordPossessiveEndingCorrection);}
    
  /** setter for mainWordPossessiveEndingCorrection - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setMainWordPossessiveEndingCorrection(String v) {
    if (ChunkCorrectedEn_Type.featOkTst && ((ChunkCorrectedEn_Type)jcasType).casFeat_mainWordPossessiveEndingCorrection == null)
      jcasType.jcas.throwFeatMissing("mainWordPossessiveEndingCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    jcasType.ll_cas.ll_setStringValue(addr, ((ChunkCorrectedEn_Type)jcasType).casFeatCode_mainWordPossessiveEndingCorrection, v);}    
   
    
  //*--------------*
  //* Feature: dependentWord

  /** getter for dependentWord - gets 
   * @generated
   * @return value of the feature 
   */
  public ExtendedWordEn getDependentWord() {
    if (ChunkCorrectedEn_Type.featOkTst && ((ChunkCorrectedEn_Type)jcasType).casFeat_dependentWord == null)
      jcasType.jcas.throwFeatMissing("dependentWord", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    return (ExtendedWordEn)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((ChunkCorrectedEn_Type)jcasType).casFeatCode_dependentWord)));}
    
  /** setter for dependentWord - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setDependentWord(ExtendedWordEn v) {
    if (ChunkCorrectedEn_Type.featOkTst && ((ChunkCorrectedEn_Type)jcasType).casFeat_dependentWord == null)
      jcasType.jcas.throwFeatMissing("dependentWord", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    jcasType.ll_cas.ll_setRefValue(addr, ((ChunkCorrectedEn_Type)jcasType).casFeatCode_dependentWord, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: dependentWordPersonalPronounCorrection

  /** getter for dependentWordPersonalPronounCorrection - gets 
   * @generated
   * @return value of the feature 
   */
  public String getDependentWordPersonalPronounCorrection() {
    if (ChunkCorrectedEn_Type.featOkTst && ((ChunkCorrectedEn_Type)jcasType).casFeat_dependentWordPersonalPronounCorrection == null)
      jcasType.jcas.throwFeatMissing("dependentWordPersonalPronounCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    return jcasType.ll_cas.ll_getStringValue(addr, ((ChunkCorrectedEn_Type)jcasType).casFeatCode_dependentWordPersonalPronounCorrection);}
    
  /** setter for dependentWordPersonalPronounCorrection - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setDependentWordPersonalPronounCorrection(String v) {
    if (ChunkCorrectedEn_Type.featOkTst && ((ChunkCorrectedEn_Type)jcasType).casFeat_dependentWordPersonalPronounCorrection == null)
      jcasType.jcas.throwFeatMissing("dependentWordPersonalPronounCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    jcasType.ll_cas.ll_setStringValue(addr, ((ChunkCorrectedEn_Type)jcasType).casFeatCode_dependentWordPersonalPronounCorrection, v);}    
   
    
  //*--------------*
  //* Feature: dependentWordPossessivePronounCorrection

  /** getter for dependentWordPossessivePronounCorrection - gets 
   * @generated
   * @return value of the feature 
   */
  public String getDependentWordPossessivePronounCorrection() {
    if (ChunkCorrectedEn_Type.featOkTst && ((ChunkCorrectedEn_Type)jcasType).casFeat_dependentWordPossessivePronounCorrection == null)
      jcasType.jcas.throwFeatMissing("dependentWordPossessivePronounCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    return jcasType.ll_cas.ll_getStringValue(addr, ((ChunkCorrectedEn_Type)jcasType).casFeatCode_dependentWordPossessivePronounCorrection);}
    
  /** setter for dependentWordPossessivePronounCorrection - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setDependentWordPossessivePronounCorrection(String v) {
    if (ChunkCorrectedEn_Type.featOkTst && ((ChunkCorrectedEn_Type)jcasType).casFeat_dependentWordPossessivePronounCorrection == null)
      jcasType.jcas.throwFeatMissing("dependentWordPossessivePronounCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    jcasType.ll_cas.ll_setStringValue(addr, ((ChunkCorrectedEn_Type)jcasType).casFeatCode_dependentWordPossessivePronounCorrection, v);}    
   
    
  //*--------------*
  //* Feature: dependentWordPossessiveEndingCorrection

  /** getter for dependentWordPossessiveEndingCorrection - gets 
   * @generated
   * @return value of the feature 
   */
  public String getDependentWordPossessiveEndingCorrection() {
    if (ChunkCorrectedEn_Type.featOkTst && ((ChunkCorrectedEn_Type)jcasType).casFeat_dependentWordPossessiveEndingCorrection == null)
      jcasType.jcas.throwFeatMissing("dependentWordPossessiveEndingCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    return jcasType.ll_cas.ll_getStringValue(addr, ((ChunkCorrectedEn_Type)jcasType).casFeatCode_dependentWordPossessiveEndingCorrection);}
    
  /** setter for dependentWordPossessiveEndingCorrection - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setDependentWordPossessiveEndingCorrection(String v) {
    if (ChunkCorrectedEn_Type.featOkTst && ((ChunkCorrectedEn_Type)jcasType).casFeat_dependentWordPossessiveEndingCorrection == null)
      jcasType.jcas.throwFeatMissing("dependentWordPossessiveEndingCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn");
    jcasType.ll_cas.ll_setStringValue(addr, ((ChunkCorrectedEn_Type)jcasType).casFeatCode_dependentWordPossessiveEndingCorrection, v);}    
  }

    