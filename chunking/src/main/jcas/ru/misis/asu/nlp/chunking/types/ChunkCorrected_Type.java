
/* First created by JCasGen Fri Jul 15 09:23:17 MSK 2016 */
package ru.misis.asu.nlp.chunking.types;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Fri Jul 15 09:23:17 MSK 2016
 * @generated */
public class ChunkCorrected_Type extends Annotation_Type {
  /** @generated 
   * @return the generator for this type
   */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (ChunkCorrected_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = ChunkCorrected_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new ChunkCorrected(addr, ChunkCorrected_Type.this);
  			   ChunkCorrected_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new ChunkCorrected(addr, ChunkCorrected_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = ChunkCorrected.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("ru.misis.asu.nlp.chunking.types.ChunkCorrected");
 
  /** @generated */
  final Feature casFeat_mainWordCorrection;
  /** @generated */
  final int     casFeatCode_mainWordCorrection;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getMainWordCorrection(int addr) {
        if (featOkTst && casFeat_mainWordCorrection == null)
      jcas.throwFeatMissing("mainWordCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrected");
    return ll_cas.ll_getStringValue(addr, casFeatCode_mainWordCorrection);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setMainWordCorrection(int addr, String v) {
        if (featOkTst && casFeat_mainWordCorrection == null)
      jcas.throwFeatMissing("mainWordCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrected");
    ll_cas.ll_setStringValue(addr, casFeatCode_mainWordCorrection, v);}
    
  
 
  /** @generated */
  final Feature casFeat_mainWordNegativeCorrection;
  /** @generated */
  final int     casFeatCode_mainWordNegativeCorrection;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getMainWordNegativeCorrection(int addr) {
        if (featOkTst && casFeat_mainWordNegativeCorrection == null)
      jcas.throwFeatMissing("mainWordNegativeCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrected");
    return ll_cas.ll_getStringValue(addr, casFeatCode_mainWordNegativeCorrection);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setMainWordNegativeCorrection(int addr, String v) {
        if (featOkTst && casFeat_mainWordNegativeCorrection == null)
      jcas.throwFeatMissing("mainWordNegativeCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrected");
    ll_cas.ll_setStringValue(addr, casFeatCode_mainWordNegativeCorrection, v);}
    
  
 
  /** @generated */
  final Feature casFeat_dependentWordCorrection;
  /** @generated */
  final int     casFeatCode_dependentWordCorrection;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getDependentWordCorrection(int addr) {
        if (featOkTst && casFeat_dependentWordCorrection == null)
      jcas.throwFeatMissing("dependentWordCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrected");
    return ll_cas.ll_getStringValue(addr, casFeatCode_dependentWordCorrection);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setDependentWordCorrection(int addr, String v) {
        if (featOkTst && casFeat_dependentWordCorrection == null)
      jcas.throwFeatMissing("dependentWordCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrected");
    ll_cas.ll_setStringValue(addr, casFeatCode_dependentWordCorrection, v);}
    
  
 
  /** @generated */
  final Feature casFeat_dependentWordNegativeCorrection;
  /** @generated */
  final int     casFeatCode_dependentWordNegativeCorrection;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getDependentWordNegativeCorrection(int addr) {
        if (featOkTst && casFeat_dependentWordNegativeCorrection == null)
      jcas.throwFeatMissing("dependentWordNegativeCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrected");
    return ll_cas.ll_getStringValue(addr, casFeatCode_dependentWordNegativeCorrection);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setDependentWordNegativeCorrection(int addr, String v) {
        if (featOkTst && casFeat_dependentWordNegativeCorrection == null)
      jcas.throwFeatMissing("dependentWordNegativeCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrected");
    ll_cas.ll_setStringValue(addr, casFeatCode_dependentWordNegativeCorrection, v);}
    
  
 
  /** @generated */
  final Feature casFeat_connectedChunks;
  /** @generated */
  final int     casFeatCode_connectedChunks;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getConnectedChunks(int addr) {
        if (featOkTst && casFeat_connectedChunks == null)
      jcas.throwFeatMissing("connectedChunks", "ru.misis.asu.nlp.chunking.types.ChunkCorrected");
    return ll_cas.ll_getRefValue(addr, casFeatCode_connectedChunks);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setConnectedChunks(int addr, int v) {
        if (featOkTst && casFeat_connectedChunks == null)
      jcas.throwFeatMissing("connectedChunks", "ru.misis.asu.nlp.chunking.types.ChunkCorrected");
    ll_cas.ll_setRefValue(addr, casFeatCode_connectedChunks, v);}
    
   /** @generated
   * @param addr low level Feature Structure reference
   * @param i index of item in the array
   * @return value at index i in the array 
   */
  public int getConnectedChunks(int addr, int i) {
        if (featOkTst && casFeat_connectedChunks == null)
      jcas.throwFeatMissing("connectedChunks", "ru.misis.asu.nlp.chunking.types.ChunkCorrected");
    if (lowLevelTypeChecks)
      return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_connectedChunks), i, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_connectedChunks), i);
	return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_connectedChunks), i);
  }
   
  /** @generated
   * @param addr low level Feature Structure reference
   * @param i index of item in the array
   * @param v value to set
   */ 
  public void setConnectedChunks(int addr, int i, int v) {
        if (featOkTst && casFeat_connectedChunks == null)
      jcas.throwFeatMissing("connectedChunks", "ru.misis.asu.nlp.chunking.types.ChunkCorrected");
    if (lowLevelTypeChecks)
      ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_connectedChunks), i, v, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_connectedChunks), i);
    ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_connectedChunks), i, v);
  }
 



  /** initialize variables to correspond with Cas Type and Features
	 * @generated
	 * @param jcas JCas
	 * @param casType Type 
	 */
  public ChunkCorrected_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_mainWordCorrection = jcas.getRequiredFeatureDE(casType, "mainWordCorrection", "uima.cas.String", featOkTst);
    casFeatCode_mainWordCorrection  = (null == casFeat_mainWordCorrection) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_mainWordCorrection).getCode();

 
    casFeat_mainWordNegativeCorrection = jcas.getRequiredFeatureDE(casType, "mainWordNegativeCorrection", "uima.cas.String", featOkTst);
    casFeatCode_mainWordNegativeCorrection  = (null == casFeat_mainWordNegativeCorrection) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_mainWordNegativeCorrection).getCode();

 
    casFeat_dependentWordCorrection = jcas.getRequiredFeatureDE(casType, "dependentWordCorrection", "uima.cas.String", featOkTst);
    casFeatCode_dependentWordCorrection  = (null == casFeat_dependentWordCorrection) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_dependentWordCorrection).getCode();

 
    casFeat_dependentWordNegativeCorrection = jcas.getRequiredFeatureDE(casType, "dependentWordNegativeCorrection", "uima.cas.String", featOkTst);
    casFeatCode_dependentWordNegativeCorrection  = (null == casFeat_dependentWordNegativeCorrection) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_dependentWordNegativeCorrection).getCode();

 
    casFeat_connectedChunks = jcas.getRequiredFeatureDE(casType, "connectedChunks", "uima.cas.FSArray", featOkTst);
    casFeatCode_connectedChunks  = (null == casFeat_connectedChunks) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_connectedChunks).getCode();

  }
}



    