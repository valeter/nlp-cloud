
/* First created by JCasGen Fri Jul 15 09:23:17 MSK 2016 */
package ru.misis.asu.nlp.chunking.types;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Fri Jul 15 09:23:17 MSK 2016
 * @generated */
public class ChunkCorrectedTree_Type extends Annotation_Type {
  /** @generated 
   * @return the generator for this type
   */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (ChunkCorrectedTree_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = ChunkCorrectedTree_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new ChunkCorrectedTree(addr, ChunkCorrectedTree_Type.this);
  			   ChunkCorrectedTree_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new ChunkCorrectedTree(addr, ChunkCorrectedTree_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = ChunkCorrectedTree.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("ru.misis.asu.nlp.chunking.types.ChunkCorrectedTree");
 
  /** @generated */
  final Feature casFeat_headChunk;
  /** @generated */
  final int     casFeatCode_headChunk;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getHeadChunk(int addr) {
        if (featOkTst && casFeat_headChunk == null)
      jcas.throwFeatMissing("headChunk", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedTree");
    return ll_cas.ll_getRefValue(addr, casFeatCode_headChunk);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setHeadChunk(int addr, int v) {
        if (featOkTst && casFeat_headChunk == null)
      jcas.throwFeatMissing("headChunk", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedTree");
    ll_cas.ll_setRefValue(addr, casFeatCode_headChunk, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	 * @generated
	 * @param jcas JCas
	 * @param casType Type 
	 */
  public ChunkCorrectedTree_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_headChunk = jcas.getRequiredFeatureDE(casType, "headChunk", "ru.misis.asu.nlp.chunking.types.ChunkCorrected", featOkTst);
    casFeatCode_headChunk  = (null == casFeat_headChunk) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_headChunk).getCode();

  }
}



    