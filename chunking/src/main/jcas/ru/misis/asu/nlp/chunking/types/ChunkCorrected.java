

/* First created by JCasGen Fri Jul 15 09:23:17 MSK 2016 */
package ru.misis.asu.nlp.chunking.types;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Fri Jul 15 09:23:17 MSK 2016
 * XML source: /home/valter/projects/nlp-cloud/chunking/src/main/resources/uima_xml/chunking-ts.uima_xml
 * @generated */
public class ChunkCorrected extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(ChunkCorrected.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected ChunkCorrected() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public ChunkCorrected(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public ChunkCorrected(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public ChunkCorrected(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: mainWordCorrection

  /** getter for mainWordCorrection - gets 
   * @generated
   * @return value of the feature 
   */
  public String getMainWordCorrection() {
    if (ChunkCorrected_Type.featOkTst && ((ChunkCorrected_Type)jcasType).casFeat_mainWordCorrection == null)
      jcasType.jcas.throwFeatMissing("mainWordCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrected");
    return jcasType.ll_cas.ll_getStringValue(addr, ((ChunkCorrected_Type)jcasType).casFeatCode_mainWordCorrection);}
    
  /** setter for mainWordCorrection - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setMainWordCorrection(String v) {
    if (ChunkCorrected_Type.featOkTst && ((ChunkCorrected_Type)jcasType).casFeat_mainWordCorrection == null)
      jcasType.jcas.throwFeatMissing("mainWordCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrected");
    jcasType.ll_cas.ll_setStringValue(addr, ((ChunkCorrected_Type)jcasType).casFeatCode_mainWordCorrection, v);}    
   
    
  //*--------------*
  //* Feature: mainWordNegativeCorrection

  /** getter for mainWordNegativeCorrection - gets 
   * @generated
   * @return value of the feature 
   */
  public String getMainWordNegativeCorrection() {
    if (ChunkCorrected_Type.featOkTst && ((ChunkCorrected_Type)jcasType).casFeat_mainWordNegativeCorrection == null)
      jcasType.jcas.throwFeatMissing("mainWordNegativeCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrected");
    return jcasType.ll_cas.ll_getStringValue(addr, ((ChunkCorrected_Type)jcasType).casFeatCode_mainWordNegativeCorrection);}
    
  /** setter for mainWordNegativeCorrection - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setMainWordNegativeCorrection(String v) {
    if (ChunkCorrected_Type.featOkTst && ((ChunkCorrected_Type)jcasType).casFeat_mainWordNegativeCorrection == null)
      jcasType.jcas.throwFeatMissing("mainWordNegativeCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrected");
    jcasType.ll_cas.ll_setStringValue(addr, ((ChunkCorrected_Type)jcasType).casFeatCode_mainWordNegativeCorrection, v);}    
   
    
  //*--------------*
  //* Feature: dependentWordCorrection

  /** getter for dependentWordCorrection - gets 
   * @generated
   * @return value of the feature 
   */
  public String getDependentWordCorrection() {
    if (ChunkCorrected_Type.featOkTst && ((ChunkCorrected_Type)jcasType).casFeat_dependentWordCorrection == null)
      jcasType.jcas.throwFeatMissing("dependentWordCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrected");
    return jcasType.ll_cas.ll_getStringValue(addr, ((ChunkCorrected_Type)jcasType).casFeatCode_dependentWordCorrection);}
    
  /** setter for dependentWordCorrection - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setDependentWordCorrection(String v) {
    if (ChunkCorrected_Type.featOkTst && ((ChunkCorrected_Type)jcasType).casFeat_dependentWordCorrection == null)
      jcasType.jcas.throwFeatMissing("dependentWordCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrected");
    jcasType.ll_cas.ll_setStringValue(addr, ((ChunkCorrected_Type)jcasType).casFeatCode_dependentWordCorrection, v);}    
   
    
  //*--------------*
  //* Feature: dependentWordNegativeCorrection

  /** getter for dependentWordNegativeCorrection - gets 
   * @generated
   * @return value of the feature 
   */
  public String getDependentWordNegativeCorrection() {
    if (ChunkCorrected_Type.featOkTst && ((ChunkCorrected_Type)jcasType).casFeat_dependentWordNegativeCorrection == null)
      jcasType.jcas.throwFeatMissing("dependentWordNegativeCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrected");
    return jcasType.ll_cas.ll_getStringValue(addr, ((ChunkCorrected_Type)jcasType).casFeatCode_dependentWordNegativeCorrection);}
    
  /** setter for dependentWordNegativeCorrection - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setDependentWordNegativeCorrection(String v) {
    if (ChunkCorrected_Type.featOkTst && ((ChunkCorrected_Type)jcasType).casFeat_dependentWordNegativeCorrection == null)
      jcasType.jcas.throwFeatMissing("dependentWordNegativeCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrected");
    jcasType.ll_cas.ll_setStringValue(addr, ((ChunkCorrected_Type)jcasType).casFeatCode_dependentWordNegativeCorrection, v);}    
   
    
  //*--------------*
  //* Feature: connectedChunks

  /** getter for connectedChunks - gets 
   * @generated
   * @return value of the feature 
   */
  public FSArray getConnectedChunks() {
    if (ChunkCorrected_Type.featOkTst && ((ChunkCorrected_Type)jcasType).casFeat_connectedChunks == null)
      jcasType.jcas.throwFeatMissing("connectedChunks", "ru.misis.asu.nlp.chunking.types.ChunkCorrected");
    return (FSArray)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((ChunkCorrected_Type)jcasType).casFeatCode_connectedChunks)));}
    
  /** setter for connectedChunks - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setConnectedChunks(FSArray v) {
    if (ChunkCorrected_Type.featOkTst && ((ChunkCorrected_Type)jcasType).casFeat_connectedChunks == null)
      jcasType.jcas.throwFeatMissing("connectedChunks", "ru.misis.asu.nlp.chunking.types.ChunkCorrected");
    jcasType.ll_cas.ll_setRefValue(addr, ((ChunkCorrected_Type)jcasType).casFeatCode_connectedChunks, jcasType.ll_cas.ll_getFSRef(v));}    
    
  /** indexed getter for connectedChunks - gets an indexed value - 
   * @generated
   * @param i index in the array to get
   * @return value of the element at index i 
   */
  public ChunkCorrected getConnectedChunks(int i) {
    if (ChunkCorrected_Type.featOkTst && ((ChunkCorrected_Type)jcasType).casFeat_connectedChunks == null)
      jcasType.jcas.throwFeatMissing("connectedChunks", "ru.misis.asu.nlp.chunking.types.ChunkCorrected");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((ChunkCorrected_Type)jcasType).casFeatCode_connectedChunks), i);
    return (ChunkCorrected)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((ChunkCorrected_Type)jcasType).casFeatCode_connectedChunks), i)));}

  /** indexed setter for connectedChunks - sets an indexed value - 
   * @generated
   * @param i index in the array to set
   * @param v value to set into the array 
   */
  public void setConnectedChunks(int i, ChunkCorrected v) { 
    if (ChunkCorrected_Type.featOkTst && ((ChunkCorrected_Type)jcasType).casFeat_connectedChunks == null)
      jcasType.jcas.throwFeatMissing("connectedChunks", "ru.misis.asu.nlp.chunking.types.ChunkCorrected");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((ChunkCorrected_Type)jcasType).casFeatCode_connectedChunks), i);
    jcasType.ll_cas.ll_setRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((ChunkCorrected_Type)jcasType).casFeatCode_connectedChunks), i, jcasType.ll_cas.ll_getFSRef(v));}
  }

    