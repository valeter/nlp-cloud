
/* First created by JCasGen Fri Jul 15 09:23:17 MSK 2016 */
package ru.misis.asu.nlp.chunking.types;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Fri Jul 15 09:23:17 MSK 2016
 * @generated */
public class Chunk_Type extends Annotation_Type {
  /** @generated 
   * @return the generator for this type
   */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Chunk_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Chunk_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Chunk(addr, Chunk_Type.this);
  			   Chunk_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Chunk(addr, Chunk_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Chunk.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("ru.misis.asu.nlp.chunking.types.Chunk");
 
  /** @generated */
  final Feature casFeat_mainWord;
  /** @generated */
  final int     casFeatCode_mainWord;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getMainWord(int addr) {
        if (featOkTst && casFeat_mainWord == null)
      jcas.throwFeatMissing("mainWord", "ru.misis.asu.nlp.chunking.types.Chunk");
    return ll_cas.ll_getRefValue(addr, casFeatCode_mainWord);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setMainWord(int addr, int v) {
        if (featOkTst && casFeat_mainWord == null)
      jcas.throwFeatMissing("mainWord", "ru.misis.asu.nlp.chunking.types.Chunk");
    ll_cas.ll_setRefValue(addr, casFeatCode_mainWord, v);}
    
  
 
  /** @generated */
  final Feature casFeat_dependentWord;
  /** @generated */
  final int     casFeatCode_dependentWord;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getDependentWord(int addr) {
        if (featOkTst && casFeat_dependentWord == null)
      jcas.throwFeatMissing("dependentWord", "ru.misis.asu.nlp.chunking.types.Chunk");
    return ll_cas.ll_getRefValue(addr, casFeatCode_dependentWord);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setDependentWord(int addr, int v) {
        if (featOkTst && casFeat_dependentWord == null)
      jcas.throwFeatMissing("dependentWord", "ru.misis.asu.nlp.chunking.types.Chunk");
    ll_cas.ll_setRefValue(addr, casFeatCode_dependentWord, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	 * @generated
	 * @param jcas JCas
	 * @param casType Type 
	 */
  public Chunk_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_mainWord = jcas.getRequiredFeatureDE(casType, "mainWord", "ru.misis.asu.nlp.extended.types.ru.ExtendedWordRu", featOkTst);
    casFeatCode_mainWord  = (null == casFeat_mainWord) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_mainWord).getCode();

 
    casFeat_dependentWord = jcas.getRequiredFeatureDE(casType, "dependentWord", "ru.misis.asu.nlp.extended.types.ru.ExtendedWordRu", featOkTst);
    casFeatCode_dependentWord  = (null == casFeat_dependentWord) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_dependentWord).getCode();

  }
}



    