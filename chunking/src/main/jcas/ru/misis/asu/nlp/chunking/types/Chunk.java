

/* First created by JCasGen Fri Jul 15 09:23:17 MSK 2016 */
package ru.misis.asu.nlp.chunking.types;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import ru.misis.asu.nlp.extended.types.ru.ExtendedWordRu;
import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Fri Jul 15 09:23:17 MSK 2016
 * XML source: /home/valter/projects/nlp-cloud/chunking/src/main/resources/uima_xml/chunking-ts.uima_xml
 * @generated */
public class Chunk extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Chunk.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Chunk() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public Chunk(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public Chunk(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public Chunk(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: mainWord

  /** getter for mainWord - gets 
   * @generated
   * @return value of the feature 
   */
  public ExtendedWordRu getMainWord() {
    if (Chunk_Type.featOkTst && ((Chunk_Type)jcasType).casFeat_mainWord == null)
      jcasType.jcas.throwFeatMissing("mainWord", "ru.misis.asu.nlp.chunking.types.Chunk");
    return (ExtendedWordRu)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((Chunk_Type)jcasType).casFeatCode_mainWord)));}
    
  /** setter for mainWord - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setMainWord(ExtendedWordRu v) {
    if (Chunk_Type.featOkTst && ((Chunk_Type)jcasType).casFeat_mainWord == null)
      jcasType.jcas.throwFeatMissing("mainWord", "ru.misis.asu.nlp.chunking.types.Chunk");
    jcasType.ll_cas.ll_setRefValue(addr, ((Chunk_Type)jcasType).casFeatCode_mainWord, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: dependentWord

  /** getter for dependentWord - gets 
   * @generated
   * @return value of the feature 
   */
  public ExtendedWordRu getDependentWord() {
    if (Chunk_Type.featOkTst && ((Chunk_Type)jcasType).casFeat_dependentWord == null)
      jcasType.jcas.throwFeatMissing("dependentWord", "ru.misis.asu.nlp.chunking.types.Chunk");
    return (ExtendedWordRu)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((Chunk_Type)jcasType).casFeatCode_dependentWord)));}
    
  /** setter for dependentWord - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setDependentWord(ExtendedWordRu v) {
    if (Chunk_Type.featOkTst && ((Chunk_Type)jcasType).casFeat_dependentWord == null)
      jcasType.jcas.throwFeatMissing("dependentWord", "ru.misis.asu.nlp.chunking.types.Chunk");
    jcasType.ll_cas.ll_setRefValue(addr, ((Chunk_Type)jcasType).casFeatCode_dependentWord, jcasType.ll_cas.ll_getFSRef(v));}    
  }

    