
/* First created by JCasGen Fri Jul 15 09:23:17 MSK 2016 */
package ru.misis.asu.nlp.chunking.types;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Fri Jul 15 09:23:17 MSK 2016
 * @generated */
public class ChunkCorrectedRu_Type extends ChunkCorrected_Type {
  /** @generated 
   * @return the generator for this type
   */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (ChunkCorrectedRu_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = ChunkCorrectedRu_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new ChunkCorrectedRu(addr, ChunkCorrectedRu_Type.this);
  			   ChunkCorrectedRu_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new ChunkCorrectedRu(addr, ChunkCorrectedRu_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = ChunkCorrectedRu.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("ru.misis.asu.nlp.chunking.types.ChunkCorrectedRu");
 
  /** @generated */
  final Feature casFeat_mainWord;
  /** @generated */
  final int     casFeatCode_mainWord;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getMainWord(int addr) {
        if (featOkTst && casFeat_mainWord == null)
      jcas.throwFeatMissing("mainWord", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedRu");
    return ll_cas.ll_getRefValue(addr, casFeatCode_mainWord);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setMainWord(int addr, int v) {
        if (featOkTst && casFeat_mainWord == null)
      jcas.throwFeatMissing("mainWord", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedRu");
    ll_cas.ll_setRefValue(addr, casFeatCode_mainWord, v);}
    
  
 
  /** @generated */
  final Feature casFeat_mainWordInclinationCorrection;
  /** @generated */
  final int     casFeatCode_mainWordInclinationCorrection;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getMainWordInclinationCorrection(int addr) {
        if (featOkTst && casFeat_mainWordInclinationCorrection == null)
      jcas.throwFeatMissing("mainWordInclinationCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedRu");
    return ll_cas.ll_getStringValue(addr, casFeatCode_mainWordInclinationCorrection);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setMainWordInclinationCorrection(int addr, String v) {
        if (featOkTst && casFeat_mainWordInclinationCorrection == null)
      jcas.throwFeatMissing("mainWordInclinationCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedRu");
    ll_cas.ll_setStringValue(addr, casFeatCode_mainWordInclinationCorrection, v);}
    
  
 
  /** @generated */
  final Feature casFeat_mainWordPrepositionCorrection;
  /** @generated */
  final int     casFeatCode_mainWordPrepositionCorrection;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getMainWordPrepositionCorrection(int addr) {
        if (featOkTst && casFeat_mainWordPrepositionCorrection == null)
      jcas.throwFeatMissing("mainWordPrepositionCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedRu");
    return ll_cas.ll_getStringValue(addr, casFeatCode_mainWordPrepositionCorrection);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setMainWordPrepositionCorrection(int addr, String v) {
        if (featOkTst && casFeat_mainWordPrepositionCorrection == null)
      jcas.throwFeatMissing("mainWordPrepositionCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedRu");
    ll_cas.ll_setStringValue(addr, casFeatCode_mainWordPrepositionCorrection, v);}
    
  
 
  /** @generated */
  final Feature casFeat_dependentWord;
  /** @generated */
  final int     casFeatCode_dependentWord;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getDependentWord(int addr) {
        if (featOkTst && casFeat_dependentWord == null)
      jcas.throwFeatMissing("dependentWord", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedRu");
    return ll_cas.ll_getRefValue(addr, casFeatCode_dependentWord);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setDependentWord(int addr, int v) {
        if (featOkTst && casFeat_dependentWord == null)
      jcas.throwFeatMissing("dependentWord", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedRu");
    ll_cas.ll_setRefValue(addr, casFeatCode_dependentWord, v);}
    
  
 
  /** @generated */
  final Feature casFeat_dependentWordInclinationCorrection;
  /** @generated */
  final int     casFeatCode_dependentWordInclinationCorrection;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getDependentWordInclinationCorrection(int addr) {
        if (featOkTst && casFeat_dependentWordInclinationCorrection == null)
      jcas.throwFeatMissing("dependentWordInclinationCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedRu");
    return ll_cas.ll_getStringValue(addr, casFeatCode_dependentWordInclinationCorrection);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setDependentWordInclinationCorrection(int addr, String v) {
        if (featOkTst && casFeat_dependentWordInclinationCorrection == null)
      jcas.throwFeatMissing("dependentWordInclinationCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedRu");
    ll_cas.ll_setStringValue(addr, casFeatCode_dependentWordInclinationCorrection, v);}
    
  
 
  /** @generated */
  final Feature casFeat_dependentWordPrepositionCorrection;
  /** @generated */
  final int     casFeatCode_dependentWordPrepositionCorrection;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getDependentWordPrepositionCorrection(int addr) {
        if (featOkTst && casFeat_dependentWordPrepositionCorrection == null)
      jcas.throwFeatMissing("dependentWordPrepositionCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedRu");
    return ll_cas.ll_getStringValue(addr, casFeatCode_dependentWordPrepositionCorrection);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setDependentWordPrepositionCorrection(int addr, String v) {
        if (featOkTst && casFeat_dependentWordPrepositionCorrection == null)
      jcas.throwFeatMissing("dependentWordPrepositionCorrection", "ru.misis.asu.nlp.chunking.types.ChunkCorrectedRu");
    ll_cas.ll_setStringValue(addr, casFeatCode_dependentWordPrepositionCorrection, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	 * @generated
	 * @param jcas JCas
	 * @param casType Type 
	 */
  public ChunkCorrectedRu_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_mainWord = jcas.getRequiredFeatureDE(casType, "mainWord", "ru.misis.asu.nlp.extended.types.ru.ExtendedWordRu", featOkTst);
    casFeatCode_mainWord  = (null == casFeat_mainWord) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_mainWord).getCode();

 
    casFeat_mainWordInclinationCorrection = jcas.getRequiredFeatureDE(casType, "mainWordInclinationCorrection", "uima.cas.String", featOkTst);
    casFeatCode_mainWordInclinationCorrection  = (null == casFeat_mainWordInclinationCorrection) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_mainWordInclinationCorrection).getCode();

 
    casFeat_mainWordPrepositionCorrection = jcas.getRequiredFeatureDE(casType, "mainWordPrepositionCorrection", "uima.cas.String", featOkTst);
    casFeatCode_mainWordPrepositionCorrection  = (null == casFeat_mainWordPrepositionCorrection) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_mainWordPrepositionCorrection).getCode();

 
    casFeat_dependentWord = jcas.getRequiredFeatureDE(casType, "dependentWord", "ru.misis.asu.nlp.extended.types.ru.ExtendedWordRu", featOkTst);
    casFeatCode_dependentWord  = (null == casFeat_dependentWord) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_dependentWord).getCode();

 
    casFeat_dependentWordInclinationCorrection = jcas.getRequiredFeatureDE(casType, "dependentWordInclinationCorrection", "uima.cas.String", featOkTst);
    casFeatCode_dependentWordInclinationCorrection  = (null == casFeat_dependentWordInclinationCorrection) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_dependentWordInclinationCorrection).getCode();

 
    casFeat_dependentWordPrepositionCorrection = jcas.getRequiredFeatureDE(casType, "dependentWordPrepositionCorrection", "uima.cas.String", featOkTst);
    casFeatCode_dependentWordPrepositionCorrection  = (null == casFeat_dependentWordPrepositionCorrection) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_dependentWordPrepositionCorrection).getCode();

  }
}



    