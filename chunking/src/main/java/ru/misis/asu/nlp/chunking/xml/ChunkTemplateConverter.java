package ru.misis.asu.nlp.chunking.xml;

import org.apache.uima.jcas.JCas;
import ru.misis.asu.nlp.chunking.model.ChunkTemplate;
import ru.misis.asu.nlp.chunking.model.ChunkWordOrder;
import ru.misis.asu.nlp.chunking.xml.common.HasWordOrder;

import java.util.List;

/**
 * @author valter
 *         14.07.16
 */
public interface ChunkTemplateConverter<IN> {
    String MATCHES_MAIN_WORD = "_MATCHES_MAIN_WORD_";
    String UNKNOWN = "_UNKNOWN_";

    List<ChunkTemplate> convert(JCas cas, List<IN> typeOfChunks);

    static ChunkWordOrder getWordOrder(HasWordOrder wordOrderWrapper) {
        switch (wordOrderWrapper.getWordOrder()) {
            case 1:
            case 4:
                return ChunkWordOrder.MAIN_WORD_FIRST;
            case 2:
            case 5:
                return ChunkWordOrder.DEPENDENT_WORD_FIRST;
            default:
                return ChunkWordOrder.ANY;
        }
    }
}
