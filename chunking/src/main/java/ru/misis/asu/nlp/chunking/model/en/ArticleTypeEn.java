package ru.misis.asu.nlp.chunking.model.en;

/**
 * @author valter
 *         02.09.16
 */
public enum ArticleTypeEn {
    ZERO,               //		Нулевой (артикль отсутствует)
    INDEFINITE,         //		Неопределенный артикль «а»
    DEFINITE,           //		Определенный артикль «the»
    ANY_ARTICLE,        //		Любой артикль (a, the или нулевой)
    ZERO_OR_DEFINITE    //		Нулевой или определенный «the»
}
