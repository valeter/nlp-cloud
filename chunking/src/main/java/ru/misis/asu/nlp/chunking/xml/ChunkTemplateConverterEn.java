package ru.misis.asu.nlp.chunking.xml;

import org.apache.uima.jcas.JCas;
import ru.misis.asu.nlp.chunking.model.*;
import ru.misis.asu.nlp.chunking.model.en.*;
import ru.misis.asu.nlp.chunking.xml.en.TypeOfChunkEn;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author valter
 * @date 16.05.2016.
 */
public class ChunkTemplateConverterEn implements ChunkTemplateConverter<TypeOfChunkEn> {
    private ChunkBuilderEn chunkBuilder = new ChunkBuilderEn();

    @Override
    public List<ChunkTemplate> convert(JCas cas, List<TypeOfChunkEn> typeOfChunks) {
        return typeOfChunks.stream()
                .map(t -> {
                    ChunkTemplateWordEn mainWord = new ChunkTemplateWordEn(cas,
                            getPos(t.getMPoSId()), getWordPrep(t.getMPRPTypeId()), getWordPrep$(t.getMPRPX0024TypeId()), getWordArticle(t.getMTypeOfArticleId()),
                            !t.isMSubjectOfClause() ? null : true, !t.isMPredicateOfClause() ? null : true,
                            !t.isMNegationMarker() ? null : true, null);

                    ChunkTemplateWordEn dependentWord = new ChunkTemplateWordEn(cas,
                            getPos(t.getDPoSId()), getWordPrep(t.getDPRPTypeId()), getWordPrep$(t.getDPRPX0024TypeId()), getWordArticle(t.getDTypeOfArticleId()),
                            !t.isDSubjectOfClause() ? null : true, !t.isDPredicateOfClause() ? null : true,
                            !t.isDNegationMarker() ? null : true, !t.isDPossessiveEndingMarker() ? null : true);

                    return new ChunkTemplate<>(t.getId(), mainWord, dependentWord, chunkBuilder,
                        ChunkTemplateConverter.getWordOrder(t));
                }).collect(Collectors.toList());
    }

    private static ArticleTypeEn getWordArticle(Integer articleTypeId) {
        if (articleTypeId == null) {
            return null;
        }
        switch (articleTypeId) {
            case 1:
                return ArticleTypeEn.ZERO;
            case 2:
                return ArticleTypeEn.INDEFINITE;
            case 3:
                return ArticleTypeEn.DEFINITE;
            case 4:
                return ArticleTypeEn.ANY_ARTICLE;
            case 5:
                return ArticleTypeEn.ZERO_OR_DEFINITE;
            default:
                return null;
        }
    }

    private static PersonalPronounTypeEn getWordPrep(Integer pronounTypeId) {
        if (pronounTypeId == null) {
            return null;
        }
        switch (pronounTypeId) {
            case 1:
                return PersonalPronounTypeEn.NOMINATIVE;
            case 2:
                return PersonalPronounTypeEn.OBLIQUE;
            case 3:
                return PersonalPronounTypeEn.REFLEXIVE;
            default:
                return null;
        }
    }

    private static PossessivePronounTypeEn getWordPrep$(Integer pronounTypeId) {
        if (pronounTypeId == null) {
            return null;
        }
        switch (pronounTypeId) {
            case 1:
                return PossessivePronounTypeEn.POSSESSIVE_DETERMINER;
            case 2:
                return PossessivePronounTypeEn.SUBST_POSSESSIVE_PRON;
            default:
                return null;
        }
    }

    private static String getPos(Integer posId) {
        switch (posId) {
            case 1: // Noun singular
                return "NN";
            case 2: // Any noun
                return "NN|NNS|NNP|NNPS";
            case 3: // Any adjective
                return "JJ|JJR|JJS";
            case 4: // Modal verb
                return "MD";
            case 5: // Cardinal number
                return "CD";
            case 6: // Verb, base form
                return "VB";
            case 7: // Verb, finite form
                return "VBD|VBP|VBZ";
            case 8: // Past participle
                return "VBN";
            case 9: // Present prticiple, gerund
                return "VBG";
            case 10: // Personal pronoun
                return "PRP";
            case 11: // Possessive pronoun
                return "PRP$";
            case 12: // Possessive wh-pronoun
                return "WP$";
            case 13: // Wh-determiner
                return "WDT";
            case 14: // Wh-pronoun
                return "WP";
            case 15: // Determiner
                return "DT";
            case 16: // Preposition or coordinating conjunction
                return "IN";
            case 17: // To
                return "TO";
            default:
                return null;
        }
    }
}
