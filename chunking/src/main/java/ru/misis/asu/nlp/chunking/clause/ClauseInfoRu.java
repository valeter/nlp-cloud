package ru.misis.asu.nlp.chunking.clause;

import ru.misis.asu.nlp.morphoanalysis.types.Wordform;

/**
 * @author valter
 * @date 08.04.2016.
 */
public class ClauseInfoRu {
    public static boolean isNotSimpleHead(Wordform main, Wordform dependent, boolean mainFirst, String mainWithNeg) {
        boolean isNounMain = "NOUN".equals(main.getPos().toUpperCase());
        boolean isNominativeMain = false;
        for (String grammem : main.getGrammems().toArray()) {
            isNominativeMain = "nomn".equals(grammem.toLowerCase());
            if (isNominativeMain) {
                break;
            }
        }
        if (isNounMain && isNominativeMain) {
            return true;
        }

        if ("нет".equals(mainWithNeg.trim().toLowerCase())
                || "не было".equals(mainWithNeg.trim().toLowerCase())
                || "не будет".equals(mainWithNeg.trim().toLowerCase())) {
            return true;
        }

        boolean isVerb = "VERB".equals(main.getPos().toUpperCase());
        if (isVerb) {
            return true;
        }

        boolean isInfMain = "INFN".equals(main.getPos().toUpperCase());
        if (isInfMain) {
            boolean isInfDependent = "INFN".equals(dependent.getPos().toUpperCase());
            if (isInfDependent && mainFirst) {
                return true;
            }

            boolean isNounDependent = "NOUN".equals(dependent.getPos().toUpperCase());
            boolean isNominativeDependent = false;
            for (String grammem : dependent.getGrammems().toArray()) {
                isNominativeDependent = "nomn".equals(grammem.toLowerCase());
                if (isNominativeDependent) {
                    break;
                }
            }
            if (isNounDependent && isNominativeDependent && mainFirst) {
                return true;
            }
        }

        return false;
    }

    public static boolean canBeMainWord(Wordform wordform) {
        boolean isNoun = "NOUN".equals(wordform.getPos().toUpperCase())
                || "NPRO".equals(wordform.getPos().toUpperCase());
        boolean isNominative = false;
        for (String grammem : wordform.getGrammems().toArray()) {
            isNominative = "nomn".equals(grammem.toLowerCase());
            if (isNominative) {
                break;
            }
        }
        if (isNoun && isNominative) {
            return true;
        }

        isNoun = "ADJF".equals(wordform.getPos().toUpperCase())
                || "ADJS".equals(wordform.getPos().toUpperCase());
        isNominative = false;
        for (String grammem : wordform.getGrammems().toArray()) {
            isNominative = "nomn".equals(grammem.toLowerCase());
            if (isNominative) {
                break;
            }
        }
        if (isNoun && isNominative) {
            return true;
        }

        isNoun = "PRTF".equals(wordform.getPos().toUpperCase())
                || "PRTS".equals(wordform.getPos().toUpperCase());
        isNominative = false;
        for (String grammem : wordform.getGrammems().toArray()) {
            isNominative = "nomn".equals(grammem.toLowerCase());
            if (isNominative) {
                break;
            }
        }
        if (isNoun && isNominative) {
            return true;
        }

        isNoun = "NUMR".equals(wordform.getPos().toUpperCase());
        isNominative = false;
        for (String grammem : wordform.getGrammems().toArray()) {
            isNominative = "nomn".equals(grammem.toLowerCase());
            if (isNominative) {
                break;
            }
        }
        if (isNoun && isNominative) {
            return true;
        }

        return false;
    }

    public static boolean canBeDependentWord(Wordform wordform, boolean mainFirst) {
        boolean isVerb = "VERB".equals(wordform.getPos().toUpperCase());
        if (isVerb) {
            return true;
        }

        boolean isNoun = "NOUN".equals(wordform.getPos().toUpperCase());
        boolean isNominative = false;
        for (String grammem : wordform.getGrammems().toArray()) {
            isNominative = "nomn".equals(grammem.toLowerCase());
            if (isNominative) {
                break;
            }
        }
        if (isNoun && isNominative && mainFirst) {
            return true;
        }

        isNoun = "ADJF".equals(wordform.getPos().toUpperCase())
                || "ADJS".equals(wordform.getPos().toUpperCase());
        isNominative = false;
        for (String grammem : wordform.getGrammems().toArray()) {
            isNominative = "nomn".equals(grammem.toLowerCase());
            if (isNominative) {
                break;
            }
        }
        if (isNoun && isNominative && mainFirst) {
            return true;
        }

        isNoun = "PRTF".equals(wordform.getPos().toUpperCase())
                || "PRTS".equals(wordform.getPos().toUpperCase());
        isNominative = false;
        for (String grammem : wordform.getGrammems().toArray()) {
            isNominative = "nomn".equals(grammem.toLowerCase());
            if (isNominative) {
                break;
            }
        }
        if (isNoun && isNominative && mainFirst) {
            return true;
        }

        return false;
    }
}
