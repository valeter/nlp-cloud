package ru.misis.asu.nlp.chunking.model;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import ru.misis.asu.nlp.commons.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @author valter
 * @date 08.04.2016.
 */
public class ChunkTemplate<ExCorrWordT extends Annotation, ChunkTempWordT extends ChunkTemplateWord> {
    private ChunkTempWordT mainWordInfo;
    private ChunkTempWordT dependentWordInfo;

    private ChunkBuilder chunkBuilder;

    protected long id;

    protected ChunkWordOrder wordOrder;

    public ChunkTemplate(long id, ChunkTempWordT mainWordInfo, ChunkTempWordT dependentWordInfo,
                         ChunkBuilder chunkBuilder,
                         ChunkWordOrder wordOrder) {
        this.mainWordInfo = mainWordInfo;
        this.dependentWordInfo = dependentWordInfo;
        this.chunkBuilder = chunkBuilder;
        this.id = id;
        this.wordOrder = wordOrder;
    }

    public List<ChunkInfo> buildChunks(AtomicInteger startId, ExCorrWordT mainWord, int mainWordInd,
                                       ExCorrWordT dependentWord, int dependentWordInd,
                                       ChunkWordOrder order, JCas aJCas) {
        if (wordOrder != null && (wordOrder != order && wordOrder != ChunkWordOrder.ANY)) {
            return Collections.emptyList();
        }

        List<Pair<ChunkInfoWord, List<ChunkInfoWord>>> lst =
            mainWordInfo.buildChunkWords(mainWord, mainWordInd, null);
        List<ChunkInfoWord> mainChunkWords = lst.stream()
            .map(w -> w.first)
            .collect(Collectors.toList());

        List<Pair<ChunkInfoWord, List<ChunkInfoWord>>> dependentChunkWords =
            dependentWordInfo.buildChunkWords(dependentWord, dependentWordInd, mainChunkWords);

        List<ChunkInfo> result = new ArrayList<>();
        for (Pair<ChunkInfoWord, List<ChunkInfoWord>> dependentChunkWord : dependentChunkWords) {
            result.addAll(dependentChunkWord.second.stream()
                .map(mw -> chunkBuilder.buildChunk(startId.incrementAndGet(),
                    mw, dependentChunkWord.first, order, this, aJCas))
                .collect(Collectors.toList()));
        }
        return result;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ChunkWordOrder getWordOrder() {
        return wordOrder;
    }

    public void setWordOrder(ChunkWordOrder wordOrder) {
        this.wordOrder = wordOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChunkTemplate that = (ChunkTemplate) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                '}';
    }
}
