package ru.misis.asu.nlp.chunking.model.ru;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.StringArray;
import ru.misis.asu.nlp.chunking.model.ChunkInfoWord;
import ru.misis.asu.nlp.chunking.model.ChunkTemplateWord;
import ru.misis.asu.nlp.chunking.xml.ChunkTemplateConverter;
import ru.misis.asu.nlp.commons.Pair;
import ru.misis.asu.nlp.extended.types.ru.*;
import ru.misis.asu.nlp.morphoanalysis.types.CorrectedWord;
import ru.misis.asu.nlp.morphoanalysis.types.Word;
import ru.misis.asu.nlp.morphoanalysis.types.Wordform;

import java.util.*;
import java.util.function.Function;

/**
 * @author valter
 * @date 08.04.2016.
 */
public class ChunkTemplateWordRu implements ChunkTemplateWord<ExtendedCorrectedWordRu> {
    private static final Set<String> GENDER_SET = new HashSet<>(Arrays.asList("masc", "femn", "neut"));
    private static final Set<String> NUMBER_SET = new HashSet<>(Arrays.asList("sing", "plur"));
    private static final Set<String> CASE_SET = new HashSet<>(Arrays.asList("nomn", "gent", "datv", "accs", "ablt", "loct"));
    private static final Map<String, String> SPECIFIC_CASE_MAP = new HashMap<String, String>() {{
        put("voct", "nomn");
        put("gen1", "gent");
        put("gen2", "gent");
        put("acc2", "accs");
        put("loc1", "loct");
        put("loc1", "loct");
    }};

    private JCas cas;

    private String wordPos;
    private String wordGender;
    private String wordCase;
    private String wordNumber;

    private Boolean isSubject;
    private Boolean isPredicate;
    private Boolean isInfinitive;

    private Boolean hasNegation;
    private Boolean hasPreposition;

    public ChunkTemplateWordRu(JCas cas, String wordPos, String wordGender, String wordCase, String wordNumber, Boolean isSubject, Boolean isPredicate, Boolean isInfinitive, Boolean hasNegation, Boolean hasPreposition) {
        this.cas = cas;
        this.wordPos = wordPos;
        this.wordGender = wordGender;
        this.wordCase = wordCase;
        this.wordNumber = wordNumber;
        this.isSubject = isSubject;
        this.isPredicate = isPredicate;
        this.isInfinitive = isInfinitive;
        this.hasNegation = hasNegation;
        this.hasPreposition = hasPreposition;
    }

    @Override
    public List<Pair<ChunkInfoWord, List<ChunkInfoWord>>> buildChunkWords(ExtendedCorrectedWordRu extendedCorrectedWord, int ind, List<ChunkInfoWord> mainChunkWords) {
        List<Pair<ChunkInfoWord, List<ChunkInfoWord>>> result = new ArrayList<>();

        CorrectedWord correctedWord = extendedCorrectedWord.getWord();
        FSArray words = correctedWord.getWords();

        for (int i = 0; i < words.size(); i++) {
            Word word = (Word) words.get(i);

            FSArray wordforms = word.getWordforms();
            for (int j = 0; j < wordforms.size(); j++) {
                Wordform wordform = (Wordform) wordforms.get(j);

                List<ChunkInfoWord> mainChunkWordsCopy = new ArrayList<>();
                if (mainChunkWords != null) {
                    mainChunkWordsCopy.addAll(mainChunkWords);
                }

                String wfPos = getPos(wordform);
                String wfCase = getCase(wordform);
                String wfGender = getGender(wordform);
                String wfNumber = getNumber(wordform);

                boolean posMatches = wordPos == null
                    || (mainChunkWords != null
                            && ChunkTemplateConverter.MATCHES_MAIN_WORD.equals(wordPos)
                            && posMatchesMain(wordform, mainChunkWordsCopy))
                    || wordPos.contains(wfPos);
                boolean genderMatches = wordGender == null
                    || (mainChunkWords != null
                            && ChunkTemplateConverter.MATCHES_MAIN_WORD.equals(wordGender)
                            && genderMatchesMain(wordform, mainChunkWordsCopy))
                    || (wfGender != null && wordGender.contains(wfGender));
                boolean caseMatches = wordCase == null
                    || (mainChunkWords != null
                            && ChunkTemplateConverter.MATCHES_MAIN_WORD.equals(wordCase)
                            && caseMatchesMain(wordform, mainChunkWordsCopy))
                    || (wfCase != null && wordCase.contains(wfCase));
                boolean numberMatches = wordNumber == null
                    || (mainChunkWords != null
                            && ChunkTemplateConverter.MATCHES_MAIN_WORD.equals(wordNumber)
                            && numberMatchesMain(wordform, mainChunkWordsCopy))
                    || (wfNumber != null && wordNumber.contains(wfNumber));

                // no subject and predicate matching
                if (posMatches && genderMatches && caseMatches && numberMatches
                        && (isInfinitive == null || isInfinitive == isInf(wordform))
                        && (hasNegation == null || extendedCorrectedWord.getNegativeParticle() != null == hasNegation)
                        && (hasPreposition == null || extendedCorrectedWord.getPreposition() != null == hasPreposition)) {

                    result.add(new Pair<>(
                        new ChunkInfoWord<>(ind,
                            correctedWord.getCorrections(i),
                            extendedCorrectedWord,
                            buildExtendedWord(extendedCorrectedWord, i),
                            wordform,
                            (o1, o2) -> o1 == o2 ? 0 : -1),
                        mainChunkWordsCopy)
                    );
                }
            }
        }

        return result;
    }

    private boolean posMatchesMain(Wordform wordform, List<ChunkInfoWord> mainChunkWords) {
        return stringMatchesMain(wordform, mainChunkWords, this::getPos, true);
    }

    private boolean caseMatchesMain(Wordform wordform, List<ChunkInfoWord> mainChunkWords) {
        return stringMatchesMain(wordform, mainChunkWords, this::getCase, false);
    }

    private boolean genderMatchesMain(Wordform wordform, List<ChunkInfoWord> mainChunkWords) {
        return stringMatchesMain(wordform, mainChunkWords, this::getGender, false);
    }

    private boolean numberMatchesMain(Wordform wordform, List<ChunkInfoWord> mainChunkWords) {
        return stringMatchesMain(wordform, mainChunkWords, this::getNumber, false);
    }

    private boolean stringMatchesMain(Wordform wordform, List<ChunkInfoWord> mainChunkWords, Function<Wordform, String> function, boolean strict) {
        String pos = function.apply(wordform);
        if (!strict && pos == null) {
            return true;
        }
        for (Iterator<ChunkInfoWord> iterator = mainChunkWords.iterator(); iterator.hasNext(); ) {
            ChunkInfoWord next = iterator.next();
            String val = function.apply((Wordform)next.getWordform());
            if (val == null && !strict) {
                continue;
            }
            if (!Objects.equals(pos, val)) {
                iterator.remove();
            }
        }

        return mainChunkWords.size() > 0;
    }

    private ExtendedWordRu buildExtendedWord(ExtendedCorrectedWordRu extendedCorrectedWord, int i) {
        ExtendedWordRu result = new ExtendedWordRu(cas);
        result.setBegin(extendedCorrectedWord.getBegin());
        result.setEnd(extendedCorrectedWord.getEnd());

        result.setWord(extendedCorrectedWord.getWord().getWords(i));

        if (extendedCorrectedWord.getInclinationParticle() != null) {
            InclinationParticleRu inclinationParticle = new InclinationParticleRu(cas);
            inclinationParticle.setBegin(extendedCorrectedWord.getInclinationParticle().getBegin());
            inclinationParticle.setEnd(extendedCorrectedWord.getInclinationParticle().getEnd());
            inclinationParticle.setGrammemes(extendedCorrectedWord.getInclinationParticle().getGrammemes());
            inclinationParticle.setWord(extendedCorrectedWord.getInclinationParticle().getWord());
            result.setInclinationParticle(inclinationParticle);
        }

        if (extendedCorrectedWord.getNegativeParticle() != null) {
            NegativeParticleRu negativeParticle = new NegativeParticleRu(cas);
            negativeParticle.setBegin(extendedCorrectedWord.getNegativeParticle().getBegin());
            negativeParticle.setEnd(extendedCorrectedWord.getNegativeParticle().getEnd());
            negativeParticle.setGrammemes(extendedCorrectedWord.getNegativeParticle().getGrammemes());
            negativeParticle.setWord(extendedCorrectedWord.getNegativeParticle().getWord());
            result.setNegativeParticle(negativeParticle);
        }

        if (extendedCorrectedWord.getPreposition() != null) {
            PrepositionRu preposition = new PrepositionRu(cas);
            preposition.setBegin(extendedCorrectedWord.getPreposition().getBegin());
            preposition.setEnd(extendedCorrectedWord.getPreposition().getEnd());
            preposition.setGrammemes(extendedCorrectedWord.getPreposition().getGrammemes());
            preposition.setWord(extendedCorrectedWord.getPreposition().getWord());
            result.setPreposition(preposition);
        }

        return result;
    }

    private boolean isInf(Wordform wordform) {
        return "infn".equals(wordform.getPos().trim().toLowerCase());
    }

    private String getPos(Wordform wordform) {
        return wordform.getPos().trim();
    }

    private String getGender(Wordform wordform) {
        return getGrammemFromSet(wordform, GENDER_SET);
    }

    private String getCase(Wordform wordform) {
        String result = getGrammemFromSet(wordform, CASE_SET);
        if (result != null) {
            return result;
        }
        result = getGrammemFromSet(wordform, SPECIFIC_CASE_MAP.keySet());
        return result == null ? null : SPECIFIC_CASE_MAP.get(result);
    }

    private String getNumber(Wordform wordform) {
        return getGrammemFromSet(wordform, NUMBER_SET);
    }

    private String getGrammemFromSet(Wordform wordform, Set<String> grammemSet) {
        StringArray grammems = wordform.getGrammems();
        for (int i = 0; i < grammems.size(); i++) {
            String grammem = grammems.get(i).trim();
            if (grammemSet.contains(grammem)) {
                return grammem;
            }
        }
        return null;
    }
}
