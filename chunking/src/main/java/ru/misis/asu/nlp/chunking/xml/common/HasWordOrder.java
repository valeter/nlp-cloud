package ru.misis.asu.nlp.chunking.xml.common;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         02.04.17
 */
public interface HasWordOrder {
    Integer getWordOrder();
}
