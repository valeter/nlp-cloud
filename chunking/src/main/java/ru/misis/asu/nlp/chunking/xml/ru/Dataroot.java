
package ru.misis.asu.nlp.chunking.xml.ru;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}Type_of_chunk" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="generated" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "typeOfChunk"
})
@XmlRootElement(name = "dataroot")
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
public class Dataroot {

    @XmlElement(name = "Type_of_chunk")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected List<TypeOfChunkRu> typeOfChunk;
    @XmlAttribute(name = "generated")
    @XmlSchemaType(name = "dateTime")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected XMLGregorianCalendar generated;

    /**
     * Gets the value of the typeOfChunk property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the typeOfChunk property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTypeOfChunk().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TypeOfChunkRu }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public List<TypeOfChunkRu> getTypeOfChunk() {
        if (typeOfChunk == null) {
            typeOfChunk = new ArrayList<TypeOfChunkRu>();
        }
        return this.typeOfChunk;
    }

    /**
     * Gets the value of the generated property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public XMLGregorianCalendar getGenerated() {
        return generated;
    }

    /**
     * Sets the value of the generated property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setGenerated(XMLGregorianCalendar value) {
        this.generated = value;
    }

}
