package ru.misis.asu.nlp.chunking.xml;

import org.apache.uima.jcas.JCas;
import ru.misis.asu.nlp.chunking.model.*;
import ru.misis.asu.nlp.chunking.model.ru.ChunkBuilderRu;
import ru.misis.asu.nlp.chunking.model.ru.ChunkTemplateWordRu;
import ru.misis.asu.nlp.chunking.xml.ru.TypeOfChunkRu;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author valter
 * @date 16.05.2016.
 */
public class ChunkTemplateConverterRu implements ChunkTemplateConverter<TypeOfChunkRu> {
    private ChunkBuilderRu chunkBuilder = new ChunkBuilderRu();

    @Override
    public List<ChunkTemplate> convert(JCas cas, List<TypeOfChunkRu> typeOfChunks) {
        return typeOfChunks.stream()
                .map(t -> {
                    ChunkTemplateWordRu mainWord = new ChunkTemplateWordRu(cas,
                            getPos(t.getMPoSId()), getGender(t.getMGenderId()),
                            getCase(t.getMCaseId()), getNumber(t.getMNumberId()),
                            getSubjectOfClause(t.getMSubjectOfClauseMarker()), getPredicateOfClause(t.getMPredicateOfClauseMarker()),
                            !t.isMInfinitiveMarker() ? null : true, !t.isMNegationNeMarker() ? null : true, !t.isMPrepositionMarker() ? null : true);

                    ChunkTemplateWordRu dependentWord = new ChunkTemplateWordRu(cas,
                            getPos(t.getDPoSId()), getGender(t.getDGenderId()),
                            getCase(t.getDCaseId()), getNumber(t.getDNumberId()),
                            !t.isDSubjectOfClauseMarker() ? null : true, !t.isDPredicateOfClauseMarker() ? null : true,
                            !t.isDInfinitiveMarker() ? null : true, !t.isDNegationNeMarker() ? null : true, !t.isDPrepositionMarker() ? null : true);

                    return new ChunkTemplate<>(t.getId(), mainWord, dependentWord, chunkBuilder,
                        ChunkTemplateConverter.getWordOrder(t));
                }).collect(Collectors.toList());
    }

    private static Boolean getPredicateOfClause(Integer predicateOfClauseId) {
        return getHasRoleInClause(predicateOfClauseId);
    }

    private static Boolean getSubjectOfClause(Integer subjectOfClauseId) {
        return getHasRoleInClause(subjectOfClauseId);
    }

    private static Boolean getHasRoleInClause(Integer id) {
        switch (id) {
            case 1:
                return true;
            case 2:
                return false;
            default:
                return null;
        }
    }

    private static String getNumber(Integer numberId) {
        switch (numberId) {
            case 1: // 1	singular
                return "sing";
            case 2: // 2	plural
                return "plur";
            case 4: // 4	matches the main word in chunk
                return MATCHES_MAIN_WORD;
            case 3: // 3	any
            default:
                return null;
        }
    }

    private static String getCase(Integer caseId) {
        switch (caseId) {
            case 1: // 1	nominative
                return "nomn";
            case 2: // 2	genitive
                return "gent";
            case 3: // 3	dative
                return "datv";
            case 4: // 4	accusative
                return "accs";
            case 5: // 5	instrumental
                return "ablt";
            case 6: // 6	prepositional
                return "loct";
            case 7: // 7	any oblique
                return "gent|datv|accs|ablt|loct";
            case 8: // 8	matches the main word in chunk
                return MATCHES_MAIN_WORD;
            case 9: // 9	defined by the preposition
            default:
                return null;
        }
    }

    private static String getGender(Integer genderId) {
        switch (genderId) {
            case 1: // 1	feminine
                return "femn";
            case 2: // 2	masculine
                return "masc";
            case 3: // 3	neuter
                return "neut";
            case 5: // 5	matches the main word in chunk
                return MATCHES_MAIN_WORD;
            case 6: // 6	common for plural
            case 4: // 4	any
            default:
                return null;
        }
    }

    private static String getPos(Integer posId) {
        switch (posId) {
            case 1: // 1	noun
                return "NOUN";
            case 2: // 2	verb
                return "VERB|INFN";
            case 4: // 4	adjective
                return "ADJF|ADJS";
            case 5: // 5	participle
            case 6: // 6	adverbial participle
                return "PRTF|PRTS";
            case 7: // 7	pronoun
                return "NPRO";
            case 8: // 8	numeral
                return "NUMR";
            case 9: // 9	adverb
                return "ADVB";
            case 10: // 10	preposition
                return "PREP";
            case 11: // 11	conjunction
                return "CONJ";
            case 3: // 3	article
                return "PRCL";
            case 12: // 12	alphanumeric identifier
                return UNKNOWN;
            default:
                return null;
        }
    }
}
