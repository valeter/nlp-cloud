package ru.misis.asu.nlp.chunking.model.en;

/**
 * @author valter
 *         14.07.16
 */
public enum PossessivePronounTypeEn {
    POSSESSIVE_DETERMINER,
    SUBST_POSSESSIVE_PRON
}
