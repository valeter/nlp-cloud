package ru.misis.asu.nlp.chunking.model;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import ru.misis.asu.nlp.chunking.ChunkUtil;
import ru.misis.asu.nlp.chunking.types.ChunkCorrected;
import ru.misis.asu.nlp.chunking.types.ChunkCorrectedTree;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author valter
 * @date 08.04.2016.
 */
public class ChunkInfoTree implements Cloneable {
    private JCas jCas;
    private ChunkCorrectedTree chunkCorrectedTree;

    private ChunkInfo head;

    private Set<Integer> wordIds = new TreeSet<>();

    private List<ChunkInfo> infos;

    private Map<Integer, List<Integer>> children;

    public ChunkInfoTree(JCas jCas) {
        this.infos = new ArrayList<>();
        this.children = new HashMap<>();
        this.chunkCorrectedTree = new ChunkCorrectedTree(jCas);
        this.jCas = jCas;
    }

    @Override
    public ChunkInfoTree clone() {
        try {
            super.clone();
        } catch (CloneNotSupportedException ignored) {
        }

        ChunkInfoTree result = new ChunkInfoTree(jCas);

        ChunkCorrected chunkCorrected = ChunkUtil.copyChunk(jCas, chunkCorrectedTree.getHeadChunk());
        result.chunkCorrectedTree.setBegin(chunkCorrected.getBegin());
        result.chunkCorrectedTree.setEnd(chunkCorrected.getEnd());
        result.chunkCorrectedTree.setHeadChunk(chunkCorrected);

        result.infos = new ArrayList<>(this.infos);
        result.children = new HashMap<>(this.children);
        result.wordIds.addAll(this.wordIds);
        result.head = this.head;

        return result;
    }

    public boolean add(ChunkInfo parent, ChunkInfo chunkInfo) {
        if (infos.contains(chunkInfo) || (parent != null && (!infos.contains(parent)/* || chunkInfo.isHeadChunk()*/))
                || wordIds.contains(chunkInfo.getDependentWordInfo().getWordInd())) {
            return false;
        }

        if (parent == null) {
            if (head == null) {
                head = chunkInfo;
                chunkCorrectedTree.setHeadChunk(head.getChunkCorrected());
                chunkCorrectedTree.setBegin(head.getChunkCorrected().getBegin());
                chunkCorrectedTree.setEnd(head.getChunkCorrected().getEnd());
            } else {
                throw new IllegalArgumentException("Tree is already have a head");
            }
        } else {
            int parentInd = infos.indexOf(parent);

            if (!children.containsKey(parentInd)) {
                children.put(parentInd, new ArrayList<>());
            }
            children.get(parentInd).add(infos.size());

            ChunkCorrected parentChunk = parent.getChunkCorrected();
            if (parentChunk.getConnectedChunks() == null) {
                parentChunk.setConnectedChunks(new FSArray(jCas, 1));
            } else {
                FSArray newConnections = new FSArray(jCas, parentChunk.getConnectedChunks().size() + 1);
                for (int i = 0; i < parentChunk.getConnectedChunks().size(); i++) {
                    newConnections.set(i, parentChunk.getConnectedChunks().get(i));
                }
                parentChunk.setConnectedChunks(newConnections);
            }
            parentChunk.setConnectedChunks(parentChunk.getConnectedChunks().size() - 1, chunkInfo.getChunkCorrected());
        }

        infos.add(chunkInfo);
        wordIds.add(chunkInfo.getMainWordInfo().getWordInd());
        wordIds.add(chunkInfo.getDependentWordInfo().getWordInd());
        return true;
    }

    public int size() {
        return wordIds.size();
    }

    public ChunkCorrectedTree getChunkCorrectedTree() {
        return chunkCorrectedTree;
    }

    public ChunkInfo getHead() {
        return head;
    }

    public List<ChunkInfo> getChildren(ChunkInfo chunkInfo) {
        List<ChunkInfo> result = new ArrayList<>();
        int ind = infos.indexOf(chunkInfo);
        if (!children.containsKey(ind)) {
            return Collections.emptyList();
        }
        result.addAll(children.get(ind).stream()
                .map(infos::get)
                .collect(Collectors.toList()));
        return result;
    }

    public List<ChunkInfo> getInfos() {
        return infos;
    }
}
