
package ru.misis.asu.nlp.chunking.xml.ru;

import ru.misis.asu.nlp.chunking.xml.common.HasWordOrder;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Name" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Example" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="M_PoS_Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="M_Gender_Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="M_Case_Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="M_Number_Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="M_Subject_of_clause_marker" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="M_Predicate_of_clause_marker" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="M_Infinitive_marker" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="M_Conjunctive_mood_marker" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="M_Negation_ne_marker" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="M_Preposition_marker" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="M_Preposition_value_" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="M_Homogeneous_group_marker" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="M_Homogeneous_group_Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="M_Compound_predicate_group_marker" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="M_Compound_predicate_group_Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="M_Quality_intensifier_marker" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="M_Quality_intensifier_Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="M_Action_intensifier_marker" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="M_Action_intensifier_Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="D_PoS_Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="D_Gender_Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="D_Case_Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="D_Number_Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="D_Subject_of_clause_marker" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="D_Predicate_of_clause_marker" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="D_Infinitive_marker" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="D_Conjunctive_mood_marker" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="D_Negation_ne_marker" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="D_Preposition_marker" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="D_Preposition_value_" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="D_Homogeneous_group_marker" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="D_Homogeneous_group_Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="D_Compound_predicate_group_marker" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="D_Compound_predicate_group_Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="D_Quality_intensifier_marker" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="D_Quality_intensifier_Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="D_Action_intensifier_marker" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="D_Action_intensifier_Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Word_order" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Date_of_creation" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "name",
    "example",
    "mPoSId",
    "mGenderId",
    "mCaseId",
    "mNumberId",
    "mSubjectOfClauseMarker",
    "mPredicateOfClauseMarker",
    "mInfinitiveMarker",
    "mConjunctiveMoodMarker",
    "mNegationNeMarker",
    "mPrepositionMarker",
    "mPrepositionValue",
    "mHomogeneousGroupMarker",
    "mHomogeneousGroupId",
    "mCompoundPredicateGroupMarker",
    "mCompoundPredicateGroupId",
    "mQualityIntensifierMarker",
    "mQualityIntensifierId",
    "mActionIntensifierMarker",
    "mActionIntensifierId",
    "dPoSId",
    "dGenderId",
    "dCaseId",
    "dNumberId",
    "dSubjectOfClauseMarker",
    "dPredicateOfClauseMarker",
    "dInfinitiveMarker",
    "dConjunctiveMoodMarker",
    "dNegationNeMarker",
    "dPrepositionMarker",
    "dPrepositionValue",
    "dHomogeneousGroupMarker",
    "dHomogeneousGroupId",
    "dCompoundPredicateGroupMarker",
    "dCompoundPredicateGroupId",
    "dQualityIntensifierMarker",
    "dQualityIntensifierId",
    "dActionIntensifierMarker",
    "dActionIntensifierId",
    "wordOrder",
    "dateOfCreation"
})
@XmlRootElement(name = "Type_of_chunk")
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
public class TypeOfChunkRu implements HasWordOrder {

    @XmlElement(name = "Id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected int id;
    @XmlElement(name = "Name")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String name;
    @XmlElement(name = "Example")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String example;
    @XmlElement(name = "M_PoS_Id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer mPoSId;
    @XmlElement(name = "M_Gender_Id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer mGenderId;
    @XmlElement(name = "M_Case_Id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer mCaseId;
    @XmlElement(name = "M_Number_Id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer mNumberId;
    @XmlElement(name = "M_Subject_of_clause_marker")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer mSubjectOfClauseMarker;
    @XmlElement(name = "M_Predicate_of_clause_marker")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer mPredicateOfClauseMarker;
    @XmlElement(name = "M_Infinitive_marker")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected boolean mInfinitiveMarker;
    @XmlElement(name = "M_Conjunctive_mood_marker")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected boolean mConjunctiveMoodMarker;
    @XmlElement(name = "M_Negation_ne_marker")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected boolean mNegationNeMarker;
    @XmlElement(name = "M_Preposition_marker")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected boolean mPrepositionMarker;
    @XmlElement(name = "M_Preposition_value_")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String mPrepositionValue;
    @XmlElement(name = "M_Homogeneous_group_marker")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected boolean mHomogeneousGroupMarker;
    @XmlElement(name = "M_Homogeneous_group_Id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer mHomogeneousGroupId;
    @XmlElement(name = "M_Compound_predicate_group_marker")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected boolean mCompoundPredicateGroupMarker;
    @XmlElement(name = "M_Compound_predicate_group_Id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer mCompoundPredicateGroupId;
    @XmlElement(name = "M_Quality_intensifier_marker")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected boolean mQualityIntensifierMarker;
    @XmlElement(name = "M_Quality_intensifier_Id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer mQualityIntensifierId;
    @XmlElement(name = "M_Action_intensifier_marker")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected boolean mActionIntensifierMarker;
    @XmlElement(name = "M_Action_intensifier_Id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer mActionIntensifierId;
    @XmlElement(name = "D_PoS_Id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer dPoSId;
    @XmlElement(name = "D_Gender_Id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer dGenderId;
    @XmlElement(name = "D_Case_Id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer dCaseId;
    @XmlElement(name = "D_Number_Id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer dNumberId;
    @XmlElement(name = "D_Subject_of_clause_marker")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected boolean dSubjectOfClauseMarker;
    @XmlElement(name = "D_Predicate_of_clause_marker")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected boolean dPredicateOfClauseMarker;
    @XmlElement(name = "D_Infinitive_marker")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected boolean dInfinitiveMarker;
    @XmlElement(name = "D_Conjunctive_mood_marker")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected boolean dConjunctiveMoodMarker;
    @XmlElement(name = "D_Negation_ne_marker")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected boolean dNegationNeMarker;
    @XmlElement(name = "D_Preposition_marker")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected boolean dPrepositionMarker;
    @XmlElement(name = "D_Preposition_value_")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String dPrepositionValue;
    @XmlElement(name = "D_Homogeneous_group_marker")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected boolean dHomogeneousGroupMarker;
    @XmlElement(name = "D_Homogeneous_group_Id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer dHomogeneousGroupId;
    @XmlElement(name = "D_Compound_predicate_group_marker")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected boolean dCompoundPredicateGroupMarker;
    @XmlElement(name = "D_Compound_predicate_group_Id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer dCompoundPredicateGroupId;
    @XmlElement(name = "D_Quality_intensifier_marker")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected boolean dQualityIntensifierMarker;
    @XmlElement(name = "D_Quality_intensifier_Id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer dQualityIntensifierId;
    @XmlElement(name = "D_Action_intensifier_marker")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected boolean dActionIntensifierMarker;
    @XmlElement(name = "D_Action_intensifier_Id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer dActionIntensifierId;
    @XmlElement(name = "Word_order")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer wordOrder;
    @XmlElement(name = "Date_of_creation")
    @XmlSchemaType(name = "dateTime")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected XMLGregorianCalendar dateOfCreation;

    /**
     * Gets the value of the id property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public int getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the example property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getExample() {
        return example;
    }

    /**
     * Sets the value of the example property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setExample(String value) {
        this.example = value;
    }

    /**
     * Gets the value of the mPoSId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Integer getMPoSId() {
        return mPoSId;
    }

    /**
     * Sets the value of the mPoSId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMPoSId(Integer value) {
        this.mPoSId = value;
    }

    /**
     * Gets the value of the mGenderId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Integer getMGenderId() {
        return mGenderId;
    }

    /**
     * Sets the value of the mGenderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMGenderId(Integer value) {
        this.mGenderId = value;
    }

    /**
     * Gets the value of the mCaseId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Integer getMCaseId() {
        return mCaseId;
    }

    /**
     * Sets the value of the mCaseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMCaseId(Integer value) {
        this.mCaseId = value;
    }

    /**
     * Gets the value of the mNumberId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Integer getMNumberId() {
        return mNumberId;
    }

    /**
     * Sets the value of the mNumberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMNumberId(Integer value) {
        this.mNumberId = value;
    }

    /**
     * Gets the value of the mSubjectOfClauseMarker property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Integer getMSubjectOfClauseMarker() {
        return mSubjectOfClauseMarker;
    }

    /**
     * Sets the value of the mSubjectOfClauseMarker property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMSubjectOfClauseMarker(Integer value) {
        this.mSubjectOfClauseMarker = value;
    }

    /**
     * Gets the value of the mPredicateOfClauseMarker property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Integer getMPredicateOfClauseMarker() {
        return mPredicateOfClauseMarker;
    }

    /**
     * Sets the value of the mPredicateOfClauseMarker property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMPredicateOfClauseMarker(Integer value) {
        this.mPredicateOfClauseMarker = value;
    }

    /**
     * Gets the value of the mInfinitiveMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public boolean isMInfinitiveMarker() {
        return mInfinitiveMarker;
    }

    /**
     * Sets the value of the mInfinitiveMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMInfinitiveMarker(boolean value) {
        this.mInfinitiveMarker = value;
    }

    /**
     * Gets the value of the mConjunctiveMoodMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public boolean isMConjunctiveMoodMarker() {
        return mConjunctiveMoodMarker;
    }

    /**
     * Sets the value of the mConjunctiveMoodMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMConjunctiveMoodMarker(boolean value) {
        this.mConjunctiveMoodMarker = value;
    }

    /**
     * Gets the value of the mNegationNeMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public boolean isMNegationNeMarker() {
        return mNegationNeMarker;
    }

    /**
     * Sets the value of the mNegationNeMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMNegationNeMarker(boolean value) {
        this.mNegationNeMarker = value;
    }

    /**
     * Gets the value of the mPrepositionMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public boolean isMPrepositionMarker() {
        return mPrepositionMarker;
    }

    /**
     * Sets the value of the mPrepositionMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMPrepositionMarker(boolean value) {
        this.mPrepositionMarker = value;
    }

    /**
     * Gets the value of the mPrepositionValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getMPrepositionValue() {
        return mPrepositionValue;
    }

    /**
     * Sets the value of the mPrepositionValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMPrepositionValue(String value) {
        this.mPrepositionValue = value;
    }

    /**
     * Gets the value of the mHomogeneousGroupMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public boolean isMHomogeneousGroupMarker() {
        return mHomogeneousGroupMarker;
    }

    /**
     * Sets the value of the mHomogeneousGroupMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMHomogeneousGroupMarker(boolean value) {
        this.mHomogeneousGroupMarker = value;
    }

    /**
     * Gets the value of the mHomogeneousGroupId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Integer getMHomogeneousGroupId() {
        return mHomogeneousGroupId;
    }

    /**
     * Sets the value of the mHomogeneousGroupId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMHomogeneousGroupId(Integer value) {
        this.mHomogeneousGroupId = value;
    }

    /**
     * Gets the value of the mCompoundPredicateGroupMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public boolean isMCompoundPredicateGroupMarker() {
        return mCompoundPredicateGroupMarker;
    }

    /**
     * Sets the value of the mCompoundPredicateGroupMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMCompoundPredicateGroupMarker(boolean value) {
        this.mCompoundPredicateGroupMarker = value;
    }

    /**
     * Gets the value of the mCompoundPredicateGroupId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Integer getMCompoundPredicateGroupId() {
        return mCompoundPredicateGroupId;
    }

    /**
     * Sets the value of the mCompoundPredicateGroupId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMCompoundPredicateGroupId(Integer value) {
        this.mCompoundPredicateGroupId = value;
    }

    /**
     * Gets the value of the mQualityIntensifierMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public boolean isMQualityIntensifierMarker() {
        return mQualityIntensifierMarker;
    }

    /**
     * Sets the value of the mQualityIntensifierMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMQualityIntensifierMarker(boolean value) {
        this.mQualityIntensifierMarker = value;
    }

    /**
     * Gets the value of the mQualityIntensifierId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Integer getMQualityIntensifierId() {
        return mQualityIntensifierId;
    }

    /**
     * Sets the value of the mQualityIntensifierId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMQualityIntensifierId(Integer value) {
        this.mQualityIntensifierId = value;
    }

    /**
     * Gets the value of the mActionIntensifierMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public boolean isMActionIntensifierMarker() {
        return mActionIntensifierMarker;
    }

    /**
     * Sets the value of the mActionIntensifierMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMActionIntensifierMarker(boolean value) {
        this.mActionIntensifierMarker = value;
    }

    /**
     * Gets the value of the mActionIntensifierId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Integer getMActionIntensifierId() {
        return mActionIntensifierId;
    }

    /**
     * Sets the value of the mActionIntensifierId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMActionIntensifierId(Integer value) {
        this.mActionIntensifierId = value;
    }

    /**
     * Gets the value of the dPoSId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Integer getDPoSId() {
        return dPoSId;
    }

    /**
     * Sets the value of the dPoSId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDPoSId(Integer value) {
        this.dPoSId = value;
    }

    /**
     * Gets the value of the dGenderId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Integer getDGenderId() {
        return dGenderId;
    }

    /**
     * Sets the value of the dGenderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDGenderId(Integer value) {
        this.dGenderId = value;
    }

    /**
     * Gets the value of the dCaseId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Integer getDCaseId() {
        return dCaseId;
    }

    /**
     * Sets the value of the dCaseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDCaseId(Integer value) {
        this.dCaseId = value;
    }

    /**
     * Gets the value of the dNumberId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Integer getDNumberId() {
        return dNumberId;
    }

    /**
     * Sets the value of the dNumberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDNumberId(Integer value) {
        this.dNumberId = value;
    }

    /**
     * Gets the value of the dSubjectOfClauseMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public boolean isDSubjectOfClauseMarker() {
        return dSubjectOfClauseMarker;
    }

    /**
     * Sets the value of the dSubjectOfClauseMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDSubjectOfClauseMarker(boolean value) {
        this.dSubjectOfClauseMarker = value;
    }

    /**
     * Gets the value of the dPredicateOfClauseMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public boolean isDPredicateOfClauseMarker() {
        return dPredicateOfClauseMarker;
    }

    /**
     * Sets the value of the dPredicateOfClauseMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDPredicateOfClauseMarker(boolean value) {
        this.dPredicateOfClauseMarker = value;
    }

    /**
     * Gets the value of the dInfinitiveMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public boolean isDInfinitiveMarker() {
        return dInfinitiveMarker;
    }

    /**
     * Sets the value of the dInfinitiveMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDInfinitiveMarker(boolean value) {
        this.dInfinitiveMarker = value;
    }

    /**
     * Gets the value of the dConjunctiveMoodMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public boolean isDConjunctiveMoodMarker() {
        return dConjunctiveMoodMarker;
    }

    /**
     * Sets the value of the dConjunctiveMoodMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDConjunctiveMoodMarker(boolean value) {
        this.dConjunctiveMoodMarker = value;
    }

    /**
     * Gets the value of the dNegationNeMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public boolean isDNegationNeMarker() {
        return dNegationNeMarker;
    }

    /**
     * Sets the value of the dNegationNeMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDNegationNeMarker(boolean value) {
        this.dNegationNeMarker = value;
    }

    /**
     * Gets the value of the dPrepositionMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public boolean isDPrepositionMarker() {
        return dPrepositionMarker;
    }

    /**
     * Sets the value of the dPrepositionMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDPrepositionMarker(boolean value) {
        this.dPrepositionMarker = value;
    }

    /**
     * Gets the value of the dPrepositionValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getDPrepositionValue() {
        return dPrepositionValue;
    }

    /**
     * Sets the value of the dPrepositionValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDPrepositionValue(String value) {
        this.dPrepositionValue = value;
    }

    /**
     * Gets the value of the dHomogeneousGroupMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public boolean isDHomogeneousGroupMarker() {
        return dHomogeneousGroupMarker;
    }

    /**
     * Sets the value of the dHomogeneousGroupMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDHomogeneousGroupMarker(boolean value) {
        this.dHomogeneousGroupMarker = value;
    }

    /**
     * Gets the value of the dHomogeneousGroupId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Integer getDHomogeneousGroupId() {
        return dHomogeneousGroupId;
    }

    /**
     * Sets the value of the dHomogeneousGroupId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDHomogeneousGroupId(Integer value) {
        this.dHomogeneousGroupId = value;
    }

    /**
     * Gets the value of the dCompoundPredicateGroupMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public boolean isDCompoundPredicateGroupMarker() {
        return dCompoundPredicateGroupMarker;
    }

    /**
     * Sets the value of the dCompoundPredicateGroupMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDCompoundPredicateGroupMarker(boolean value) {
        this.dCompoundPredicateGroupMarker = value;
    }

    /**
     * Gets the value of the dCompoundPredicateGroupId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Integer getDCompoundPredicateGroupId() {
        return dCompoundPredicateGroupId;
    }

    /**
     * Sets the value of the dCompoundPredicateGroupId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDCompoundPredicateGroupId(Integer value) {
        this.dCompoundPredicateGroupId = value;
    }

    /**
     * Gets the value of the dQualityIntensifierMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public boolean isDQualityIntensifierMarker() {
        return dQualityIntensifierMarker;
    }

    /**
     * Sets the value of the dQualityIntensifierMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDQualityIntensifierMarker(boolean value) {
        this.dQualityIntensifierMarker = value;
    }

    /**
     * Gets the value of the dQualityIntensifierId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Integer getDQualityIntensifierId() {
        return dQualityIntensifierId;
    }

    /**
     * Sets the value of the dQualityIntensifierId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDQualityIntensifierId(Integer value) {
        this.dQualityIntensifierId = value;
    }

    /**
     * Gets the value of the dActionIntensifierMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public boolean isDActionIntensifierMarker() {
        return dActionIntensifierMarker;
    }

    /**
     * Sets the value of the dActionIntensifierMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDActionIntensifierMarker(boolean value) {
        this.dActionIntensifierMarker = value;
    }

    /**
     * Gets the value of the dActionIntensifierId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Integer getDActionIntensifierId() {
        return dActionIntensifierId;
    }

    /**
     * Sets the value of the dActionIntensifierId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDActionIntensifierId(Integer value) {
        this.dActionIntensifierId = value;
    }

    /**
     * Gets the value of the wordOrder property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    @Override
    public Integer getWordOrder() {
        return wordOrder;
    }

    /**
     * Sets the value of the wordOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setWordOrder(Integer value) {
        this.wordOrder = value;
    }

    /**
     * Gets the value of the dateOfCreation property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public XMLGregorianCalendar getDateOfCreation() {
        return dateOfCreation;
    }

    /**
     * Sets the value of the dateOfCreation property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-05-16T09:00:51+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDateOfCreation(XMLGregorianCalendar value) {
        this.dateOfCreation = value;
    }

}
