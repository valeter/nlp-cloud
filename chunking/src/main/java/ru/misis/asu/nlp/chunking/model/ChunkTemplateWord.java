package ru.misis.asu.nlp.chunking.model;

import org.apache.uima.jcas.tcas.Annotation;
import ru.misis.asu.nlp.commons.Pair;

import java.util.List;

/**
 * @author valter
 *         14.07.16
 */
public interface ChunkTemplateWord<ExCorrWordT extends Annotation> {
    List<Pair<ChunkInfoWord, List<ChunkInfoWord>>> buildChunkWords(
        ExCorrWordT extendedCorrectedWord, int ind, List<ChunkInfoWord> mainChunkWords);
}
