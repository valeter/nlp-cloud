package ru.misis.asu.nlp.chunking.model.en;

import org.apache.uima.jcas.JCas;
import ru.misis.asu.nlp.chunking.clause.ClauseInfoEn;
import ru.misis.asu.nlp.chunking.model.*;
import ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn;
import ru.misis.asu.nlp.extended.types.en.ExtendedCorrectedWordEn;
import ru.misis.asu.nlp.extended.types.en.ExtendedWordEn;
import ru.misis.asu.nlp.tagging.types.Wordform;

/**
 * @author valter
 * @date 08.04.2016.
 */
public class ChunkBuilderEn implements ChunkBuilder {
    @Override
    public ChunkInfo buildChunk(int id,
                                ChunkInfoWord mainChunkWord,
                                ChunkInfoWord dependentChunkWord,
                                ChunkWordOrder wordOrder,
                                ChunkTemplate chunkTemplate,
                                JCas aJCas) {
        ChunkInfo result = new ChunkInfo(id);

        ChunkCorrectedEn chunk = new ChunkCorrectedEn(aJCas);

        chunk.setMainWord((ExtendedWordEn) mainChunkWord.getExtendedWord());
        chunk.setDependentWord((ExtendedWordEn) dependentChunkWord.getExtendedWord());
        chunk.setBegin(mainChunkWord.getExtendedCorrectedWord().getBegin());
        chunk.setEnd(mainChunkWord.getExtendedCorrectedWord().getEnd());

        chunk.setMainWordCorrection(mainChunkWord.getCorrection());
        chunk.setDependentWordCorrection(dependentChunkWord.getCorrection());

        ExtendedCorrectedWordEn mainWord = (ExtendedCorrectedWordEn) mainChunkWord.getExtendedCorrectedWord();
        ExtendedCorrectedWordEn dependentWord = (ExtendedCorrectedWordEn) dependentChunkWord.getExtendedCorrectedWord();

        chunk.setMainWordPossessiveEndingCorrection(mainWord.getPossessiveEnding() == null ? null : mainWord.getPossessiveEnding().getCorrection());
        chunk.setMainWordNegativeCorrection(mainWord.getNegativeParticle() == null ? null : mainWord.getNegativeParticle().getCorrection());
        chunk.setMainWordPossessivePronounCorrection(mainWord.getPossessivePronoun() == null ? null : mainWord.getPossessivePronoun().getCorrection());
        chunk.setMainWordPersonalPronounCorrection(mainWord.getPersonalPronoun() == null ? null : mainWord.getPersonalPronoun().getCorrection());

        chunk.setDependentWordPossessiveEndingCorrection(dependentWord.getPossessiveEnding() == null ? null : dependentWord.getPossessiveEnding().getCorrection());
        chunk.setDependentWordNegativeCorrection(dependentWord.getNegativeParticle() == null ? null : dependentWord.getNegativeParticle().getCorrection());
        chunk.setDependentWordPossessivePronounCorrection(dependentWord.getPossessivePronoun() == null ? null : dependentWord.getPossessivePronoun().getCorrection());
        chunk.setDependentWordPersonalPronounCorrection(dependentWord.getPersonalPronoun() == null ? null : dependentWord.getPersonalPronoun().getCorrection());

        result.setChunkCorrected(chunk);
        result.setMainWordInfo(mainChunkWord);
        result.setDependentWordInfo(dependentChunkWord);
        result.setTemplate(chunkTemplate);
        result.setHeadChunk(isHeadChunk(mainChunkWord, dependentChunkWord, wordOrder));
        return result;
    }

    private boolean isHeadChunk(ChunkInfoWord mainChunkWord, ChunkInfoWord dependentChunkWord, ChunkWordOrder wordOrder) {
        boolean mainFirst = wordOrder == ChunkWordOrder.MAIN_WORD_FIRST;
        return ClauseInfoEn.canBeMainWord((Wordform) mainChunkWord.getWordform())
            && ClauseInfoEn.canBeDependentWord((Wordform) dependentChunkWord.getWordform(), mainFirst);
    }
}
