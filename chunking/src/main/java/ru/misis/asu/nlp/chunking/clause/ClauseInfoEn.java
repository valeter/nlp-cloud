package ru.misis.asu.nlp.chunking.clause;

import ru.misis.asu.nlp.tagging.types.Wordform;

/**
 * @author valter
 * @date 08.04.2016.
 */
public class ClauseInfoEn {
    public static boolean canBeMainWord(Wordform wordform) {
        boolean isNoun = "NN|NNS|NNP|NNPS".contains(wordform.getPos().toUpperCase());
        if (isNoun) {
            return true;
        }

        boolean isPersonalPronoun = "PRP".equals(wordform.getPos().toUpperCase());
        if (isPersonalPronoun) {
            return true;
        }

        boolean isWhPronoun = "WP".equals(wordform.getPos().toUpperCase());
        if (isWhPronoun) {
            return true;
        }

        boolean isGerund = "VBG".equals(wordform.getPos().toUpperCase());
        if (isGerund) {
            return true;
        }

        boolean isDeterminer = "DT".equals(wordform.getPos().toUpperCase());
        if (isDeterminer) {
            return true;
        }

        boolean isCardinalNumber = "CD".equals(wordform.getPos().toUpperCase());
        if (isCardinalNumber) {
            return true;
        }

        boolean isSubstPossessivePronoun = "PRP$".equals(wordform.getPos().toUpperCase());
        if (isSubstPossessivePronoun) {
            return true;
        }

        return false;
    }

    public static boolean canBeDependentWord(Wordform wordform, boolean mainFirst) {
        boolean isFiniteVerb = "VBD|VBP|VBZ".contains(wordform.getPos().toUpperCase());
        if (isFiniteVerb) {
            return true;
        }

        boolean isModalVerb = "MD".equals(wordform.getPos().toUpperCase());
        if (isModalVerb) {
            return true;
        }

        return false;
    }
}
