package ru.misis.asu.nlp.chunking.model;


import org.apache.uima.jcas.tcas.Annotation;
import ru.misis.asu.nlp.commons.util.WordformUtil;

import java.util.Comparator;

/**
 * @author valter
 * @date 08.04.2016.
 */
public class ChunkInfoWord<
    ExCorrWordT extends Annotation,
    ExWordT extends Annotation,
    WordformT extends Annotation> {

    private int wordInd;
    private String correction;
    private ExCorrWordT extendedCorrectedWord;
    private ExWordT extendedWord;
    private WordformT wordform;
    private Comparator<? super Annotation> comparator;

    public ChunkInfoWord(int wordInd, String correction,
                         ExCorrWordT extendedCorrectedWord,
                         ExWordT extendedWord,
                         WordformT wordform,
                         Comparator<? super Annotation> comparator) {
        this.wordInd = wordInd;
        this.correction = correction;
        this.extendedCorrectedWord = extendedCorrectedWord;
        this.extendedWord = extendedWord;
        this.wordform = wordform;
        this.comparator = comparator;
    }

    public int getWordInd() {
        return wordInd;
    }

    public String getCorrection() {
        return correction;
    }

    public ExCorrWordT getExtendedCorrectedWord() {
        return extendedCorrectedWord;
    }

    public ExWordT getExtendedWord() {
        return extendedWord;
    }

    public WordformT getWordform() {
        return wordform;
    }

    public Comparator<? super Annotation> getComparator() {
        return comparator;
    }

    @Override
    public String toString() {
        return "{" +
            "text='" + extendedCorrectedWord.getCoveredText() + "'" +
            ",correction='" + getCorrection() + '\'' +
            ",wordform=" + WordformUtil.fixWordformString(wordform.toString()) + "" +
            ",wordInd=" + wordInd +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ChunkInfoWord that = (ChunkInfoWord) o;

        return wordform != null ? comparator.compare(wordform, that.wordform) == 0 : that.wordform == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (wordform != null ? wordform.hashCode() : 0);
        return result;
    }
}
