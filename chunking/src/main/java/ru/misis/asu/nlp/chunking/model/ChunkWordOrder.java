package ru.misis.asu.nlp.chunking.model;

/**
 * @author valter
 * @date 08.04.2016.
 */
public enum ChunkWordOrder {
    ANY,
    MAIN_WORD_FIRST,
    DEPENDENT_WORD_FIRST;
}
