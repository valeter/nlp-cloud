package ru.misis.asu.nlp.chunking.model.en;

/**
 * @author valter
 *         14.07.16
 */
public enum PersonalPronounTypeEn {
    NOMINATIVE,
    OBLIQUE,
    REFLEXIVE
}
