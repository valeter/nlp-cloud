package ru.misis.asu.nlp.chunking;

import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.jcas.JCas;
import ru.misis.asu.nlp.chunking.model.ChunkInfo;
import ru.misis.asu.nlp.chunking.model.ChunkInfoTree;
import ru.misis.asu.nlp.chunking.types.ChunkCorrected;
import ru.misis.asu.nlp.chunking.types.ChunkCorrectedEn;
import ru.misis.asu.nlp.chunking.types.ChunkCorrectedRu;
import ru.misis.asu.nlp.chunking.types.ChunkCorrectedTree;
import ru.misis.asu.nlp.commons.Pair;
import ru.misis.asu.nlp.commons.cas.FSUtils;
import ru.misis.asu.nlp.commons.config.Config;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author valter
 * @date 13.05.2016.
 */
public class ChunkUtil {
    public static String printResult(String text, List<ChunkCorrectedTree> resultTrees) {
        try {
            String s = getTreeString(text, resultTrees);
            System.out.println("Output sentence: [" + s.trim() + "]");
            Files.write(Paths.get(Config.getProperty(Config.OUTPUT_FILE)), s.getBytes("UTF-8"),
                    StandardOpenOption.APPEND, StandardOpenOption.CREATE);
            return s;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getTreeString(String text, List<ChunkCorrectedTree> resultTrees) {
        return getTreeString(text, resultTrees, 0);
    }

    public static String getTreeString(String text, List<ChunkCorrectedTree> resultTrees, int offset) {
        StringBuilder result = new StringBuilder(text.length());

        AtomicInteger from = new AtomicInteger(0);
        AtomicInteger to = new AtomicInteger(text.length());

        for (ChunkCorrectedTree resultTree : resultTrees) {
            Map<Pair<Integer, Integer>, String> corrections = new TreeMap<>((o1, o2) -> {
                int firstCmp = Integer.compare(o1.first, o2.first);
                int secondCmp = Integer.compare(o1.second, o2.second);
                return firstCmp != 0 ? firstCmp : secondCmp;
            });

            ChunkCorrected chunkCorrected = resultTree.getHeadChunk();
            if (chunkCorrected instanceof ChunkCorrectedRu) {
                getCorrections((ChunkCorrectedRu) chunkCorrected, corrections);
            } else if (chunkCorrected instanceof ChunkCorrectedEn) {
                getCorrections((ChunkCorrectedEn) chunkCorrected, corrections);
            } else {
                throw new IllegalArgumentException();
            }

            for (Map.Entry<Pair<Integer, Integer>, String> correction : corrections.entrySet()) {
                result.append(text.substring(from.get(), Math.min(to.get(), correction.getKey().first - offset)));
                String old = text.substring(correction.getKey().first - offset, correction.getKey().second - offset);
                if (!Objects.equals(old, correction.getValue())) {
                    result.append(correction.getValue());
                } else {
                    result.append(correction.getValue());
                }
                from.set(correction.getKey().second - offset);
            }
        }

        result.append(text.substring(from.get(), to.get()));
        return result.toString();
    }

    public static void getCorrections(ChunkCorrectedRu chunk, Map<Pair<Integer, Integer>, String> corrections) {
        corrections.put(new Pair<>(chunk.getMainWord().getBegin(), chunk.getMainWord().getEnd()), chunk.getMainWordCorrection());
        corrections.put(new Pair<>(chunk.getDependentWord().getBegin(), chunk.getDependentWord().getEnd()), chunk.getDependentWordCorrection());
        if (chunk.getMainWordInclinationCorrection() != null) {
            corrections.put(new Pair<>(chunk.getMainWord().getInclinationParticle().getBegin(), chunk.getMainWord().getInclinationParticle().getEnd()), chunk.getMainWordInclinationCorrection());
        }
        if (chunk.getMainWordNegativeCorrection() != null) {
            corrections.put(new Pair<>(chunk.getMainWord().getNegativeParticle().getBegin(), chunk.getMainWord().getNegativeParticle().getEnd()), chunk.getMainWordNegativeCorrection());
        }
        if (chunk.getMainWordPrepositionCorrection() != null) {
            corrections.put(new Pair<>(chunk.getMainWord().getPreposition().getBegin(), chunk.getMainWord().getPreposition().getEnd()), chunk.getMainWordPrepositionCorrection());
        }

        if (chunk.getDependentWordInclinationCorrection() != null) {
            corrections.put(new Pair<>(chunk.getDependentWord().getInclinationParticle().getBegin(), chunk.getDependentWord().getInclinationParticle().getEnd()), chunk.getDependentWordInclinationCorrection());
        }
        if (chunk.getDependentWordNegativeCorrection() != null) {
            corrections.put(new Pair<>(chunk.getDependentWord().getNegativeParticle().getBegin(), chunk.getDependentWord().getNegativeParticle().getEnd()), chunk.getDependentWordNegativeCorrection());
        }
        if (chunk.getDependentWordPrepositionCorrection() != null) {
            corrections.put(new Pair<>(chunk.getDependentWord().getPreposition().getBegin(), chunk.getDependentWord().getPreposition().getEnd()), chunk.getDependentWordPrepositionCorrection());
        }

        if (chunk.getConnectedChunks() != null) {
            for (FeatureStructure featureStructure : chunk.getConnectedChunks().toArray()) {
                ChunkCorrectedRu connection = (ChunkCorrectedRu) featureStructure;
                getCorrections(connection, corrections);
            }
        }
    }

    private static void getCorrections(ChunkCorrectedEn chunk, Map<Pair<Integer, Integer>, String> corrections) {
        corrections.put(new Pair<>(chunk.getMainWord().getBegin(), chunk.getMainWord().getEnd()), chunk.getMainWordCorrection());
        corrections.put(new Pair<>(chunk.getDependentWord().getBegin(), chunk.getDependentWord().getEnd()), chunk.getDependentWordCorrection());
        if (chunk.getMainWordPossessiveEndingCorrection() != null) {
            corrections.put(new Pair<>(chunk.getMainWord().getPossessiveEnding().getBegin(), chunk.getMainWord().getPossessiveEnding().getEnd()), chunk.getMainWordPossessiveEndingCorrection());
        }
        if (chunk.getMainWordNegativeCorrection() != null) {
            corrections.put(new Pair<>(chunk.getMainWord().getNegativeParticle().getBegin(), chunk.getMainWord().getNegativeParticle().getEnd()), chunk.getMainWordNegativeCorrection());
        }
        if (chunk.getMainWordPersonalPronounCorrection() != null) {
            corrections.put(new Pair<>(chunk.getMainWord().getPersonalPronoun().getBegin(), chunk.getMainWord().getPersonalPronoun().getEnd()), chunk.getMainWordPersonalPronounCorrection());
        }
        if (chunk.getMainWordPossessivePronounCorrection() != null) {
            corrections.put(new Pair<>(chunk.getMainWord().getPossessivePronoun().getBegin(), chunk.getMainWord().getPossessivePronoun().getEnd()), chunk.getMainWordPossessivePronounCorrection());
        }

        if (chunk.getDependentWordPossessiveEndingCorrection() != null) {
            corrections.put(new Pair<>(chunk.getDependentWord().getPossessiveEnding().getBegin(), chunk.getDependentWord().getPossessiveEnding().getEnd()), chunk.getDependentWordPossessiveEndingCorrection());
        }
        if (chunk.getDependentWordNegativeCorrection() != null) {
            corrections.put(new Pair<>(chunk.getDependentWord().getNegativeParticle().getBegin(), chunk.getDependentWord().getNegativeParticle().getEnd()), chunk.getDependentWordNegativeCorrection());
        }
        if (chunk.getDependentWordPersonalPronounCorrection() != null) {
            corrections.put(new Pair<>(chunk.getDependentWord().getPersonalPronoun().getBegin(), chunk.getDependentWord().getPersonalPronoun().getEnd()), chunk.getDependentWordPersonalPronounCorrection());
        }
        if (chunk.getDependentWordPossessivePronounCorrection() != null) {
            corrections.put(new Pair<>(chunk.getDependentWord().getPossessivePronoun().getBegin(), chunk.getDependentWord().getPossessivePronoun().getEnd()), chunk.getDependentWordPossessivePronounCorrection());
        }

        if (chunk.getConnectedChunks() != null) {
            for (FeatureStructure featureStructure : chunk.getConnectedChunks().toArray()) {
                ChunkCorrectedEn connection = (ChunkCorrectedEn) featureStructure;
                getCorrections(connection, corrections);
            }
        }
    }

    public static void printChunkTree(ChunkInfoTree tree, ChunkInfo chunkInfo, String tab) {
        ChunkCorrected chunkCorrected = chunkInfo.getChunkCorrected();
        if (chunkCorrected instanceof ChunkCorrectedEn) {
            printChunkTreeEn(tree, chunkInfo, tab);
        } else if (chunkCorrected instanceof ChunkCorrectedRu) {
            printChunkTreeRu(tree, chunkInfo, tab);
        } else {
            throw new IllegalArgumentException();
        }
    }

    private static void printChunkTreeRu(ChunkInfoTree tree, ChunkInfo chunkInfo, String tab) {
        ChunkCorrectedRu chunkCorrected = (ChunkCorrectedRu) chunkInfo.getChunkCorrected();
        String mwn = chunkCorrected.getMainWordNegativeCorrection() == null ? "" : chunkCorrected.getMainWordNegativeCorrection() + " ";
        String mwp = chunkCorrected.getMainWordPrepositionCorrection() == null ? "" : chunkCorrected.getMainWordPrepositionCorrection() + " ";
        String mwi = chunkCorrected.getMainWordInclinationCorrection() == null ? "" : chunkCorrected.getMainWordInclinationCorrection() + " ";
        String mw = mwn + mwp + chunkCorrected.getMainWordCorrection() + mwi;

        String dwn = chunkCorrected.getDependentWordNegativeCorrection() == null ? "" : chunkCorrected.getDependentWordNegativeCorrection() + " ";
        String dwp = chunkCorrected.getDependentWordPrepositionCorrection() == null ? "" : chunkCorrected.getDependentWordPrepositionCorrection() + " ";
        String dwi = chunkCorrected.getDependentWordInclinationCorrection() == null ? "" : chunkCorrected.getDependentWordInclinationCorrection() + " ";
        String dw = dwn + dwp + chunkCorrected.getDependentWordCorrection() + dwi;

        System.out.println(tab + "[" + mwn + mwp + mwi + mw + "] [" + dwn + dwp + dwi + dw
            + "] (templateId: " + chunkInfo.getTemplate().getId() + ", chunkId: " + chunkInfo.getId() + ")");
        for (ChunkInfo child : tree.getChildren(chunkInfo)) {
            printChunkTreeRu(tree, child, tab + "  ");
        }
    }

    private static void printChunkTreeEn(ChunkInfoTree tree, ChunkInfo chunkInfo, String tab) {
        ChunkCorrectedEn chunkCorrected = (ChunkCorrectedEn) chunkInfo.getChunkCorrected();
        String mwn = chunkCorrected.getMainWordNegativeCorrection() == null ? "" : chunkCorrected.getMainWordNegativeCorrection() + " ";
        String mw = mwn + chunkCorrected.getMainWordCorrection();

        String dwn = chunkCorrected.getDependentWordNegativeCorrection() == null ? "" : chunkCorrected.getDependentWordNegativeCorrection() + " ";
        String dw = dwn + chunkCorrected.getDependentWordCorrection();

        System.out.println(tab + mw + " " + dw + " (" + chunkInfo.getTemplate().getId() + ")");
        for (ChunkInfo child : tree.getChildren(chunkInfo)) {
            printChunkTreeEn(tree, child, tab + "  ");
        }
    }

    public static ChunkCorrected copyChunk(JCas cas, ChunkCorrected chunk) {
        if (chunk instanceof ChunkCorrectedEn) {
            return copyChunkEn(cas, (ChunkCorrectedEn) chunk);
        } else if (chunk instanceof ChunkCorrectedRu) {
            return copyChunkRu(cas, (ChunkCorrectedRu) chunk);
        } else {
            throw new IllegalArgumentException();
        }
    }

    private static ChunkCorrectedEn copyChunkEn(JCas cas, ChunkCorrectedEn chunk) {
        ChunkCorrectedEn result = new ChunkCorrectedEn(cas);
        result.setBegin(chunk.getBegin());
        result.setEnd(chunk.getEnd());
        result.setMainWord(chunk.getMainWord());
        result.setMainWordCorrection(chunk.getMainWordCorrection());
        result.setMainWordNegativeCorrection(chunk.getMainWordNegativeCorrection());
        result.setMainWordPersonalPronounCorrection(chunk.getMainWordPersonalPronounCorrection());
        result.setMainWordPossessivePronounCorrection(chunk.getMainWordPossessivePronounCorrection());
        result.setMainWordPossessiveEndingCorrection(chunk.getMainWordPossessiveEndingCorrection());
        result.setDependentWord(chunk.getDependentWord());
        result.setDependentWordCorrection(chunk.getDependentWordCorrection());
        result.setDependentWordNegativeCorrection(chunk.getDependentWordNegativeCorrection());
        result.setDependentWordPersonalPronounCorrection(chunk.getDependentWordPersonalPronounCorrection());
        result.setDependentWordPossessivePronounCorrection(chunk.getDependentWordPossessivePronounCorrection());
        result.setDependentWordPossessiveEndingCorrection(chunk.getDependentWordPossessiveEndingCorrection());
        List<ChunkCorrected> connections = new ArrayList<>();
        if (chunk.getConnectedChunks() != null) {
            for (FeatureStructure featureStructure : chunk.getConnectedChunks().toArray()) {
                ChunkCorrectedEn connection = (ChunkCorrectedEn) featureStructure;
                connections.add(copyChunkEn(cas, connection));
            }
        }
        result.setConnectedChunks(FSUtils.toFSArray(cas, connections));
        return result;
    }

    private static ChunkCorrectedRu copyChunkRu(JCas cas, ChunkCorrectedRu chunk) {
        ChunkCorrectedRu result = new ChunkCorrectedRu(cas);
        result.setBegin(chunk.getBegin());
        result.setEnd(chunk.getEnd());
        result.setMainWord(chunk.getMainWord());
        result.setMainWordCorrection(chunk.getMainWordCorrection());
        result.setMainWordInclinationCorrection(chunk.getMainWordInclinationCorrection());
        result.setMainWordNegativeCorrection(chunk.getMainWordNegativeCorrection());
        result.setMainWordPrepositionCorrection(chunk.getMainWordPrepositionCorrection());
        result.setDependentWord(chunk.getDependentWord());
        result.setDependentWordCorrection(chunk.getDependentWordCorrection());
        result.setDependentWordInclinationCorrection(chunk.getDependentWordInclinationCorrection());
        result.setDependentWordNegativeCorrection(chunk.getDependentWordNegativeCorrection());
        result.setDependentWordPrepositionCorrection(chunk.getDependentWordPrepositionCorrection());
        List<ChunkCorrected> connections = new ArrayList<>();
        if (chunk.getConnectedChunks() != null) {
            for (FeatureStructure featureStructure : chunk.getConnectedChunks().toArray()) {
                ChunkCorrectedRu connection = (ChunkCorrectedRu) featureStructure;
                connections.add(copyChunkRu(cas, connection));
            }
        }
        result.setConnectedChunks(FSUtils.toFSArray(cas, connections));
        return result;
    }
}
