package ru.misis.asu.nlp.chunking.model;

import org.apache.uima.jcas.JCas;

/**
 * @author Ivan Anisimov
 *         valter@yandex-team.ru
 *         02.04.17
 */
public interface ChunkBuilder {
    ChunkInfo buildChunk(int id,
                         ChunkInfoWord mainChunkWord,
                         ChunkInfoWord dependentChunkWord,
                         ChunkWordOrder wordOrder,
                         ChunkTemplate chunkTemplate,
                         JCas aJCas);
}
