package ru.misis.asu.nlp.chunking.model.ru;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import ru.misis.asu.nlp.chunking.clause.ClauseInfoRu;
import ru.misis.asu.nlp.chunking.model.*;
import ru.misis.asu.nlp.chunking.types.ChunkCorrectedRu;
import ru.misis.asu.nlp.extended.types.ru.ExtendedCorrectedWordRu;
import ru.misis.asu.nlp.extended.types.ru.ExtendedWordRu;
import ru.misis.asu.nlp.morphoanalysis.types.Word;
import ru.misis.asu.nlp.morphoanalysis.types.Wordform;

/**
 * @author valter
 * @date 08.04.2016.
 */
public class ChunkBuilderRu implements ChunkBuilder {
    @Override
    public ChunkInfo buildChunk(int id,
                                ChunkInfoWord mainChunkWord,
                                ChunkInfoWord dependentChunkWord,
                                ChunkWordOrder wordOrder,
                                ChunkTemplate chunkTemplate,
                                JCas aJCas) {
        ChunkInfo result = new ChunkInfo(id);

        ChunkCorrectedRu chunk = new ChunkCorrectedRu(aJCas);

        ExtendedWordRu mainExWord = (ExtendedWordRu)mainChunkWord.getExtendedWord().clone();
        Word mainWord = (Word)mainExWord.getWord().clone();
        mainWord.setWordforms(new FSArray(aJCas, 1));
        mainWord.setWordforms(0, (Wordform) mainChunkWord.getWordform().clone());
        mainExWord.setWord(mainWord);
        chunk.setMainWord(mainExWord);

        ExtendedWordRu dependentExWord = (ExtendedWordRu)dependentChunkWord.getExtendedWord().clone();
        Word dependentWord = (Word)dependentExWord.getWord().clone();
        dependentWord.setWordforms(new FSArray(aJCas, 1));
        dependentWord.setWordforms(0, (Wordform) dependentChunkWord.getWordform().clone());
        dependentExWord.setWord(dependentWord);
        chunk.setDependentWord(dependentExWord);

        chunk.setBegin(mainChunkWord.getExtendedCorrectedWord().getBegin());
        chunk.setEnd(mainChunkWord.getExtendedCorrectedWord().getEnd());

        chunk.setMainWordCorrection(mainChunkWord.getCorrection());
        chunk.setDependentWordCorrection(dependentChunkWord.getCorrection());

        ExtendedCorrectedWordRu mainExCorrWord = (ExtendedCorrectedWordRu)mainChunkWord.getExtendedCorrectedWord();
        ExtendedCorrectedWordRu dependentExCorrWord = (ExtendedCorrectedWordRu)dependentChunkWord.getExtendedCorrectedWord();

        chunk.setMainWordInclinationCorrection(mainExCorrWord.getInclinationParticle() == null ? null : mainExCorrWord.getInclinationParticle().getCorrection());
        chunk.setMainWordNegativeCorrection(mainExCorrWord.getNegativeParticle() == null ? null : mainExCorrWord.getNegativeParticle().getCorrection());
        chunk.setMainWordPrepositionCorrection(mainExCorrWord.getPreposition() == null ? null : mainExCorrWord.getPreposition().getCorrection());

        chunk.setDependentWordInclinationCorrection(dependentExCorrWord.getInclinationParticle() == null ? null : dependentExCorrWord.getInclinationParticle().getCorrection());
        chunk.setDependentWordNegativeCorrection(dependentExCorrWord.getNegativeParticle() == null ? null : dependentExCorrWord.getNegativeParticle().getCorrection());
        chunk.setDependentWordPrepositionCorrection(dependentExCorrWord.getPreposition() == null ? null : dependentExCorrWord.getPreposition().getCorrection());

        result.setChunkCorrected(chunk);
        result.setMainWordInfo(mainChunkWord);
        result.setDependentWordInfo(dependentChunkWord);
        result.setTemplate(chunkTemplate);
        result.setHeadChunk(isHeadChunk(mainChunkWord, dependentChunkWord, wordOrder));
        return result;
    }

    private boolean isHeadChunk(ChunkInfoWord mainChunkWord, ChunkInfoWord dependentChunkWord, ChunkWordOrder wordOrder) {
        boolean mainFirst = wordOrder == ChunkWordOrder.MAIN_WORD_FIRST;
        boolean simpleHead = ClauseInfoRu.canBeMainWord((Wordform)mainChunkWord.getWordform())
            && ClauseInfoRu.canBeDependentWord((Wordform)dependentChunkWord.getWordform(), mainFirst);

        return simpleHead || ClauseInfoRu.isNotSimpleHead((Wordform)mainChunkWord.getWordform(),
            (Wordform)dependentChunkWord.getWordform(), mainFirst, getMainWithNeg(mainChunkWord));
    }

    private String getMainWithNeg(ChunkInfoWord mainChunkWord) {
        String result = "";
        ExtendedCorrectedWordRu word = (ExtendedCorrectedWordRu) mainChunkWord.getExtendedCorrectedWord();
        if (word.getNegativeParticle() != null) {
            result += word.getNegativeParticle().getCorrection().trim().toLowerCase() + " ";
        }
        result += mainChunkWord.getCorrection().trim().toLowerCase();
        return result;
    }
}
