package ru.misis.asu.nlp.chunking.model;

import ru.misis.asu.nlp.chunking.types.ChunkCorrected;

/**
 * @author valter
 * @date 08.04.2016.
 */
public class ChunkInfo {
    private int id;

    private ChunkTemplate template;

    private ChunkCorrected chunkCorrected;

    private ChunkInfoWord mainWordInfo;
    private ChunkInfoWord dependentWordInfo;

    private boolean headChunk;

    public ChunkInfo(int id) {
        this.id = id;
    }

    public ChunkInfo(int id, ChunkTemplate template, ChunkCorrected chunkCorrected, ChunkInfoWord mainWordInfo, ChunkInfoWord dependentWordInfo, boolean headChunk) {
        this.id = id;
        this.template = template;
        this.chunkCorrected = chunkCorrected;
        this.mainWordInfo = mainWordInfo;
        this.dependentWordInfo = dependentWordInfo;
        this.headChunk = headChunk;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ChunkTemplate getTemplate() {
        return template;
    }

    public void setTemplate(ChunkTemplate template) {
        this.template = template;
    }

    public ChunkCorrected getChunkCorrected() {
        return chunkCorrected;
    }

    public void setChunkCorrected(ChunkCorrected chunkCorrected) {
        this.chunkCorrected = chunkCorrected;
    }

    public ChunkInfoWord getMainWordInfo() {
        return mainWordInfo;
    }

    public void setMainWordInfo(ChunkInfoWord mainWordInfo) {
        this.mainWordInfo = mainWordInfo;
    }

    public ChunkInfoWord getDependentWordInfo() {
        return dependentWordInfo;
    }

    public void setDependentWordInfo(ChunkInfoWord dependentWordInfo) {
        this.dependentWordInfo = dependentWordInfo;
    }

    public boolean isHeadChunk() {
        return headChunk;
    }

    public void setHeadChunk(boolean headChunk) {
        this.headChunk = headChunk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChunkInfo chunkInfo = (ChunkInfo) o;

        if (mainWordInfo != null ? mainWordInfo.getWordInd() != chunkInfo.mainWordInfo.getWordInd() : chunkInfo.mainWordInfo != null)
            return false;
        return dependentWordInfo != null ? dependentWordInfo.getWordInd() == chunkInfo.dependentWordInfo.getWordInd() : chunkInfo.dependentWordInfo == null;

    }

    @Override
    public int hashCode() {
        int result = mainWordInfo != null ? mainWordInfo.getWordInd() : 0;
        result = 31 * result + (dependentWordInfo != null ? dependentWordInfo.getWordInd() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "{" +
                "chunkId=" + id +
                ", template=" + template +
                ", mainWord=" + mainWordInfo +
                ", dependentWord=" + dependentWordInfo +
                ", isHeadChunk=" + headChunk +
                '}';
    }
}
