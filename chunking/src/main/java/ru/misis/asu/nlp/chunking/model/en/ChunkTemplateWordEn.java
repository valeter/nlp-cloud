package ru.misis.asu.nlp.chunking.model.en;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import ru.misis.asu.nlp.chunking.model.ChunkInfoWord;
import ru.misis.asu.nlp.chunking.model.ChunkTemplateWord;
import ru.misis.asu.nlp.chunking.xml.ChunkTemplateConverter;
import ru.misis.asu.nlp.commons.Pair;
import ru.misis.asu.nlp.extended.types.en.*;
import ru.misis.asu.nlp.tagging.types.CorrectedWord;
import ru.misis.asu.nlp.tagging.types.Word;
import ru.misis.asu.nlp.tagging.types.Wordform;

import java.util.*;
import java.util.function.Function;

/**
 * @author valter
 * @date 08.04.1006.
 */
public class ChunkTemplateWordEn implements ChunkTemplateWord<ExtendedCorrectedWordEn> {
    private static final Map<String, List<PersonalPronounTypeEn>> PERSONAL_PRONOUN_TYPES = new HashMap<String, List<PersonalPronounTypeEn>>() {{
        put("i", Collections.singletonList(PersonalPronounTypeEn.values()[0]));
        put("you", Arrays.asList(PersonalPronounTypeEn.values()[0], PersonalPronounTypeEn.values()[1]));
        put("she", Collections.singletonList(PersonalPronounTypeEn.values()[0]));
        put("he", Collections.singletonList(PersonalPronounTypeEn.values()[0]));
        put("it", Arrays.asList(PersonalPronounTypeEn.values()[0], PersonalPronounTypeEn.values()[1]));
        put("we", Collections.singletonList(PersonalPronounTypeEn.values()[0]));
        put("they", Collections.singletonList(PersonalPronounTypeEn.values()[0]));
        put("me", Collections.singletonList(PersonalPronounTypeEn.values()[1]));
        put("her", Collections.singletonList(PersonalPronounTypeEn.values()[1]));
        put("him", Collections.singletonList(PersonalPronounTypeEn.values()[1]));
        put("us", Collections.singletonList(PersonalPronounTypeEn.values()[1]));
        put("them", Collections.singletonList(PersonalPronounTypeEn.values()[1]));
        put("myself", Collections.singletonList(PersonalPronounTypeEn.values()[2]));
        put("yourself", Collections.singletonList(PersonalPronounTypeEn.values()[2]));
        put("yourselves", Collections.singletonList(PersonalPronounTypeEn.values()[2]));
        put("herself", Collections.singletonList(PersonalPronounTypeEn.values()[2]));
        put("himself", Collections.singletonList(PersonalPronounTypeEn.values()[2]));
        put("itself", Collections.singletonList(PersonalPronounTypeEn.values()[2]));
        put("ourselves", Collections.singletonList(PersonalPronounTypeEn.values()[2]));
        put("themselves", Collections.singletonList(PersonalPronounTypeEn.values()[2]));
        put("ownself", Collections.singletonList(PersonalPronounTypeEn.values()[2]));
        put("ownselves", Collections.singletonList(PersonalPronounTypeEn.values()[2]));
        put("oneself", Collections.singletonList(PersonalPronounTypeEn.values()[2]));
    }};

    private static final Map<String, List<PossessivePronounTypeEn>> POSSESSIVE_PRONOUN_TYPES = new HashMap<String, List<PossessivePronounTypeEn>>() {{
        put("my", Collections.singletonList(PossessivePronounTypeEn.values()[0]));
        put("your", Collections.singletonList(PossessivePronounTypeEn.values()[0]));
        put("his", Arrays.asList(PossessivePronounTypeEn.values()[0], PossessivePronounTypeEn.values()[1]));
        put("her", Collections.singletonList(PossessivePronounTypeEn.values()[0]));
        put("its", Arrays.asList(PossessivePronounTypeEn.values()[0], PossessivePronounTypeEn.values()[1]));
        put("our", Collections.singletonList(PossessivePronounTypeEn.values()[0]));
        put("their", Collections.singletonList(PossessivePronounTypeEn.values()[0]));
        put("thy", Collections.singletonList(PossessivePronounTypeEn.values()[0]));
        put("mine", Collections.singletonList(PossessivePronounTypeEn.values()[1]));
        put("yours", Collections.singletonList(PossessivePronounTypeEn.values()[1]));
        put("hers", Collections.singletonList(PossessivePronounTypeEn.values()[1]));
        put("ours", Collections.singletonList(PossessivePronounTypeEn.values()[1]));
        put("theirs", Collections.singletonList(PossessivePronounTypeEn.values()[1]));
    }};

    private static final Map<String, ArticleTypeEn> ARTICLE_TYPES = new HashMap<String, ArticleTypeEn>() {{
        put("a", ArticleTypeEn.INDEFINITE);
        put("the", ArticleTypeEn.DEFINITE);
    }};

    private JCas cas;

    private String wordPos;
    private PersonalPronounTypeEn wordPrep;
    private PossessivePronounTypeEn wordPrep$;
    private ArticleTypeEn article;

    private Boolean isSubject;
    private Boolean isPredicate;

    private Boolean hasNegation;
    private Boolean hasPossessiveEnding;

    public ChunkTemplateWordEn(JCas cas, String wordPos, PersonalPronounTypeEn wordPrep, PossessivePronounTypeEn wordPrep$, ArticleTypeEn article,
                               Boolean isSubject, Boolean isPredicate,
                               Boolean hasNegation, Boolean hasPossessiveEnding) {
        this.cas = cas;
        this.wordPos = wordPos;
        this.wordPrep = wordPrep;
        this.wordPrep$ = wordPrep$;
        this.article = article;
        this.isSubject = isSubject;
        this.isPredicate = isPredicate;
        this.hasNegation = hasNegation;
        this.hasPossessiveEnding = hasPossessiveEnding;
    }

    @Override
    public List<Pair<ChunkInfoWord, List<ChunkInfoWord>>> buildChunkWords(ExtendedCorrectedWordEn extendedCorrectedWord, int ind, List<ChunkInfoWord> mainChunkWords) {
        List<Pair<ChunkInfoWord, List<ChunkInfoWord>>> result = new ArrayList<>();

        CorrectedWord correctedWord = extendedCorrectedWord.getWord();
        FSArray words = correctedWord.getWords();

        for (int i = 0; i < words.size(); i++) {
            Word word = (Word) words.get(i);

            FSArray wordforms = word.getWordforms();
            for (int j = 0; j < wordforms.size(); j++) {
                Wordform wordform = (Wordform) wordforms.get(j);

                List<ChunkInfoWord> mainChunkWordsCopy = new ArrayList<>();
                if (mainChunkWords != null) {
                    mainChunkWordsCopy.addAll(mainChunkWords);
                }

                boolean posMatches = wordPos == null
                    || (mainChunkWords != null
                            && ChunkTemplateConverter.MATCHES_MAIN_WORD.equals(wordPos)
                            && posMatchesMain(wordform, mainChunkWordsCopy))
                    || wordPos.equals(getPos(wordform))
                    || (wordPos.contains("|")
                            && Arrays.stream(wordPos.split("\\|"))
                                    .filter(w -> w.equals(getPos(wordform)))
                                    .count() > 0);
                boolean prepMatches = wordPrep == null
                    || (extendedCorrectedWord.getPersonalPronoun() != null
                            && getPrepType(extendedCorrectedWord.getPersonalPronoun().getCorrection()).contains(wordPrep))
                    || ("PRP".equals(wordform.getPos()));
                boolean prep$Matches = wordPrep$ == null
                    || (extendedCorrectedWord.getPossessivePronoun() != null && getPrep$Type(extendedCorrectedWord.getPossessivePronoun().getCorrection()).contains(wordPrep$))
                    || ("PRP$".equals(wordform.getPos()));
                boolean articleMatches = article == null
                    || article == ArticleTypeEn.ANY_ARTICLE
                    || (extendedCorrectedWord.getArticle() != null
                            && article.equals(getArticleType(extendedCorrectedWord.getArticle().getCorrection())))
                    || (extendedCorrectedWord.getArticle() == null && article == ArticleTypeEn.ZERO)
                    || ((extendedCorrectedWord.getArticle() == null || "the".equals(extendedCorrectedWord.getArticle().getCorrection()))
                            && article == ArticleTypeEn.ZERO_OR_DEFINITE);

                // no subject and predicate matching
                if (posMatches && prepMatches && prep$Matches && articleMatches
                    && (hasNegation == null || extendedCorrectedWord.getNegativeParticle() != null == hasNegation)
                    && (hasPossessiveEnding == null || extendedCorrectedWord.getPossessiveEnding() != null == hasPossessiveEnding)) {

                    result.add(new Pair<>(
                        new ChunkInfoWord<>(ind,
                            correctedWord.getCorrections(i),
                            extendedCorrectedWord,
                            buildExtendedWord(extendedCorrectedWord, i),
                            wordform,
                            (o1, o2) -> o1 == o2 ? 0 : -1),
                        mainChunkWordsCopy)
                    );
                }
            }
        }

        return result;
    }

    private boolean posMatchesMain(Wordform wordform, List<ChunkInfoWord> mainChunkWords) {
        return stringMatchesMain(wordform, mainChunkWords, this::getPos, true);
    }

    private boolean stringMatchesMain(Wordform wordform, List<ChunkInfoWord> mainChunkWords, Function<Wordform, String> function, boolean strict) {
        String pos = function.apply(wordform);
        if (!strict && pos == null) {
            return true;
        }
        for (Iterator<ChunkInfoWord> iterator = mainChunkWords.iterator(); iterator.hasNext(); ) {
            ChunkInfoWord next = iterator.next();
            String val = function.apply((Wordform) next.getWordform());
            if (val == null && !strict) {
                continue;
            }
            if (!Objects.equals(pos, val)) {
                iterator.remove();
            }
        }

        return mainChunkWords.size() > 0;
    }

    private ExtendedWordEn buildExtendedWord(ExtendedCorrectedWordEn extendedCorrectedWord, int i) {
        ExtendedWordEn result = new ExtendedWordEn(cas);
        result.setBegin(extendedCorrectedWord.getBegin());
        result.setEnd(extendedCorrectedWord.getEnd());

        result.setWord(extendedCorrectedWord.getWord().getWords(i));

        if (extendedCorrectedWord.getPossessiveEnding() != null) {
            PossessiveEndingEn possessiveEnding = new PossessiveEndingEn(cas);
            possessiveEnding.setBegin(extendedCorrectedWord.getPossessiveEnding().getBegin());
            possessiveEnding.setEnd(extendedCorrectedWord.getPossessiveEnding().getEnd());
            possessiveEnding.setGrammemes(extendedCorrectedWord.getPossessiveEnding().getGrammemes());
            possessiveEnding.setWord(extendedCorrectedWord.getPossessiveEnding().getWord());
            result.setPossessiveEnding(possessiveEnding);
        }

        if (extendedCorrectedWord.getNegativeParticle() != null) {
            NegativeParticleEn negativeParticle = new NegativeParticleEn(cas);
            negativeParticle.setBegin(extendedCorrectedWord.getNegativeParticle().getBegin());
            negativeParticle.setEnd(extendedCorrectedWord.getNegativeParticle().getEnd());
            negativeParticle.setGrammemes(extendedCorrectedWord.getNegativeParticle().getGrammemes());
            negativeParticle.setWord(extendedCorrectedWord.getNegativeParticle().getWord());
            result.setNegativeParticle(negativeParticle);
        }

        if (extendedCorrectedWord.getPersonalPronoun() != null) {
            PersonalPronounEn personalPronoun = new PersonalPronounEn(cas);
            personalPronoun.setBegin(extendedCorrectedWord.getPersonalPronoun().getBegin());
            personalPronoun.setEnd(extendedCorrectedWord.getPersonalPronoun().getEnd());
            personalPronoun.setGrammemes(extendedCorrectedWord.getPersonalPronoun().getGrammemes());
            personalPronoun.setWord(extendedCorrectedWord.getPersonalPronoun().getWord());
            result.setPersonalPronoun(personalPronoun);
        }

        if (extendedCorrectedWord.getPossessivePronoun() != null) {
            PossessivePronounEn possessivePronoun = new PossessivePronounEn(cas);
            possessivePronoun.setBegin(extendedCorrectedWord.getPossessivePronoun().getBegin());
            possessivePronoun.setEnd(extendedCorrectedWord.getPossessivePronoun().getEnd());
            possessivePronoun.setGrammemes(extendedCorrectedWord.getPossessivePronoun().getGrammemes());
            possessivePronoun.setWord(extendedCorrectedWord.getPossessivePronoun().getWord());
            result.setPossessivePronoun(possessivePronoun);
        }

        return result;
    }

    private String getPos(Wordform wordform) {
        return wordform.getPos().trim();
    }

    private List<PersonalPronounTypeEn> getPrepType(String pronoun) {
        return pronoun == null ? Collections.emptyList() : PERSONAL_PRONOUN_TYPES.getOrDefault(pronoun.toLowerCase().trim(), Collections.emptyList());
    }

    private List<PossessivePronounTypeEn> getPrep$Type(String pronoun) {
        return pronoun == null ? Collections.emptyList() : POSSESSIVE_PRONOUN_TYPES.getOrDefault(pronoun.toLowerCase().trim(), Collections.emptyList());
    }

    private ArticleTypeEn getArticleType(String article) {
        return article == null ? ArticleTypeEn.ANY_ARTICLE : ARTICLE_TYPES.get(article.toLowerCase().trim());
    }
}
