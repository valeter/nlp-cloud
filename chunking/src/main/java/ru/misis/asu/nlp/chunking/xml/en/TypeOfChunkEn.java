
package ru.misis.asu.nlp.chunking.xml.en;

import ru.misis.asu.nlp.chunking.xml.common.HasWordOrder;

import javax.annotation.Generated;
import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Name_of_chunk" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Example" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="M_PoS_Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="M_PRP_type_Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="M_PRP_x0024__type_Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="M_Subject_of_clause" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="M_Predicate_of_clause" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="M_Negation_Marker" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="D_PoS_Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="D_PRP_type_Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="D_PRP_x0024__type_Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="D_Subject_of_clause" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="D_Predicate_of_clause" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="D_Negation_marker" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="D_Preposition_marker" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="D_Possessive_ending_marker" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Word_order_in_chunk_Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="M_type_of_article_Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="D_type_of_article_Id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Date_of_change" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "nameOfChunk",
    "example",
    "mPoSId",
    "mprpTypeId",
    "mprpx0024TypeId",
    "mSubjectOfClause",
    "mPredicateOfClause",
    "mNegationMarker",
    "dPoSId",
    "dprpTypeId",
    "dprpx0024TypeId",
    "dSubjectOfClause",
    "dPredicateOfClause",
    "dNegationMarker",
    "dPrepositionMarker",
    "dPossessiveEndingMarker",
    "wordOrderInChunkId",
    "mTypeOfArticleId",
    "dTypeOfArticleId",
    "dateOfChange"
})
@XmlRootElement(name = "Type_of_chunks")
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
public class TypeOfChunkEn implements HasWordOrder {

    @XmlElement(name = "Id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected int id;
    @XmlElement(name = "Name_of_chunk")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String nameOfChunk;
    @XmlElement(name = "Example")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String example;
    @XmlElement(name = "M_PoS_Id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer mPoSId;
    @XmlElement(name = "M_PRP_type_Id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer mprpTypeId;
    @XmlElement(name = "M_PRP_x0024__type_Id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer mprpx0024TypeId;
    @XmlElement(name = "M_Subject_of_clause")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected boolean mSubjectOfClause;
    @XmlElement(name = "M_Predicate_of_clause")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected boolean mPredicateOfClause;
    @XmlElement(name = "M_Negation_Marker")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected boolean mNegationMarker;
    @XmlElement(name = "D_PoS_Id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer dPoSId;
    @XmlElement(name = "D_PRP_type_Id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer dprpTypeId;
    @XmlElement(name = "D_PRP_x0024__type_Id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer dprpx0024TypeId;
    @XmlElement(name = "D_Subject_of_clause")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected boolean dSubjectOfClause;
    @XmlElement(name = "D_Predicate_of_clause")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected boolean dPredicateOfClause;
    @XmlElement(name = "D_Negation_marker")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected boolean dNegationMarker;
    @XmlElement(name = "D_Preposition_marker")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected boolean dPrepositionMarker;
    @XmlElement(name = "D_Possessive_ending_marker")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected boolean dPossessiveEndingMarker;
    @XmlElement(name = "Word_order_in_chunk_Id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer wordOrderInChunkId;
    @XmlElement(name = "M_type_of_article_Id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer mTypeOfArticleId;
    @XmlElement(name = "D_type_of_article_Id")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected Integer dTypeOfArticleId;
    @XmlElement(name = "Date_of_change")
    @XmlSchemaType(name = "dateTime")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected XMLGregorianCalendar dateOfChange;

    /**
     * Gets the value of the id property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public int getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the nameOfChunk property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getNameOfChunk() {
        return nameOfChunk;
    }

    /**
     * Sets the value of the nameOfChunk property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setNameOfChunk(String value) {
        this.nameOfChunk = value;
    }

    /**
     * Gets the value of the example property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getExample() {
        return example;
    }

    /**
     * Sets the value of the example property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setExample(String value) {
        this.example = value;
    }

    /**
     * Gets the value of the mPoSId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Integer getMPoSId() {
        return mPoSId;
    }

    /**
     * Sets the value of the mPoSId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMPoSId(Integer value) {
        this.mPoSId = value;
    }

    /**
     * Gets the value of the mprpTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Integer getMPRPTypeId() {
        return mprpTypeId;
    }

    /**
     * Sets the value of the mprpTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMPRPTypeId(Integer value) {
        this.mprpTypeId = value;
    }

    /**
     * Gets the value of the mprpx0024TypeId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Integer getMPRPX0024TypeId() {
        return mprpx0024TypeId;
    }

    /**
     * Sets the value of the mprpx0024TypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMPRPX0024TypeId(Integer value) {
        this.mprpx0024TypeId = value;
    }

    /**
     * Gets the value of the mSubjectOfClause property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public boolean isMSubjectOfClause() {
        return mSubjectOfClause;
    }

    /**
     * Sets the value of the mSubjectOfClause property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMSubjectOfClause(boolean value) {
        this.mSubjectOfClause = value;
    }

    /**
     * Gets the value of the mPredicateOfClause property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public boolean isMPredicateOfClause() {
        return mPredicateOfClause;
    }

    /**
     * Sets the value of the mPredicateOfClause property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMPredicateOfClause(boolean value) {
        this.mPredicateOfClause = value;
    }

    /**
     * Gets the value of the mNegationMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public boolean isMNegationMarker() {
        return mNegationMarker;
    }

    /**
     * Sets the value of the mNegationMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMNegationMarker(boolean value) {
        this.mNegationMarker = value;
    }

    /**
     * Gets the value of the dPoSId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Integer getDPoSId() {
        return dPoSId;
    }

    /**
     * Sets the value of the dPoSId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDPoSId(Integer value) {
        this.dPoSId = value;
    }

    /**
     * Gets the value of the dprpTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Integer getDPRPTypeId() {
        return dprpTypeId;
    }

    /**
     * Sets the value of the dprpTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDPRPTypeId(Integer value) {
        this.dprpTypeId = value;
    }

    /**
     * Gets the value of the dprpx0024TypeId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Integer getDPRPX0024TypeId() {
        return dprpx0024TypeId;
    }

    /**
     * Sets the value of the dprpx0024TypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDPRPX0024TypeId(Integer value) {
        this.dprpx0024TypeId = value;
    }

    /**
     * Gets the value of the dSubjectOfClause property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public boolean isDSubjectOfClause() {
        return dSubjectOfClause;
    }

    /**
     * Sets the value of the dSubjectOfClause property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDSubjectOfClause(boolean value) {
        this.dSubjectOfClause = value;
    }

    /**
     * Gets the value of the dPredicateOfClause property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public boolean isDPredicateOfClause() {
        return dPredicateOfClause;
    }

    /**
     * Sets the value of the dPredicateOfClause property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDPredicateOfClause(boolean value) {
        this.dPredicateOfClause = value;
    }

    /**
     * Gets the value of the dNegationMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public boolean isDNegationMarker() {
        return dNegationMarker;
    }

    /**
     * Sets the value of the dNegationMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDNegationMarker(boolean value) {
        this.dNegationMarker = value;
    }

    /**
     * Gets the value of the dPrepositionMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public boolean isDPrepositionMarker() {
        return dPrepositionMarker;
    }

    /**
     * Sets the value of the dPrepositionMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDPrepositionMarker(boolean value) {
        this.dPrepositionMarker = value;
    }

    /**
     * Gets the value of the dPossessiveEndingMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public boolean isDPossessiveEndingMarker() {
        return dPossessiveEndingMarker;
    }

    /**
     * Sets the value of the dPossessiveEndingMarker property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDPossessiveEndingMarker(boolean value) {
        this.dPossessiveEndingMarker = value;
    }

    /**
     * Gets the value of the wordOrderInChunkId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Integer getWordOrderInChunkId() {
        return wordOrderInChunkId;
    }

    @Override
    public Integer getWordOrder() {
        return wordOrderInChunkId;
    }

    /**
     * Sets the value of the wordOrderInChunkId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setWordOrderInChunkId(Integer value) {
        this.wordOrderInChunkId = value;
    }

    /**
     * Gets the value of the mTypeOfArticleId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Integer getMTypeOfArticleId() {
        return mTypeOfArticleId;
    }

    /**
     * Sets the value of the mTypeOfArticleId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMTypeOfArticleId(Integer value) {
        this.mTypeOfArticleId = value;
    }

    /**
     * Gets the value of the dTypeOfArticleId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public Integer getDTypeOfArticleId() {
        return dTypeOfArticleId;
    }

    /**
     * Sets the value of the dTypeOfArticleId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDTypeOfArticleId(Integer value) {
        this.dTypeOfArticleId = value;
    }

    /**
     * Gets the value of the dateOfChange property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public XMLGregorianCalendar getDateOfChange() {
        return dateOfChange;
    }

    /**
     * Sets the value of the dateOfChange property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2016-09-02T09:39:48+03:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDateOfChange(XMLGregorianCalendar value) {
        this.dateOfChange = value;
    }

}
