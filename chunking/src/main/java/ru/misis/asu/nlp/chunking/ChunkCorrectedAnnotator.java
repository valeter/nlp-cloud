package ru.misis.asu.nlp.chunking;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import ru.misis.asu.nlp.chunking.model.*;
import ru.misis.asu.nlp.chunking.types.ChunkCorrectedTree;
import ru.misis.asu.nlp.chunking.types.TextCorrected;
import ru.misis.asu.nlp.chunking.xml.ChunkTemplateConverter;
import ru.misis.asu.nlp.chunking.xml.ChunkTemplateConverterEn;
import ru.misis.asu.nlp.chunking.xml.ChunkTemplateConverterRu;
import ru.misis.asu.nlp.chunking.xml.en.Dataroot;
import ru.misis.asu.nlp.chunking.xml.en.TypeOfChunkEn;
import ru.misis.asu.nlp.chunking.xml.ru.TypeOfChunkRu;
import ru.misis.asu.nlp.commons.Pair;
import ru.misis.asu.nlp.commons.ngram.NgramDictionary;
import ru.misis.asu.nlp.segmentation.types.Sentence;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @author valter
 * @date 08.04.2016.
 */
public class ChunkCorrectedAnnotator extends JCasAnnotator_ImplBase {
    private static final String WORD_TYPE_PARAM = "ExtendedWordType";
    private static final String PM_SEGMENT_TYPE_PARAM = "SentenceType";
    private static final String CHUNK_TYPES_FILE_PARAM = "TypeOfChunkXml";
    private static final String NGRAM_DICT_NAME_PARAM = "NgramDictionaryName";
    private static final String LANGUAGE_PARAM = "LanguageName";

    private String wordTypeName;
    private String PMSegmentTypeName;
    private String chunkTypesFileName;
    private String languageName;

    private NgramDictionary nGramDictionary;

    private ChunkTemplateConverter chunkTemplateConverter;

    @Override
    public void initialize(UimaContext aContext)
        throws ResourceInitializationException {
        super.initialize(aContext);

        wordTypeName = (String) aContext
            .getConfigParameterValue(WORD_TYPE_PARAM);
        PMSegmentTypeName = (String) aContext
            .getConfigParameterValue(PM_SEGMENT_TYPE_PARAM);
        chunkTypesFileName = (String) aContext
            .getConfigParameterValue(CHUNK_TYPES_FILE_PARAM);
        languageName = (String) aContext
            .getConfigParameterValue(LANGUAGE_PARAM);
        nGramDictionary = NgramDictionary.Instance.valueOf((String) aContext
            .getConfigParameterValue(NGRAM_DICT_NAME_PARAM)).get();

        if ("en".equals(languageName)) {
            chunkTemplateConverter = new ChunkTemplateConverterEn();
        } else if ("ru".equals(languageName)) {
            chunkTemplateConverter = new ChunkTemplateConverterRu();
        } else {
            throw new RuntimeException("Unsupported language " + languageName);
        }
    }

    private volatile JCas aJCas;

    @Override
    public void process(JCas aJCas) throws AnalysisEngineProcessException {
        this.aJCas = aJCas;

        List<ChunkTemplate> chunkTemplates;
        try {
            chunkTemplates = loadChunkInfos(chunkTemplateConverter);
        } catch (JAXBException e) {
            throw new RuntimeException("Could not unmarshal file " + chunkTypesFileName, e);
        }

        String text = aJCas.getDocumentText();
        TypeSystem ts = aJCas.getTypeSystem();
        Type wordType = ts.getType(wordTypeName);
        Type segmentType = ts.getType(PMSegmentTypeName);
        AnnotationIndex<Annotation> wordsIndex = aJCas.getAnnotationIndex(wordType);
        AnnotationIndex<Annotation> segmentsIndex = aJCas.getAnnotationIndex(segmentType);

        List<ChunkCorrectedTree> result = new ArrayList<>();
        for (Annotation aSegmentsIndex : segmentsIndex) {
            Sentence sentence = (Sentence) aSegmentsIndex;
            Iterator<Annotation> wordIterator = wordsIndex.subiterator(sentence);

            List<Annotation> words = new ArrayList<>();
            while (wordIterator.hasNext()) {
                words.add(wordIterator.next());
            }

            List<ChunkInfo> chunkInfos = getChunks(words, chunkTemplates);
            List<ChunkInfo> headChunks = chunkInfos.stream()
                .filter(ChunkInfo::isHeadChunk)
                .collect(Collectors.toList());

            System.out.println("Sentence chunk stat [" + sentence.getCoveredText() + "]");
            System.out.println("All chunks: " + chunkInfos.size());
            System.out.println(chunkInfos.stream().map(String::valueOf).collect(Collectors.joining("\n\t", "\t", "")));
            System.out.println();
            System.out.println("Head chunks: " + headChunks.size());
            System.out.println(headChunks.stream().map(String::valueOf).collect(Collectors.joining("\n\t", "\t", "")));
            System.out.println();

            List<ChunkInfoTree> trees = new ArrayList<>();
            int minSize = -1;
            for (ChunkInfo headChunk : headChunks) {
                List<ChunkInfoTree> headTrees = buildTrees(headChunk, new ArrayList<>(chunkInfos),
                    words.size(), minSize, aJCas);
                headTrees = headTrees.stream()
                    .filter(t -> t.size() > 1)
                    .collect(Collectors.toList());
                if (headTrees.size() > 0) {
                    minSize = Math.max(minSize, headTrees.stream().map(ChunkInfoTree::size).max(Integer::compare).orElse(minSize));
                    trees.addAll(headTrees);
                }
            }

            System.out.println(trees.size() + " trees found");
            System.out.println();
            ChunkInfoTree resultTree = getBestChunkTree(sentence.getCoveredText(), trees, sentence.getBegin());

            System.out.println();
            System.out.println("All trees: ");
            for (ChunkInfoTree tree : trees) {
                ChunkUtil.printChunkTree(tree, tree.getHead(), "");
                System.out.println();
            }
            System.out.println();

            if (resultTree != null) {
                ChunkCorrectedTree tree = resultTree.getChunkCorrectedTree();
                tree.addToIndexes();
                System.out.println("Best tree: ");
                ChunkUtil.printChunkTree(resultTree, resultTree.getHead(), "");
                System.out.println();
                System.out.println("================================================================================================================\n");
                System.out.println();
                result.add(tree);
            } else {
                System.out.println("Chunk tree not found in sentence [" + sentence.getCoveredText() + "]");
                System.out.println();
                System.out.println("================================================================================================================\n");
                System.out.println();
            }

        }

        String resultText = ChunkUtil.printResult(text, result);
        TextCorrected textCorrected = new TextCorrected(aJCas);
        textCorrected.setCorrection(resultText);
        textCorrected.setBegin(0);
        textCorrected.setEnd(aJCas.getDocumentText().length());
        textCorrected.addToIndexes();
    }

    private ChunkInfoTree getBestChunkTree(String sentenceText, List<ChunkInfoTree> trees, int from) {
        List<ChunkInfoTree> biggestTrees = new ArrayList<>();
        int maxSize = 0;
        for (ChunkInfoTree tree : trees) {
            if (tree.size() > maxSize) {
                maxSize = tree.size();
            }
        }

        for (ChunkInfoTree tree : trees) {
            if (tree.size() == maxSize) {
                biggestTrees.add(tree);
            }
        }

        ChunkInfoTree result = null;
        double maxFrequency = -1;
        for (ChunkInfoTree biggestTree : biggestTrees) {
            double frequency = getFrequency(sentenceText, biggestTree, from);
            if (frequency > maxFrequency) {
                maxFrequency = frequency;
                result = biggestTree;
            }
        }
        return result;
    }

    private double getFrequency(String sentenceText, ChunkInfoTree tree, int from) {
        String sentence = ChunkUtil.getTreeString(sentenceText, Collections.singletonList(tree.getChunkCorrectedTree()), from);
        String[] words = sentence.split("[^А-Яа-яA-Za-z]+");
        int result = 0;
        for (int i = 0; i < words.length - 1; i++) {
            result += nGramDictionary.getFrequency(words[i].toLowerCase(), words[i + 1].toLowerCase());
        }
        return result;
    }

    private boolean isConnected(ChunkInfo a, ChunkInfo b) {
        return a.getDependentWordInfo().getWordform() == b.getMainWordInfo().getWordform();
    }

    private List<ChunkInfoTree> buildTrees(ChunkInfo currentChunk, List<ChunkInfo> allChunks,
                                           int maxLength, int minLength, JCas aJCas) {
        int head = -1;
        Graph g = new Graph();
        for (int i = 0; i < allChunks.size(); i++) {
            ChunkInfo info = allChunks.get(i);
            if (info == currentChunk) {
                head = i;
            }
            List<Integer> edges = new ArrayList<>();
            for (int j = 0; j < allChunks.size(); j++) {
                if (i == j) continue;
                ChunkInfo neighbour = allChunks.get(j);
                if (isConnected(info, neighbour)) {
                    edges.add(j);
                }
            }
            g.put(i, edges);
        }

        int topBorder = Math.min(maxLength, allChunks.size() < 100 ? maxLength : (
            allChunks.size() < 500 ? 6 : (
                allChunks.size() < 1000 ? 5 : 4
            )
        ));

        List<ChunkInfoTree> trees = new ArrayList<>();
        Map<Integer, Integer> groups = buildGroups(allChunks);
        for (int treeLength = topBorder; treeLength >= Math.max(0, minLength); treeLength--) {
            if (treeLength == 0) {
                continue;
            }
            List<Graph> currentTrees = findTrees(g, head, treeLength, groups);
            if (!currentTrees.isEmpty()) {
                for (Graph currentTree : currentTrees) {
                    List<Pair<ChunkInfo, ChunkInfo>> chunks = new ArrayList<>();
                    for (Map.Entry<Integer, List<Integer>> entry : currentTree.entrySet()) {
                        for (Integer right : entry.getValue()) {
                            chunks.add(new Pair<>(allChunks.get(entry.getKey()), allChunks.get(right)));
                        }
                    }
                    ChunkInfoTree tree = buildTree(chunks, currentChunk, aJCas);
                    trees.add(tree);
                }
                break;
            }
        }
        return trees;
    }

    private Map<Integer, Integer> buildGroups(List<ChunkInfo> chunks) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < chunks.size(); i++) {
            ChunkInfo chunk = chunks.get(i);
            map.put(i, chunk.getDependentWordInfo().getWordInd());
        }
        return map;
    }

    static class Graph extends HashMap<Integer, List<Integer>> {
        Graph() {
        }

        Graph(Set<Integer> nodes, List<List<Integer>> edges) {
            if (nodes.size() != edges.size() || nodes.size() == 0) {
                throw new IllegalArgumentException("Bad graph: nodes " + nodes.size() + " edges " + edges.size());
            }
            edges = edges.stream()
                .map(e -> e.stream().filter(nodes::contains).collect(Collectors.toList()))
                .collect(Collectors.toList());
            int i = 0;
            for (Integer node : nodes) {
                put(node, edges.get(i++));
            }
        }
    }

    private List<Graph> findTrees(Graph g, Integer head, int treeLength, Map<Integer, Integer> groups) {
        List<Graph> trees = new ArrayList<>();

        Map<Integer, AtomicInteger> usedGroups = new HashMap<>();
        for (Integer groupId : groups.values()) {
            usedGroups.put(groupId, new AtomicInteger(0));
        }
        Set<Integer> usedNodes = new LinkedHashSet<>();
        List<List<Integer>> edgeGroups = new ArrayList<>();

        AtomicBoolean finishedFirst = new AtomicBoolean(false);
        class FinishAllTrees {
            private List<Graph> make(int remainingLength, int edgeGroup, int edgePosition) {
                List<Graph> result = new ArrayList<>();

                outer: while (!finishedFirst.get() && edgeGroup < edgeGroups.size()) {
                    List<Integer> edges = edgeGroups.get(edgeGroup);

                    while (!finishedFirst.get() && edgePosition < edges.size()) {
                        Integer edge = edges.get(edgePosition);
                        edgePosition++;

                        if (usedNodes.contains(edge) || usedGroups.get(groups.get(edge)).get() > 0) {
                            continue;
                        }

                        usedNodes.add(edge);
                        edgeGroups.add(g.get(edge));
                        usedGroups.get(groups.get(edge)).incrementAndGet();
                        if (1 == remainingLength) {
                            result.add(new Graph(usedNodes, new ArrayList<>(edgeGroups)));
                            finishedFirst.set(true);
                            break outer;
                        } else {
                            result.addAll(new FinishAllTrees()
                                .make(remainingLength - 1, edgeGroup, edgePosition));
                        }
                        usedGroups.get(groups.get(edge)).decrementAndGet();
                        edgeGroups.remove(edgeGroups.size() - 1);
                        usedNodes.remove(edge);
                    }
                    edgePosition = 0;
                    edgeGroup++;
                }

                return result;
            }
        }

        usedNodes.add(head);
        edgeGroups.add(g.get(head));
        usedGroups.get(groups.get(head)).incrementAndGet();

        trees.addAll(new FinishAllTrees()
            .make(treeLength - 1, 0, 0));

        return trees;
    }

    private ChunkInfoTree buildTree(List<Pair<ChunkInfo, ChunkInfo>> usedEdges, ChunkInfo headChunk, JCas jCas) {
        ChunkInfoTree tree = new ChunkInfoTree(jCas);

        Map<Integer, ChunkInfo> cloneMap = new HashMap<>();
        List<Pair<ChunkInfo, ChunkInfo>> usedEdgesClone = new ArrayList<>();
        for (Pair<ChunkInfo, ChunkInfo> usedEdge : usedEdges) {
            cloneMap.putIfAbsent(usedEdge.first.getId(), clone(usedEdge.first, jCas));
            cloneMap.putIfAbsent(usedEdge.second.getId(), clone(usedEdge.second, jCas));
            usedEdgesClone.add(new Pair<>(
                cloneMap.get(usedEdge.first.getId()),
                cloneMap.get(usedEdge.second.getId())
            ));
        }
        tree.add(null, cloneMap.get(headChunk.getId()));
        while (true) {
            boolean added = false;
            for (Pair<ChunkInfo, ChunkInfo> p : usedEdgesClone) {
                added |= tree.add(p.first, p.second);
            }
            if (!added) {
                break;
            }
        }
        return tree;
    }

    private ChunkInfo clone(ChunkInfo info, JCas jCas) {
        return new ChunkInfo(info.getId(), info.getTemplate(), ChunkUtil.copyChunk(jCas, info.getChunkCorrected()),
            new ChunkInfoWord<>(info.getMainWordInfo().getWordInd(), info.getMainWordInfo().getCorrection(),
                info.getMainWordInfo().getExtendedCorrectedWord(), info.getMainWordInfo().getExtendedWord(),
                info.getMainWordInfo().getWordform(), info.getMainWordInfo().getComparator()),
            new ChunkInfoWord(info.getDependentWordInfo().getWordInd(), info.getDependentWordInfo().getCorrection(),
                info.getDependentWordInfo().getExtendedCorrectedWord(), info.getDependentWordInfo().getExtendedWord(),
                info.getDependentWordInfo().getWordform(), info.getDependentWordInfo().getComparator()),
            info.isHeadChunk());
    }

    private List<ChunkInfo> getChunks(List<Annotation> words, List<ChunkTemplate> chunkTemplates) {
        AtomicInteger startId = new AtomicInteger(0);
        List<ChunkInfo> result = new ArrayList<>();
        for (int i = 0; i < words.size(); i++) {
            for (int j = 0; j < words.size(); j++) {
                if (i == j) {
                    continue;
                }

                Annotation mainWord = words.get(i);
                Annotation dependentWord = words.get(j);

                List<ChunkInfo> chunkAlternatives = new ArrayList<>();
                for (ChunkTemplate chunkTemplate : chunkTemplates) {
                    chunkAlternatives.addAll(chunkTemplate.buildChunks(startId, mainWord, i, dependentWord, j,
                        i < j ? ChunkWordOrder.MAIN_WORD_FIRST : ChunkWordOrder.DEPENDENT_WORD_FIRST,
                        aJCas));
                }

                if (chunkAlternatives.size() != 0) {
                    result.addAll(chunkAlternatives);
                }
            }
        }
        return result;
    }

    private List<ChunkTemplate> loadChunkInfos(ChunkTemplateConverter converter) throws JAXBException {
        InputStream in = ChunkCorrectedAnnotator.class.getClassLoader().getResourceAsStream(chunkTypesFileName);
        JAXBContext context = JAXBContext.newInstance("ru.misis.asu.nlp.chunking.xml." + languageName);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        if ("en".equals(languageName)) {
            ru.misis.asu.nlp.chunking.xml.en.Dataroot result = (ru.misis.asu.nlp.chunking.xml.en.Dataroot) unmarshaller.unmarshal(in);
            List<TypeOfChunkEn> typeOfChunks = result.getTypeOfChunks();
            return converter.convert(aJCas, typeOfChunks);
        } else if ("ru".equals(languageName)) {
            ru.misis.asu.nlp.chunking.xml.ru.Dataroot result = (ru.misis.asu.nlp.chunking.xml.ru.Dataroot)unmarshaller.unmarshal(in);
            List<TypeOfChunkRu> typeOfChunks = result.getTypeOfChunk();
            return converter.convert(aJCas, typeOfChunks);
        } else {
            throw new RuntimeException("Unsupported language " + languageName);
        }
    }
}
