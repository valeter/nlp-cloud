@ECHO OFF

IF "%1"=="" GOTO RUNJ
IF %1=="help" GOTO HELP ELSE GOTO RUNJ

:HELP
java -classpath aggregate-0.4-jar-with-dependencies.jar -DInputLanguage=en ru.misis.asu.nlp.aggregate.utils.Launcher %*
GOTO DONE

:RUNJ
java -classpath aggregate-0.4-jar-with-dependencies.jar -DInputLanguage=en %* ru.misis.asu.nlp.aggregate.utils.Launcher

:DONE