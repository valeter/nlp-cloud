#!/bin/bash

if [ "$1" = "help" ]
then
    java -classpath aggregate-0.4-jar-with-dependencies.jar -DInputLanguage=ru ru.misis.asu.nlp.aggregate.utils.Launcher $1
else
    java -classpath aggregate-0.4-jar-with-dependencies.jar -DInputLanguage=ru $@ ru.misis.asu.nlp.aggregate.utils.Launcher mode=INTERACTIVE
fi