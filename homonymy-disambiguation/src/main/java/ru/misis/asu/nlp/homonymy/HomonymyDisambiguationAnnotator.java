package ru.misis.asu.nlp.homonymy;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import ru.misis.asu.nlp.commons.util.WordformUtil;
import ru.misis.asu.nlp.morphoanalysis.types.Wordform;
import ru.misis.asu.nlp.morphoanalysis.types.CorrectedWord;
import ru.misis.asu.nlp.morphoanalysis.types.Word;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * @author valter
 *         06.04.17
 */
public class HomonymyDisambiguationAnnotator extends JCasAnnotator_ImplBase {
    private static final String CORRECTED_WORD_TYPE_PARAM = "CorrectedWordType";

    private String correctedWordTypeName;

    private static final Consumer<Word> REMOVE_NOUN_DISAMBIGUATOR = word -> {
        FSArray array = word.getWordforms();
        int newSize = array.size();
        for (int i = 0; i < array.size(); i++) {
            Wordform wordform = (Wordform) array.get(i);
            if ("NOUN".equals(wordform.getPos().toUpperCase())) {
                if (newSize == 1) {
                    break;
                }
                newSize--;
                array.set(i, null);
            }
        }
        try {
            FSArray newArray = new FSArray(word.getCAS().getJCas(), newSize);
            int j = 0;
            for (int i = 0; i < array.size(); i++) {
                if (array.get(i) == null) {
                    continue;
                }
                newArray.set(j++, array.get(i));
            }
            word.setWordforms(newArray);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    };

    private static final Set<String> NOUNS_OF_2_LETTERS = new HashSet<>(Arrays.asList("ад", "ас", "еж", "ус", "ум", "щи", "юг", "ют", "яд", "як", "яр"));

    private static final Map<String, Consumer<Word>> WORD_DISAMBIGUATORS = new HashMap<String, Consumer<Word>>() {{
        put("а", REMOVE_NOUN_DISAMBIGUATOR);
        put("в", REMOVE_NOUN_DISAMBIGUATOR);
        put("и", REMOVE_NOUN_DISAMBIGUATOR);
        put("к", REMOVE_NOUN_DISAMBIGUATOR);
        put("о", REMOVE_NOUN_DISAMBIGUATOR);
        put("с", REMOVE_NOUN_DISAMBIGUATOR);
        put("у", REMOVE_NOUN_DISAMBIGUATOR);
        put("я", REMOVE_NOUN_DISAMBIGUATOR);
        put("а", REMOVE_NOUN_DISAMBIGUATOR);
        put("абы", REMOVE_NOUN_DISAMBIGUATOR);
        put("авось", REMOVE_NOUN_DISAMBIGUATOR);
        put("аж", REMOVE_NOUN_DISAMBIGUATOR);
        put("аки", REMOVE_NOUN_DISAMBIGUATOR);
        put("али", REMOVE_NOUN_DISAMBIGUATOR);
        put("аль", REMOVE_NOUN_DISAMBIGUATOR);
        put("а-ля", REMOVE_NOUN_DISAMBIGUATOR);
        put("ан", REMOVE_NOUN_DISAMBIGUATOR);
        put("аще", REMOVE_NOUN_DISAMBIGUATOR);
        put("без", REMOVE_NOUN_DISAMBIGUATOR);
        put("безусловно", REMOVE_NOUN_DISAMBIGUATOR);
        put("бесспорно", REMOVE_NOUN_DISAMBIGUATOR);
        put("благо", REMOVE_NOUN_DISAMBIGUATOR);
        put("благодаря", REMOVE_NOUN_DISAMBIGUATOR);
        put("близ", REMOVE_NOUN_DISAMBIGUATOR);
        put("бо", REMOVE_NOUN_DISAMBIGUATOR);
        put("буде", REMOVE_NOUN_DISAMBIGUATOR);
        put("будто", REMOVE_NOUN_DISAMBIGUATOR);
        put("бывает", REMOVE_NOUN_DISAMBIGUATOR);
        put("бывало", REMOVE_NOUN_DISAMBIGUATOR);
        put("бывалоча", REMOVE_NOUN_DISAMBIGUATOR);
        put("в", REMOVE_NOUN_DISAMBIGUATOR);
        put("вблизи", REMOVE_NOUN_DISAMBIGUATOR);
        put("вверху", REMOVE_NOUN_DISAMBIGUATOR);
        put("ввиду", REMOVE_NOUN_DISAMBIGUATOR);
        put("в-восьмых", REMOVE_NOUN_DISAMBIGUATOR);
        put("вглубь", REMOVE_NOUN_DISAMBIGUATOR);
        put("в-девятых", REMOVE_NOUN_DISAMBIGUATOR);
        put("в-десятых", REMOVE_NOUN_DISAMBIGUATOR);
        put("вдоль", REMOVE_NOUN_DISAMBIGUATOR);
        put("ведь", REMOVE_NOUN_DISAMBIGUATOR);
        put("вернее", REMOVE_NOUN_DISAMBIGUATOR);
        put("верно", REMOVE_NOUN_DISAMBIGUATOR);
        put("вероятно", REMOVE_NOUN_DISAMBIGUATOR);
        put("вестимо", REMOVE_NOUN_DISAMBIGUATOR);
        put("взамен", REMOVE_NOUN_DISAMBIGUATOR);
        put("видать", REMOVE_NOUN_DISAMBIGUATOR);
        put("видимо", REMOVE_NOUN_DISAMBIGUATOR);
        put("видно", REMOVE_NOUN_DISAMBIGUATOR);
        put("включая", REMOVE_NOUN_DISAMBIGUATOR);
        put("вкруг", REMOVE_NOUN_DISAMBIGUATOR);
        put("вместо", REMOVE_NOUN_DISAMBIGUATOR);
        put("вне", REMOVE_NOUN_DISAMBIGUATOR);
        put("внизу", REMOVE_NOUN_DISAMBIGUATOR);
        put("внутри", REMOVE_NOUN_DISAMBIGUATOR);
        put("внутрь", REMOVE_NOUN_DISAMBIGUATOR);
        put("вовнутрь", REMOVE_NOUN_DISAMBIGUATOR);
        put("во-вторых", REMOVE_NOUN_DISAMBIGUATOR);
        put("возле", REMOVE_NOUN_DISAMBIGUATOR);
        put("возможно", REMOVE_NOUN_DISAMBIGUATOR);
        put("вокруг", REMOVE_NOUN_DISAMBIGUATOR);
        put("вообще", REMOVE_NOUN_DISAMBIGUATOR);
        put("вообще-то", REMOVE_NOUN_DISAMBIGUATOR);
        put("во-первых", REMOVE_NOUN_DISAMBIGUATOR);
        put("вопреки", REMOVE_NOUN_DISAMBIGUATOR);
        put("вослед", REMOVE_NOUN_DISAMBIGUATOR);
        put("вперёд", REMOVE_NOUN_DISAMBIGUATOR);
        put("впереди", REMOVE_NOUN_DISAMBIGUATOR);
        put("впрочем", REMOVE_NOUN_DISAMBIGUATOR);
        put("в-пятых", REMOVE_NOUN_DISAMBIGUATOR);
        put("вроде", REMOVE_NOUN_DISAMBIGUATOR);
        put("в-седьмых", REMOVE_NOUN_DISAMBIGUATOR);
        put("всеконечно", REMOVE_NOUN_DISAMBIGUATOR);
        put("вслед", REMOVE_NOUN_DISAMBIGUATOR);
        put("вследствие", REMOVE_NOUN_DISAMBIGUATOR);
        put("в-третьих", REMOVE_NOUN_DISAMBIGUATOR);
        put("в-четвёртых", REMOVE_NOUN_DISAMBIGUATOR);
        put("в-шестых", REMOVE_NOUN_DISAMBIGUATOR);
        put("выключая", REMOVE_NOUN_DISAMBIGUATOR);
        put("выше", REMOVE_NOUN_DISAMBIGUATOR);
        put("где", REMOVE_NOUN_DISAMBIGUATOR);
        put("главное", REMOVE_NOUN_DISAMBIGUATOR);
        put("говорят", REMOVE_NOUN_DISAMBIGUATOR);
        put("да", REMOVE_NOUN_DISAMBIGUATOR);
        put("дабы", REMOVE_NOUN_DISAMBIGUATOR);
        put("дак", REMOVE_NOUN_DISAMBIGUATOR);
        put("действительно", REMOVE_NOUN_DISAMBIGUATOR);
        put("дескать", REMOVE_NOUN_DISAMBIGUATOR);
        put("для", REMOVE_NOUN_DISAMBIGUATOR);
        put("до", REMOVE_NOUN_DISAMBIGUATOR);
        put("добро", REMOVE_NOUN_DISAMBIGUATOR);
        put("доколе", REMOVE_NOUN_DISAMBIGUATOR);
        put("допустим", REMOVE_NOUN_DISAMBIGUATOR);
        put("едва", REMOVE_NOUN_DISAMBIGUATOR);
        put("ежели", REMOVE_NOUN_DISAMBIGUATOR);
        put("если", REMOVE_NOUN_DISAMBIGUATOR);
        put("естественно", REMOVE_NOUN_DISAMBIGUATOR);
        put("ж", REMOVE_NOUN_DISAMBIGUATOR);
        put("же", REMOVE_NOUN_DISAMBIGUATOR);
        put("за", REMOVE_NOUN_DISAMBIGUATOR);
        put("заместо", REMOVE_NOUN_DISAMBIGUATOR);
        put("зане", REMOVE_NOUN_DISAMBIGUATOR);
        put("зато", REMOVE_NOUN_DISAMBIGUATOR);
        put("знаете", REMOVE_NOUN_DISAMBIGUATOR);
        put("знаешь", REMOVE_NOUN_DISAMBIGUATOR);
        put("знамо", REMOVE_NOUN_DISAMBIGUATOR);
        put("знать", REMOVE_NOUN_DISAMBIGUATOR);
        put("значит", REMOVE_NOUN_DISAMBIGUATOR);
        put("и", REMOVE_NOUN_DISAMBIGUATOR);
        put("ибо", REMOVE_NOUN_DISAMBIGUATOR);
        put("из", REMOVE_NOUN_DISAMBIGUATOR);
        put("известно", REMOVE_NOUN_DISAMBIGUATOR);
        put("из-за", REMOVE_NOUN_DISAMBIGUATOR);
        put("изнутри", REMOVE_NOUN_DISAMBIGUATOR);
        put("из-под", REMOVE_NOUN_DISAMBIGUATOR);
        put("или", REMOVE_NOUN_DISAMBIGUATOR);
        put("иль", REMOVE_NOUN_DISAMBIGUATOR);
        put("имхо", REMOVE_NOUN_DISAMBIGUATOR);
        put("иначе", REMOVE_NOUN_DISAMBIGUATOR);
        put("исключая", REMOVE_NOUN_DISAMBIGUATOR);
        put("итак", REMOVE_NOUN_DISAMBIGUATOR);
        put("к", REMOVE_NOUN_DISAMBIGUATOR);
        put("кабы", REMOVE_NOUN_DISAMBIGUATOR);
        put("кажется", REMOVE_NOUN_DISAMBIGUATOR);
        put("казалось", REMOVE_NOUN_DISAMBIGUATOR);
        put("как", REMOVE_NOUN_DISAMBIGUATOR);
        put("касаемо", REMOVE_NOUN_DISAMBIGUATOR);
        put("касательно", REMOVE_NOUN_DISAMBIGUATOR);
        put("кверх", REMOVE_NOUN_DISAMBIGUATOR);
        put("когда", REMOVE_NOUN_DISAMBIGUATOR);
        put("коли", REMOVE_NOUN_DISAMBIGUATOR);
        put("коль", REMOVE_NOUN_DISAMBIGUATOR);
        put("конечно", REMOVE_NOUN_DISAMBIGUATOR);
        put("короче", REMOVE_NOUN_DISAMBIGUATOR);
        put("кроме", REMOVE_NOUN_DISAMBIGUATOR);
        put("кругом", REMOVE_NOUN_DISAMBIGUATOR);
        put("кстати", REMOVE_NOUN_DISAMBIGUATOR);
        put("ли", REMOVE_NOUN_DISAMBIGUATOR);
        put("либо", REMOVE_NOUN_DISAMBIGUATOR);
        put("лишь", REMOVE_NOUN_DISAMBIGUATOR);
        put("ль", REMOVE_NOUN_DISAMBIGUATOR);
        put("меж", REMOVE_NOUN_DISAMBIGUATOR);
        put("между", REMOVE_NOUN_DISAMBIGUATOR);
        put("мимо", REMOVE_NOUN_DISAMBIGUATOR);
        put("минус", REMOVE_NOUN_DISAMBIGUATOR);
        put("минус", REMOVE_NOUN_DISAMBIGUATOR);
        put("может", REMOVE_NOUN_DISAMBIGUATOR);
        put("на", REMOVE_NOUN_DISAMBIGUATOR);
        put("наверно", REMOVE_NOUN_DISAMBIGUATOR);
        put("наверное", REMOVE_NOUN_DISAMBIGUATOR);
        put("наверняка", REMOVE_NOUN_DISAMBIGUATOR);
        put("навроде", REMOVE_NOUN_DISAMBIGUATOR);
        put("над", REMOVE_NOUN_DISAMBIGUATOR);
        put("накануне", REMOVE_NOUN_DISAMBIGUATOR);
        put("наконец", REMOVE_NOUN_DISAMBIGUATOR);
        put("наконец-то", REMOVE_NOUN_DISAMBIGUATOR);
        put("наместо", REMOVE_NOUN_DISAMBIGUATOR);
        put("наоборот", REMOVE_NOUN_DISAMBIGUATOR);
        put("наперекор", REMOVE_NOUN_DISAMBIGUATOR);
        put("наподобие", REMOVE_NOUN_DISAMBIGUATOR);
        put("например", REMOVE_NOUN_DISAMBIGUATOR);
        put("напротив", REMOVE_NOUN_DISAMBIGUATOR);
        put("напротив", REMOVE_NOUN_DISAMBIGUATOR);
        put("насупротив", REMOVE_NOUN_DISAMBIGUATOR);
        put("насчёт", REMOVE_NOUN_DISAMBIGUATOR);
        put("натурально", REMOVE_NOUN_DISAMBIGUATOR);
        put("небось", REMOVE_NOUN_DISAMBIGUATOR);
        put("невзирая", REMOVE_NOUN_DISAMBIGUATOR);
        put("нежели", REMOVE_NOUN_DISAMBIGUATOR);
        put("несмотря", REMOVE_NOUN_DISAMBIGUATOR);
        put("несомненно", REMOVE_NOUN_DISAMBIGUATOR);
        put("ни", REMOVE_NOUN_DISAMBIGUATOR);
        put("ниже", REMOVE_NOUN_DISAMBIGUATOR);
        put("ниже", REMOVE_NOUN_DISAMBIGUATOR);
        put("но", REMOVE_NOUN_DISAMBIGUATOR);
        put("о", REMOVE_NOUN_DISAMBIGUATOR);
        put("обок", REMOVE_NOUN_DISAMBIGUATOR);
        put("однако", REMOVE_NOUN_DISAMBIGUATOR);
        put("оказывается", REMOVE_NOUN_DISAMBIGUATOR);
        put("около", REMOVE_NOUN_DISAMBIGUATOR);
        put("окрест", REMOVE_NOUN_DISAMBIGUATOR);
        put("окромя", REMOVE_NOUN_DISAMBIGUATOR);
        put("опричь", REMOVE_NOUN_DISAMBIGUATOR);
        put("от", REMOVE_NOUN_DISAMBIGUATOR);
        put("относительно", REMOVE_NOUN_DISAMBIGUATOR);
        put("отчего", REMOVE_NOUN_DISAMBIGUATOR);
        put("очевидно", REMOVE_NOUN_DISAMBIGUATOR);
        put("перед", REMOVE_NOUN_DISAMBIGUATOR);
        put("плюс", REMOVE_NOUN_DISAMBIGUATOR);
        put("плюс", REMOVE_NOUN_DISAMBIGUATOR);
        put("по", REMOVE_NOUN_DISAMBIGUATOR);
        put("по-вашему", REMOVE_NOUN_DISAMBIGUATOR);
        put("поверх", REMOVE_NOUN_DISAMBIGUATOR);
        put("поверь", REMOVE_NOUN_DISAMBIGUATOR);
        put("поверьте", REMOVE_NOUN_DISAMBIGUATOR);
        put("по-видимому", REMOVE_NOUN_DISAMBIGUATOR);
        put("под", REMOVE_NOUN_DISAMBIGUATOR);
        put("поди", REMOVE_NOUN_DISAMBIGUATOR);
        put("подле", REMOVE_NOUN_DISAMBIGUATOR);
        put("подобно", REMOVE_NOUN_DISAMBIGUATOR);
        put("подстать", REMOVE_NOUN_DISAMBIGUATOR);
        put("поелику", REMOVE_NOUN_DISAMBIGUATOR);
        put("пожалуй", REMOVE_NOUN_DISAMBIGUATOR);
        put("по-за", REMOVE_NOUN_DISAMBIGUATOR);
        put("позади", REMOVE_NOUN_DISAMBIGUATOR);
        put("позадь", REMOVE_NOUN_DISAMBIGUATOR);
        put("пока", REMOVE_NOUN_DISAMBIGUATOR);
        put("покамест", REMOVE_NOUN_DISAMBIGUATOR);
        put("поколе", REMOVE_NOUN_DISAMBIGUATOR);
        put("поколь", REMOVE_NOUN_DISAMBIGUATOR);
        put("покуда", REMOVE_NOUN_DISAMBIGUATOR);
        put("положим", REMOVE_NOUN_DISAMBIGUATOR);
        put("помимо", REMOVE_NOUN_DISAMBIGUATOR);
        put("помнится", REMOVE_NOUN_DISAMBIGUATOR);
        put("по-моему", REMOVE_NOUN_DISAMBIGUATOR);
        put("по-над", REMOVE_NOUN_DISAMBIGUATOR);
        put("понеже", REMOVE_NOUN_DISAMBIGUATOR);
        put("понимаете", REMOVE_NOUN_DISAMBIGUATOR);
        put("понимаешь", REMOVE_NOUN_DISAMBIGUATOR);
        put("понятно", REMOVE_NOUN_DISAMBIGUATOR);
        put("поперёк", REMOVE_NOUN_DISAMBIGUATOR);
        put("посереди", REMOVE_NOUN_DISAMBIGUATOR);
        put("посередине", REMOVE_NOUN_DISAMBIGUATOR);
        put("посередь", REMOVE_NOUN_DISAMBIGUATOR);
        put("поскольку", REMOVE_NOUN_DISAMBIGUATOR);
        put("после", REMOVE_NOUN_DISAMBIGUATOR);
        put("посреди", REMOVE_NOUN_DISAMBIGUATOR);
        put("посредине", REMOVE_NOUN_DISAMBIGUATOR);
        put("посредством", REMOVE_NOUN_DISAMBIGUATOR);
        put("постольку", REMOVE_NOUN_DISAMBIGUATOR);
        put("по-твоему", REMOVE_NOUN_DISAMBIGUATOR);
        put("похоже", REMOVE_NOUN_DISAMBIGUATOR);
        put("почитай", REMOVE_NOUN_DISAMBIGUATOR);
        put("правда", REMOVE_NOUN_DISAMBIGUATOR);
        put("право", REMOVE_NOUN_DISAMBIGUATOR);
        put("пред", REMOVE_NOUN_DISAMBIGUATOR);
        put("предположим", REMOVE_NOUN_DISAMBIGUATOR);
        put("предположительно", REMOVE_NOUN_DISAMBIGUATOR);
        put("прежде", REMOVE_NOUN_DISAMBIGUATOR);
        put("при", REMOVE_NOUN_DISAMBIGUATOR);
        put("признаться", REMOVE_NOUN_DISAMBIGUATOR);
        put("притом", REMOVE_NOUN_DISAMBIGUATOR);
        put("причём", REMOVE_NOUN_DISAMBIGUATOR);
        put("про", REMOVE_NOUN_DISAMBIGUATOR);
        put("промеж", REMOVE_NOUN_DISAMBIGUATOR);
        put("промежду", REMOVE_NOUN_DISAMBIGUATOR);
        put("против", REMOVE_NOUN_DISAMBIGUATOR);
        put("противно", REMOVE_NOUN_DISAMBIGUATOR);
        put("противу", REMOVE_NOUN_DISAMBIGUATOR);
        put("пускай", REMOVE_NOUN_DISAMBIGUATOR);
        put("пусть", REMOVE_NOUN_DISAMBIGUATOR);
        put("путём", REMOVE_NOUN_DISAMBIGUATOR);
        put("равно", REMOVE_NOUN_DISAMBIGUATOR);
        put("ради", REMOVE_NOUN_DISAMBIGUATOR);
        put("раз", REMOVE_NOUN_DISAMBIGUATOR);
        put("разве", REMOVE_NOUN_DISAMBIGUATOR);
        put("разумеется", REMOVE_NOUN_DISAMBIGUATOR);
        put("ровно", REMOVE_NOUN_DISAMBIGUATOR);
        put("с", REMOVE_NOUN_DISAMBIGUATOR);
        put("сверх", REMOVE_NOUN_DISAMBIGUATOR);
        put("сверху", REMOVE_NOUN_DISAMBIGUATOR);
        put("свыше", REMOVE_NOUN_DISAMBIGUATOR);
        put("середь", REMOVE_NOUN_DISAMBIGUATOR);
        put("сзади", REMOVE_NOUN_DISAMBIGUATOR);
        put("сиречь", REMOVE_NOUN_DISAMBIGUATOR);
        put("скажем", REMOVE_NOUN_DISAMBIGUATOR);
        put("сквозь", REMOVE_NOUN_DISAMBIGUATOR);
        put("сколько", REMOVE_NOUN_DISAMBIGUATOR);
        put("скорее", REMOVE_NOUN_DISAMBIGUATOR);
        put("следовательно", REMOVE_NOUN_DISAMBIGUATOR);
        put("следственно", REMOVE_NOUN_DISAMBIGUATOR);
        put("словно", REMOVE_NOUN_DISAMBIGUATOR);
        put("словом", REMOVE_NOUN_DISAMBIGUATOR);
        put("случаем", REMOVE_NOUN_DISAMBIGUATOR);
        put("случается", REMOVE_NOUN_DISAMBIGUATOR);
        put("случайно", REMOVE_NOUN_DISAMBIGUATOR);
        put("слыхать", REMOVE_NOUN_DISAMBIGUATOR);
        put("слышно", REMOVE_NOUN_DISAMBIGUATOR);
        put("снаружи", REMOVE_NOUN_DISAMBIGUATOR);
        put("снизу", REMOVE_NOUN_DISAMBIGUATOR);
        put("собственно", REMOVE_NOUN_DISAMBIGUATOR);
        put("согласно", REMOVE_NOUN_DISAMBIGUATOR);
        put("сообразно", REMOVE_NOUN_DISAMBIGUATOR);
        put("соответственно", REMOVE_NOUN_DISAMBIGUATOR);
        put("соответственно", REMOVE_NOUN_DISAMBIGUATOR);
        put("соразмерно", REMOVE_NOUN_DISAMBIGUATOR);
        put("спереди", REMOVE_NOUN_DISAMBIGUATOR);
        put("спустя", REMOVE_NOUN_DISAMBIGUATOR);
        put("среди", REMOVE_NOUN_DISAMBIGUATOR);
        put("средь", REMOVE_NOUN_DISAMBIGUATOR);
        put("сродни", REMOVE_NOUN_DISAMBIGUATOR);
        put("столько", REMOVE_NOUN_DISAMBIGUATOR);
        put("супротив", REMOVE_NOUN_DISAMBIGUATOR);
        put("так", REMOVE_NOUN_DISAMBIGUATOR);
        put("тем", REMOVE_NOUN_DISAMBIGUATOR);
        put("типа", REMOVE_NOUN_DISAMBIGUATOR);
        put("типа", REMOVE_NOUN_DISAMBIGUATOR);
        put("то", REMOVE_NOUN_DISAMBIGUATOR);
        put("то-есть", REMOVE_NOUN_DISAMBIGUATOR);
        put("только", REMOVE_NOUN_DISAMBIGUATOR);
        put("только-только", REMOVE_NOUN_DISAMBIGUATOR);
        put("точнее", REMOVE_NOUN_DISAMBIGUATOR);
        put("точно", REMOVE_NOUN_DISAMBIGUATOR);
        put("у", REMOVE_NOUN_DISAMBIGUATOR);
        put("фактически", REMOVE_NOUN_DISAMBIGUATOR);
        put("хоть", REMOVE_NOUN_DISAMBIGUATOR);
        put("хотя", REMOVE_NOUN_DISAMBIGUATOR);
        put("часом", REMOVE_NOUN_DISAMBIGUATOR);
        put("чем", REMOVE_NOUN_DISAMBIGUATOR);
        put("через", REMOVE_NOUN_DISAMBIGUATOR);
        put("чрез", REMOVE_NOUN_DISAMBIGUATOR);
        put("что", REMOVE_NOUN_DISAMBIGUATOR);
        put("чтоб", REMOVE_NOUN_DISAMBIGUATOR);
        put("чтобы", REMOVE_NOUN_DISAMBIGUATOR);
        put("чуть", REMOVE_NOUN_DISAMBIGUATOR);
        put("яко", REMOVE_NOUN_DISAMBIGUATOR);
        put("якобы", REMOVE_NOUN_DISAMBIGUATOR);
    }};


    @Override
    public void initialize(UimaContext ctx)
            throws ResourceInitializationException {
        super.initialize(ctx);
        correctedWordTypeName = (String) ctx.getConfigParameterValue(CORRECTED_WORD_TYPE_PARAM);
    }

    @Override
    public void process(JCas cas) throws AnalysisEngineProcessException {
        System.out.println("Starting homonymy disambiguation");
        Type correctedWordType = cas.getTypeSystem().getType(correctedWordTypeName);

        AnnotationIndex<Annotation> wordsIndex = cas.getAnnotationIndex(correctedWordType);

        System.out.println();
        System.out.println();
        for (Annotation annotation : wordsIndex) {
            CorrectedWord correctedWord = (CorrectedWord) annotation;
            for (int i = 0; i < correctedWord.getCorrections().size(); i++) {
                String correction = correctedWord.getCorrections(i);
                if (WORD_DISAMBIGUATORS.containsKey(correction.toLowerCase()) || correction.toLowerCase().length() <= 2) {
                    if (NOUNS_OF_2_LETTERS.contains(correction.toLowerCase())) {
                        continue;
                    }
                    System.out.println("Starting homonymy disambiguation for correction " + correction);
                    System.out.println(Arrays.stream(correctedWord.getWords(i).getWordforms().toStringArray()).map(WordformUtil::fixWordformString).collect(Collectors.toList()) + " wordforms before disambiguation");
                    WORD_DISAMBIGUATORS.getOrDefault(correction.toLowerCase(), REMOVE_NOUN_DISAMBIGUATOR).accept(correctedWord.getWords(i));
                    System.out.println(Arrays.stream(correctedWord.getWords(i).getWordforms().toStringArray()).map(WordformUtil::fixWordformString).collect(Collectors.toList()) + " wordforms after disambiguation");
                }
            }
        }
        System.out.println();
        System.out.println();
    }
}
