package ru.misis.asu.nlp.extended.ru;

import ru.misis.asu.nlp.morphoanalysis.types.Word;

import java.util.Objects;

public class POSdet {
    public POSdet() {
    }

    boolean isEqual(Word w, String POS) {
        for (int i = 0; i < w.getWordforms().size(); i++) {
            if (Objects.equals(w.getWordforms(i).getPos(), POS)) return true;
        }

        return false;
    }
}
