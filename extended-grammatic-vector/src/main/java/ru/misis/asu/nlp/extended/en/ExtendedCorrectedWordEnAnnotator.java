package ru.misis.asu.nlp.extended.en;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import ru.misis.asu.nlp.extended.CorrectedAnalyzer;

/**
 * @author valter
 *         14.07.16
 */
public class ExtendedCorrectedWordEnAnnotator extends JCasAnnotator_ImplBase {
    private static final String WORD_TYPE_PARAM = "CorrectedWordType";
        private static final String PM_SEGMENT_TYPE_PARAM = "SentenceType";

    private static String wordTypeName;
    private static String PMSegmentTypeName;

    @Override
    public void initialize(UimaContext aContext)
            throws ResourceInitializationException {
        super.initialize(aContext);

        wordTypeName = (String) aContext
                .getConfigParameterValue(WORD_TYPE_PARAM);
        PMSegmentTypeName = (String) aContext
                .getConfigParameterValue(PM_SEGMENT_TYPE_PARAM);
    }

    @Override
    public void process(JCas jCas) throws AnalysisEngineProcessException {
        TypeSystem ts = jCas.getTypeSystem();
        Type wordType = ts.getType(wordTypeName);
        Type segmentType = ts.getType(PMSegmentTypeName);
        AnnotationIndex<Annotation> wordsIndex = jCas.getAnnotationIndex(wordType);
        AnnotationIndex<Annotation> segmentsIndex = jCas.getAnnotationIndex(segmentType);

        CorrectedAnalyzer an = new CorrectedAnalyzerEn();
        for (Annotation segment : segmentsIndex) {
            FSIterator<Annotation> sentence = wordsIndex.subiterator(segment);
            an.process(jCas, sentence);
        }
    }
}
