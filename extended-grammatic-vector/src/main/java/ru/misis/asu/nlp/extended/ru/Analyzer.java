package ru.misis.asu.nlp.extended.ru;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import ru.misis.asu.nlp.extended.types.ru.*;
import ru.misis.asu.nlp.morphoanalysis.types.Word;

import java.io.IOException;
import java.util.*;


public class Analyzer {
    HashMap<String, ExtendedPreposition> map;
    LinkedList<ExtendedWordRu> lst;
    JCas aJCas;
    POSdet pd = new POSdet();

    public Analyzer(HashMap<String, ExtendedPreposition> map, JCas aJCas) {
        this.map = map;
        this.aJCas = aJCas;
    }

    //My Handlers
    Set<String> extraWords = new HashSet<>(Arrays.asList("хотя", "хоть", "лишь"));
    int lastVerbindex;
    int not_found = -100;
    ExtendedWordRu exW;
    ServiceRu gottenPREP, gottenNPRCL, gottenIPRCL;

    private int prepositionHandler(Word w) {
        if (gottenPREP != null) {
            if (pd.isEqual(w, "ADJF") || pd.isEqual(w, "PRTF") || pd.isEqual(w, "NUMR")) {
                exW.setPreposition((PrepositionRu) gottenPREP);
            }
            if (pd.isEqual(w, "NOUN") || pd.isEqual(w, "NPRO")) {
                exW.setPreposition((PrepositionRu) gottenPREP);

                gottenPREP = null;
            }
        } else if (pd.isEqual(w, "PREP")) {
            if (map.containsKey(w.getWordforms(0).getLemma())) {
                gottenPREP = (map.get(w.getWordforms(0).getLemma())).getPreposition(aJCas, w);
                return 1;
            }
        }
        return 0;
    }

    private int negativeParticleHandler(Word w) {
        if (gottenNPRCL != null) {
            if (pd.isEqual(w, "NOUN") || pd.isEqual(w, "VERB") || pd.isEqual(w, "INFN") || pd.isEqual(w, "ADVB")) {
                exW.setNegativeParticle((NegativeParticleRu) gottenNPRCL);

                gottenNPRCL = null;
            }
        } else if (pd.isEqual(w, "PRCL")) {
            String val = w.getWordforms(0).getLemma().toLowerCase();
            if (val.equals("не") || val.equals("ни")) {
                gottenNPRCL = new NegativeParticleRu(aJCas);
                gottenNPRCL.setWord(w);
                gottenNPRCL.setBegin(w.getBegin());
                gottenNPRCL.setEnd(w.getEnd());
                return 1;
            }
        }
        return 0;
    }

    private int inclinationParticleHandler(Word w) {
        String val = w.getWordforms(0).getLemma().toLowerCase();
        if (pd.isEqual(w, "VERB") || pd.isEqual(w, "INFN") || extraWords.contains(val)) {
            if (gottenIPRCL != null) {
                if (lastVerbindex != not_found && !(lastVerbindex == lst.size() - 1 && pd.isEqual(w, "VERB") && pd.isEqual(lst.get(lastVerbindex).getWord(), "INFN")))
                    lst.get(lastVerbindex).setInclinationParticle((InclinationParticleRu) gottenIPRCL);
                else
                    exW.setInclinationParticle((InclinationParticleRu) gottenIPRCL);

                gottenIPRCL = null;
            } else {
                lastVerbindex = lst.size();
            }
        } else if (pd.isEqual(w, "PRCL")) {
            if (val.equals("бы") || val.equals("б")) {
                gottenIPRCL = new InclinationParticleRu(aJCas);
                gottenIPRCL.setWord(w);
                gottenIPRCL.setBegin(w.getBegin());
                gottenIPRCL.setEnd(w.getEnd());

                if (lastVerbindex != lst.size() - 1)
                    lastVerbindex = not_found;

                return 1;
            }
        }

        return 0;
    }
    //

    public void process(final Iterator<Annotation> iter) {
        lastVerbindex = not_found;
        lst = new LinkedList<>();
        gottenPREP = null;
        gottenNPRCL = null;
        gottenIPRCL = null;

        while (iter.hasNext()) {
            Word w = (Word) iter.next();
            exW = new ExtendedWordRu(aJCas);
            exW.setWord(w);
            exW.setBegin(w.getBegin());
            exW.setEnd(w.getEnd());

            if (prepositionHandler(w) == 1)
                continue;

            if (negativeParticleHandler(w) == 1)
                continue;

            if (inclinationParticleHandler(w) == 1)
                continue;

            lst.add(exW);
        }

        for (int i = 0; i < lst.size(); i++)
            aJCas.addFsToIndexes(lst.get(i));
    }
}
