package ru.misis.asu.nlp.extended.en;

import ru.misis.asu.nlp.tagging.types.CorrectedWord;

import java.util.List;
import java.util.function.*;

/**
 * @author valter
 *         14.07.16
 */
public class ExtensionRuleEn {
    private Function<WordTuple, Integer> wordPredicate;

    public ExtensionRuleEn(Function<WordTuple, Integer> wordPredicate) {
        this.wordPredicate = wordPredicate;
    }

    public Integer test(List<CorrectedWord> words, Integer mainWordPosition, Integer grammarSignPosition) {
        return wordPredicate.apply(new WordTuple(words, mainWordPosition, grammarSignPosition));
    }

    public static class WordTuple {
        public List<CorrectedWord> words;
        public Integer mainWordPosition;
        public Integer grammarSignPosition;

        public WordTuple(List<CorrectedWord> words, Integer mainWordPosition, Integer grammarSignPosition) {
            this.words = words;
            this.mainWordPosition = mainWordPosition;
            this.grammarSignPosition = grammarSignPosition;
        }

        public CorrectedWord mainWord() {
            return words.get(mainWordPosition);
        }

        public CorrectedWord grammarSign() {
            return words.get(grammarSignPosition);
        }
    }
}
