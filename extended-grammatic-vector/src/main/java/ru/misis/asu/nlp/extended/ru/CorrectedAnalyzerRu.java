package ru.misis.asu.nlp.extended.ru;

import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import ru.misis.asu.nlp.extended.types.ru.*;
import ru.misis.asu.nlp.morphoanalysis.types.CorrectedWord;
import ru.misis.asu.nlp.morphoanalysis.types.Word;

import java.util.*;


public class CorrectedAnalyzerRu  {
    HashMap<String, ExtendedPreposition> map;
    LinkedList<ExtendedCorrectedWordRu> lst;
    JCas aJCas;
    POSdet pd = new POSdet();

    public CorrectedAnalyzerRu(HashMap<String, ExtendedPreposition> map, JCas aJCas) {
        this.map = map;
        this.aJCas = aJCas;
    }

    //My Handlers
    Set<String> extraWords = new HashSet<>(Arrays.asList("хотя", "хоть", "лишь"));
    int lastVerbindex;
    int not_found = -100;
    ExtendedCorrectedWordRu exW;
    CorrectedServiceRu gottenPREP, gottenNPRCL, gottenIPRCL;

    private int prepositionHandler(CorrectedWord w) {
        Word isPrep = null;
        String correction = null;
        int i = 0;
        for (FeatureStructure featureStructure : w.getWords().toArray()) {
            Word word = (Word) featureStructure;
            if (pd.isEqual(word, "PREP")) {
                isPrep = word;
                correction = w.getCorrections(i);
                break;
            }
            i++;
        }
        if (gottenPREP != null) {
            boolean firstCondition = false;
            boolean secondCondition = false;
            for (FeatureStructure featureStructure : w.getWords().toArray()) {
                Word word = (Word) featureStructure;
                if (pd.isEqual(word, "ADJF") || pd.isEqual(word, "PRTF") || pd.isEqual(word, "NUMR")) {
                    firstCondition = true;
                }

                if (pd.isEqual(word, "NOUN") || pd.isEqual(word, "NPRO")) {
                    secondCondition = true;
                }
            }
            if (firstCondition) {
                exW.setPreposition((CorrectedPrepositionRu) gottenPREP);
            }
            if (secondCondition) {
                exW.setPreposition((CorrectedPrepositionRu) gottenPREP);
                gottenPREP = null;
            }
        } else if (isPrep != null) {
            if (map.containsKey(isPrep.getWordforms(0).getLemma())) {
                gottenPREP = (map.get(isPrep.getWordforms(0).getLemma())).getCorrectedPreposition(aJCas, isPrep, correction);
                return 1;
            }
        }
        return 0;
    }

    private int negativeParticleHandler(CorrectedWord w) {
        Word isParticle = null;
        String correction = null;
        int i = 0;
        for (FeatureStructure featureStructure : w.getWords().toArray()) {
            Word word = (Word) featureStructure;
            if (pd.isEqual(word, "PRCL")) {
                isParticle = word;
                correction = w.getCorrections(i);
                break;
            }
            i++;
        }
        if (gottenNPRCL != null) {
            boolean condition = false;
            for (FeatureStructure featureStructure : w.getWords().toArray()) {
                Word word = (Word) featureStructure;
                if (pd.isEqual(word, "NOUN") || pd.isEqual(word, "VERB") || pd.isEqual(word, "INFN") || pd.isEqual(word, "ADVB")) {
                    condition = true;
                }
            }
            if (condition) {
                exW.setNegativeParticle((CorrectedNegativeParticleRu) gottenNPRCL);
                gottenNPRCL = null;
            }
        } else if (isParticle != null) {
            String val = isParticle.getWordforms(0).getLemma().toLowerCase();
            if (val.equals("не") || val.equals("ни")) {
                gottenNPRCL = new CorrectedNegativeParticleRu(aJCas);
                gottenNPRCL.setWord(isParticle);
                gottenNPRCL.setCorrection(correction);
                gottenNPRCL.setBegin(w.getBegin());
                gottenNPRCL.setEnd(w.getEnd());
                return 1;
            }
        }
        return 0;
    }

    private int inclinationParticleHandler(CorrectedWord w) {
        boolean condition = false;
        boolean condition2 = false;
        Word condition3 = null;
        String correction = null;
        int i = 0;
        for (FeatureStructure featureStructure : w.getWords().toArray()) {
            Word word = (Word) featureStructure;
            if (pd.isEqual(word, "VERB") || pd.isEqual(word, "INFN") || extraWords.contains(word.getWordforms(0).getLemma().toLowerCase())) {
                condition = true;
            }

            if (pd.isEqual(word, "VERB")) {
                condition2 = true;
            }

            if (pd.isEqual(word, "PRCL")) {
                condition3 = word;
                correction = w.getCorrections(i);
                break;
            }
            i++;
        }

        if (condition) {
            if (gottenIPRCL != null) {
                if (lastVerbindex != not_found && !(lastVerbindex == lst.size() - 1 && condition2)) {
                    boolean mayBeInf = false;
                    for (FeatureStructure fs : lst.get(lastVerbindex).getWord().getWords().toArray()) {
                        Word word = (Word) fs;
                        mayBeInf |= pd.isEqual(word, "INFN");
                    }
                    if (mayBeInf) {
                        lst.get(lastVerbindex).setInclinationParticle((CorrectedInclinationParticleRu) gottenIPRCL);
                    } else {
                        exW.setInclinationParticle((CorrectedInclinationParticleRu) gottenIPRCL);
                    }
                } else
                    exW.setInclinationParticle((CorrectedInclinationParticleRu) gottenIPRCL);

                gottenIPRCL = null;
            } else {
                lastVerbindex = lst.size();
            }
        } else if (condition3 != null) {
            if (condition3.getWordforms(0).getLemma().toLowerCase().equals("бы")
                    || condition3.getWordforms(0).getLemma().toLowerCase().equals("б")) {
                gottenIPRCL = new CorrectedInclinationParticleRu(aJCas);
                gottenIPRCL.setWord(condition3);
                gottenIPRCL.setCorrection(correction);
                gottenIPRCL.setBegin(w.getBegin());
                gottenIPRCL.setEnd(w.getEnd());

                if (lastVerbindex != lst.size() - 1)
                    lastVerbindex = not_found;

                return 1;
            }
        }

        return 0;
    }
    //

    public void process(final Iterator<Annotation> iter) {
        lastVerbindex = not_found;
        lst = new LinkedList<>();
        gottenPREP = null;
        gottenNPRCL = null;
        gottenIPRCL = null;

        while (iter.hasNext()) {
            CorrectedWord w = (CorrectedWord) iter.next();
            exW = new ExtendedCorrectedWordRu(aJCas);
            exW.setWord(w);
            exW.setBegin(w.getBegin());
            exW.setEnd(w.getEnd());

            if (prepositionHandler(w) == 1)
                continue;

            if (negativeParticleHandler(w) == 1)
                continue;

            if (inclinationParticleHandler(w) == 1)
                continue;

            lst.add(exW);
        }

        for (int i = 0; i < lst.size(); i++)
            aJCas.addFsToIndexes(lst.get(i));
    }
}
