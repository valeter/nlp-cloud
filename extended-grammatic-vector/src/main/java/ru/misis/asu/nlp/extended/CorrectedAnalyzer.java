package ru.misis.asu.nlp.extended;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;

import java.util.Iterator;

/**
 * @author valter
 *         14.07.16
 */
public interface CorrectedAnalyzer {
    void process(JCas jcas, Iterator<Annotation> sentence);
}
