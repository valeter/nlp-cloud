package ru.misis.asu.nlp.extended.ru;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;

public class ExtendedCorrectedWordAnnotator extends JCasAnnotator_ImplBase {
    static HashMap<String, ExtendedPreposition> map;

    @Override
    public void initialize(UimaContext aContext)
            throws ResourceInitializationException {
        super.initialize(aContext);
        map = new HashMap<>();
    }

    @Override
    public void process(final JCas aJCas) throws AnalysisEngineProcessException {
        TypeSystem ts = aJCas.getTypeSystem();
        Type tw = ts.getType("ru.misis.asu.nlp.morphoanalysis.types.CorrectedWord");
        Type t = ts.getType("ru.misis.asu.nlp.segmentation.types.Sentence");
        AnnotationIndex<Annotation> indw = aJCas.getAnnotationIndex(tw);
        AnnotationIndex<Annotation> inds = aJCas.getAnnotationIndex(t);

        CorrectedAnalyzerRu an = new CorrectedAnalyzerRu(map, aJCas);

        for (Iterator<Annotation> it = inds.iterator(); it.hasNext(); ) {
            FSIterator<Annotation> iter = indw.subiterator(it.next());

            an.process(iter);
        }
    }
}
