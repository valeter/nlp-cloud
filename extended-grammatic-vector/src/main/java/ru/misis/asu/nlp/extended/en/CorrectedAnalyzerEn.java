package ru.misis.asu.nlp.extended.en;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.StringArray;
import org.apache.uima.jcas.tcas.Annotation;
import ru.misis.asu.nlp.extended.CorrectedAnalyzer;
import ru.misis.asu.nlp.extended.types.en.*;
import ru.misis.asu.nlp.tagging.types.CorrectedWord;
import ru.misis.asu.nlp.tagging.types.Word;
import ru.misis.asu.nlp.tagging.types.Wordform;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

/**
 * @author valter
 *         14.07.16
 */
public class CorrectedAnalyzerEn implements CorrectedAnalyzer {
    private List<ExtensionRuleEn> negationRules = Arrays.asList(
            new ExtensionRuleEn(
                    (wt) -> {
                        int mainWordIsFiniteVerb = hasPos(wt.mainWord(), "VBD", "VBP", "VBZ", "MD");
                        int singIsNegation = hasCorrection(wt.grammarSign(), "not", "n't");
                        boolean signAfterMainWord = wt.grammarSignPosition - wt.mainWordPosition == 1;
                        return mainWordIsFiniteVerb >= 0 && singIsNegation >= 0 && signAfterMainWord ? singIsNegation : null;
                    }
            ),
            new ExtensionRuleEn(
                    (wt) -> {
                        int mainWordIsGerund = hasPos(wt.mainWord(), "VBG", "VBN");
                        int singIsNegation = hasCorrection(wt.grammarSign(), "not", "n't");
                        boolean signBeforeMainWord = wt.mainWordPosition - wt.grammarSignPosition == 2;
                        int rbPosition = wt.grammarSignPosition + 1;
                        boolean rbStaysBetweenWords = wt.words.size() > rbPosition && hasPos(wt.words.get(rbPosition), "RB") >= 0;
                        return mainWordIsGerund >= 0 && singIsNegation >= 0 && signBeforeMainWord && rbStaysBetweenWords ? singIsNegation : null;
                    }
            ),
            new ExtensionRuleEn(
                    (wt) -> {
                        int mainWordIsBaseVerb = hasPos(wt.mainWord(), "VB");
                        int singIsNegation = hasCorrection(wt.grammarSign(), "not");
                        boolean signAfterMainWord = wt.grammarSignPosition - wt.mainWordPosition >= 1;
                        int toPosition = wt.grammarSignPosition + 1;
                        boolean toStaysBetweenWords = wt.grammarSignPosition - wt.mainWordPosition == 1
                                ||  wt.words.size() > toPosition && (hasCorrection(wt.words.get(toPosition), "to") >= 0 || hasPos(wt.words.get(toPosition), "RB") >= 0) ;
                        return mainWordIsBaseVerb >= 0 && singIsNegation >= 0 && signAfterMainWord && toStaysBetweenWords ? singIsNegation : null;
                    }
            )
    );

    private List<ExtensionRuleEn> possessiveEndingRules = Arrays.asList(
            new ExtensionRuleEn(
                    (wt) -> {
                        int signIsPossessiveEnding = hasPos(wt.grammarSign(), "POS");
                        boolean signAfterMainWord = wt.grammarSignPosition - wt.mainWordPosition == 1;
                        return signIsPossessiveEnding >= 0 && signAfterMainWord ? signIsPossessiveEnding : null;
                    }
            )
    );

    private List<ExtensionRuleEn> articleRules = Arrays.asList(
            new ExtensionRuleEn(
                    (wt) -> {
                        int mainWordIsNoun = hasPos(wt.mainWord(), "NN", "NNS", "NNP", "NNPS");
                        int singIsAArticle = hasCorrection(wt.grammarSign(), "a");
                        boolean signBeforeMainWord = wt.mainWordPosition - wt.grammarSignPosition >= 1;
                        boolean betweenIsOk = true;
                        for (int i = wt.grammarSignPosition + 1; i < wt.mainWordPosition; i++) {
                            betweenIsOk = betweenIsOk
                                    && hasPos(wt.words.get(i), "CD", "DT", "JJ", "JJR", "JJS", "PDT", "RB", "RBR", "RBS", "WDT", "WP$") >= 0;
                        }
                        return mainWordIsNoun >= 0 && singIsAArticle >= 0 && signBeforeMainWord && betweenIsOk ? singIsAArticle : null;
                    }
            ),
            new ExtensionRuleEn(
                    (wt) -> {
                        int mainWordIsNoun = hasPos(wt.mainWord(), "NN", "NNS", "NNP", "NNPS");
                        int singIsAArticle = hasCorrection(wt.grammarSign(), "a");
                        boolean signBeforeMainWord = wt.mainWordPosition - wt.grammarSignPosition >= 2;
                        int nnPosition = wt.grammarSignPosition + 1;
                        boolean nnStaysBetweenWords = wt.words.size() > nnPosition && hasPos(wt.words.get(nnPosition), "NN") >= 0;
                        return mainWordIsNoun >= 0 && singIsAArticle >= 0 && signBeforeMainWord && nnStaysBetweenWords ? singIsAArticle : null;
                    }
            ),
            new ExtensionRuleEn(
                    (wt) -> {
                        int mainWordIsNoun = hasPos(wt.mainWord(), "NN", "NNS", "NNP", "NNPS");
                        int singIsTheArticle = hasCorrection(wt.grammarSign(), "the");
                        boolean signBeforeMainWord = wt.mainWordPosition - wt.grammarSignPosition >= 1;
                        boolean betweenIsOk = true;
                        for (int i = wt.grammarSignPosition + 1; i < wt.mainWordPosition; i++) {
                            betweenIsOk = betweenIsOk
                                    && hasPos(wt.words.get(i), "CD", "DT", "JJ", "JJR", "JJS", "PDT", "RB", "RBR", "RBS", "WDT", "WP$") >= 0;
                        }
                        return mainWordIsNoun >= 0 && singIsTheArticle >= 0 && signBeforeMainWord && betweenIsOk ? singIsTheArticle : null;
                    }
            ),
            new ExtensionRuleEn(
                    (wt) -> {
                        int mainWordIsNoun = hasPos(wt.mainWord(), "NN", "NNS", "NNP", "NNPS");
                        int singIsTheArticle = hasCorrection(wt.grammarSign(), "the");
                        boolean signBeforeMainWord = wt.mainWordPosition - wt.grammarSignPosition >= 2;
                        int nnPosition = wt.grammarSignPosition + 1;
                        boolean nnStaysBetweenWords = wt.words.size() > nnPosition && hasPos(wt.words.get(nnPosition), "NN") >= 0;
                        return mainWordIsNoun >= 0 && singIsTheArticle >= 0 && signBeforeMainWord && nnStaysBetweenWords ? singIsTheArticle : null;
                    }
            ),
            new ExtensionRuleEn(
                    (wt) -> {
                        int mainWordIsAdjOrCard = hasPos(wt.mainWord(), "JJ", "JJR", "JJS", "CD");
                        int singIsTheArticle = hasCorrection(wt.grammarSign(), "the");
                        boolean signBeforeMainWord = wt.mainWordPosition - wt.grammarSignPosition >= 1;
                        int nnPosition = wt.grammarSignPosition + 1;
                        boolean rbrStaysBetweenWords = wt.mainWordPosition - wt.grammarSignPosition == 1
                                || wt.words.size() > nnPosition && hasPos(wt.words.get(nnPosition), "RBR", "RBS") >= 0;
                        return mainWordIsAdjOrCard >= 0 && singIsTheArticle >= 0 && signBeforeMainWord && rbrStaysBetweenWords ? singIsTheArticle : null;
                    }
            )
    );

    private static int hasPos(CorrectedWord correctedWord, String... possiblePos) {
        Set<String> possiblePosSet = new HashSet<>(Arrays.asList(possiblePos));
        int hasPos = -1;
        FSArray words = correctedWord.getWords();
        for (int i = 0; i < words.size(); i++) {
            Word word = (Word) words.get(i);
            FSArray wordforms = word.getWordforms();
            for (int j = 0; j < wordforms.size(); j++) {
                Wordform wordform = (Wordform) wordforms.get(j);
                hasPos = possiblePosSet.contains(wordform.getPos().toUpperCase()) ? i : -1;
                if (hasPos > 0) {
                    return hasPos;
                }
            }
        }
        return hasPos;
    }

    private static int hasCorrection(CorrectedWord correctedWord, String... possibleCorrections) {
        Set<String> possibleCorrectionsSet = new HashSet<>(Arrays.asList(possibleCorrections));
        StringArray corrections = correctedWord.getCorrections();
        int hasCorrection = -1;
        for (int i = 0; i < corrections.size(); i++) {
            String correction = corrections.get(i);
            hasCorrection = possibleCorrectionsSet.contains(correction.toLowerCase()) ? i : -1;
            if (hasCorrection > 0) {
                return hasCorrection;
            }
        }
        return hasCorrection;
    }

    @Override
    public void process(JCas jcas, Iterator<Annotation> sentence) {
        List<CorrectedWord> words = new ArrayList<>();

        for (;sentence.hasNext();) {
            CorrectedWord next =  (CorrectedWord) sentence.next();
            words.add(next);
        }

        List<ExtendedCorrectedWordEn> extendedWords = words.stream()
                .map(w -> extend(jcas, w))
                .collect(Collectors.toList());

        for (int i = 0; i < words.size(); i++) {
            CorrectedWord word = words.get(i);
            int isPersonalPronoun = hasPos(word, "PRP");
            if (isPersonalPronoun >= 0) {
                CorrectedPersonalPronounEn personalPronoun = new CorrectedPersonalPronounEn(jcas);
                personalPronoun.setBegin(word.getBegin());
                personalPronoun.setEnd(word.getEnd());
                personalPronoun.setCorrection(word.getCorrections(isPersonalPronoun));
                personalPronoun.setWord(word.getWords(isPersonalPronoun));
                extendedWords.get(i).setPersonalPronoun(personalPronoun);
            }

            int isPossessivePronoun = hasPos(word, "PRP$");
            if (isPossessivePronoun >= 0) {
                CorrectedPossessivePronounEn possessivePronoun = new CorrectedPossessivePronounEn(jcas);
                possessivePronoun.setBegin(word.getBegin());
                possessivePronoun.setEnd(word.getEnd());
                possessivePronoun.setCorrection(word.getCorrections(isPossessivePronoun));
                possessivePronoun.setWord(word.getWords(isPossessivePronoun));
                extendedWords.get(i).setPossessivePronoun(possessivePronoun);
            }
        }

        Set<Integer> grammarSigns = new HashSet<>();

        checkRules(words, extendedWords, grammarSigns, negationRules, (mainWord, grammarSign, result) -> {
            CorrectedNegativeParticleEn negativeParticle = new CorrectedNegativeParticleEn(jcas);
            negativeParticle.setBegin(grammarSign.getBegin());
            negativeParticle.setEnd(grammarSign.getEnd());
            negativeParticle.setCorrection(grammarSign.getCorrections(result));
            negativeParticle.setWord(grammarSign.getWords(result));
            mainWord.setNegativeParticle(negativeParticle);
        }, true);

        checkRules(words, extendedWords, grammarSigns, possessiveEndingRules, (mainWord, grammarSign, result) -> {
            CorrectedPossessiveEndingEn possessiveEnding = new CorrectedPossessiveEndingEn(jcas);
            possessiveEnding.setBegin(grammarSign.getBegin());
            possessiveEnding.setEnd(grammarSign.getEnd());
            possessiveEnding.setCorrection(grammarSign.getCorrections(result));
            possessiveEnding.setWord(grammarSign.getWords(result));
            mainWord.setPossessiveEnding(possessiveEnding);
        }, true);

        checkRules(words, extendedWords, grammarSigns, articleRules, (mainWord, grammarSign, result) -> {
            CorrectedArticleEn article = new CorrectedArticleEn(jcas);
            article.setBegin(grammarSign.getBegin());
            article.setEnd(grammarSign.getEnd());
            article.setCorrection(grammarSign.getCorrections(result));
            article.setWord(grammarSign.getWords(result));
            mainWord.setArticle(article);
        }, true);

        System.out.println();
        for (int i = 0; i < extendedWords.size(); i++) {
            if (grammarSigns.contains(i)) {
                continue;
            }

            ExtendedCorrectedWordEn word = extendedWords.get(i);
            word.addToIndexes();

            printExtendedWord(word);
        }

        System.out.println();
        System.out.println("-------------------------------------------");
        System.out.println();
        System.out.println();
    }

    private void printExtendedWord(ExtendedCorrectedWordEn word) {
        System.out.println("ExtendedWord [" + word.getCoveredText() + "] attributes: [");
        System.out.println("\t article: " + (word.getArticle() == null ? null : word.getArticle().getCorrection()));
        System.out.println("\t personal pronoun: " + (word.getPersonalPronoun() == null ? null : PERSONAL_PRONOUN_TYPES.get(word.getPersonalPronoun().getCorrection())));
        System.out.println("\t possessive pronoun: " + (word.getPossessivePronoun() == null ? null : POSSESSIVE_PRONOUN_TYPES.get(word.getPossessivePronoun().getCorrection())));
        System.out.println("\t possessive ending: " + (word.getPossessiveEnding() == null ? null : word.getPossessiveEnding().getCorrection()));
        System.out.println("\t negative particle: " + (word.getNegativeParticle() == null ? null : word.getNegativeParticle().getCorrection()));
        System.out.println("\t preposition: " + (word.getPreposition() == null ? null : word.getPreposition().getCorrection()));
        System.out.println("]");
    }

    private static final Map<String, List<Integer>> PERSONAL_PRONOUN_TYPES = new HashMap<String, List<Integer>>() {{
        put("i", Collections.singletonList(0));
        put("you", Arrays.asList(0, 1));
        put("she", Collections.singletonList(0));
        put("he", Collections.singletonList(0));
        put("it", Arrays.asList(0, 1));
        put("we", Collections.singletonList(0));
        put("they", Collections.singletonList(0));
        put("me", Collections.singletonList(1));
        put("her", Collections.singletonList(1));
        put("him", Collections.singletonList(1));
        put("us", Collections.singletonList(1));
        put("them", Collections.singletonList(1));
        put("myself", Collections.singletonList(2));
        put("yourself", Collections.singletonList(2));
        put("yourselves", Collections.singletonList(2));
        put("herself", Collections.singletonList(2));
        put("himself", Collections.singletonList(2));
        put("itself", Collections.singletonList(2));
        put("ourselves", Collections.singletonList(2));
        put("themselves", Collections.singletonList(2));
        put("ownself", Collections.singletonList(2));
        put("ownselves", Collections.singletonList(2));
        put("oneself", Collections.singletonList(2));
    }};

    private static final Map<String, List<Integer>> POSSESSIVE_PRONOUN_TYPES = new HashMap<String, List<Integer>>() {{
        put("my", Collections.singletonList(0));
        put("your", Collections.singletonList(0));
        put("his", Arrays.asList(0, 1));
        put("her", Collections.singletonList(0));
        put("its", Arrays.asList(0, 1));
        put("our", Collections.singletonList(0));
        put("their", Collections.singletonList(0));
        put("thy", Collections.singletonList(0));
        put("mine", Collections.singletonList(1));
        put("yours", Collections.singletonList(1));
        put("hers", Collections.singletonList(1));
        put("ours",Collections.singletonList( 1));
        put("theirs", Collections.singletonList(1));
    }};


    private ExtendedCorrectedWordEn extend(JCas jcas, CorrectedWord word) {
        ExtendedCorrectedWordEn extendedWord = new ExtendedCorrectedWordEn(jcas);
        extendedWord.setBegin(word.getBegin());
        extendedWord.setEnd(word.getEnd());
        extendedWord.setWord(word);
        return extendedWord;
    }

    private void checkRules(List<CorrectedWord> words, List<ExtendedCorrectedWordEn> extendedWords,
                            Set<Integer> grammarSigns,
                            List<ExtensionRuleEn> rules, TriConsumer<ExtendedCorrectedWordEn, CorrectedWord, Integer> action, boolean addToSigns) {
        for (ExtensionRuleEn rule : rules) {
            for (int i = 0; i < words.size(); i++) {
                if (grammarSigns.contains(i)) {
                    continue;
                }
                ExtendedCorrectedWordEn mainWord = extendedWords.get(i);

                for (int j = 0; j < words.size(); j++) {
                    if (i == j || grammarSigns.contains(j)) {
                        continue;
                    }
                    CorrectedWord grammarSign = words.get(j);

                    Integer result = rule.test(words, i, j);
                    if (result != null) {
                        action.accept(mainWord, grammarSign, result);
                        if (addToSigns)
                            grammarSigns.add(j);
                    }
                }
            }
        }
    }

    private interface TriConsumer<T1, T2, T3> {
        void accept(T1 t1, T2 t2, T3 t3);
    }
}
