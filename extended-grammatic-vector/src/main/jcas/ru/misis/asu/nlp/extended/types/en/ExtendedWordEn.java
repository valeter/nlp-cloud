

/* First created by JCasGen Sun Sep 04 13:56:10 MSK 2016 */
package ru.misis.asu.nlp.extended.types.en;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import ru.misis.asu.nlp.tagging.types.Word;
import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Sun Sep 04 13:56:10 MSK 2016
 * XML source: /home/valter/projects/nlp-cloud/extended-grammatic-vector/src/main/resources/uima_xml/extended-grammatic-vector-ts.uima_xml
 * @generated */
public class ExtendedWordEn extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(ExtendedWordEn.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected ExtendedWordEn() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public ExtendedWordEn(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public ExtendedWordEn(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public ExtendedWordEn(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: word

  /** getter for word - gets 
   * @generated
   * @return value of the feature 
   */
  public Word getWord() {
    if (ExtendedWordEn_Type.featOkTst && ((ExtendedWordEn_Type)jcasType).casFeat_word == null)
      jcasType.jcas.throwFeatMissing("word", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    return (Word)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((ExtendedWordEn_Type)jcasType).casFeatCode_word)));}
    
  /** setter for word - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setWord(Word v) {
    if (ExtendedWordEn_Type.featOkTst && ((ExtendedWordEn_Type)jcasType).casFeat_word == null)
      jcasType.jcas.throwFeatMissing("word", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    jcasType.ll_cas.ll_setRefValue(addr, ((ExtendedWordEn_Type)jcasType).casFeatCode_word, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: personalPronoun

  /** getter for personalPronoun - gets 
   * @generated
   * @return value of the feature 
   */
  public PersonalPronounEn getPersonalPronoun() {
    if (ExtendedWordEn_Type.featOkTst && ((ExtendedWordEn_Type)jcasType).casFeat_personalPronoun == null)
      jcasType.jcas.throwFeatMissing("personalPronoun", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    return (PersonalPronounEn)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((ExtendedWordEn_Type)jcasType).casFeatCode_personalPronoun)));}
    
  /** setter for personalPronoun - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setPersonalPronoun(PersonalPronounEn v) {
    if (ExtendedWordEn_Type.featOkTst && ((ExtendedWordEn_Type)jcasType).casFeat_personalPronoun == null)
      jcasType.jcas.throwFeatMissing("personalPronoun", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    jcasType.ll_cas.ll_setRefValue(addr, ((ExtendedWordEn_Type)jcasType).casFeatCode_personalPronoun, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: possessivePronoun

  /** getter for possessivePronoun - gets 
   * @generated
   * @return value of the feature 
   */
  public PossessivePronounEn getPossessivePronoun() {
    if (ExtendedWordEn_Type.featOkTst && ((ExtendedWordEn_Type)jcasType).casFeat_possessivePronoun == null)
      jcasType.jcas.throwFeatMissing("possessivePronoun", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    return (PossessivePronounEn)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((ExtendedWordEn_Type)jcasType).casFeatCode_possessivePronoun)));}
    
  /** setter for possessivePronoun - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setPossessivePronoun(PossessivePronounEn v) {
    if (ExtendedWordEn_Type.featOkTst && ((ExtendedWordEn_Type)jcasType).casFeat_possessivePronoun == null)
      jcasType.jcas.throwFeatMissing("possessivePronoun", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    jcasType.ll_cas.ll_setRefValue(addr, ((ExtendedWordEn_Type)jcasType).casFeatCode_possessivePronoun, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: possessiveEnding

  /** getter for possessiveEnding - gets 
   * @generated
   * @return value of the feature 
   */
  public PossessiveEndingEn getPossessiveEnding() {
    if (ExtendedWordEn_Type.featOkTst && ((ExtendedWordEn_Type)jcasType).casFeat_possessiveEnding == null)
      jcasType.jcas.throwFeatMissing("possessiveEnding", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    return (PossessiveEndingEn)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((ExtendedWordEn_Type)jcasType).casFeatCode_possessiveEnding)));}
    
  /** setter for possessiveEnding - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setPossessiveEnding(PossessiveEndingEn v) {
    if (ExtendedWordEn_Type.featOkTst && ((ExtendedWordEn_Type)jcasType).casFeat_possessiveEnding == null)
      jcasType.jcas.throwFeatMissing("possessiveEnding", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    jcasType.ll_cas.ll_setRefValue(addr, ((ExtendedWordEn_Type)jcasType).casFeatCode_possessiveEnding, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: negativeParticle

  /** getter for negativeParticle - gets 
   * @generated
   * @return value of the feature 
   */
  public NegativeParticleEn getNegativeParticle() {
    if (ExtendedWordEn_Type.featOkTst && ((ExtendedWordEn_Type)jcasType).casFeat_negativeParticle == null)
      jcasType.jcas.throwFeatMissing("negativeParticle", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    return (NegativeParticleEn)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((ExtendedWordEn_Type)jcasType).casFeatCode_negativeParticle)));}
    
  /** setter for negativeParticle - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setNegativeParticle(NegativeParticleEn v) {
    if (ExtendedWordEn_Type.featOkTst && ((ExtendedWordEn_Type)jcasType).casFeat_negativeParticle == null)
      jcasType.jcas.throwFeatMissing("negativeParticle", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    jcasType.ll_cas.ll_setRefValue(addr, ((ExtendedWordEn_Type)jcasType).casFeatCode_negativeParticle, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: article

  /** getter for article - gets 
   * @generated
   * @return value of the feature 
   */
  public ArticleEn getArticle() {
    if (ExtendedWordEn_Type.featOkTst && ((ExtendedWordEn_Type)jcasType).casFeat_article == null)
      jcasType.jcas.throwFeatMissing("article", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    return (ArticleEn)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((ExtendedWordEn_Type)jcasType).casFeatCode_article)));}
    
  /** setter for article - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setArticle(ArticleEn v) {
    if (ExtendedWordEn_Type.featOkTst && ((ExtendedWordEn_Type)jcasType).casFeat_article == null)
      jcasType.jcas.throwFeatMissing("article", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    jcasType.ll_cas.ll_setRefValue(addr, ((ExtendedWordEn_Type)jcasType).casFeatCode_article, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: preposition

  /** getter for preposition - gets 
   * @generated
   * @return value of the feature 
   */
  public PrepositionEn getPreposition() {
    if (ExtendedWordEn_Type.featOkTst && ((ExtendedWordEn_Type)jcasType).casFeat_preposition == null)
      jcasType.jcas.throwFeatMissing("preposition", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    return (PrepositionEn)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((ExtendedWordEn_Type)jcasType).casFeatCode_preposition)));}
    
  /** setter for preposition - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setPreposition(PrepositionEn v) {
    if (ExtendedWordEn_Type.featOkTst && ((ExtendedWordEn_Type)jcasType).casFeat_preposition == null)
      jcasType.jcas.throwFeatMissing("preposition", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    jcasType.ll_cas.ll_setRefValue(addr, ((ExtendedWordEn_Type)jcasType).casFeatCode_preposition, jcasType.ll_cas.ll_getFSRef(v));}    
  }

    