
/* First created by JCasGen Sun Sep 04 13:56:10 MSK 2016 */
package ru.misis.asu.nlp.extended.types.ru;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Sun Sep 04 13:56:10 MSK 2016
 * @generated */
public class ExtendedWordRu_Type extends Annotation_Type {
  /** @generated 
   * @return the generator for this type
   */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (ExtendedWordRu_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = ExtendedWordRu_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new ExtendedWordRu(addr, ExtendedWordRu_Type.this);
  			   ExtendedWordRu_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new ExtendedWordRu(addr, ExtendedWordRu_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = ExtendedWordRu.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("ru.misis.asu.nlp.extended.types.ru.ExtendedWordRu");
 
  /** @generated */
  final Feature casFeat_word;
  /** @generated */
  final int     casFeatCode_word;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getWord(int addr) {
        if (featOkTst && casFeat_word == null)
      jcas.throwFeatMissing("word", "ru.misis.asu.nlp.extended.types.ru.ExtendedWordRu");
    return ll_cas.ll_getRefValue(addr, casFeatCode_word);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setWord(int addr, int v) {
        if (featOkTst && casFeat_word == null)
      jcas.throwFeatMissing("word", "ru.misis.asu.nlp.extended.types.ru.ExtendedWordRu");
    ll_cas.ll_setRefValue(addr, casFeatCode_word, v);}
    
  
 
  /** @generated */
  final Feature casFeat_preposition;
  /** @generated */
  final int     casFeatCode_preposition;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getPreposition(int addr) {
        if (featOkTst && casFeat_preposition == null)
      jcas.throwFeatMissing("preposition", "ru.misis.asu.nlp.extended.types.ru.ExtendedWordRu");
    return ll_cas.ll_getRefValue(addr, casFeatCode_preposition);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setPreposition(int addr, int v) {
        if (featOkTst && casFeat_preposition == null)
      jcas.throwFeatMissing("preposition", "ru.misis.asu.nlp.extended.types.ru.ExtendedWordRu");
    ll_cas.ll_setRefValue(addr, casFeatCode_preposition, v);}
    
  
 
  /** @generated */
  final Feature casFeat_negativeParticle;
  /** @generated */
  final int     casFeatCode_negativeParticle;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getNegativeParticle(int addr) {
        if (featOkTst && casFeat_negativeParticle == null)
      jcas.throwFeatMissing("negativeParticle", "ru.misis.asu.nlp.extended.types.ru.ExtendedWordRu");
    return ll_cas.ll_getRefValue(addr, casFeatCode_negativeParticle);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setNegativeParticle(int addr, int v) {
        if (featOkTst && casFeat_negativeParticle == null)
      jcas.throwFeatMissing("negativeParticle", "ru.misis.asu.nlp.extended.types.ru.ExtendedWordRu");
    ll_cas.ll_setRefValue(addr, casFeatCode_negativeParticle, v);}
    
  
 
  /** @generated */
  final Feature casFeat_inclinationParticle;
  /** @generated */
  final int     casFeatCode_inclinationParticle;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getInclinationParticle(int addr) {
        if (featOkTst && casFeat_inclinationParticle == null)
      jcas.throwFeatMissing("inclinationParticle", "ru.misis.asu.nlp.extended.types.ru.ExtendedWordRu");
    return ll_cas.ll_getRefValue(addr, casFeatCode_inclinationParticle);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setInclinationParticle(int addr, int v) {
        if (featOkTst && casFeat_inclinationParticle == null)
      jcas.throwFeatMissing("inclinationParticle", "ru.misis.asu.nlp.extended.types.ru.ExtendedWordRu");
    ll_cas.ll_setRefValue(addr, casFeatCode_inclinationParticle, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	 * @generated
	 * @param jcas JCas
	 * @param casType Type 
	 */
  public ExtendedWordRu_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_word = jcas.getRequiredFeatureDE(casType, "word", "ru.misis.asu.nlp.morphoanalysis.types.Word", featOkTst);
    casFeatCode_word  = (null == casFeat_word) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_word).getCode();

 
    casFeat_preposition = jcas.getRequiredFeatureDE(casType, "preposition", "ru.misis.asu.nlp.extended.types.ru.PrepositionRu", featOkTst);
    casFeatCode_preposition  = (null == casFeat_preposition) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_preposition).getCode();

 
    casFeat_negativeParticle = jcas.getRequiredFeatureDE(casType, "negativeParticle", "ru.misis.asu.nlp.extended.types.ru.NegativeParticleRu", featOkTst);
    casFeatCode_negativeParticle  = (null == casFeat_negativeParticle) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_negativeParticle).getCode();

 
    casFeat_inclinationParticle = jcas.getRequiredFeatureDE(casType, "inclinationParticle", "ru.misis.asu.nlp.extended.types.ru.InclinationParticleRu", featOkTst);
    casFeatCode_inclinationParticle  = (null == casFeat_inclinationParticle) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_inclinationParticle).getCode();

  }
}



    