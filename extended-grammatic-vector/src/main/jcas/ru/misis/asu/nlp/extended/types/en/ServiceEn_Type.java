
/* First created by JCasGen Sun Sep 04 13:56:10 MSK 2016 */
package ru.misis.asu.nlp.extended.types.en;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Sun Sep 04 13:56:10 MSK 2016
 * @generated */
public class ServiceEn_Type extends Annotation_Type {
  /** @generated 
   * @return the generator for this type
   */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (ServiceEn_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = ServiceEn_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new ServiceEn(addr, ServiceEn_Type.this);
  			   ServiceEn_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new ServiceEn(addr, ServiceEn_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = ServiceEn.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("ru.misis.asu.nlp.extended.types.en.ServiceEn");
 
  /** @generated */
  final Feature casFeat_word;
  /** @generated */
  final int     casFeatCode_word;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getWord(int addr) {
        if (featOkTst && casFeat_word == null)
      jcas.throwFeatMissing("word", "ru.misis.asu.nlp.extended.types.en.ServiceEn");
    return ll_cas.ll_getRefValue(addr, casFeatCode_word);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setWord(int addr, int v) {
        if (featOkTst && casFeat_word == null)
      jcas.throwFeatMissing("word", "ru.misis.asu.nlp.extended.types.en.ServiceEn");
    ll_cas.ll_setRefValue(addr, casFeatCode_word, v);}
    
  
 
  /** @generated */
  final Feature casFeat_grammemes;
  /** @generated */
  final int     casFeatCode_grammemes;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getGrammemes(int addr) {
        if (featOkTst && casFeat_grammemes == null)
      jcas.throwFeatMissing("grammemes", "ru.misis.asu.nlp.extended.types.en.ServiceEn");
    return ll_cas.ll_getRefValue(addr, casFeatCode_grammemes);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setGrammemes(int addr, int v) {
        if (featOkTst && casFeat_grammemes == null)
      jcas.throwFeatMissing("grammemes", "ru.misis.asu.nlp.extended.types.en.ServiceEn");
    ll_cas.ll_setRefValue(addr, casFeatCode_grammemes, v);}
    
   /** @generated
   * @param addr low level Feature Structure reference
   * @param i index of item in the array
   * @return value at index i in the array 
   */
  public String getGrammemes(int addr, int i) {
        if (featOkTst && casFeat_grammemes == null)
      jcas.throwFeatMissing("grammemes", "ru.misis.asu.nlp.extended.types.en.ServiceEn");
    if (lowLevelTypeChecks)
      return ll_cas.ll_getStringArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_grammemes), i, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_grammemes), i);
	return ll_cas.ll_getStringArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_grammemes), i);
  }
   
  /** @generated
   * @param addr low level Feature Structure reference
   * @param i index of item in the array
   * @param v value to set
   */ 
  public void setGrammemes(int addr, int i, String v) {
        if (featOkTst && casFeat_grammemes == null)
      jcas.throwFeatMissing("grammemes", "ru.misis.asu.nlp.extended.types.en.ServiceEn");
    if (lowLevelTypeChecks)
      ll_cas.ll_setStringArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_grammemes), i, v, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_grammemes), i);
    ll_cas.ll_setStringArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_grammemes), i, v);
  }
 



  /** initialize variables to correspond with Cas Type and Features
	 * @generated
	 * @param jcas JCas
	 * @param casType Type 
	 */
  public ServiceEn_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_word = jcas.getRequiredFeatureDE(casType, "word", "ru.misis.asu.nlp.tagging.types.Word", featOkTst);
    casFeatCode_word  = (null == casFeat_word) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_word).getCode();

 
    casFeat_grammemes = jcas.getRequiredFeatureDE(casType, "grammemes", "uima.cas.StringArray", featOkTst);
    casFeatCode_grammemes  = (null == casFeat_grammemes) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_grammemes).getCode();

  }
}



    