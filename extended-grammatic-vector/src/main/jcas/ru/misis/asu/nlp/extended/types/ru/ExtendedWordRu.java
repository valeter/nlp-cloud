

/* First created by JCasGen Sun Sep 04 13:56:10 MSK 2016 */
package ru.misis.asu.nlp.extended.types.ru;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import ru.misis.asu.nlp.morphoanalysis.types.Word;
import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Sun Sep 04 13:56:10 MSK 2016
 * XML source: /home/valter/projects/nlp-cloud/extended-grammatic-vector/src/main/resources/uima_xml/extended-grammatic-vector-ts.uima_xml
 * @generated */
public class ExtendedWordRu extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(ExtendedWordRu.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected ExtendedWordRu() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public ExtendedWordRu(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public ExtendedWordRu(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public ExtendedWordRu(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: word

  /** getter for word - gets 
   * @generated
   * @return value of the feature 
   */
  public Word getWord() {
    if (ExtendedWordRu_Type.featOkTst && ((ExtendedWordRu_Type)jcasType).casFeat_word == null)
      jcasType.jcas.throwFeatMissing("word", "ru.misis.asu.nlp.extended.types.ru.ExtendedWordRu");
    return (Word)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((ExtendedWordRu_Type)jcasType).casFeatCode_word)));}
    
  /** setter for word - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setWord(Word v) {
    if (ExtendedWordRu_Type.featOkTst && ((ExtendedWordRu_Type)jcasType).casFeat_word == null)
      jcasType.jcas.throwFeatMissing("word", "ru.misis.asu.nlp.extended.types.ru.ExtendedWordRu");
    jcasType.ll_cas.ll_setRefValue(addr, ((ExtendedWordRu_Type)jcasType).casFeatCode_word, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: preposition

  /** getter for preposition - gets 
   * @generated
   * @return value of the feature 
   */
  public PrepositionRu getPreposition() {
    if (ExtendedWordRu_Type.featOkTst && ((ExtendedWordRu_Type)jcasType).casFeat_preposition == null)
      jcasType.jcas.throwFeatMissing("preposition", "ru.misis.asu.nlp.extended.types.ru.ExtendedWordRu");
    return (PrepositionRu)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((ExtendedWordRu_Type)jcasType).casFeatCode_preposition)));}
    
  /** setter for preposition - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setPreposition(PrepositionRu v) {
    if (ExtendedWordRu_Type.featOkTst && ((ExtendedWordRu_Type)jcasType).casFeat_preposition == null)
      jcasType.jcas.throwFeatMissing("preposition", "ru.misis.asu.nlp.extended.types.ru.ExtendedWordRu");
    jcasType.ll_cas.ll_setRefValue(addr, ((ExtendedWordRu_Type)jcasType).casFeatCode_preposition, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: negativeParticle

  /** getter for negativeParticle - gets 
   * @generated
   * @return value of the feature 
   */
  public NegativeParticleRu getNegativeParticle() {
    if (ExtendedWordRu_Type.featOkTst && ((ExtendedWordRu_Type)jcasType).casFeat_negativeParticle == null)
      jcasType.jcas.throwFeatMissing("negativeParticle", "ru.misis.asu.nlp.extended.types.ru.ExtendedWordRu");
    return (NegativeParticleRu)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((ExtendedWordRu_Type)jcasType).casFeatCode_negativeParticle)));}
    
  /** setter for negativeParticle - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setNegativeParticle(NegativeParticleRu v) {
    if (ExtendedWordRu_Type.featOkTst && ((ExtendedWordRu_Type)jcasType).casFeat_negativeParticle == null)
      jcasType.jcas.throwFeatMissing("negativeParticle", "ru.misis.asu.nlp.extended.types.ru.ExtendedWordRu");
    jcasType.ll_cas.ll_setRefValue(addr, ((ExtendedWordRu_Type)jcasType).casFeatCode_negativeParticle, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: inclinationParticle

  /** getter for inclinationParticle - gets 
   * @generated
   * @return value of the feature 
   */
  public InclinationParticleRu getInclinationParticle() {
    if (ExtendedWordRu_Type.featOkTst && ((ExtendedWordRu_Type)jcasType).casFeat_inclinationParticle == null)
      jcasType.jcas.throwFeatMissing("inclinationParticle", "ru.misis.asu.nlp.extended.types.ru.ExtendedWordRu");
    return (InclinationParticleRu)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((ExtendedWordRu_Type)jcasType).casFeatCode_inclinationParticle)));}
    
  /** setter for inclinationParticle - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setInclinationParticle(InclinationParticleRu v) {
    if (ExtendedWordRu_Type.featOkTst && ((ExtendedWordRu_Type)jcasType).casFeat_inclinationParticle == null)
      jcasType.jcas.throwFeatMissing("inclinationParticle", "ru.misis.asu.nlp.extended.types.ru.ExtendedWordRu");
    jcasType.ll_cas.ll_setRefValue(addr, ((ExtendedWordRu_Type)jcasType).casFeatCode_inclinationParticle, jcasType.ll_cas.ll_getFSRef(v));}    
  }

    