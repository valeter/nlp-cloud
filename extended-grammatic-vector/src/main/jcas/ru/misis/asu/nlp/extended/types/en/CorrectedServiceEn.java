

/* First created by JCasGen Sun Sep 04 13:56:10 MSK 2016 */
package ru.misis.asu.nlp.extended.types.en;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import ru.misis.asu.nlp.extended.types.CorrectedService;
import ru.misis.asu.nlp.tagging.types.Word;


/** 
 * Updated by JCasGen Sun Sep 04 13:56:10 MSK 2016
 * XML source: /home/valter/projects/nlp-cloud/extended-grammatic-vector/src/main/resources/uima_xml/extended-grammatic-vector-ts.uima_xml
 * @generated */
public class CorrectedServiceEn extends CorrectedService {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(CorrectedServiceEn.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected CorrectedServiceEn() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public CorrectedServiceEn(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public CorrectedServiceEn(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public CorrectedServiceEn(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: word

  /** getter for word - gets 
   * @generated
   * @return value of the feature 
   */
  public Word getWord() {
    if (CorrectedServiceEn_Type.featOkTst && ((CorrectedServiceEn_Type)jcasType).casFeat_word == null)
      jcasType.jcas.throwFeatMissing("word", "ru.misis.asu.nlp.extended.types.en.CorrectedServiceEn");
    return (Word)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((CorrectedServiceEn_Type)jcasType).casFeatCode_word)));}
    
  /** setter for word - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setWord(Word v) {
    if (CorrectedServiceEn_Type.featOkTst && ((CorrectedServiceEn_Type)jcasType).casFeat_word == null)
      jcasType.jcas.throwFeatMissing("word", "ru.misis.asu.nlp.extended.types.en.CorrectedServiceEn");
    jcasType.ll_cas.ll_setRefValue(addr, ((CorrectedServiceEn_Type)jcasType).casFeatCode_word, jcasType.ll_cas.ll_getFSRef(v));}    
  }

    