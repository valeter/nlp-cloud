
/* First created by JCasGen Sun Sep 04 13:56:10 MSK 2016 */
package ru.misis.asu.nlp.extended.types.en;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Sun Sep 04 13:56:10 MSK 2016
 * @generated */
public class ExtendedWordEn_Type extends Annotation_Type {
  /** @generated 
   * @return the generator for this type
   */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (ExtendedWordEn_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = ExtendedWordEn_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new ExtendedWordEn(addr, ExtendedWordEn_Type.this);
  			   ExtendedWordEn_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new ExtendedWordEn(addr, ExtendedWordEn_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = ExtendedWordEn.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
 
  /** @generated */
  final Feature casFeat_word;
  /** @generated */
  final int     casFeatCode_word;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getWord(int addr) {
        if (featOkTst && casFeat_word == null)
      jcas.throwFeatMissing("word", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    return ll_cas.ll_getRefValue(addr, casFeatCode_word);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setWord(int addr, int v) {
        if (featOkTst && casFeat_word == null)
      jcas.throwFeatMissing("word", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    ll_cas.ll_setRefValue(addr, casFeatCode_word, v);}
    
  
 
  /** @generated */
  final Feature casFeat_personalPronoun;
  /** @generated */
  final int     casFeatCode_personalPronoun;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getPersonalPronoun(int addr) {
        if (featOkTst && casFeat_personalPronoun == null)
      jcas.throwFeatMissing("personalPronoun", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    return ll_cas.ll_getRefValue(addr, casFeatCode_personalPronoun);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setPersonalPronoun(int addr, int v) {
        if (featOkTst && casFeat_personalPronoun == null)
      jcas.throwFeatMissing("personalPronoun", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    ll_cas.ll_setRefValue(addr, casFeatCode_personalPronoun, v);}
    
  
 
  /** @generated */
  final Feature casFeat_possessivePronoun;
  /** @generated */
  final int     casFeatCode_possessivePronoun;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getPossessivePronoun(int addr) {
        if (featOkTst && casFeat_possessivePronoun == null)
      jcas.throwFeatMissing("possessivePronoun", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    return ll_cas.ll_getRefValue(addr, casFeatCode_possessivePronoun);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setPossessivePronoun(int addr, int v) {
        if (featOkTst && casFeat_possessivePronoun == null)
      jcas.throwFeatMissing("possessivePronoun", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    ll_cas.ll_setRefValue(addr, casFeatCode_possessivePronoun, v);}
    
  
 
  /** @generated */
  final Feature casFeat_possessiveEnding;
  /** @generated */
  final int     casFeatCode_possessiveEnding;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getPossessiveEnding(int addr) {
        if (featOkTst && casFeat_possessiveEnding == null)
      jcas.throwFeatMissing("possessiveEnding", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    return ll_cas.ll_getRefValue(addr, casFeatCode_possessiveEnding);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setPossessiveEnding(int addr, int v) {
        if (featOkTst && casFeat_possessiveEnding == null)
      jcas.throwFeatMissing("possessiveEnding", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    ll_cas.ll_setRefValue(addr, casFeatCode_possessiveEnding, v);}
    
  
 
  /** @generated */
  final Feature casFeat_negativeParticle;
  /** @generated */
  final int     casFeatCode_negativeParticle;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getNegativeParticle(int addr) {
        if (featOkTst && casFeat_negativeParticle == null)
      jcas.throwFeatMissing("negativeParticle", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    return ll_cas.ll_getRefValue(addr, casFeatCode_negativeParticle);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setNegativeParticle(int addr, int v) {
        if (featOkTst && casFeat_negativeParticle == null)
      jcas.throwFeatMissing("negativeParticle", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    ll_cas.ll_setRefValue(addr, casFeatCode_negativeParticle, v);}
    
  
 
  /** @generated */
  final Feature casFeat_article;
  /** @generated */
  final int     casFeatCode_article;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getArticle(int addr) {
        if (featOkTst && casFeat_article == null)
      jcas.throwFeatMissing("article", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    return ll_cas.ll_getRefValue(addr, casFeatCode_article);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setArticle(int addr, int v) {
        if (featOkTst && casFeat_article == null)
      jcas.throwFeatMissing("article", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    ll_cas.ll_setRefValue(addr, casFeatCode_article, v);}
    
  
 
  /** @generated */
  final Feature casFeat_preposition;
  /** @generated */
  final int     casFeatCode_preposition;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getPreposition(int addr) {
        if (featOkTst && casFeat_preposition == null)
      jcas.throwFeatMissing("preposition", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    return ll_cas.ll_getRefValue(addr, casFeatCode_preposition);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setPreposition(int addr, int v) {
        if (featOkTst && casFeat_preposition == null)
      jcas.throwFeatMissing("preposition", "ru.misis.asu.nlp.extended.types.en.ExtendedWordEn");
    ll_cas.ll_setRefValue(addr, casFeatCode_preposition, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	 * @generated
	 * @param jcas JCas
	 * @param casType Type 
	 */
  public ExtendedWordEn_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_word = jcas.getRequiredFeatureDE(casType, "word", "ru.misis.asu.nlp.tagging.types.Word", featOkTst);
    casFeatCode_word  = (null == casFeat_word) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_word).getCode();

 
    casFeat_personalPronoun = jcas.getRequiredFeatureDE(casType, "personalPronoun", "ru.misis.asu.nlp.extended.types.en.PersonalPronounEn", featOkTst);
    casFeatCode_personalPronoun  = (null == casFeat_personalPronoun) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_personalPronoun).getCode();

 
    casFeat_possessivePronoun = jcas.getRequiredFeatureDE(casType, "possessivePronoun", "ru.misis.asu.nlp.extended.types.en.PossessivePronounEn", featOkTst);
    casFeatCode_possessivePronoun  = (null == casFeat_possessivePronoun) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_possessivePronoun).getCode();

 
    casFeat_possessiveEnding = jcas.getRequiredFeatureDE(casType, "possessiveEnding", "ru.misis.asu.nlp.extended.types.en.PossessiveEndingEn", featOkTst);
    casFeatCode_possessiveEnding  = (null == casFeat_possessiveEnding) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_possessiveEnding).getCode();

 
    casFeat_negativeParticle = jcas.getRequiredFeatureDE(casType, "negativeParticle", "ru.misis.asu.nlp.extended.types.en.NegativeParticleEn", featOkTst);
    casFeatCode_negativeParticle  = (null == casFeat_negativeParticle) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_negativeParticle).getCode();

 
    casFeat_article = jcas.getRequiredFeatureDE(casType, "article", "ru.misis.asu.nlp.extended.types.en.ArticleEn", featOkTst);
    casFeatCode_article  = (null == casFeat_article) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_article).getCode();

 
    casFeat_preposition = jcas.getRequiredFeatureDE(casType, "preposition", "ru.misis.asu.nlp.extended.types.en.PrepositionEn", featOkTst);
    casFeatCode_preposition  = (null == casFeat_preposition) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_preposition).getCode();

  }
}



    