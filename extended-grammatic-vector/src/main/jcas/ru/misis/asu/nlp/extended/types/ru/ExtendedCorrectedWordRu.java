

/* First created by JCasGen Sun Sep 04 13:56:10 MSK 2016 */
package ru.misis.asu.nlp.extended.types.ru;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import ru.misis.asu.nlp.morphoanalysis.types.CorrectedWord;
import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Sun Sep 04 13:56:10 MSK 2016
 * XML source: /home/valter/projects/nlp-cloud/extended-grammatic-vector/src/main/resources/uima_xml/extended-grammatic-vector-ts.uima_xml
 * @generated */
public class ExtendedCorrectedWordRu extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(ExtendedCorrectedWordRu.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected ExtendedCorrectedWordRu() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public ExtendedCorrectedWordRu(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public ExtendedCorrectedWordRu(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public ExtendedCorrectedWordRu(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: word

  /** getter for word - gets 
   * @generated
   * @return value of the feature 
   */
  public CorrectedWord getWord() {
    if (ExtendedCorrectedWordRu_Type.featOkTst && ((ExtendedCorrectedWordRu_Type)jcasType).casFeat_word == null)
      jcasType.jcas.throwFeatMissing("word", "ru.misis.asu.nlp.extended.types.ru.ExtendedCorrectedWordRu");
    return (CorrectedWord)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((ExtendedCorrectedWordRu_Type)jcasType).casFeatCode_word)));}
    
  /** setter for word - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setWord(CorrectedWord v) {
    if (ExtendedCorrectedWordRu_Type.featOkTst && ((ExtendedCorrectedWordRu_Type)jcasType).casFeat_word == null)
      jcasType.jcas.throwFeatMissing("word", "ru.misis.asu.nlp.extended.types.ru.ExtendedCorrectedWordRu");
    jcasType.ll_cas.ll_setRefValue(addr, ((ExtendedCorrectedWordRu_Type)jcasType).casFeatCode_word, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: preposition

  /** getter for preposition - gets 
   * @generated
   * @return value of the feature 
   */
  public CorrectedPrepositionRu getPreposition() {
    if (ExtendedCorrectedWordRu_Type.featOkTst && ((ExtendedCorrectedWordRu_Type)jcasType).casFeat_preposition == null)
      jcasType.jcas.throwFeatMissing("preposition", "ru.misis.asu.nlp.extended.types.ru.ExtendedCorrectedWordRu");
    return (CorrectedPrepositionRu)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((ExtendedCorrectedWordRu_Type)jcasType).casFeatCode_preposition)));}
    
  /** setter for preposition - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setPreposition(CorrectedPrepositionRu v) {
    if (ExtendedCorrectedWordRu_Type.featOkTst && ((ExtendedCorrectedWordRu_Type)jcasType).casFeat_preposition == null)
      jcasType.jcas.throwFeatMissing("preposition", "ru.misis.asu.nlp.extended.types.ru.ExtendedCorrectedWordRu");
    jcasType.ll_cas.ll_setRefValue(addr, ((ExtendedCorrectedWordRu_Type)jcasType).casFeatCode_preposition, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: negativeParticle

  /** getter for negativeParticle - gets 
   * @generated
   * @return value of the feature 
   */
  public CorrectedNegativeParticleRu getNegativeParticle() {
    if (ExtendedCorrectedWordRu_Type.featOkTst && ((ExtendedCorrectedWordRu_Type)jcasType).casFeat_negativeParticle == null)
      jcasType.jcas.throwFeatMissing("negativeParticle", "ru.misis.asu.nlp.extended.types.ru.ExtendedCorrectedWordRu");
    return (CorrectedNegativeParticleRu)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((ExtendedCorrectedWordRu_Type)jcasType).casFeatCode_negativeParticle)));}
    
  /** setter for negativeParticle - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setNegativeParticle(CorrectedNegativeParticleRu v) {
    if (ExtendedCorrectedWordRu_Type.featOkTst && ((ExtendedCorrectedWordRu_Type)jcasType).casFeat_negativeParticle == null)
      jcasType.jcas.throwFeatMissing("negativeParticle", "ru.misis.asu.nlp.extended.types.ru.ExtendedCorrectedWordRu");
    jcasType.ll_cas.ll_setRefValue(addr, ((ExtendedCorrectedWordRu_Type)jcasType).casFeatCode_negativeParticle, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: inclinationParticle

  /** getter for inclinationParticle - gets 
   * @generated
   * @return value of the feature 
   */
  public CorrectedInclinationParticleRu getInclinationParticle() {
    if (ExtendedCorrectedWordRu_Type.featOkTst && ((ExtendedCorrectedWordRu_Type)jcasType).casFeat_inclinationParticle == null)
      jcasType.jcas.throwFeatMissing("inclinationParticle", "ru.misis.asu.nlp.extended.types.ru.ExtendedCorrectedWordRu");
    return (CorrectedInclinationParticleRu)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((ExtendedCorrectedWordRu_Type)jcasType).casFeatCode_inclinationParticle)));}
    
  /** setter for inclinationParticle - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setInclinationParticle(CorrectedInclinationParticleRu v) {
    if (ExtendedCorrectedWordRu_Type.featOkTst && ((ExtendedCorrectedWordRu_Type)jcasType).casFeat_inclinationParticle == null)
      jcasType.jcas.throwFeatMissing("inclinationParticle", "ru.misis.asu.nlp.extended.types.ru.ExtendedCorrectedWordRu");
    jcasType.ll_cas.ll_setRefValue(addr, ((ExtendedCorrectedWordRu_Type)jcasType).casFeatCode_inclinationParticle, jcasType.ll_cas.ll_getFSRef(v));}    
  }

    