package ru.misis.asu.nlp.morphoanalysis.resource;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.*;
import java.nio.charset.Charset;
import java.util.zip.ZipInputStream;

import static java.lang.System.currentTimeMillis;

public class XmlDictionaryParser {
    public static OpenCorporaMorphDictionaryImpl parse(InputStream in) throws IOException,
            SAXException, ParserConfigurationException {
        SAXParser xmlParser = SAXParserFactory.newInstance().newSAXParser();
        XMLReader xmlReader = xmlParser.getXMLReader();

        DictionaryXmlHandler dictHandler = new DictionaryXmlHandler();
        dictHandler.addLemmaFilter(new LemmaByGrammemFilter("Surn", "Patr",
                "Orgn"));

        xmlReader.setContentHandler(dictHandler);
        InputSource xmlSource = new InputSource(in);
        System.out.println("About to parse uima_xml dictionary file");
        long timeBefore = currentTimeMillis();
        xmlReader.parse(xmlSource);
        System.out.println("Parsing finished in "
                + (currentTimeMillis() - timeBefore) + " ms");
        return dictHandler.getDictionary();
    }

    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            System.err
                    .println("Usage: <uima_xml-dictionary-file> <serialized-output-path>");
            return;
        }

        File dictXmlFile = new File(args[0]);
        if (!dictXmlFile.isFile()) {
            throw new IllegalStateException(dictXmlFile + " does not exist");
        }
        File outPath = new File(args[1]);

        OpenCorporaMorphDictionaryImpl dict = parse(new FileInputStream(dictXmlFile));

        System.out.println("Preparing to serialization...");
        long timeBefore = currentTimeMillis();
        OutputStream fout = new BufferedOutputStream(new FileOutputStream(
                outPath), 8192 * 8);
        ObjectOutputStream out = new ObjectOutputStream(fout);
        try {
            out.writeObject(dict);
        } finally {
            out.close();
        }
        System.out.println("Serialization finished in "
                + (currentTimeMillis() - timeBefore) + " ms");
    }
}