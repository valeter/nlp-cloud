package ru.misis.asu.nlp.morphoanalysis.resource;

import org.apache.uima.resource.DataResource;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.SharedResourceObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;

import static java.lang.System.currentTimeMillis;

/**
 * @author Rinat Gareev (Kazan Federal University)
 */
public class SerializedDictionaryResource implements SharedResourceObject {
    private static final int DICTIONARY_READING_BUFFER_SIZE = 1024 * 1024;

    private OpenCorporaMorphDictionary dict;

    @Override
    public void load(DataResource dr) throws ResourceInitializationException {
        System.out.println("About to deserialize OpenCorporaMorphDictionary...");
        try {
            long timeBefore = currentTimeMillis();
            InputStream in = new BufferedInputStream(dr.getInputStream(),
                    DICTIONARY_READING_BUFFER_SIZE);
            ObjectInputStream ois = new ObjectInputStream(in);
            try {
                dict = (OpenCorporaMorphDictionary) ois.readObject();
            } finally {
                ois.close();
            }
            System.out.println("Deserialization of OpenCorporaMorphDictionary finished in " + (currentTimeMillis() - timeBefore) + " ms");
        } catch (Exception e) {
            throw new ResourceInitializationException(e);
        }
    }

    public OpenCorporaMorphDictionary getDictionary() {
        dict.setWfPredictor(new DummyWordformPredictor(dict));
        return dict;
    }
}