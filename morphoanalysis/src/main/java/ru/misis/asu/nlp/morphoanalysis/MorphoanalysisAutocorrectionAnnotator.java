package ru.misis.asu.nlp.morphoanalysis;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.LongArray;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceAccessException;
import org.apache.uima.resource.ResourceConfigurationException;
import org.apache.uima.resource.ResourceInitializationException;
import ru.misis.asu.nlp.commons.Pair;
import ru.misis.asu.nlp.commons.cas.FSUtils;
import ru.misis.asu.nlp.commons.exceptions.ExceptionHandler;
import ru.misis.asu.nlp.commons.algo.LevenshteinGenerator;
import ru.misis.asu.nlp.commons.ngram.NgramDictionary;
import ru.misis.asu.nlp.morphoanalysis.model.Lemma;
import ru.misis.asu.nlp.morphoanalysis.model.Wordform;
import ru.misis.asu.nlp.morphoanalysis.model.Wordform.Builder;
import ru.misis.asu.nlp.commons.ngram.RuNgramDictionary;
import ru.misis.asu.nlp.morphoanalysis.resource.OpenCorporaMorphDictionary;
import ru.misis.asu.nlp.morphoanalysis.resource.SerializedDictionaryResource;
import ru.misis.asu.nlp.morphoanalysis.types.CorrectedWord;
import ru.misis.asu.nlp.morphoanalysis.types.Word;
import ru.misis.asu.nlp.tokenization.types.ComplexLetters;
import ru.misis.asu.nlp.tokenization.types.Token;

import java.util.*;
import java.util.stream.Collectors;

public class MorphoanalysisAutocorrectionAnnotator extends JCasAnnotator_ImplBase {
    private static final String LETTERS_TYPE_PARAM = "LettersType";
    private static final String SENTENCE_TYPE_PARAM = "SentenceType";
    private static final String MORPH_DICTIONARY_RESOURCE = "MorphDictionary";

    private OpenCorporaMorphDictionary dict;
    private LevenshteinGenerator levenshteinGenerator;
    private NgramDictionary ruNgramDictionary = NgramDictionary.Instance.RU.get();

    private String lettersTypeName;
    private String sentenceTypeName;

    @Override
    public void reconfigure() throws ResourceConfigurationException,
            ResourceInitializationException {
        super.reconfigure();
    }

    @Override
    public void initialize(UimaContext ctx)
            throws ResourceInitializationException {
        super.initialize(ctx);
        lettersTypeName = (String) ctx.getConfigParameterValue(LETTERS_TYPE_PARAM);
        sentenceTypeName = (String) ctx.getConfigParameterValue(SENTENCE_TYPE_PARAM);
        try {
            SerializedDictionaryResource dictResource = (SerializedDictionaryResource) ctx
                    .getResourceObject(MORPH_DICTIONARY_RESOURCE);
            dict = dictResource.getDictionary();
            dict.init();
        } catch (ResourceAccessException e) {
            ExceptionHandler.logAndRethrow(ctx.getLogger(), "Morph dictionary initialization error: ", e);
        }

        levenshteinGenerator = dict.getLevenshteinGenerator(1);
    }

    @Override
    public void process(JCas cas) {
        Type lettersType = cas.getTypeSystem().getType(lettersTypeName);
        Type sentenceType = cas.getTypeSystem().getType(sentenceTypeName);

        AnnotationIndex<Annotation> words = cas.getAnnotationIndex(lettersType);
        AnnotationIndex<Annotation> sentences = cas.getAnnotationIndex(sentenceType);
        int s = 0;
        for (Annotation sentence : sentences) {
            System.out.println("Sentence " + ++s + "[" + sentence.getCoveredText() + "]: ");
            System.out.println();

            long start = System.currentTimeMillis();
            Iterator<Annotation> tokens = words.subiterator(sentence);
            List<Pair<List<String>, Token>> possibleCorrections = getPossibleCorrections(tokens);
            System.out.println();
            System.out.println("Finished generating possible corrections in " + (System.currentTimeMillis() - start) + " ms");
            System.out.println();
            System.out.println("Possible corrections: ");
            for (Pair<List<String>, Token> possibleCorrection : possibleCorrections) {
                System.out.println("\ttoken: " + possibleCorrection.getSecond().getCoveredText() + "; corrections: " + possibleCorrection.getFirst());
            }

            start = System.currentTimeMillis();
            List<List<String>> filteredCorrections = filterWithNGramDictWithoutTags(possibleCorrections.stream()
                    .map(Pair::getFirst)
                    .collect(Collectors.toList()));

            System.out.println();
            int sum = 0;
            for (int i = 0; i < possibleCorrections.size(); i++) {
                if (filteredCorrections.get(i).size() < possibleCorrections.get(i).getFirst().size()) {
                    int correctionsFiltered = possibleCorrections.get(i).getFirst().size() - filteredCorrections.get(i).size();
                    System.out.println("For token: " + possibleCorrections.get(i).getSecond().getCoveredText() +
                            " ngrams filtered " + correctionsFiltered + " corrections, " +
                            filteredCorrections.get(i).size() + " corrections left"
                    );
                    sum += correctionsFiltered;
                }
            }
            System.out.println();
            System.out.println("Total corrections filtered by ngrams: " + sum);
            System.out.println();

            System.out.println("Finished filtering sentences with simple n-gram dictionary in " + (System.currentTimeMillis() - start) + " ms");
            List<List<List<Wordform>>> analyzed = filteredCorrections.stream()
                    .map(l -> l.stream().map(c -> {
                        List<Wordform> wordforms = dict.getEntries(c);
                        if (wordforms == null || wordforms.isEmpty()) {
                            return Collections.singletonList(new Wordform(-1, new BitSet()));
                        }
                        return wordforms;
                    }).collect(Collectors.toList()))
                    .collect(Collectors.toList());

            for (int j = 0; j < possibleCorrections.size(); j++) {
                Token token = possibleCorrections.get(j).getSecond();
                System.out.println("Word [" + token.getNorm() + "]  wordforms: [");
                List<String> corrections = filteredCorrections.get(j);
                List<List<Wordform>> analyzedWords = analyzed.get(j);

                for (int i = 0; i < corrections.size(); i++) {
                    System.out.println("\t [");
                    System.out.println("\t\t correction:\"" + corrections.get(i) + "\",");
                    System.out.println("\t\t wordforms:[");
                    for (Wordform wordform : analyzedWords.get(i)) {
                        StringBuilder grammems = new StringBuilder();
                        grammems.append(dict.getPos(getLemma(wordform.getLemmaId(), token.getCoveredText()))).append(",");
                        BitSet grammemBits = wordform.getGrammems();
                        int setBit = 0;
                        while ((setBit = grammemBits.nextSetBit(setBit)) >= 0) {
                            grammems.append(dict.getGrammem(setBit).getAlias()).append(",");
                            setBit++;
                        }
                        if (grammems.length() > 0) {
                            grammems.deleteCharAt(grammems.length() - 1);
                        }
                        System.out.println("\t\t\t wordform=[" + grammems.toString() + "]");
                    }
                    System.out.println("\t\t ]");
                    System.out.println("\t ]");
                }
                System.out.println("]");


                if (corrections.size() == 1) {
                    makeCorrectedWordAnnotation(cas, token, analyzedWords.get(0), corrections.get(0));
                } else {
                    List<Word> correctedWords = new ArrayList<>();
                    for (int i = 0; i < corrections.size(); i++) {
                        List<Wordform> wfDictEntries = analyzedWords.get(i);
                        correctedWords.add(makeWordAnnotation(cas, token, wfDictEntries));
                    }
                    makeCorrectedWordAnnotation(cas, (Token) token, correctedWords, corrections);
                }
            }
        }
    }

    private List<Pair<List<String>, Token>> getPossibleCorrections(Iterator<Annotation> tokens) {
        List<Pair<List<String>, Token>> possibleCorrections = new ArrayList<>();
        for (; tokens.hasNext(); ) {
            Token next = (Token) tokens.next();
            String word = next.getNorm();
            if (!dict.contains(word)) {
                Collection<String> corrections = levenshteinGenerator.generate(word);

                if (next instanceof ComplexLetters) {
                    corrections.addAll(getComplexWordCorrections((ComplexLetters) next));
                }

                if (corrections.isEmpty()) {
                    corrections.add(word);
                }
                System.out.println("Word missing in dictionary: " + word);
                System.out.println("Replacements: [");
                corrections.forEach(System.out::println);
                System.out.println("]");
                possibleCorrections.add(new Pair<>(new ArrayList<>(corrections), next));
            } else {
                possibleCorrections.add(new Pair<>(Collections.singletonList("i".equals(word) ? "I" : word), next));
            }
        }
        return possibleCorrections;
    }

    private Collection<String> getComplexWordCorrections(ComplexLetters complexWord) {
        Collection<String> leftCorrections = null;
        boolean needCorrectLeft = !dict.contains(complexWord.getLeft());
        Collection<String> rightCorrections = null;
        boolean needCorrectRight = !dict.contains(complexWord.getRight());

        if (needCorrectLeft) {
            leftCorrections = levenshteinGenerator.generate(complexWord.getLeft());
        }
        if (needCorrectRight) {
            rightCorrections = levenshteinGenerator.generate(complexWord.getRight());
        }

        if (!needCorrectLeft && !needCorrectRight) {
            return Collections.singletonList(complexWord.getNorm());
        }

        if (leftCorrections == null || leftCorrections.isEmpty()) {
            leftCorrections = Collections.singletonList(complexWord.getLeft());
        }

        if (rightCorrections == null || rightCorrections.isEmpty()) {
            rightCorrections = Collections.singletonList(complexWord.getRight());
        }

        List<String> corrections = new ArrayList<>();
        for (String leftCorrection : leftCorrections) {
            corrections.addAll(rightCorrections.stream()
                    .map(rightCorrection -> leftCorrection + complexWord.getSeparator() + rightCorrection)
                    .collect(Collectors.toList()));
        }
        return corrections;
    }

    private List<List<String>> filterWithNGramDictWithoutTags(List<List<String>> arg) {
        List<List<String>> result = new ArrayList<>(arg.size());
        for (List<String> strings : arg) {
            result.add(new ArrayList<>(strings));
        }
        for (int i = 0; i < result.size(); i++) {
            if (result.get(i).size() == 1)
                continue;

            Set<Integer> indsToRemove = new TreeSet<>(Comparator.reverseOrder());
            Set<Integer> semiGoodInds = new TreeSet<>(Comparator.reverseOrder());
            for (int j = result.get(i).size() - 1; j >= 0; j--) {
                int quality = 0;

                String word = result.get(i).get(j);
                if (i - 1 >= 0) {
                    for (int k = 0; k < result.get(i - 1).size(); k++) {
                        String left = result.get(i - 1).get(k);
                        if (ruNgramDictionary.getFrequency(left, word) > 0) {
                            quality++;
                            break;
                        }
                    }
                }
                if (i + 1 < result.size()) {
                    for (int k = 0; k < result.get(i + 1).size(); k++) {
                        String right = result.get(i + 1).get(k);
                        if (ruNgramDictionary.getFrequency(word, right) > 0) {
                            quality++;
                            break;
                        }
                    }
                }
                if (quality == 0) {
                    indsToRemove.add(j);
                } else if (quality == 1) {
                    indsToRemove.add(j);
                    semiGoodInds.add(j);
                }
            }

            // if there's good option, remove all bad options
            // if there's semi good options and no good - leave semi good
            if (indsToRemove.size() != result.get(i).size()) {
                for (int ind : indsToRemove) {
                    result.get(i).remove(ind);
                }
            } else if (semiGoodInds.size() != 0) {
                for (int j = result.get(i).size() - 1; j >= 0; j--) {
                    if (!semiGoodInds.contains(j)) {
                        result.get(i).remove(j);
                    }
                }
            }
        }
        return result;
    }

    private void makeCorrectedWordAnnotation(JCas cas, Annotation token, List<Wordform> wfDictEntries, String correction) {
        CorrectedWord correctedWord = new CorrectedWord(cas);
        correctedWord.setBegin(token.getBegin());
        correctedWord.setEnd(token.getEnd());
        correctedWord.setToken((Token) token);
        Word word = makeWordAnnotation(cas, token, wfDictEntries);
        correctedWord.setWords(FSUtils.toFSArray(cas, Collections.singletonList(word)));
        correctedWord.setCorrections(FSUtils.toStringArray(cas, Collections.singletonList(correction)));
        correctedWord.addToIndexes();
    }

    private void makeCorrectedWordAnnotation(JCas cas, Annotation token, List<Word> words, Collection<String> corrections) {
        CorrectedWord correctedWord = new CorrectedWord(cas);
        correctedWord.setBegin(token.getBegin());
        correctedWord.setEnd(token.getEnd());
        correctedWord.setToken((Token) token);
        correctedWord.setWords(FSUtils.toFSArray(cas, words));
        correctedWord.setCorrections(FSUtils.toStringArray(cas, corrections));
        correctedWord.addToIndexes();
    }

    private Word makeWordAnnotation(JCas cas, Annotation token, List<Wordform> wfDictEntries) {
        Word word = new Word(cas);
        word.setBegin(token.getBegin());
        word.setEnd(token.getEnd());
        List<ru.misis.asu.nlp.morphoanalysis.types.Wordform> casWfList = new LinkedList<>();
        for (Wordform wf : wfDictEntries) {
            ru.misis.asu.nlp.morphoanalysis.types.Wordform casWf = new ru.misis.asu.nlp.morphoanalysis.types.Wordform(
                    cas);

            BitSet grammems = wf.getGrammems();
            int lemmaId = wf.getLemmaId();
            Lemma lemma = getLemma(lemmaId, token.getCoveredText());
            casWf.setLemmaId(lemmaId);
            casWf.setLemma(lemma.getString());
            casWf.setPos(dict.getPos(lemma));
            grammems.or(lemma.getGrammems());
            grammems.andNot(dict.getPosBits());
            List<String> gramSet = dict.toGramSet(grammems);
            casWf.setGrammems(FSUtils.toStringArray(cas, gramSet));

            Builder wfBuilder = Wordform.builder(dict, 0);
            for (String grammeme : gramSet) {
                wfBuilder = wfBuilder.addGrammeme(grammeme);
            }
            wfBuilder.addGrammeme(dict.getPos(lemma));
            long[] longarr = wfBuilder.buildSimple().getGrammems()
                    .toLongArray();
            LongArray la = new LongArray(cas, longarr.length);
            la.copyFromArray(longarr, 0, 0, longarr.length);
            casWf.setGrammemBits(la);

            casWfList.add(casWf);
        }
        word.setWordforms(FSUtils.toFSArray(cas, casWfList));

        return word;
    }

    private Lemma getLemma(int id, String word) {
        Lemma lemma;
        if (id < 0) {
            lemma = new Lemma(word, new BitSet());
            lemma.setId(id);
        } else {
            lemma = dict.getLemma(id);
        }
        return lemma;
    }
}
