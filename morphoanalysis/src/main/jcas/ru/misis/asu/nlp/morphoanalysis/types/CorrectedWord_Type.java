/* First created by JCasGen Sun Feb 07 11:18:56 MSK 2016 */
package ru.misis.asu.nlp.morphoanalysis.types;

import org.apache.uima.cas.Feature;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.tcas.Annotation_Type;

/**
 * Updated by JCasGen Sun Feb 07 11:18:56 MSK 2016
 *
 * @generated
 */
public class CorrectedWord_Type extends Annotation_Type {
    /**
     * @return the generator for this type
     * @generated
     */
    @Override
    protected FSGenerator getFSGenerator() {
        return fsGenerator;
    }

    /**
     * @generated
     */
    private final FSGenerator fsGenerator =
            new FSGenerator() {
                public FeatureStructure createFS(int addr, CASImpl cas) {
                    if (CorrectedWord_Type.this.useExistingInstance) {
                        // Return eq fs instance if already created
                        FeatureStructure fs = CorrectedWord_Type.this.jcas.getJfsFromCaddr(addr);
                        if (null == fs) {
                            fs = new CorrectedWord(addr, CorrectedWord_Type.this);
                            CorrectedWord_Type.this.jcas.putJfsFromCaddr(addr, fs);
                            return fs;
                        }
                        return fs;
                    } else return new CorrectedWord(addr, CorrectedWord_Type.this);
                }
            };
    /**
     * @generated
     */
    @SuppressWarnings("hiding")
    public final static int typeIndexID = CorrectedWord.typeIndexID;
    /**
     * @generated
     * @modifiable
     */
    @SuppressWarnings("hiding")
    public final static boolean featOkTst = JCasRegistry.getFeatOkTst("ru.misis.asu.nlp.morphoanalysis.types.CorrectedWord");

    /**
     * @generated
     */
    final Feature casFeat_token;
    /**
     * @generated
     */
    final int casFeatCode_token;

    /**
     * @param addr low level Feature Structure reference
     * @return the feature value
     * @generated
     */
    public int getToken(int addr) {
        if (featOkTst && casFeat_token == null)
            jcas.throwFeatMissing("token", "ru.misis.asu.nlp.morphoanalysis.types.CorrectedWord");
        return ll_cas.ll_getRefValue(addr, casFeatCode_token);
    }

    /**
     * @param addr low level Feature Structure reference
     * @param v    value to set
     * @generated
     */
    public void setToken(int addr, int v) {
        if (featOkTst && casFeat_token == null)
            jcas.throwFeatMissing("token", "ru.misis.asu.nlp.morphoanalysis.types.CorrectedWord");
        ll_cas.ll_setRefValue(addr, casFeatCode_token, v);
    }


    /**
     * @generated
     */
    final Feature casFeat_words;
    /**
     * @generated
     */
    final int casFeatCode_words;

    /**
     * @param addr low level Feature Structure reference
     * @return the feature value
     * @generated
     */
    public int getWords(int addr) {
        if (featOkTst && casFeat_words == null)
            jcas.throwFeatMissing("words", "ru.misis.asu.nlp.morphoanalysis.types.CorrectedWord");
        return ll_cas.ll_getRefValue(addr, casFeatCode_words);
    }

    /**
     * @param addr low level Feature Structure reference
     * @param v    value to set
     * @generated
     */
    public void setWords(int addr, int v) {
        if (featOkTst && casFeat_words == null)
            jcas.throwFeatMissing("words", "ru.misis.asu.nlp.morphoanalysis.types.CorrectedWord");
        ll_cas.ll_setRefValue(addr, casFeatCode_words, v);
    }

    /**
     * @param addr low level Feature Structure reference
     * @param i    index of item in the array
     * @return value at index i in the array
     * @generated
     */
    public int getWords(int addr, int i) {
        if (featOkTst && casFeat_words == null)
            jcas.throwFeatMissing("words", "ru.misis.asu.nlp.morphoanalysis.types.CorrectedWord");
        if (lowLevelTypeChecks)
            return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_words), i, true);
        jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_words), i);
        return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_words), i);
    }

    /**
     * @param addr low level Feature Structure reference
     * @param i    index of item in the array
     * @param v    value to set
     * @generated
     */
    public void setWords(int addr, int i, int v) {
        if (featOkTst && casFeat_words == null)
            jcas.throwFeatMissing("words", "ru.misis.asu.nlp.morphoanalysis.types.CorrectedWord");
        if (lowLevelTypeChecks)
            ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_words), i, v, true);
        jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_words), i);
        ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_words), i, v);
    }


    /**
     * @generated
     */
    final Feature casFeat_corrections;
    /**
     * @generated
     */
    final int casFeatCode_corrections;

    /**
     * @param addr low level Feature Structure reference
     * @return the feature value
     * @generated
     */
    public int getCorrections(int addr) {
        if (featOkTst && casFeat_corrections == null)
            jcas.throwFeatMissing("corrections", "ru.misis.asu.nlp.morphoanalysis.types.CorrectedWord");
        return ll_cas.ll_getRefValue(addr, casFeatCode_corrections);
    }

    /**
     * @param addr low level Feature Structure reference
     * @param v    value to set
     * @generated
     */
    public void setCorrections(int addr, int v) {
        if (featOkTst && casFeat_corrections == null)
            jcas.throwFeatMissing("corrections", "ru.misis.asu.nlp.morphoanalysis.types.CorrectedWord");
        ll_cas.ll_setRefValue(addr, casFeatCode_corrections, v);
    }

    /**
     * @param addr low level Feature Structure reference
     * @param i    index of item in the array
     * @return value at index i in the array
     * @generated
     */
    public String getCorrections(int addr, int i) {
        if (featOkTst && casFeat_corrections == null)
            jcas.throwFeatMissing("corrections", "ru.misis.asu.nlp.morphoanalysis.types.CorrectedWord");
        if (lowLevelTypeChecks)
            return ll_cas.ll_getStringArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_corrections), i, true);
        jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_corrections), i);
        return ll_cas.ll_getStringArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_corrections), i);
    }

    /**
     * @param addr low level Feature Structure reference
     * @param i    index of item in the array
     * @param v    value to set
     * @generated
     */
    public void setCorrections(int addr, int i, String v) {
        if (featOkTst && casFeat_corrections == null)
            jcas.throwFeatMissing("corrections", "ru.misis.asu.nlp.morphoanalysis.types.CorrectedWord");
        if (lowLevelTypeChecks)
            ll_cas.ll_setStringArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_corrections), i, v, true);
        jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_corrections), i);
        ll_cas.ll_setStringArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_corrections), i, v);
    }


    /**
     * initialize variables to correspond with Cas Type and Features
     *
     * @param jcas    JCas
     * @param casType Type
     * @generated
     */
    public CorrectedWord_Type(JCas jcas, Type casType) {
        super(jcas, casType);
        casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl) this.casType, getFSGenerator());


        casFeat_token = jcas.getRequiredFeatureDE(casType, "token", "ru.misis.asu.nlp.tokenization.types.Token", featOkTst);
        casFeatCode_token = (null == casFeat_token) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl) casFeat_token).getCode();


        casFeat_words = jcas.getRequiredFeatureDE(casType, "words", "uima.cas.FSArray", featOkTst);
        casFeatCode_words = (null == casFeat_words) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl) casFeat_words).getCode();


        casFeat_corrections = jcas.getRequiredFeatureDE(casType, "corrections", "uima.cas.StringArray", featOkTst);
        casFeatCode_corrections = (null == casFeat_corrections) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl) casFeat_corrections).getCode();

    }
}



    