/* First created by JCasGen Sun Feb 07 11:18:56 MSK 2016 */
package ru.misis.asu.nlp.morphoanalysis.types;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.StringArray;
import org.apache.uima.jcas.cas.TOP_Type;
import org.apache.uima.jcas.tcas.Annotation;
import ru.misis.asu.nlp.tokenization.types.Token;


/**
 * Updated by JCasGen Sun Feb 07 11:18:56 MSK 2016
 * XML source: C:/Users/����/projects/NLP-Cloud/morphoanalysis/src/main/resources/uima_xml/morphoanalysis-ts.uima_xml
 *
 * @generated
 */
public class CorrectedWord extends Annotation {
    /**
     * @generated
     * @ordered
     */
    @SuppressWarnings("hiding")
    public final static int typeIndexID = JCasRegistry.register(CorrectedWord.class);
    /**
     * @generated
     * @ordered
     */
    @SuppressWarnings("hiding")
    public final static int type = typeIndexID;

    /**
     * @return index of the type
     * @generated
     */
    @Override
    public int getTypeIndexID() {
        return typeIndexID;
    }

    /**
     * Never called.  Disable default constructor
     *
     * @generated
     */
    protected CorrectedWord() {/* intentionally empty block */}

    /**
     * Internal - constructor used by generator
     *
     * @param addr low level Feature Structure reference
     * @param type the type of this Feature Structure
     * @generated
     */
    public CorrectedWord(int addr, TOP_Type type) {
        super(addr, type);
        readObject();
    }

    /**
     * @param jcas JCas to which this Feature Structure belongs
     * @generated
     */
    public CorrectedWord(JCas jcas) {
        super(jcas);
        readObject();
    }

    /**
     * @param jcas  JCas to which this Feature Structure belongs
     * @param begin offset to the begin spot in the SofA
     * @param end   offset to the end spot in the SofA
     * @generated
     */
    public CorrectedWord(JCas jcas, int begin, int end) {
        super(jcas);
        setBegin(begin);
        setEnd(end);
        readObject();
    }

    /**
     * <!-- begin-user-doc -->
     * Write your own initialization here
     * <!-- end-user-doc -->
     *
     * @generated modifiable
     */
    private void readObject() {/*default - does nothing empty block */}


    //*--------------*
    //* Feature: token

    /**
     * getter for token - gets
     *
     * @return value of the feature
     * @generated
     */
    public Token getToken() {
        if (CorrectedWord_Type.featOkTst && ((CorrectedWord_Type) jcasType).casFeat_token == null)
            jcasType.jcas.throwFeatMissing("token", "ru.misis.asu.nlp.morphoanalysis.types.CorrectedWord");
        return (Token) (jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((CorrectedWord_Type) jcasType).casFeatCode_token)));
    }

    /**
     * setter for token - sets
     *
     * @param v value to set into the feature
     * @generated
     */
    public void setToken(Token v) {
        if (CorrectedWord_Type.featOkTst && ((CorrectedWord_Type) jcasType).casFeat_token == null)
            jcasType.jcas.throwFeatMissing("token", "ru.misis.asu.nlp.morphoanalysis.types.CorrectedWord");
        jcasType.ll_cas.ll_setRefValue(addr, ((CorrectedWord_Type) jcasType).casFeatCode_token, jcasType.ll_cas.ll_getFSRef(v));
    }


    //*--------------*
    //* Feature: words

    /**
     * getter for words - gets
     *
     * @return value of the feature
     * @generated
     */
    public FSArray getWords() {
        if (CorrectedWord_Type.featOkTst && ((CorrectedWord_Type) jcasType).casFeat_words == null)
            jcasType.jcas.throwFeatMissing("words", "ru.misis.asu.nlp.morphoanalysis.types.CorrectedWord");
        return (FSArray) (jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((CorrectedWord_Type) jcasType).casFeatCode_words)));
    }

    /**
     * setter for words - sets
     *
     * @param v value to set into the feature
     * @generated
     */
    public void setWords(FSArray v) {
        if (CorrectedWord_Type.featOkTst && ((CorrectedWord_Type) jcasType).casFeat_words == null)
            jcasType.jcas.throwFeatMissing("words", "ru.misis.asu.nlp.morphoanalysis.types.CorrectedWord");
        jcasType.ll_cas.ll_setRefValue(addr, ((CorrectedWord_Type) jcasType).casFeatCode_words, jcasType.ll_cas.ll_getFSRef(v));
    }

    /**
     * indexed getter for words - gets an indexed value -
     *
     * @param i index in the array to get
     * @return value of the element at index i
     * @generated
     */
    public Word getWords(int i) {
        if (CorrectedWord_Type.featOkTst && ((CorrectedWord_Type) jcasType).casFeat_words == null)
            jcasType.jcas.throwFeatMissing("words", "ru.misis.asu.nlp.morphoanalysis.types.CorrectedWord");
        jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((CorrectedWord_Type) jcasType).casFeatCode_words), i);
        return (Word) (jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((CorrectedWord_Type) jcasType).casFeatCode_words), i)));
    }

    /**
     * indexed setter for words - sets an indexed value -
     *
     * @param i index in the array to set
     * @param v value to set into the array
     * @generated
     */
    public void setWords(int i, Word v) {
        if (CorrectedWord_Type.featOkTst && ((CorrectedWord_Type) jcasType).casFeat_words == null)
            jcasType.jcas.throwFeatMissing("words", "ru.misis.asu.nlp.morphoanalysis.types.CorrectedWord");
        jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((CorrectedWord_Type) jcasType).casFeatCode_words), i);
        jcasType.ll_cas.ll_setRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((CorrectedWord_Type) jcasType).casFeatCode_words), i, jcasType.ll_cas.ll_getFSRef(v));
    }


    //*--------------*
    //* Feature: corrections

    /**
     * getter for corrections - gets
     *
     * @return value of the feature
     * @generated
     */
    public StringArray getCorrections() {
        if (CorrectedWord_Type.featOkTst && ((CorrectedWord_Type) jcasType).casFeat_corrections == null)
            jcasType.jcas.throwFeatMissing("corrections", "ru.misis.asu.nlp.morphoanalysis.types.CorrectedWord");
        return (StringArray) (jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((CorrectedWord_Type) jcasType).casFeatCode_corrections)));
    }

    /**
     * setter for corrections - sets
     *
     * @param v value to set into the feature
     * @generated
     */
    public void setCorrections(StringArray v) {
        if (CorrectedWord_Type.featOkTst && ((CorrectedWord_Type) jcasType).casFeat_corrections == null)
            jcasType.jcas.throwFeatMissing("corrections", "ru.misis.asu.nlp.morphoanalysis.types.CorrectedWord");
        jcasType.ll_cas.ll_setRefValue(addr, ((CorrectedWord_Type) jcasType).casFeatCode_corrections, jcasType.ll_cas.ll_getFSRef(v));
    }

    /**
     * indexed getter for corrections - gets an indexed value -
     *
     * @param i index in the array to get
     * @return value of the element at index i
     * @generated
     */
    public String getCorrections(int i) {
        if (CorrectedWord_Type.featOkTst && ((CorrectedWord_Type) jcasType).casFeat_corrections == null)
            jcasType.jcas.throwFeatMissing("corrections", "ru.misis.asu.nlp.morphoanalysis.types.CorrectedWord");
        jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((CorrectedWord_Type) jcasType).casFeatCode_corrections), i);
        return jcasType.ll_cas.ll_getStringArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((CorrectedWord_Type) jcasType).casFeatCode_corrections), i);
    }

    /**
     * indexed setter for corrections - sets an indexed value -
     *
     * @param i index in the array to set
     * @param v value to set into the array
     * @generated
     */
    public void setCorrections(int i, String v) {
        if (CorrectedWord_Type.featOkTst && ((CorrectedWord_Type) jcasType).casFeat_corrections == null)
            jcasType.jcas.throwFeatMissing("corrections", "ru.misis.asu.nlp.morphoanalysis.types.CorrectedWord");
        jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((CorrectedWord_Type) jcasType).casFeatCode_corrections), i);
        jcasType.ll_cas.ll_setStringArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((CorrectedWord_Type) jcasType).casFeatCode_corrections), i, v);
    }
}

    