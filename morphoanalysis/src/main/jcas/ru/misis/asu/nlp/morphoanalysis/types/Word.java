/* First created by JCasGen Sun Feb 07 11:18:56 MSK 2016 */
package ru.misis.asu.nlp.morphoanalysis.types;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.TOP_Type;
import org.apache.uima.jcas.tcas.Annotation;
import ru.misis.asu.nlp.tokenization.types.Token;


/**
 * Updated by JCasGen Sun Feb 07 11:18:56 MSK 2016
 * XML source: C:/Users/����/projects/NLP-Cloud/morphoanalysis/src/main/resources/uima_xml/morphoanalysis-ts.uima_xml
 *
 * @generated
 */
public class Word extends Annotation {
    /**
     * @generated
     * @ordered
     */
    @SuppressWarnings("hiding")
    public final static int typeIndexID = JCasRegistry.register(Word.class);
    /**
     * @generated
     * @ordered
     */
    @SuppressWarnings("hiding")
    public final static int type = typeIndexID;

    /**
     * @return index of the type
     * @generated
     */
    @Override
    public int getTypeIndexID() {
        return typeIndexID;
    }

    /**
     * Never called.  Disable default constructor
     *
     * @generated
     */
    protected Word() {/* intentionally empty block */}

    /**
     * Internal - constructor used by generator
     *
     * @param addr low level Feature Structure reference
     * @param type the type of this Feature Structure
     * @generated
     */
    public Word(int addr, TOP_Type type) {
        super(addr, type);
        readObject();
    }

    /**
     * @param jcas JCas to which this Feature Structure belongs
     * @generated
     */
    public Word(JCas jcas) {
        super(jcas);
        readObject();
    }

    /**
     * @param jcas  JCas to which this Feature Structure belongs
     * @param begin offset to the begin spot in the SofA
     * @param end   offset to the end spot in the SofA
     * @generated
     */
    public Word(JCas jcas, int begin, int end) {
        super(jcas);
        setBegin(begin);
        setEnd(end);
        readObject();
    }

    /**
     * <!-- begin-user-doc -->
     * Write your own initialization here
     * <!-- end-user-doc -->
     *
     * @generated modifiable
     */
    private void readObject() {/*default - does nothing empty block */}


    //*--------------*
    //* Feature: token

    /**
     * getter for token - gets
     *
     * @return value of the feature
     * @generated
     */
    public Token getToken() {
        if (Word_Type.featOkTst && ((Word_Type) jcasType).casFeat_token == null)
            jcasType.jcas.throwFeatMissing("token", "ru.misis.asu.nlp.morphoanalysis.types.Word");
        return (Token) (jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((Word_Type) jcasType).casFeatCode_token)));
    }

    /**
     * setter for token - sets
     *
     * @param v value to set into the feature
     * @generated
     */
    public void setToken(Token v) {
        if (Word_Type.featOkTst && ((Word_Type) jcasType).casFeat_token == null)
            jcasType.jcas.throwFeatMissing("token", "ru.misis.asu.nlp.morphoanalysis.types.Word");
        jcasType.ll_cas.ll_setRefValue(addr, ((Word_Type) jcasType).casFeatCode_token, jcasType.ll_cas.ll_getFSRef(v));
    }


    //*--------------*
    //* Feature: wordforms

    /**
     * getter for wordforms - gets
     *
     * @return value of the feature
     * @generated
     */
    public FSArray getWordforms() {
        if (Word_Type.featOkTst && ((Word_Type) jcasType).casFeat_wordforms == null)
            jcasType.jcas.throwFeatMissing("wordforms", "ru.misis.asu.nlp.morphoanalysis.types.Word");
        return (FSArray) (jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((Word_Type) jcasType).casFeatCode_wordforms)));
    }

    /**
     * setter for wordforms - sets
     *
     * @param v value to set into the feature
     * @generated
     */
    public void setWordforms(FSArray v) {
        if (Word_Type.featOkTst && ((Word_Type) jcasType).casFeat_wordforms == null)
            jcasType.jcas.throwFeatMissing("wordforms", "ru.misis.asu.nlp.morphoanalysis.types.Word");
        jcasType.ll_cas.ll_setRefValue(addr, ((Word_Type) jcasType).casFeatCode_wordforms, jcasType.ll_cas.ll_getFSRef(v));
    }

    /**
     * indexed getter for wordforms - gets an indexed value -
     *
     * @param i index in the array to get
     * @return value of the element at index i
     * @generated
     */
    public Wordform getWordforms(int i) {
        if (Word_Type.featOkTst && ((Word_Type) jcasType).casFeat_wordforms == null)
            jcasType.jcas.throwFeatMissing("wordforms", "ru.misis.asu.nlp.morphoanalysis.types.Word");
        jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((Word_Type) jcasType).casFeatCode_wordforms), i);
        return (Wordform) (jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((Word_Type) jcasType).casFeatCode_wordforms), i)));
    }

    /**
     * indexed setter for wordforms - sets an indexed value -
     *
     * @param i index in the array to set
     * @param v value to set into the array
     * @generated
     */
    public void setWordforms(int i, Wordform v) {
        if (Word_Type.featOkTst && ((Word_Type) jcasType).casFeat_wordforms == null)
            jcasType.jcas.throwFeatMissing("wordforms", "ru.misis.asu.nlp.morphoanalysis.types.Word");
        jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((Word_Type) jcasType).casFeatCode_wordforms), i);
        jcasType.ll_cas.ll_setRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((Word_Type) jcasType).casFeatCode_wordforms), i, jcasType.ll_cas.ll_getFSRef(v));
    }
}

    