package ru.misis.asu.nlp.postprocessing;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.impl.XmiCasSerializer;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.XMLSerializer;
import org.xml.sax.SAXException;
import ru.misis.asu.nlp.commons.config.Config;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author valter
 */
public class XmiWriter extends JCasAnnotator_ImplBase {
    private File outputFile;

    @Override
    public void initialize(UimaContext aContext)
            throws ResourceInitializationException {
        super.initialize(aContext);
        String outputFileName = Config.getProperty(Config.OUTPUT_XMI_FILE);
        outputFile = new File(outputFileName);
    }

    @Override
    public void process(JCas aCAS) throws AnalysisEngineProcessException {
        try {
            if (Boolean.getBoolean(Config.getProperty(Config.OUTPUT_XMI))) {
                writeXmi(aCAS.getCas(), outputFile);
            }
        } catch (IOException | SAXException e) {
            e.printStackTrace();
        }
    }

    private void writeXmi(CAS aCas, File name) throws IOException, SAXException {
        /*try (FileOutputStream out = new FileOutputStream(name)) {
            XmiCasSerializer ser = new XmiCasSerializer(aCas.getTypeSystem());
            XMLSerializer xmlSer = new XMLSerializer(out, false);
            ser.serialize(aCas, xmlSer.getContentHandler());
        }*/
    }
}
