package ru.misis.asu.nlp.tagging.pos;

import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.ling.WordLemmaTag;
import edu.stanford.nlp.ling.WordTag;
import edu.stanford.nlp.process.Morphology;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import org.apache.uima.resource.DataResource;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.SharedResourceObject;
import org.apache.uima.resource.impl.DataResource_impl;
import org.apache.uima.resource.impl.FileResourceSpecifier_impl;

import java.util.*;
import java.util.stream.Collectors;

import static java.lang.System.currentTimeMillis;

/**
 * @author valter
 *         03.07.16
 */
public class PosTagger implements SharedResourceObject {
    private MaxentTagger tagger;

    public static void main(String[] args) throws Exception {
        PosTagger posTagger = new PosTagger();
        DataResource_impl resource_impl = new DataResource_impl();
        FileResourceSpecifier_impl specifier_impl = new FileResourceSpecifier_impl();
        specifier_impl.setFileUrl("/home/valter/projects/nlp-cloud/pos-tagging/src/main/resources/taggers/english-bidirectional-distsim.tagger");
        resource_impl.initialize(specifier_impl, new HashMap<>());
        posTagger.load(resource_impl);
        //System.out.println(posTagger.analyse(Arrays.asList("a bit confused".split("\\s"))));

        Map<String, Set<String>> mappings = new HashMap<>();
        try (Scanner in = new Scanner(PosTagger.class.getClassLoader().getResourceAsStream("pos.ngram.txt"))) {
            while (in.hasNext()) {
                int freq = in.nextInt();
                String w1 = in.next();
                String w2 = in.next();
                String p1 = in.next();
                String p2 = in.next();
                List<WordLemmaTag> tags = posTagger.analyse(Arrays.asList(w1, w2));
                if (!mappings.containsKey(p1)) {
                    mappings.put(p1, new TreeSet<>());
                }
                if (!mappings.containsKey(p2)) {
                    mappings.put(p2, new TreeSet<>());
                }
                mappings.get(p1).add(tags.get(0).tag().toLowerCase());
                mappings.get(p2).add(tags.get(1).tag().toLowerCase());
            }
        }
        System.out.println();
        for (String s : mappings.keySet()) {
            String str = "put(\"" + s + "\", new HashSet<>(Arrays.toList(";
            Set<String> set = mappings.get(s);
            for (String s1 : set) {
                str += "\"" + s1 + "\", ";
            }
            str = str.substring(0, str.length() - 2);

            System.out.println(str + "));");
        }
    }

    public List<WordLemmaTag> analyse(List<String> words) {
        List<TaggedWord> taggedWords = tagger.tagSentence(words.stream()
                .map(Word::new)
                .collect(Collectors.toList()));

        return taggedWords.stream()
                .map(w -> new WordTag(w.word(), w.tag()))
                .map(Morphology::lemmatizeStatic)
                .collect(Collectors.toList());
    }

    @Override
    public void load(DataResource dataResource) throws ResourceInitializationException {
        System.out.println("Loading pos-tagger...");
        try {
            long timeBefore = currentTimeMillis();
            tagger = new MaxentTagger(dataResource.getUrl().toString());
            System.out.println("Loading pos-tagger finished in " + (currentTimeMillis() - timeBefore) + " ms");
        } catch (Exception e) {
            throw new ResourceInitializationException(e);
        }
    }
}
