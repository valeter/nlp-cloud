package ru.misis.asu.nlp.tagging;

import edu.stanford.nlp.ling.WordLemmaTag;
import edu.stanford.nlp.util.Pair;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceAccessException;
import org.apache.uima.resource.ResourceConfigurationException;
import org.apache.uima.resource.ResourceInitializationException;
import ru.misis.asu.nlp.commons.algo.LevenshteinGenerator;
import ru.misis.asu.nlp.commons.cas.FSUtils;
import ru.misis.asu.nlp.commons.ngram.NgramDictionary;
import ru.misis.asu.nlp.commons.ngram.PosNgramDictionary;
import ru.misis.asu.nlp.tagging.pos.PosDictionary;
import ru.misis.asu.nlp.commons.ngram.EnPosNGramDictionary;
import ru.misis.asu.nlp.tagging.pos.PosTagger;
import ru.misis.asu.nlp.tagging.types.CorrectedWord;
import ru.misis.asu.nlp.tagging.types.Word;
import ru.misis.asu.nlp.tagging.types.Wordform;
import ru.misis.asu.nlp.tokenization.types.ComplexLetters;
import ru.misis.asu.nlp.tokenization.types.Token;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author valter
 *         03.07.16.
 */
public class PosTagAnnotator extends JCasAnnotator_ImplBase {
    private static final String LETTERS_TYPE_PARAM = "LettersType";
    private static final String SENTENCE_TYPE_NAME = "SentenceType";

    private static final String POS_TAGGER_RESOURCE = "PosTagger";
    private static final String POS_DICTIONARY_RESOURCE = "PosDictionary";

    private PosTagger tagger;
    private PosDictionary posDictionary;
    private PosNgramDictionary enPosNGramDictionary = NgramDictionary.Instance.EN.get();
    private LevenshteinGenerator levenshteinGenerator;

    private String lettersTypeName;
    private String sentenceTypeName;

    @Override
    public void reconfigure() throws ResourceConfigurationException,
            ResourceInitializationException {
        super.reconfigure();
    }

    @Override
    public void initialize(UimaContext ctx) throws ResourceInitializationException {
        super.initialize(ctx);

        lettersTypeName = (String) ctx.getConfigParameterValue(LETTERS_TYPE_PARAM);
        sentenceTypeName = (String) ctx.getConfigParameterValue(SENTENCE_TYPE_NAME);

        try {
            tagger = (PosTagger) ctx.getResourceObject(POS_TAGGER_RESOURCE);
            posDictionary = (PosDictionary) ctx.getResourceObject(POS_DICTIONARY_RESOURCE);
            levenshteinGenerator = new LevenshteinGenerator(1, posDictionary.allWords());
        } catch (ResourceAccessException e) {
            throw new RuntimeException("Pos module initialization error", e);
        }
    }

    @Override
    public void process(JCas cas) {
        Type lettersType = cas.getTypeSystem().getType(lettersTypeName);
        Type sentenceType = cas.getTypeSystem().getType(sentenceTypeName);

        AnnotationIndex<Annotation> words = cas.getAnnotationIndex(lettersType);
        AnnotationIndex<Annotation> sentences = cas.getAnnotationIndex(sentenceType);
        int i = 0;
        for (Annotation sentence : sentences) {
            System.out.println("Sentence " + ++i + "[" + sentence.getCoveredText() + "]: ");
            System.out.println();

            long start = System.currentTimeMillis();
            List<Pair<List<String>, Token>> possibleCorrections = getPossibleCorrections(words.subiterator(sentence));
            System.out.println();
            System.out.println("Finished generating possible corrections in " + (System.currentTimeMillis() - start) + " ms");

            start = System.currentTimeMillis();
            List<List<String>> filteredCorrections = filterWithNGramDictWithoutTags(possibleCorrections.stream()
                    .map(Pair::first)
                    .collect(Collectors.toList()));
            System.out.println("Finished filtering sentences with simple n-gram dictionary in " + (System.currentTimeMillis() - start) + " ms");

            start = System.currentTimeMillis();
            List<List<WordLemmaTag>> sentencePossibilities = getSentences(filteredCorrections);
            System.out.println("Finished generating possible sentences and pos-tagging in " + (System.currentTimeMillis() - start) + " ms");

            start = System.currentTimeMillis();
            List<List<WordLemmaTag>> transposed = filterWithNGramDict(transpose(sentencePossibilities));
            System.out.println("Finished filtering sentences with pos n-gram dictionary in " + (System.currentTimeMillis() - start) + " ms");

            System.out.println();

            List<CorrectedWord> wordsAnnotations = makeAnnotations(transposed, possibleCorrections.stream()
                    .map(Pair::second)
                    .collect(Collectors.toList()), cas);
            wordsAnnotations.forEach(CorrectedWord::addToIndexes);

            System.out.println();
            System.out.println("-------------------------------------------");
        }
    }

    private List<Pair<List<String>, Token>> getPossibleCorrections(Iterator<Annotation> tokens) {
        List<Pair<List<String>, Token>> possibleCorrections = new ArrayList<>();
        for (; tokens.hasNext(); ) {
            Token next = (Token) tokens.next();
            String word = next.getNorm();
            if (!word.contains("'") && !posDictionary.contains(word)) {
                Collection<String> corrections = levenshteinGenerator.generate(word);

                if (next instanceof ComplexLetters) {
                    corrections.addAll(getComplexWordCorrections((ComplexLetters) next));
                }

                if (corrections.isEmpty()) {
                    corrections.add(word);
                }
                System.out.println("Word missing in dictionary: " + word);
                System.out.println("Replacements: [");
                corrections.forEach(System.out::println);
                System.out.println("]");
                possibleCorrections.add(new Pair<>(new ArrayList<>(corrections), next));
            } else {
                possibleCorrections.add(new Pair<>(Collections.singletonList("i".equals(word) ? "I" : word), next));
            }
        }
        return possibleCorrections;
    }

    private Collection<String> getComplexWordCorrections(ComplexLetters complexWord) {
        Collection<String> leftCorrections = null;
        boolean needCorrectLeft = !posDictionary.contains(complexWord.getLeft());
        Collection<String> rightCorrections = null;
        boolean needCorrectRight = !posDictionary.contains(complexWord.getRight());

        if (needCorrectLeft) {
            leftCorrections = levenshteinGenerator.generate(complexWord.getLeft());
        }
        if (needCorrectRight) {
            rightCorrections = levenshteinGenerator.generate(complexWord.getRight());
        }

        if (!needCorrectLeft && !needCorrectRight) {
            return Collections.singletonList(complexWord.getNorm());
        }

        if (leftCorrections == null || leftCorrections.isEmpty()) {
            leftCorrections = Collections.singletonList(complexWord.getLeft());
        }

        if (rightCorrections == null || rightCorrections.isEmpty()) {
            rightCorrections = Collections.singletonList(complexWord.getRight());
        }

        List<String> corrections = new ArrayList<>();
        for (String leftCorrection : leftCorrections) {
            corrections.addAll(rightCorrections.stream()
                    .map(rightCorrection -> leftCorrection + complexWord.getSeparator() + rightCorrection)
                    .collect(Collectors.toList()));
        }
        return corrections;
    }

    private List<List<WordLemmaTag>> getSentences(List<List<String>> possibleCorrections) {
        List<List<String>> sentences = new ArrayList<>();
        sentences.add(new ArrayList<>());
        for (List<String> possibleCorrection : possibleCorrections) {
            List<List<String>> newSentences = new ArrayList<>();

            for (String correction : possibleCorrection) {
                for (List<String> sentence : sentences) {
                    List<String> newSentence = new ArrayList<>(sentence);
                    newSentence.add(correction);
                    newSentences.add(newSentence);
                }
            }

            sentences = newSentences;
        }

        return sentences.stream().parallel()
                .map(tagger::analyse)
                .collect(Collectors.toList());
    }

    private List<List<WordLemmaTag>> transpose(List<List<WordLemmaTag>> list) {
        List<List<WordLemmaTag>> result = new ArrayList<>();
        if (list.size() == 0) {
            return result;
        }
        for (int i = 0; i < list.get(0).size(); i++) {
            Set<WordLemmaTag> tags = new HashSet<>();
            for (List<WordLemmaTag> sentence : list) {
                tags.add(sentence.get(i));
            }
            result.add(new ArrayList<>(tags));
        }
        return result;
    }

    private List<List<String>> filterWithNGramDictWithoutTags(List<List<String>> result) {
        for (int i = 0; i < result.size(); i++) {
            if (result.get(i).size() == 1)
                continue;

            Set<Integer> indsToRemove = new TreeSet<>(Comparator.reverseOrder());
            Set<Integer> semiGoodInds = new TreeSet<>(Comparator.reverseOrder());
            for (int j = result.get(i).size() - 1; j >= 0; j--) {
                int quality = 0;

                String word = result.get(i).get(j);
                if (i - 1 >= 0) {
                    for (int k = 0; k < result.get(i - 1).size(); k++) {
                        String left = result.get(i - 1).get(k);
                        if (enPosNGramDictionary.getFrequency(left, word) > 0) {
                            quality++;
                            break;
                        }
                    }
                }
                if (i + 1 < result.size()) {
                    for (int k = 0; k < result.get(i + 1).size(); k++) {
                        String right = result.get(i + 1).get(k);
                        if (enPosNGramDictionary.getFrequency(word, right) > 0) {
                            quality++;
                            break;
                        }
                    }
                }
                if (quality == 0) {
                    indsToRemove.add(j);
                } else if (quality == 1) {
                    indsToRemove.add(j);
                    semiGoodInds.add(j);
                }
            }

            // if there's good option, remove all bad options
            // if there's semi good options and no good - leave semi good
            if (indsToRemove.size() != result.get(i).size()) {
                for (int ind : indsToRemove) {
                    result.get(i).remove(ind);
                }
            } else if (semiGoodInds.size() != 0) {
                for (int j = result.get(i).size() - 1; j >= 0; j--) {
                    if (!semiGoodInds.contains(j)) {
                        result.get(i).remove(j);
                    }
                }
            }
        }
        return result;
    }

    private List<List<WordLemmaTag>> filterWithNGramDict(List<List<WordLemmaTag>> result) {
        for (int i = 0; i < result.size(); i++) {
            if (result.get(i).size() == 1)
                continue;

            Set<Integer> indsToRemove = new TreeSet<>(Comparator.reverseOrder());
            Set<Integer> semiGoodInds = new TreeSet<>(Comparator.reverseOrder());
            for (int j = result.get(i).size() - 1; j >= 0; j--) {
                int quality = 0;

                WordLemmaTag word = result.get(i).get(j);
                if (i - 1 >= 0) {
                    for (int k = 0; k < result.get(i - 1).size(); k++) {
                        WordLemmaTag left = result.get(i - 1).get(k);
                        if (enPosNGramDictionary.getFrequency(left.word(), left.tag(), word.word(), word.tag()) > 0) {
                            quality++;
                            break;
                        }
                    }
                }
                if (i + 1 < result.size()) {
                    for (int k = 0; k < result.get(i + 1).size(); k++) {
                        WordLemmaTag right = result.get(i + 1).get(k);
                        if (enPosNGramDictionary.getFrequency(word.word(), word.tag(), right.word(), right.tag()) > 0) {
                            quality++;
                            break;
                        }
                    }
                }
                if (quality == 0) {
                    indsToRemove.add(j);
                } else if (quality == 1) {
                    indsToRemove.add(j);
                    semiGoodInds.add(j);
                }
            }

            // if there's good option, remove all bad options
            // if there's semi good options and no good - leave semi good
            if (indsToRemove.size() != result.get(i).size()) {
                for (int ind : indsToRemove) {
                    result.get(i).remove(ind);
                }
            } else if (semiGoodInds.size() != 0) {
                for (int j = result.get(i).size() - 1; j >= 0; j--) {
                    if (!semiGoodInds.contains(j)) {
                        result.get(i).remove(j);
                    }
                }
            }
        }
        return result;
    }


    private List<CorrectedWord> makeAnnotations(List<List<WordLemmaTag>> wordCorrections, List<Token> tokens, JCas cas) {
        List<CorrectedWord> correctedWords = new ArrayList<>();
        for (int i = 0; i < tokens.size(); i++) {
            Token token = tokens.get(i);
            System.out.println("Word [" + token.getNorm() + "]  wordforms: [");
            wordCorrections.get(i).forEach(t -> {
                System.out.println("\t [word:\"" + t.word() + "\",pos:\"" + t.tag() + "\",lemma:\"" + t.lemma() + "\"]");
            });
            System.out.println("]");

            CorrectedWord correctedWord = new CorrectedWord(cas);
            correctedWord.setBegin(token.getBegin());
            correctedWord.setEnd(token.getEnd());
            correctedWord.setToken(token);
            correctedWord.setWords(FSUtils.toFSArray(cas, getWords(wordCorrections.get(i), token, cas)));
            correctedWord.setCorrections(FSUtils.toStringArray(cas, wordCorrections.get(i).stream()
                    .map(WordLemmaTag::word)
                    .collect(Collectors.toList())
            ));
            correctedWords.add(correctedWord);
        }
        return correctedWords;
    }

    private List<Word> getWords(List<WordLemmaTag> tags, Token token, JCas cas) {
        List<Word> words = new ArrayList<>();
        for (WordLemmaTag tag : tags) {
            Wordform wordform = new Wordform(cas);
            wordform.setBegin(token.getBegin());
            wordform.setEnd(token.getEnd());
            wordform.setPos(tag.tag());
            wordform.setLemma(tag.lemma());

            Word word = new Word(cas);
            word.setBegin(token.getBegin());
            word.setEnd(token.getEnd());
            word.setToken(token);
            word.setWordforms(FSUtils.toFSArray(cas, Collections.singleton(wordform)));

            words.add(word);
        }

        return words;
    }
}
