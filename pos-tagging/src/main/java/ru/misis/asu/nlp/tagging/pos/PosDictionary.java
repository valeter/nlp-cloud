package ru.misis.asu.nlp.tagging.pos;

import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.ling.WordLemmaTag;
import edu.stanford.nlp.ling.WordTag;
import edu.stanford.nlp.process.Morphology;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import org.apache.uima.resource.DataResource;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.SharedResourceObject;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.System.currentTimeMillis;

/**
 *  @author valter
 *          09.07.16
 */
public class PosDictionary implements Serializable, SharedResourceObject {
    public static final long serialVersionUID = 1L;

    private Map<String, Set<WordLemmaTag>> tags = new HashMap<>();

    public static void main(String[] args) throws Exception {
        createDictionary();
    }

    public static void createDictionary() throws Exception {
        PosDictionary dictionary = new PosDictionary();

        String dirName = "/home/valter/Downloads/wordLemPoS";
        Files.list(Paths.get(dirName))
                .flatMap(f -> {
                    try {
                        System.out.println("Processing file "  + f.getFileName());
                        return Files.lines(f);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                })
                .map(l -> {
                    String[] parts = l.split("\t");
                    if (parts.length == 3 && !parts[0].startsWith("@")) {
                        return new WordLemmaTag(parts[0].trim().toLowerCase(), parts[1].toLowerCase(), parts[2].toLowerCase());
                    } else {
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .forEach(dictionary::add);

        System.out.println("Dictionary size: " + dictionary.tags.size());

        String outputFileName = "/home/valter/projects/nlp-cloud/pos-tagging/src/main/resources/pos.dictionary.raw";
        try (ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(outputFileName)))) {
            out.writeObject(dictionary);
        }
        System.out.println("Pos dictionary successfully created");
    }

    public boolean contains(String word) {
        return tags.containsKey(word);
    }

    public Collection<WordLemmaTag> get(String word) {
        return tags.get(word);
    }

    public Collection<String> allWords() {
        Set<String> result = new HashSet<>();
        for (Iterator<String> iterator = new HashSet<>(tags.keySet()).iterator(); iterator.hasNext(); ) {
            String next = iterator.next();
            result.add(next);
        }
        return result;
    }

    private void add(WordLemmaTag tag) {
        String word = tag.word();
        if (!tags.containsKey(word)) {
            tags.put(word, new HashSet<>());
        }
        tags.get(word).add(tag);
    }

    @Override
    public void load(DataResource dataResource) throws ResourceInitializationException {
        System.out.println("Loading pos dictionary...");
        try {
            long timeBefore = currentTimeMillis();

            PosDictionary dictionary;
            try (ObjectInputStream in = new ObjectInputStream(dataResource.getInputStream())) {
                dictionary = (PosDictionary)in.readObject();
            }
            this.tags = dictionary.tags;
            System.out.println("Pos dictionary size: " + dictionary.tags.size());

            System.out.println("Loading pos dictionary finished in " + (currentTimeMillis() - timeBefore) + " ms");
        } catch (Exception e) {
            throw new ResourceInitializationException(e);
        }
    }
}
