package ru.misis.asu.nlp.segmentation;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import ru.misis.asu.nlp.segmentation.types.Sentence;
import ru.misis.asu.nlp.tokenization.types.PM;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SentenceAnnotator extends JCasAnnotator_ImplBase {
    private static final String PM_TYPE_PARAM = "PMType";

    private static String PMTypeName;

    @Override
    public void initialize(UimaContext aContext)
            throws ResourceInitializationException {
        super.initialize(aContext);
        PMTypeName = (String) aContext.getConfigParameterValue(PM_TYPE_PARAM);
    }

    @Override
    public void process(final JCas arg0) {
        Pattern regex = Pattern.compile("[^?!\\.\n]+");
        Matcher matcher = regex.matcher(arg0.getDocumentText());
        while (matcher.find()) {
            makeSentenceAnnotation(arg0, matcher.start(), matcher.end(), null);
        }
    }

    private static void makeSentenceAnnotation(final JCas cas, final int begin,
                                               final int end, final PM eos) {
        Sentence sentence = new Sentence(cas);
        sentence.setBegin(begin);
        sentence.setEnd(end);
        sentence.setEos(eos);
        sentence.addToIndexes();
    }
}
