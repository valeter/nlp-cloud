/* First created by JCasGen Sun Feb 07 11:31:02 MSK 2016 */
package ru.misis.asu.nlp.segmentation.types;

import org.apache.uima.cas.Feature;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.tcas.Annotation_Type;

/**
 * Updated by JCasGen Sun Feb 07 11:31:02 MSK 2016
 *
 * @generated
 */
public class ClauseSegment_Type extends Annotation_Type {
    /**
     * @return the generator for this type
     * @generated
     */
    @Override
    protected FSGenerator getFSGenerator() {
        return fsGenerator;
    }

    /**
     * @generated
     */
    private final FSGenerator fsGenerator =
            new FSGenerator() {
                public FeatureStructure createFS(int addr, CASImpl cas) {
                    if (ClauseSegment_Type.this.useExistingInstance) {
                        // Return eq fs instance if already created
                        FeatureStructure fs = ClauseSegment_Type.this.jcas.getJfsFromCaddr(addr);
                        if (null == fs) {
                            fs = new ClauseSegment(addr, ClauseSegment_Type.this);
                            ClauseSegment_Type.this.jcas.putJfsFromCaddr(addr, fs);
                            return fs;
                        }
                        return fs;
                    } else return new ClauseSegment(addr, ClauseSegment_Type.this);
                }
            };
    /**
     * @generated
     */
    @SuppressWarnings("hiding")
    public final static int typeIndexID = ClauseSegment.typeIndexID;
    /**
     * @generated
     * @modifiable
     */
    @SuppressWarnings("hiding")
    public final static boolean featOkTst = JCasRegistry.getFeatOkTst("ru.misis.asu.nlp.segmentation.types.ClauseSegment");

    /**
     * @generated
     */
    final Feature casFeat_sentence;
    /**
     * @generated
     */
    final int casFeatCode_sentence;

    /**
     * @param addr low level Feature Structure reference
     * @return the feature value
     * @generated
     */
    public int getSentence(int addr) {
        if (featOkTst && casFeat_sentence == null)
            jcas.throwFeatMissing("sentence", "ru.misis.asu.nlp.segmentation.types.ClauseSegment");
        return ll_cas.ll_getRefValue(addr, casFeatCode_sentence);
    }

    /**
     * @param addr low level Feature Structure reference
     * @param v    value to set
     * @generated
     */
    public void setSentence(int addr, int v) {
        if (featOkTst && casFeat_sentence == null)
            jcas.throwFeatMissing("sentence", "ru.misis.asu.nlp.segmentation.types.ClauseSegment");
        ll_cas.ll_setRefValue(addr, casFeatCode_sentence, v);
    }


    /**
     * @generated
     */
    final Feature casFeat_eopm;
    /**
     * @generated
     */
    final int casFeatCode_eopm;

    /**
     * @param addr low level Feature Structure reference
     * @return the feature value
     * @generated
     */
    public int getEopm(int addr) {
        if (featOkTst && casFeat_eopm == null)
            jcas.throwFeatMissing("eopm", "ru.misis.asu.nlp.segmentation.types.ClauseSegment");
        return ll_cas.ll_getRefValue(addr, casFeatCode_eopm);
    }

    /**
     * @param addr low level Feature Structure reference
     * @param v    value to set
     * @generated
     */
    public void setEopm(int addr, int v) {
        if (featOkTst && casFeat_eopm == null)
            jcas.throwFeatMissing("eopm", "ru.misis.asu.nlp.segmentation.types.ClauseSegment");
        ll_cas.ll_setRefValue(addr, casFeatCode_eopm, v);
    }


    /**
     * initialize variables to correspond with Cas Type and Features
     *
     * @param jcas    JCas
     * @param casType Type
     * @generated
     */
    public ClauseSegment_Type(JCas jcas, Type casType) {
        super(jcas, casType);
        casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl) this.casType, getFSGenerator());


        casFeat_sentence = jcas.getRequiredFeatureDE(casType, "sentence", "ru.misis.asu.nlp.segmentation.types.Sentence", featOkTst);
        casFeatCode_sentence = (null == casFeat_sentence) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl) casFeat_sentence).getCode();


        casFeat_eopm = jcas.getRequiredFeatureDE(casType, "eopm", "ru.misis.asu.nlp.tokenization.types.PM", featOkTst);
        casFeatCode_eopm = (null == casFeat_eopm) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl) casFeat_eopm).getCode();

    }
}



    