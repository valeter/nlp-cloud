/* First created by JCasGen Sun Feb 07 11:31:02 MSK 2016 */
package ru.misis.asu.nlp.segmentation.types;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;
import org.apache.uima.jcas.tcas.Annotation;
import ru.misis.asu.nlp.tokenization.types.PM;


/**
 * Updated by JCasGen Sun Feb 07 11:31:02 MSK 2016
 * XML source: C:/Users/����/projects/NLP-Cloud/segmentation/src/main/resources/uima_xml/segmentation-ts.uima_xml
 *
 * @generated
 */
public class ClauseSegment extends Annotation {
    /**
     * @generated
     * @ordered
     */
    @SuppressWarnings("hiding")
    public final static int typeIndexID = JCasRegistry.register(ClauseSegment.class);
    /**
     * @generated
     * @ordered
     */
    @SuppressWarnings("hiding")
    public final static int type = typeIndexID;

    /**
     * @return index of the type
     * @generated
     */
    @Override
    public int getTypeIndexID() {
        return typeIndexID;
    }

    /**
     * Never called.  Disable default constructor
     *
     * @generated
     */
    protected ClauseSegment() {/* intentionally empty block */}

    /**
     * Internal - constructor used by generator
     *
     * @param addr low level Feature Structure reference
     * @param type the type of this Feature Structure
     * @generated
     */
    public ClauseSegment(int addr, TOP_Type type) {
        super(addr, type);
        readObject();
    }

    /**
     * @param jcas JCas to which this Feature Structure belongs
     * @generated
     */
    public ClauseSegment(JCas jcas) {
        super(jcas);
        readObject();
    }

    /**
     * @param jcas  JCas to which this Feature Structure belongs
     * @param begin offset to the begin spot in the SofA
     * @param end   offset to the end spot in the SofA
     * @generated
     */
    public ClauseSegment(JCas jcas, int begin, int end) {
        super(jcas);
        setBegin(begin);
        setEnd(end);
        readObject();
    }

    /**
     * <!-- begin-user-doc -->
     * Write your own initialization here
     * <!-- end-user-doc -->
     *
     * @generated modifiable
     */
    private void readObject() {/*default - does nothing empty block */}


    //*--------------*
    //* Feature: sentence

    /**
     * getter for sentence - gets
     *
     * @return value of the feature
     * @generated
     */
    public Sentence getSentence() {
        if (ClauseSegment_Type.featOkTst && ((ClauseSegment_Type) jcasType).casFeat_sentence == null)
            jcasType.jcas.throwFeatMissing("sentence", "ru.misis.asu.nlp.segmentation.types.ClauseSegment");
        return (Sentence) (jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((ClauseSegment_Type) jcasType).casFeatCode_sentence)));
    }

    /**
     * setter for sentence - sets
     *
     * @param v value to set into the feature
     * @generated
     */
    public void setSentence(Sentence v) {
        if (ClauseSegment_Type.featOkTst && ((ClauseSegment_Type) jcasType).casFeat_sentence == null)
            jcasType.jcas.throwFeatMissing("sentence", "ru.misis.asu.nlp.segmentation.types.ClauseSegment");
        jcasType.ll_cas.ll_setRefValue(addr, ((ClauseSegment_Type) jcasType).casFeatCode_sentence, jcasType.ll_cas.ll_getFSRef(v));
    }


    //*--------------*
    //* Feature: eopm

    /**
     * getter for eopm - gets
     *
     * @return value of the feature
     * @generated
     */
    public PM getEopm() {
        if (ClauseSegment_Type.featOkTst && ((ClauseSegment_Type) jcasType).casFeat_eopm == null)
            jcasType.jcas.throwFeatMissing("eopm", "ru.misis.asu.nlp.segmentation.types.ClauseSegment");
        return (PM) (jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((ClauseSegment_Type) jcasType).casFeatCode_eopm)));
    }

    /**
     * setter for eopm - sets
     *
     * @param v value to set into the feature
     * @generated
     */
    public void setEopm(PM v) {
        if (ClauseSegment_Type.featOkTst && ((ClauseSegment_Type) jcasType).casFeat_eopm == null)
            jcasType.jcas.throwFeatMissing("eopm", "ru.misis.asu.nlp.segmentation.types.ClauseSegment");
        jcasType.ll_cas.ll_setRefValue(addr, ((ClauseSegment_Type) jcasType).casFeatCode_eopm, jcasType.ll_cas.ll_getFSRef(v));
    }
}

    