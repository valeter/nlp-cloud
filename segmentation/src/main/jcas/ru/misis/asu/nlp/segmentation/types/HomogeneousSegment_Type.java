/* First created by JCasGen Sun Feb 07 11:31:02 MSK 2016 */
package ru.misis.asu.nlp.segmentation.types;

import org.apache.uima.cas.Feature;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.tcas.Annotation_Type;

/**
 * Updated by JCasGen Sun Feb 07 11:31:02 MSK 2016
 *
 * @generated
 */
public class HomogeneousSegment_Type extends Annotation_Type {
    /**
     * @return the generator for this type
     * @generated
     */
    @Override
    protected FSGenerator getFSGenerator() {
        return fsGenerator;
    }

    /**
     * @generated
     */
    private final FSGenerator fsGenerator =
            new FSGenerator() {
                public FeatureStructure createFS(int addr, CASImpl cas) {
                    if (HomogeneousSegment_Type.this.useExistingInstance) {
                        // Return eq fs instance if already created
                        FeatureStructure fs = HomogeneousSegment_Type.this.jcas.getJfsFromCaddr(addr);
                        if (null == fs) {
                            fs = new HomogeneousSegment(addr, HomogeneousSegment_Type.this);
                            HomogeneousSegment_Type.this.jcas.putJfsFromCaddr(addr, fs);
                            return fs;
                        }
                        return fs;
                    } else return new HomogeneousSegment(addr, HomogeneousSegment_Type.this);
                }
            };
    /**
     * @generated
     */
    @SuppressWarnings("hiding")
    public final static int typeIndexID = HomogeneousSegment.typeIndexID;
    /**
     * @generated
     * @modifiable
     */
    @SuppressWarnings("hiding")
    public final static boolean featOkTst = JCasRegistry.getFeatOkTst("ru.misis.asu.nlp.segmentation.types.HomogeneousSegment");

    /**
     * @generated
     */
    final Feature casFeat_sentence;
    /**
     * @generated
     */
    final int casFeatCode_sentence;

    /**
     * @param addr low level Feature Structure reference
     * @return the feature value
     * @generated
     */
    public int getSentence(int addr) {
        if (featOkTst && casFeat_sentence == null)
            jcas.throwFeatMissing("sentence", "ru.misis.asu.nlp.segmentation.types.HomogeneousSegment");
        return ll_cas.ll_getRefValue(addr, casFeatCode_sentence);
    }

    /**
     * @param addr low level Feature Structure reference
     * @param v    value to set
     * @generated
     */
    public void setSentence(int addr, int v) {
        if (featOkTst && casFeat_sentence == null)
            jcas.throwFeatMissing("sentence", "ru.misis.asu.nlp.segmentation.types.HomogeneousSegment");
        ll_cas.ll_setRefValue(addr, casFeatCode_sentence, v);
    }


    /**
     * @generated
     */
    final Feature casFeat_eopms;
    /**
     * @generated
     */
    final int casFeatCode_eopms;

    /**
     * @param addr low level Feature Structure reference
     * @return the feature value
     * @generated
     */
    public int getEopms(int addr) {
        if (featOkTst && casFeat_eopms == null)
            jcas.throwFeatMissing("eopms", "ru.misis.asu.nlp.segmentation.types.HomogeneousSegment");
        return ll_cas.ll_getRefValue(addr, casFeatCode_eopms);
    }

    /**
     * @param addr low level Feature Structure reference
     * @param v    value to set
     * @generated
     */
    public void setEopms(int addr, int v) {
        if (featOkTst && casFeat_eopms == null)
            jcas.throwFeatMissing("eopms", "ru.misis.asu.nlp.segmentation.types.HomogeneousSegment");
        ll_cas.ll_setRefValue(addr, casFeatCode_eopms, v);
    }

    /**
     * @param addr low level Feature Structure reference
     * @param i    index of item in the array
     * @return value at index i in the array
     * @generated
     */
    public int getEopms(int addr, int i) {
        if (featOkTst && casFeat_eopms == null)
            jcas.throwFeatMissing("eopms", "ru.misis.asu.nlp.segmentation.types.HomogeneousSegment");
        if (lowLevelTypeChecks)
            return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_eopms), i, true);
        jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_eopms), i);
        return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_eopms), i);
    }

    /**
     * @param addr low level Feature Structure reference
     * @param i    index of item in the array
     * @param v    value to set
     * @generated
     */
    public void setEopms(int addr, int i, int v) {
        if (featOkTst && casFeat_eopms == null)
            jcas.throwFeatMissing("eopms", "ru.misis.asu.nlp.segmentation.types.HomogeneousSegment");
        if (lowLevelTypeChecks)
            ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_eopms), i, v, true);
        jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_eopms), i);
        ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_eopms), i, v);
    }


    /**
     * @generated
     */
    final Feature casFeat_words;
    /**
     * @generated
     */
    final int casFeatCode_words;

    /**
     * @param addr low level Feature Structure reference
     * @return the feature value
     * @generated
     */
    public int getWords(int addr) {
        if (featOkTst && casFeat_words == null)
            jcas.throwFeatMissing("words", "ru.misis.asu.nlp.segmentation.types.HomogeneousSegment");
        return ll_cas.ll_getRefValue(addr, casFeatCode_words);
    }

    /**
     * @param addr low level Feature Structure reference
     * @param v    value to set
     * @generated
     */
    public void setWords(int addr, int v) {
        if (featOkTst && casFeat_words == null)
            jcas.throwFeatMissing("words", "ru.misis.asu.nlp.segmentation.types.HomogeneousSegment");
        ll_cas.ll_setRefValue(addr, casFeatCode_words, v);
    }

    /**
     * @param addr low level Feature Structure reference
     * @param i    index of item in the array
     * @return value at index i in the array
     * @generated
     */
    public int getWords(int addr, int i) {
        if (featOkTst && casFeat_words == null)
            jcas.throwFeatMissing("words", "ru.misis.asu.nlp.segmentation.types.HomogeneousSegment");
        if (lowLevelTypeChecks)
            return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_words), i, true);
        jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_words), i);
        return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_words), i);
    }

    /**
     * @param addr low level Feature Structure reference
     * @param i    index of item in the array
     * @param v    value to set
     * @generated
     */
    public void setWords(int addr, int i, int v) {
        if (featOkTst && casFeat_words == null)
            jcas.throwFeatMissing("words", "ru.misis.asu.nlp.segmentation.types.HomogeneousSegment");
        if (lowLevelTypeChecks)
            ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_words), i, v, true);
        jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_words), i);
        ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_words), i, v);
    }


    /**
     * initialize variables to correspond with Cas Type and Features
     *
     * @param jcas    JCas
     * @param casType Type
     * @generated
     */
    public HomogeneousSegment_Type(JCas jcas, Type casType) {
        super(jcas, casType);
        casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl) this.casType, getFSGenerator());


        casFeat_sentence = jcas.getRequiredFeatureDE(casType, "sentence", "ru.misis.asu.nlp.segmentation.types.Sentence", featOkTst);
        casFeatCode_sentence = (null == casFeat_sentence) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl) casFeat_sentence).getCode();


        casFeat_eopms = jcas.getRequiredFeatureDE(casType, "eopms", "uima.cas.FSArray", featOkTst);
        casFeatCode_eopms = (null == casFeat_eopms) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl) casFeat_eopms).getCode();


        casFeat_words = jcas.getRequiredFeatureDE(casType, "words", "uima.cas.FSArray", featOkTst);
        casFeatCode_words = (null == casFeat_words) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl) casFeat_words).getCode();

    }
}



    