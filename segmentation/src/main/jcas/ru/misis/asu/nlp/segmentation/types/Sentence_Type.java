/* First created by JCasGen Sun Feb 07 11:31:02 MSK 2016 */
package ru.misis.asu.nlp.segmentation.types;

import org.apache.uima.cas.Feature;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.tcas.Annotation_Type;

/**
 * Updated by JCasGen Sun Feb 07 11:31:02 MSK 2016
 *
 * @generated
 */
public class Sentence_Type extends Annotation_Type {
    /**
     * @return the generator for this type
     * @generated
     */
    @Override
    protected FSGenerator getFSGenerator() {
        return fsGenerator;
    }

    /**
     * @generated
     */
    private final FSGenerator fsGenerator =
            new FSGenerator() {
                public FeatureStructure createFS(int addr, CASImpl cas) {
                    if (Sentence_Type.this.useExistingInstance) {
                        // Return eq fs instance if already created
                        FeatureStructure fs = Sentence_Type.this.jcas.getJfsFromCaddr(addr);
                        if (null == fs) {
                            fs = new Sentence(addr, Sentence_Type.this);
                            Sentence_Type.this.jcas.putJfsFromCaddr(addr, fs);
                            return fs;
                        }
                        return fs;
                    } else return new Sentence(addr, Sentence_Type.this);
                }
            };
    /**
     * @generated
     */
    @SuppressWarnings("hiding")
    public final static int typeIndexID = Sentence.typeIndexID;
    /**
     * @generated
     * @modifiable
     */
    @SuppressWarnings("hiding")
    public final static boolean featOkTst = JCasRegistry.getFeatOkTst("ru.misis.asu.nlp.segmentation.types.Sentence");

    /**
     * @generated
     */
    final Feature casFeat_eos;
    /**
     * @generated
     */
    final int casFeatCode_eos;

    /**
     * @param addr low level Feature Structure reference
     * @return the feature value
     * @generated
     */
    public int getEos(int addr) {
        if (featOkTst && casFeat_eos == null)
            jcas.throwFeatMissing("eos", "ru.misis.asu.nlp.segmentation.types.Sentence");
        return ll_cas.ll_getRefValue(addr, casFeatCode_eos);
    }

    /**
     * @param addr low level Feature Structure reference
     * @param v    value to set
     * @generated
     */
    public void setEos(int addr, int v) {
        if (featOkTst && casFeat_eos == null)
            jcas.throwFeatMissing("eos", "ru.misis.asu.nlp.segmentation.types.Sentence");
        ll_cas.ll_setRefValue(addr, casFeatCode_eos, v);
    }


    /**
     * initialize variables to correspond with Cas Type and Features
     *
     * @param jcas    JCas
     * @param casType Type
     * @generated
     */
    public Sentence_Type(JCas jcas, Type casType) {
        super(jcas, casType);
        casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl) this.casType, getFSGenerator());


        casFeat_eos = jcas.getRequiredFeatureDE(casType, "eos", "ru.misis.asu.nlp.tokenization.types.PM", featOkTst);
        casFeatCode_eos = (null == casFeat_eos) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl) casFeat_eos).getCode();

    }
}



    