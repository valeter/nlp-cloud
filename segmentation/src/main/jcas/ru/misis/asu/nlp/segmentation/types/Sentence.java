/* First created by JCasGen Sun Feb 07 11:31:02 MSK 2016 */
package ru.misis.asu.nlp.segmentation.types;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;
import org.apache.uima.jcas.tcas.Annotation;
import ru.misis.asu.nlp.tokenization.types.PM;


/**
 * Updated by JCasGen Sun Feb 07 11:31:02 MSK 2016
 * XML source: C:/Users/����/projects/NLP-Cloud/segmentation/src/main/resources/uima_xml/segmentation-ts.uima_xml
 *
 * @generated
 */
public class Sentence extends Annotation {
    /**
     * @generated
     * @ordered
     */
    @SuppressWarnings("hiding")
    public final static int typeIndexID = JCasRegistry.register(Sentence.class);
    /**
     * @generated
     * @ordered
     */
    @SuppressWarnings("hiding")
    public final static int type = typeIndexID;

    /**
     * @return index of the type
     * @generated
     */
    @Override
    public int getTypeIndexID() {
        return typeIndexID;
    }

    /**
     * Never called.  Disable default constructor
     *
     * @generated
     */
    protected Sentence() {/* intentionally empty block */}

    /**
     * Internal - constructor used by generator
     *
     * @param addr low level Feature Structure reference
     * @param type the type of this Feature Structure
     * @generated
     */
    public Sentence(int addr, TOP_Type type) {
        super(addr, type);
        readObject();
    }

    /**
     * @param jcas JCas to which this Feature Structure belongs
     * @generated
     */
    public Sentence(JCas jcas) {
        super(jcas);
        readObject();
    }

    /**
     * @param jcas  JCas to which this Feature Structure belongs
     * @param begin offset to the begin spot in the SofA
     * @param end   offset to the end spot in the SofA
     * @generated
     */
    public Sentence(JCas jcas, int begin, int end) {
        super(jcas);
        setBegin(begin);
        setEnd(end);
        readObject();
    }

    /**
     * <!-- begin-user-doc -->
     * Write your own initialization here
     * <!-- end-user-doc -->
     *
     * @generated modifiable
     */
    private void readObject() {/*default - does nothing empty block */}


    //*--------------*
    //* Feature: eos

    /**
     * getter for eos - gets
     *
     * @return value of the feature
     * @generated
     */
    public PM getEos() {
        if (Sentence_Type.featOkTst && ((Sentence_Type) jcasType).casFeat_eos == null)
            jcasType.jcas.throwFeatMissing("eos", "ru.misis.asu.nlp.segmentation.types.Sentence");
        return (PM) (jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((Sentence_Type) jcasType).casFeatCode_eos)));
    }

    /**
     * setter for eos - sets
     *
     * @param v value to set into the feature
     * @generated
     */
    public void setEos(PM v) {
        if (Sentence_Type.featOkTst && ((Sentence_Type) jcasType).casFeat_eos == null)
            jcasType.jcas.throwFeatMissing("eos", "ru.misis.asu.nlp.segmentation.types.Sentence");
        jcasType.ll_cas.ll_setRefValue(addr, ((Sentence_Type) jcasType).casFeatCode_eos, jcasType.ll_cas.ll_getFSRef(v));
    }
}

    