/* First created by JCasGen Sun Feb 07 11:31:02 MSK 2016 */
package ru.misis.asu.nlp.segmentation.types;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.TOP_Type;
import org.apache.uima.jcas.tcas.Annotation;
import ru.misis.asu.nlp.morphoanalysis.types.Word;
import ru.misis.asu.nlp.tokenization.types.PM;


/**
 * Updated by JCasGen Sun Feb 07 11:31:02 MSK 2016
 * XML source: C:/Users/����/projects/NLP-Cloud/segmentation/src/main/resources/uima_xml/segmentation-ts.uima_xml
 *
 * @generated
 */
public class HomogeneousSegment extends Annotation {
    /**
     * @generated
     * @ordered
     */
    @SuppressWarnings("hiding")
    public final static int typeIndexID = JCasRegistry.register(HomogeneousSegment.class);
    /**
     * @generated
     * @ordered
     */
    @SuppressWarnings("hiding")
    public final static int type = typeIndexID;

    /**
     * @return index of the type
     * @generated
     */
    @Override
    public int getTypeIndexID() {
        return typeIndexID;
    }

    /**
     * Never called.  Disable default constructor
     *
     * @generated
     */
    protected HomogeneousSegment() {/* intentionally empty block */}

    /**
     * Internal - constructor used by generator
     *
     * @param addr low level Feature Structure reference
     * @param type the type of this Feature Structure
     * @generated
     */
    public HomogeneousSegment(int addr, TOP_Type type) {
        super(addr, type);
        readObject();
    }

    /**
     * @param jcas JCas to which this Feature Structure belongs
     * @generated
     */
    public HomogeneousSegment(JCas jcas) {
        super(jcas);
        readObject();
    }

    /**
     * @param jcas  JCas to which this Feature Structure belongs
     * @param begin offset to the begin spot in the SofA
     * @param end   offset to the end spot in the SofA
     * @generated
     */
    public HomogeneousSegment(JCas jcas, int begin, int end) {
        super(jcas);
        setBegin(begin);
        setEnd(end);
        readObject();
    }

    /**
     * <!-- begin-user-doc -->
     * Write your own initialization here
     * <!-- end-user-doc -->
     *
     * @generated modifiable
     */
    private void readObject() {/*default - does nothing empty block */}


    //*--------------*
    //* Feature: sentence

    /**
     * getter for sentence - gets
     *
     * @return value of the feature
     * @generated
     */
    public Sentence getSentence() {
        if (HomogeneousSegment_Type.featOkTst && ((HomogeneousSegment_Type) jcasType).casFeat_sentence == null)
            jcasType.jcas.throwFeatMissing("sentence", "ru.misis.asu.nlp.segmentation.types.HomogeneousSegment");
        return (Sentence) (jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((HomogeneousSegment_Type) jcasType).casFeatCode_sentence)));
    }

    /**
     * setter for sentence - sets
     *
     * @param v value to set into the feature
     * @generated
     */
    public void setSentence(Sentence v) {
        if (HomogeneousSegment_Type.featOkTst && ((HomogeneousSegment_Type) jcasType).casFeat_sentence == null)
            jcasType.jcas.throwFeatMissing("sentence", "ru.misis.asu.nlp.segmentation.types.HomogeneousSegment");
        jcasType.ll_cas.ll_setRefValue(addr, ((HomogeneousSegment_Type) jcasType).casFeatCode_sentence, jcasType.ll_cas.ll_getFSRef(v));
    }


    //*--------------*
    //* Feature: eopms

    /**
     * getter for eopms - gets
     *
     * @return value of the feature
     * @generated
     */
    public FSArray getEopms() {
        if (HomogeneousSegment_Type.featOkTst && ((HomogeneousSegment_Type) jcasType).casFeat_eopms == null)
            jcasType.jcas.throwFeatMissing("eopms", "ru.misis.asu.nlp.segmentation.types.HomogeneousSegment");
        return (FSArray) (jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((HomogeneousSegment_Type) jcasType).casFeatCode_eopms)));
    }

    /**
     * setter for eopms - sets
     *
     * @param v value to set into the feature
     * @generated
     */
    public void setEopms(FSArray v) {
        if (HomogeneousSegment_Type.featOkTst && ((HomogeneousSegment_Type) jcasType).casFeat_eopms == null)
            jcasType.jcas.throwFeatMissing("eopms", "ru.misis.asu.nlp.segmentation.types.HomogeneousSegment");
        jcasType.ll_cas.ll_setRefValue(addr, ((HomogeneousSegment_Type) jcasType).casFeatCode_eopms, jcasType.ll_cas.ll_getFSRef(v));
    }

    /**
     * indexed getter for eopms - gets an indexed value -
     *
     * @param i index in the array to get
     * @return value of the element at index i
     * @generated
     */
    public PM getEopms(int i) {
        if (HomogeneousSegment_Type.featOkTst && ((HomogeneousSegment_Type) jcasType).casFeat_eopms == null)
            jcasType.jcas.throwFeatMissing("eopms", "ru.misis.asu.nlp.segmentation.types.HomogeneousSegment");
        jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((HomogeneousSegment_Type) jcasType).casFeatCode_eopms), i);
        return (PM) (jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((HomogeneousSegment_Type) jcasType).casFeatCode_eopms), i)));
    }

    /**
     * indexed setter for eopms - sets an indexed value -
     *
     * @param i index in the array to set
     * @param v value to set into the array
     * @generated
     */
    public void setEopms(int i, PM v) {
        if (HomogeneousSegment_Type.featOkTst && ((HomogeneousSegment_Type) jcasType).casFeat_eopms == null)
            jcasType.jcas.throwFeatMissing("eopms", "ru.misis.asu.nlp.segmentation.types.HomogeneousSegment");
        jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((HomogeneousSegment_Type) jcasType).casFeatCode_eopms), i);
        jcasType.ll_cas.ll_setRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((HomogeneousSegment_Type) jcasType).casFeatCode_eopms), i, jcasType.ll_cas.ll_getFSRef(v));
    }


    //*--------------*
    //* Feature: words

    /**
     * getter for words - gets
     *
     * @return value of the feature
     * @generated
     */
    public FSArray getWords() {
        if (HomogeneousSegment_Type.featOkTst && ((HomogeneousSegment_Type) jcasType).casFeat_words == null)
            jcasType.jcas.throwFeatMissing("words", "ru.misis.asu.nlp.segmentation.types.HomogeneousSegment");
        return (FSArray) (jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((HomogeneousSegment_Type) jcasType).casFeatCode_words)));
    }

    /**
     * setter for words - sets
     *
     * @param v value to set into the feature
     * @generated
     */
    public void setWords(FSArray v) {
        if (HomogeneousSegment_Type.featOkTst && ((HomogeneousSegment_Type) jcasType).casFeat_words == null)
            jcasType.jcas.throwFeatMissing("words", "ru.misis.asu.nlp.segmentation.types.HomogeneousSegment");
        jcasType.ll_cas.ll_setRefValue(addr, ((HomogeneousSegment_Type) jcasType).casFeatCode_words, jcasType.ll_cas.ll_getFSRef(v));
    }

    /**
     * indexed getter for words - gets an indexed value -
     *
     * @param i index in the array to get
     * @return value of the element at index i
     * @generated
     */
    public Word getWords(int i) {
        if (HomogeneousSegment_Type.featOkTst && ((HomogeneousSegment_Type) jcasType).casFeat_words == null)
            jcasType.jcas.throwFeatMissing("words", "ru.misis.asu.nlp.segmentation.types.HomogeneousSegment");
        jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((HomogeneousSegment_Type) jcasType).casFeatCode_words), i);
        return (Word) (jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((HomogeneousSegment_Type) jcasType).casFeatCode_words), i)));
    }

    /**
     * indexed setter for words - sets an indexed value -
     *
     * @param i index in the array to set
     * @param v value to set into the array
     * @generated
     */
    public void setWords(int i, Word v) {
        if (HomogeneousSegment_Type.featOkTst && ((HomogeneousSegment_Type) jcasType).casFeat_words == null)
            jcasType.jcas.throwFeatMissing("words", "ru.misis.asu.nlp.segmentation.types.HomogeneousSegment");
        jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((HomogeneousSegment_Type) jcasType).casFeatCode_words), i);
        jcasType.ll_cas.ll_setRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((HomogeneousSegment_Type) jcasType).casFeatCode_words), i, jcasType.ll_cas.ll_getFSRef(v));
    }
}

    