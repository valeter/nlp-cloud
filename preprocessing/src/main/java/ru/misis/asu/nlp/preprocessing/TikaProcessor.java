package ru.misis.asu.nlp.preprocessing;

import org.apache.tika.Tika;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.exception.TikaException;
import org.apache.tika.language.LanguageIdentifier;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TikaProcessor {
    private String text;
    private String language;

    public static TikaProcessor newInstance(URI fileURI) {
        TikaProcessor processor = new TikaProcessor();
        processor.initialize(fileURI);
        return processor;
    }

    private TikaConfig getTikaConfig() {
        return TikaConfig.getDefaultConfig();
    }

    private void initialize(URI fileURI) {
        Tika tika = new Tika(getTikaConfig());

        byte[] bytes;
        try {
            bytes = Files.readAllBytes(Paths.get(fileURI));
        } catch (Exception e) {
            throw new RuntimeException("IOException: " + e.getMessage(), e);
        }

        try (InputStream in = new BufferedInputStream(new ByteArrayInputStream(
                bytes))) {
            text = tika.parseToString(in);
            LanguageIdentifier langIndent = new LanguageIdentifier(text);
            language = langIndent.getLanguage();
        } catch (IOException e) {
            throw new RuntimeException("IOException: " + e.getMessage(), e);
        } catch (TikaException e) {
            throw new RuntimeException("TikaException: " + e.getMessage(), e);
        }
    }

    public String getText() {
        return text;
    }

    public String getLanguage() {
        return language;
    }
}
