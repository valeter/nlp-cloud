package ru.misis.asu.nlp.preprocessing;

import org.apache.uima.cas.CAS;
import org.apache.uima.collection.CollectionException;
import org.apache.uima.collection.CollectionReader_ImplBase;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Logger;
import org.apache.uima.util.Progress;
import org.apache.uima.util.ProgressImpl;
import ru.misis.asu.nlp.commons.config.Config;
import ru.misis.asu.nlp.commons.exceptions.ExceptionHandler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


/**
 * You can use it to read a list of files from a directory. InputDirectory param contains string representation of directory URI
 */
public class FileSystemCollectionReader extends CollectionReader_ImplBase {

    private static Logger logger;

    private ArrayList<File> files = new ArrayList<>();
    private int currentFile = 0;
    private boolean searchSubdirectories;

    @Override
    public void initialize() throws ResourceInitializationException {
        super.initialize();
        logger = getLogger();
        searchSubdirectories = Boolean.getBoolean(Config.getProperty(Config.INPUT_SEARCH_SUBDIRECTORIES));
        String dirName = Config.getProperty(Config.INPUT_DIRECTORY);
        File dir = new File(dirName);
        if (!dir.exists() || !dir.isDirectory()) {
            ExceptionHandler.logAndThrow(logger, "Input directory does not exists or not a directory");
        }
        addFiles(dir);
    }

    private void addFiles(File dir) {
        File[] fileList = dir.listFiles();
        if (fileList != null) {
            for (File file : fileList) {
                if (!file.isDirectory()) {
                    files.add(file);
                } else if (searchSubdirectories) {
                    addFiles(file);
                }
            }
        }
    }

    private SentenceReader sentenceReader;

    private Long startTime;

    @Override
    public void getNext(final CAS aCAS) throws IOException, CollectionException {
        if (sentenceReader == null || !sentenceReader.hasNext()) {
            File file = files.get(currentFile++);
            System.out.println("========================================================================");
            System.out.println();
            System.out.println("Reading file " + currentFile + ": " + file.getName());
            System.out.println();
            System.out.println("========================================================================");
            TikaProcessor processor = new TikaProcessor();
            try {
                processor = TikaProcessor.newInstance(file.toURI());
            } catch (Exception e) {
                ExceptionHandler.logAndRethrow(logger, "TikaProcessor: ", e);
            }

            String documentText = processor.getText();
            if (documentText == null || documentText.length() == 0) {
                ExceptionHandler.logAndThrow(logger, "Document text is null or empty");
            }
            sentenceReader = new SentenceReader(documentText, processor.getLanguage());
        }

        aCAS.setDocumentText(sentenceReader.next());
        aCAS.setDocumentLanguage(sentenceReader.language);
        startTime = System.currentTimeMillis();
    }

    @Override
    public boolean hasNext() throws IOException, CollectionException {
        if (startTime != null) {
            System.out.println("========================================================================");
            System.out.println("Processing time " + (System.currentTimeMillis() - startTime) + " ms");
            System.out.println("Next sentence...");
            System.out.println("========================================================================");
        }
        return (currentFile < files.size()) || (sentenceReader != null && sentenceReader.hasNext());
    }

    @Override
    public Progress[] getProgress() {
        return new Progress[]{new ProgressImpl(currentFile, files.size(), Progress.ENTITIES)};
    }

    @Override
    public void close() throws IOException {
    }

    private class SentenceReader {
        String[] sentences;
        int cur;
        String language;

        SentenceReader(String text, String language) {
            this.sentences = text.split("\n");
            this.language = language;
        }

        String next() {
            return sentences[cur++] + "\n";
        }

        boolean hasNext() {
            return cur < sentences.length;
        }
    }
}
