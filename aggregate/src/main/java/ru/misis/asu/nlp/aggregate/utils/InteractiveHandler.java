package ru.misis.asu.nlp.aggregate.utils;

import org.apache.uima.UIMAException;
import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.Type;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceManager;
import org.apache.uima.util.XMLInputSource;
import ru.misis.asu.nlp.chunking.types.TextCorrected;
import ru.misis.asu.nlp.commons.config.Config;

import java.io.IOException;

import static java.lang.System.currentTimeMillis;

public class InteractiveHandler {
    private AnalysisEngine ae;

    @SuppressWarnings("ConstantConditions")
    public InteractiveHandler() throws IOException, UIMAException {
        System.out.println("Descriptor file: " + Config.getProperty(Config.DESCRIPTOR_FILE_INTERACTIVE));
        XMLInputSource aeDescInput = new XMLInputSource(
                InteractiveHandler.class.getClassLoader().getResource(Config.getProperty(Config.DESCRIPTOR_FILE_INTERACTIVE)));
        AnalysisEngineDescription aeDesc = UIMAFramework.getXMLParser().parseAnalysisEngineDescription(aeDescInput);

        ResourceManager resourceManager = UIMAFramework.newDefaultResourceManager();
        ae = UIMAFramework.produceAnalysisEngine(aeDesc, resourceManager, null);
    }

    public String handleString(String s, String language) throws UIMAException {
        JCas cas = ae.newJCas();
        cas.setDocumentText(s + "\n");
        cas.setDocumentLanguage(language);
        System.out.println("Start processing jcas");
        long timeBefore = currentTimeMillis();
        ae.process(cas);
        System.out.println("Finished in " + (currentTimeMillis() - timeBefore) + " ms");
        Type type = cas.getTypeSystem().getType("ru.misis.asu.nlp.chunking.types.TextCorrected");
        return ((TextCorrected)cas.getAnnotationIndex(type).iterator().next()).getCorrection();
    }
}
