package ru.misis.asu.nlp.aggregate.utils;

import org.apache.uima.UIMAException;
import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceManager;
import org.apache.uima.util.XMLInputSource;
import ru.misis.asu.nlp.commons.config.Config;

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

import static java.lang.System.currentTimeMillis;

/**
 * @author valter
 */
public class Launcher {
    @SuppressWarnings("ConstantConditions")
    public static void main(String[] args) throws Exception {
        if (Arrays.stream(args).anyMatch("help"::equals)) {
            Config.printUsage(System.out);
            return;
        }

        Mode mode = Arrays.stream(args)
                .filter(a -> a.startsWith("mode="))
                .map(a -> a.substring("mode=".length()))
                .map(Mode::valueOf)
                .findFirst()
                .orElse(Mode.STATIC);

        if (mode == Mode.STATIC) {
            System.out.println("Descriptor file: " + Config.getProperty(Config.DESCRIPTOR_FILE));
            XMLInputSource aeDescInput = new XMLInputSource(
                    Launcher.class.getClassLoader().getResource(Config.getProperty(Config.DESCRIPTOR_FILE)));
            AnalysisEngineDescription aeDesc = UIMAFramework.getXMLParser().parseAnalysisEngineDescription(aeDescInput);

            ResourceManager resMgr = UIMAFramework.newDefaultResourceManager();
            AnalysisEngine ae = UIMAFramework.produceAnalysisEngine(aeDesc, resMgr, null);
            JCas cas = ae.newJCas();
            System.out.println("Start processing jcas");
            long timeBefore = currentTimeMillis();
            ae.process(cas);
            System.out.println("Finished in " + (currentTimeMillis() - timeBefore) + " ms");
        } else if (mode == Mode.INTERACTIVE){
            InteractiveHandler interactiveHandler = new InteractiveHandler();
            System.out.println("Type 'stop' to end session");
            System.out.print("Input sentence: ");
            Scanner scanner = new Scanner(System.in);
            while (scanner.hasNext()) {
                String next = scanner.nextLine();
                if ("stop".equals(next)) {
                    break;
                }
                interactiveHandler.handleString(next, Config.getProperty(Config.LANGUAGE));
                System.out.print("Input sentence: ");
            }
            System.out.println("Finished interactive session");
        } else if (mode == Mode.SERVER) {
            NlpCloudServer server = new NlpCloudServer(
                    Integer.parseInt(Config.getProperty(Config.PORT)),
                    Config.getProperty(Config.LANGUAGE));
            server.start();
        }
    }

    private enum Mode {
        STATIC,
        INTERACTIVE,
        SERVER
    }
}
