package ru.misis.asu.nlp.aggregate.utils;

import org.apache.uima.UIMAException;
import org.eclipse.jetty.http.HttpMethod;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Objects;
import java.util.stream.Collectors;

public class NlpCloudServer {
    private int port;
    private String language;
    private Server server;
    private InteractiveHandler interactiveHandler;

    public NlpCloudServer(int port, String language) throws IOException, UIMAException {
        this.port = port;
        this.language = language;
        this.interactiveHandler = new InteractiveHandler();
    }

    public void start() throws Exception {
        server = new Server(port);
        server.setHandler(new AutocorrectionHandler());
        server.start();
        System.out.println("Server started on port " + port);
        server.join();
    }

    public class AutocorrectionHandler extends AbstractHandler {
        public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response)
                throws IOException, ServletException {
            if (!Objects.equals(request.getMethod(), HttpMethod.POST.name())) {
                return;
            }

            Charset charset = request.getCharacterEncoding() != null
                    ? Charset.forName(request.getCharacterEncoding())
                    : Charset.forName("UTF-8");
            String requestString;
            try (BufferedReader in = new BufferedReader(new InputStreamReader(request.getInputStream(), charset))) {
                requestString = in.lines().collect(Collectors.joining(System.lineSeparator()));
            }
            try {
                String result = interactiveHandler.handleString(requestString, language);
                response.setContentType("text/plain;charset=utf-8");
                response.setStatus(HttpServletResponse.SC_OK);
                baseRequest.setHandled(true);
                response.getWriter().println(result);
            } catch (Exception e) {
                e.printStackTrace();
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                baseRequest.setHandled(true);
            }
        }
    }
}
