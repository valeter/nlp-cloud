package ru.misis.asu.nlp.aggregate.utils;/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */


import org.apache.uima.UIMAFramework;
import org.apache.uima.cas.*;
import org.apache.uima.collection.CollectionProcessingEngine;
import org.apache.uima.collection.EntityProcessStatus;
import org.apache.uima.collection.StatusCallbackListener;
import org.apache.uima.collection.impl.metadata.cpe.CpeDescriptorFactory;
import org.apache.uima.collection.metadata.*;
import org.apache.uima.tools.components.FileSystemCollectionReader;
import org.apache.uima.tools.components.InlineXmlCasConsumer;
import org.apache.uima.util.AnalysisEnginePerformanceReports;

import java.io.File;

/**
 * An example application that reads documents from the file system, sends them though an Analysis
 * Engine(AE), and produces XML files with inline annotations. This application uses a
 * {@link CollectionProcessingEngine} to drive the processing. For a simpler introduction to using
 * <p>
 * <code>Usage: java org.apache.uima.examples.RunAE [OPTIONS] 
 * &lt;AE descriptor or JAR file name&gt; &lt;input dir&gt; 
 * [&lt;output dir&gt;]</code>
 * <p>
 * If <code>output dir</code> is not specified, the analysis results will not be output. This can
 * be useful when only interested in performance statistics.
 * <p>
 * <u>OPTIONS</u>
 * <p>
 * -t &lt;TagName&gt; (XML Text Tag) - specifies the name of an XML tag, found within the input
 * documents, that contains the text to be analyzed. The text will also be detagged. If this option
 * is not specified, the entire document will be processed. <br>
 * -l &lt;ISO code&gt; (Language) - specifies the ISO code for the language of the input documents.
 * Some AEs (e.g. PersonTitleAnnotator) require this. <br>
 * -e &lt;Encoding&gt; - specifies character encoding of the input documents. The default is UTF-8.
 * <br>
 * -q (Quiet) - supresses progress messages that are normally printed as each document is processed.
 * <br>
 * -s&lt;x&gt; (Stats level) - determines the verboseness of performance statistics. s0=none,
 * s1=brief, s2=full. The default is brief. <br>
 * -x - process input files as XCAS files.
 */
public class RunAE implements StatusCallbackListener {
    // Values read from cmd line args
    private String aeSpecifierFile = "/Users/valter/personal_projects/nlp-cloud/aggregate/src/main/resources/uima_xml/ru-spellcheck-ae.uima_xml";

    private File inputDir = new File("/Users/valter/tmp/in_ru");

    private File outputDir = new File("/Users/valter/tmp/out");

    private String encoding = "UTF-8";

    private boolean genProgressMessages = true;

    private int statsLevel = 2;

    private boolean xcasInput = false;

    private boolean xmiInput = false;

    private boolean xLenient = false;

    int docsProcessed;

    private CollectionProcessingEngine mCPE;

    /**
     * Constructor. Sets up and runs an Analysis Engine.
     */
    private RunAE() {
        try {
            // build a Collection Processing Engine descriptor that will drive processing
            CpeDescription cpeDesc = CpeDescriptorFactory.produceDescriptor();

            // add collection reader that will read input docs
            cpeDesc.addCollectionReader(FileSystemCollectionReader.getDescriptorURL().toString());
            // specify configuration parameters for collection reader
            CasProcessorConfigurationParameterSettings crSettings = CpeDescriptorFactory
                    .produceCasProcessorConfigurationParameterSettings();
            CpeCollectionReader cpeCollRdr = cpeDesc.getAllCollectionCollectionReaders()[0];
            cpeCollRdr.setConfigurationParameterSettings(crSettings);
            crSettings.setParameterValue(FileSystemCollectionReader.PARAM_INPUTDIR, inputDir
                    .getAbsolutePath());
            crSettings.setParameterValue(FileSystemCollectionReader.PARAM_ENCODING, encoding);
            if (xcasInput) {
                crSettings.setParameterValue(FileSystemCollectionReader.PARAM_XCAS, "XCAS");
            } else if (xmiInput) {
                crSettings.setParameterValue(FileSystemCollectionReader.PARAM_XCAS, "XMI");
            }
            if (xLenient) {
                crSettings.setParameterValue(FileSystemCollectionReader.PARAM_LENIENT, "true");
            }

            // add user's AE to CPE
            CpeCasProcessor casProc = CpeDescriptorFactory.produceCasProcessor("UserAE");
            CpeComponentDescriptor cpeComponentDescriptor =
                    CpeDescriptorFactory.produceComponentDescriptor(aeSpecifierFile);
            casProc.setCpeComponentDescriptor(cpeComponentDescriptor);
            casProc.setMaxErrorCount(0);
            cpeDesc.addCasProcessor(casProc);

            // add CAS Consumer that will write the output
            // create and configure CAS consumer that will write the output
            CpeCasProcessor casCon = null;
            if (outputDir != null) {
                casCon = CpeDescriptorFactory.produceCasProcessor("CasConsumer");
                cpeComponentDescriptor =
                        CpeDescriptorFactory.produceComponentDescriptor(InlineXmlCasConsumer.getDescriptorURL().toString());
                casCon.setCpeComponentDescriptor(cpeComponentDescriptor);
                CasProcessorConfigurationParameterSettings consumerSettings = CpeDescriptorFactory
                        .produceCasProcessorConfigurationParameterSettings();
                casCon.setConfigurationParameterSettings(consumerSettings);
                consumerSettings.setParameterValue(InlineXmlCasConsumer.PARAM_OUTPUTDIR, outputDir
                        .getAbsolutePath());
                if (xcasInput) {
                    consumerSettings.setParameterValue(InlineXmlCasConsumer.PARAM_XCAS, "XCAS");
                } else if (xmiInput) {
                    consumerSettings.setParameterValue(InlineXmlCasConsumer.PARAM_XCAS, "XMI");
                }
                casCon.setMaxErrorCount(0);
                cpeDesc.addCasProcessor(casCon);
            }

            // instantiate CPE
            mCPE = UIMAFramework.produceCollectionProcessingEngine(cpeDesc);
            // register callback listener
            mCPE.addStatusCallbackListener(this);

            // execute
            docsProcessed = 0;
            mCPE.process();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * @see org.apache.uima.collection.base_cpm.BaseStatusCallbackListener#initializationComplete()
     */
    public void initializationComplete() {
    }

    /**
     * @see org.apache.uima.collection.StatusCallbackListener#entityProcessComplete(org.apache.uima.cas.CAS,
     *      org.apache.uima.collection.EntityProcessStatus)
     */
    public void entityProcessComplete(CAS aCas, EntityProcessStatus aStatus) {
        if (aStatus.isException()) {
            for (Object o : aStatus.getExceptions()) {
                ((Throwable) o).printStackTrace();
            }
        } else if (genProgressMessages) {
            // retrieve the filename of the input file from the CAS
            // (it was put there by the FileSystemCollectionReader)
            if (!(xcasInput || xmiInput)) {
                Type fileLocType = aCas.getTypeSystem().getType(
                        "org.apache.uima.examples.SourceDocumentInformation");
                Feature fileNameFeat = fileLocType.getFeatureByBaseName("uri");
                FSIterator it = aCas.getAnnotationIndex(fileLocType).iterator();
                FeatureStructure fileLoc = it.get();
                File inFile = new File(fileLoc.getStringValue(fileNameFeat));
                System.out.println("Processed Document " + inFile.getName());
            } else {
                System.out.println("doc" + docsProcessed++ + " processed successfully");
            }
        }
    }

    /**
     * @see org.apache.uima.collection.base_cpm.BaseStatusCallbackListener#aborted()
     */
    public void aborted() {
        System.out.println("Processing Aborted");
    }

    /**
     * @see org.apache.uima.collection.base_cpm.BaseStatusCallbackListener#batchProcessComplete()
     */
    public void batchProcessComplete() {
    }

    /**
     * @see org.apache.uima.collection.base_cpm.BaseStatusCallbackListener#collectionProcessComplete()
     */
    public void collectionProcessComplete() {
        // output performance stats
        if (statsLevel > 0) {
            AnalysisEnginePerformanceReports performanceReports = new AnalysisEnginePerformanceReports(
                    mCPE.getPerformanceReport());
            System.out.println("\n\nPERFORMANCE STATS\n-----------------\n\n");
            if (statsLevel > 1) {
                System.out.println(performanceReports.getFullReport());
                System.out.println();
            }
            System.out.println(performanceReports);
        }
    }

    /**
     * @see org.apache.uima.collection.base_cpm.BaseStatusCallbackListener#paused()
     */
    public void paused() {
    }

    /**
     * @see org.apache.uima.collection.base_cpm.BaseStatusCallbackListener#resumed()
     */
    public void resumed() {
    }

    public static void main(String[] args) {
        new RunAE();
    }
}