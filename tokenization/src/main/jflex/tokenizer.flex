package ru.misis.asu.nlp.tokenization;
import ru.misis.asu.nlp.tokenization.types.Token;
import ru.misis.asu.nlp.tokenization.types.Letters;
import ru.misis.asu.nlp.tokenization.types.LettersRus;
import ru.misis.asu.nlp.tokenization.types.LettersEng;
import ru.misis.asu.nlp.tokenization.types.ComplexLetters;
import ru.misis.asu.nlp.tokenization.types.ComplexLettersRus;
import ru.misis.asu.nlp.tokenization.types.ComplexLettersEng;
import ru.misis.asu.nlp.tokenization.types.Number;
import ru.misis.asu.nlp.tokenization.types.Separator;
import ru.misis.asu.nlp.tokenization.types.PM;
import ru.misis.asu.nlp.tokenization.types.Bracket;
import ru.misis.asu.nlp.tokenization.types.Symbol;
import ru.misis.asu.nlp.tokenization.types.Range;
import ru.misis.asu.nlp.tokenization.types.Abbrevation;
import ru.misis.asu.nlp.tokenization.types.Currency;
import ru.misis.asu.nlp.tokenization.types.Measurement;
import ru.misis.asu.nlp.tokenization.types.Date;
import ru.misis.asu.nlp.tokenization.types.Email;
import ru.misis.asu.nlp.tokenization.types.Abbrevation;
import ru.misis.asu.nlp.tokenization.types.Percent;
import org.apache.uima.jcas.JCas;
%%
%public
%class JFlexTokenizer
%standalone
%type Token
%unicode

%eofval{
return null;
%eofval}

%state IN_RANGE
%state IN_CURRENCY
%state IN_MEAS
%state IN_COMPLEX
%state IN_DATE_YMD
%state IN_DATE_DMY
%state IN_PERCENTS
// Java-code ------------------------------------- 
%{
    private static String RUSSIAN = "Russian";
    private static String ENGLISH = "English";

	private JCas UIMA_JCas;

	private Range currentRange;
	private Currency currentCurrency;
	private ComplexLetters currentComplexWord;
	private Date currentDate;
	
	private boolean inRange;
	private boolean currencyEnd;
	private boolean inComplex;
	private boolean inDate;

	private int beginPosition;
	

	JFlexTokenizer(java.io.Reader in, JCas UIMA_JCas) {
		this.UIMA_JCas = UIMA_JCas;
    	this.zzReader = in;
    	allBooleansToFalse();
    }
    
    private void allBooleansToFalse() {
    	this.inRange = false; 
    	this.currencyEnd = false;
    	this.inComplex = false;
    	this.inDate = false; 		
    }

	boolean isEof() {
		return zzAtEOF;
	}

	private void back() {
		yypushback(yylength());
	}	  
	
	private Letters getLettersToken(String language, String letterCase) {
	    Letters token;

	    if (language == RUSSIAN) {
	        token = new LettersRus(UIMA_JCas);
	    } else if (language == ENGLISH) {
	        token = new LettersEng(UIMA_JCas);
	    } else {
		    token = new Letters(UIMA_JCas);
		}

		token.setText(yytext());
		token.setNorm(token.getText().trim().toLowerCase());
		token.setLanguage(language);
		token.setLetterCase(letterCase);
		token.setBegin(zzCurrentPos);
		token.setEnd(zzCurrentPos + yylength());
		return token;
	}

    private ComplexLetters getComplexLettersToken(String left, String right, String separator, String language) {
        ComplexLetters token = new ComplexLetters(UIMA_JCas);
        token.setText(yytext());
        token.setNorm(token.getText().trim().toLowerCase());
        token.setBegin(zzCurrentPos);
        token.setEnd(zzCurrentPos + yylength());
        token.setLeft(left);
        token.setRight(right);
        token.setLanguage(language);
        token.setSeparator(separator);
        return token;
    }

    private ComplexLettersRus getComplexLettersRusToken(String left, String right, String separator) {
        ComplexLettersRus token = new ComplexLettersRus(UIMA_JCas);
        token.setText(yytext());
        token.setNorm(token.getText().trim().toLowerCase());
        token.setBegin(zzCurrentPos);
        token.setEnd(zzCurrentPos + yylength());
        token.setLeft(left);
        token.setRight(right);
        token.setLanguage(RUSSIAN);
        token.setSeparator(separator);
        return token;
    }

    private ComplexLettersEng getComplexLettersEngToken(String left, String right, String separator) {
        ComplexLettersEng token = new ComplexLettersEng(UIMA_JCas);
        token.setText(yytext());
        token.setNorm(token.getText().trim().toLowerCase());
        token.setBegin(zzCurrentPos);
        token.setEnd(zzCurrentPos + yylength());
        token.setLeft(left);
        token.setRight(right);
        token.setLanguage(ENGLISH);
        token.setSeparator(separator);
        return token;
    }

    private ComplexLetters addToComplexLetters(ComplexLetters letters, String text, String language, boolean isSeparator) {
        ComplexLetters result;
        if (letters.getLeft() == null) {
            if (language == RUSSIAN) {
                result = getComplexLettersRusToken(text, null, null);
            } else if (language == ENGLISH) {
                result = getComplexLettersEngToken(text, null, null);
            } else {
                result = getComplexLettersToken(text, null, null, language);
            }
        } else if (isSeparator) {
            if (letters.getLanguage() == RUSSIAN) {
                result = getComplexLettersRusToken(letters.getLeft(), null, text);
            } else if (letters.getLanguage() == ENGLISH) {
                result = getComplexLettersEngToken(letters.getLeft(), null, text);
            } else {
                result = getComplexLettersToken(letters.getLeft(), null, text, language);
            }
        } else {
            if (language == RUSSIAN && letters.getLanguage() == RUSSIAN) {
                result = getComplexLettersRusToken(letters.getLeft(), text, letters.getSeparator());
            } else if (language == ENGLISH && letters.getLanguage() == ENGLISH) {
                result = getComplexLettersEngToken(letters.getLeft(), text, letters.getSeparator());
            } else {
                result = getComplexLettersToken(letters.getLeft(), text, letters.getSeparator(), letters.getLanguage() + "-" + language);
            }
        }
        result.setText(letters.getText());
        result.setNorm(letters.getNorm());
        result.setBegin(letters.getBegin());
        return result;
    }
	
	private Number getNumberToken(String kind, String sign) {
		Number token = new Number(UIMA_JCas);
		token.setNorm(null);
		token.setText(yytext());
		token.setKind(kind);
		token.setSign(sign);
		token.setBegin(zzCurrentPos);
		token.setEnd(zzCurrentPos + yylength());
		return token;
	}
	
	private Separator getSeparatorToken(String kind) {
		Separator token = new Separator(UIMA_JCas);
		token.setNorm(null);
		token.setText(yytext());
		token.setKind(kind);		
		token.setBegin(zzCurrentPos);
		token.setEnd(zzCurrentPos + yylength());
		return token;
	}
	
	private PM getPmToken() {
		PM token = new PM(UIMA_JCas);
		token.setNorm(null);
		token.setText(yytext());
		token.setBegin(zzCurrentPos);
		token.setEnd(zzCurrentPos + yylength());
		return token;
	}
	
	private Bracket getBracketToken() {
		Bracket token = new Bracket(UIMA_JCas);
		token.setNorm(null);
		token.setText(yytext());
		token.setBegin(zzCurrentPos);
		token.setEnd(zzCurrentPos + yylength());
		return token;
	}
	
	private Symbol getSymbolToken() {
		Symbol token = new Symbol(UIMA_JCas);
		token.setNorm(null);
		token.setText(yytext());
		token.setBegin(zzCurrentPos);
		token.setEnd(zzCurrentPos + yylength());
		return token;
	}
	
	private Abbrevation getAbbrevationToken(String lang) {
		Abbrevation token = new Abbrevation(UIMA_JCas);
		token.setNorm(null);
		token.setText(yytext());
		token.setBegin(zzCurrentPos);
		token.setEnd(zzCurrentPos + yylength());
		token.setLanguage(lang);
		return token;
	}

	private Range getRangeToken(String left, String right) {
		Range token = new Range(UIMA_JCas);
		token.setNorm(null);
		token.setText(yytext());
		token.setBegin(zzCurrentPos);
		token.setEnd(zzCurrentPos + yylength());
		token.setLeft(left);
		token.setRight(right);
		return token;
	}

	private Currency getCurrencyToken(String value, String kindOfCurrency) {
		Currency token = new Currency(UIMA_JCas);
		token.setNorm(null);
		token.setText(yytext());
		token.setBegin(zzCurrentPos);
		token.setEnd(zzCurrentPos + yylength());
		token.setValue(value);
		token.setCurrencySymbol(kindOfCurrency);
		return token;
	}

	private Measurement getMeasurementToken(Number value, String unitName) {
		Measurement token = new Measurement(UIMA_JCas);
		token.setNorm(null);
		token.setText(yytext());
		token.setValue(value);
		token.setUnitName(unitName);
		return token;
	}

	private Date getDateToken(String year, String mounth, String day) {
		Date token = new Date(UIMA_JCas);
		token.setNorm(null);
		token.setText(yytext());
		token.setBegin(zzCurrentPos);
		token.setEnd(zzCurrentPos + yylength());
		token.setYear(year);
		token.setMonth(mounth);
		token.setDay(day);
		return token;
	}

	private Email getEmailToken() {
		Email token = new Email(UIMA_JCas);
		token.setNorm(null);
		token.setText(yytext());
		token.setBegin(zzCurrentPos);
		token.setEnd(zzCurrentPos + yylength());
		return token;
	}
	
%}
// -----------------------------------------------

// REGULARS --------------------------------------	
	
	// SEPATARORS
	NEW_LINE = "\n" 
	CAR_RET = "\r" 
	TAB = "\t" 
	SPACE = " "
	FORM_FEED = "\f" 
	DEC_SEPARATOR = ","|"."
	
	

	// NUMBERS	
	DIGITS = [0-9]+
	NUMBER_REAL_POSITIVE = "+"?{DIGITS}{DEC_SEPARATOR}{DIGITS} 
	NUMBER_REAL_NEGATIVE = ("-"|"−"){DIGITS}{DEC_SEPARATOR}{DIGITS} 
	NUMBER_INTEGER_POSITIVE = ("+")?{DIGITS} 
	NUMBER_INTEGER_NEGATIVE = ("-"|"−"){DIGITS}
	NUMBER = {NUMBER_REAL_POSITIVE}|{NUMBER_REAL_NEGATIVE}|{NUMBER_INTEGER_POSITIVE}|{NUMBER_INTEGER_NEGATIVE} 	

	
	PERCENT_SYMBOL = "%"
	PERCENTS_IP_1 = {NUMBER_INTEGER_POSITIVE}{SPACE}{PERCENT_SYMBOL} 
	PERCENTS_IN_1 = {NUMBER_INTEGER_NEGATIVE}{SPACE}{PERCENT_SYMBOL}
	PERCENTS_IP_2 = {NUMBER_INTEGER_POSITIVE}{PERCENT_SYMBOL} 
	PERCENTS_IN_2 = {NUMBER_INTEGER_NEGATIVE}{PERCENT_SYMBOL}	
	PERCENTS_RP_1 = {NUMBER_REAL_POSITIVE}{SPACE}{PERCENT_SYMBOL}
	PERCENTS_RN_1 = {NUMBER_REAL_NEGATIVE}{SPACE}{PERCENT_SYMBOL}	
	PERCENTS_RP_2 = {NUMBER_REAL_POSITIVE}{PERCENT_SYMBOL}
	PERCENTS_RN_2 = {NUMBER_REAL_NEGATIVE}{PERCENT_SYMBOL}
	PERCENTS = {PERCENTS_IP_1}|{PERCENTS_IN_1}|{PERCENTS_IP_2}|{PERCENTS_IN_2}|{PERCENTS_RP_1}|{PERCENTS_RN_1}|{PERCENTS_RP_2}|{PERCENTS_RN_2}
	
	CURRENCY_SYMBOL = [$£¥₣€]
	CURRENCY_IP_1 = {NUMBER_INTEGER_POSITIVE}{CURRENCY_SYMBOL}
	CURRENCY_IP_2 = {NUMBER_INTEGER_POSITIVE}{SPACE}{CURRENCY_SYMBOL}
	CURRENCY_IP_3 = {CURRENCY_SYMBOL}{NUMBER_INTEGER_POSITIVE}
	CURRENCY_IP_4 = {CURRENCY_SYMBOL}{SPACE}{NUMBER_INTEGER_POSITIVE}
	CURRENCY_IN_1 = {NUMBER_INTEGER_NEGATIVE}{CURRENCY_SYMBOL}
	CURRENCY_IN_2 = {NUMBER_INTEGER_NEGATIVE}{SPACE}{CURRENCY_SYMBOL}
	CURRENCY_IN_3 = {CURRENCY_SYMBOL}{NUMBER_INTEGER_NEGATIVE}
	CURRENCY_IN_4 = {CURRENCY_SYMBOL}{SPACE}{NUMBER_INTEGER_NEGATIVE}
	CURRENCY_RP_1 = {NUMBER_REAL_POSITIVE}{CURRENCY_SYMBOL}
	CURRENCY_RP_2 = {NUMBER_REAL_POSITIVE}{SPACE}{CURRENCY_SYMBOL}
	CURRENCY_RP_3 = {CURRENCY_SYMBOL}{NUMBER_INTEGER_POSITIVE}
	CURRENCY_RP_4 = {CURRENCY_SYMBOL}{SPACE}{NUMBER_REAL_POSITIVE}
	CURRENCY_RN_1 = {NUMBER_REAL_NEGATIVE}{CURRENCY_SYMBOL}
	CURRENCY_RN_2 = {NUMBER_REAL_NEGATIVE}{SPACE}{CURRENCY_SYMBOL}
	CURRENCY_RN_3 = {CURRENCY_SYMBOL}{NUMBER_REAL_NEGATIVE}
	CURRENCY_RN_4 = {CURRENCY_SYMBOL}{SPACE}{NUMBER_REAL_NEGATIVE}
	CURRENCY = {CURRENCY_IP_1}|{CURRENCY_IP_2}|{CURRENCY_IP_3}|{CURRENCY_IP_4}|{CURRENCY_IN_1}|{CURRENCY_IN_2}|{CURRENCY_IN_3}|{CURRENCY_IN_4}|{CURRENCY_RP_1}|{CURRENCY_RP_2}|{CURRENCY_RP_3}|{CURRENCY_RP_4}|{CURRENCY_RN_1}|{CURRENCY_RN_2}|{CURRENCY_RN_3}|{CURRENCY_RN_4}
	
	// TEMPERATURE	
	TEMPERATURE_SYMBOL = "°С"|"°F" 
	TEMPERATURE_IP_1 = {NUMBER_INTEGER_POSITIVE}{SPACE}{TEMPERATURE_SYMBOL}
	TEMPERATURE_IN_1 = {NUMBER_INTEGER_NEGATIVE}{SPACE}{TEMPERATURE_SYMBOL}
	TEMPERATURE_RP_1 = {NUMBER_REAL_POSITIVE}{SPACE}{TEMPERATURE_SYMBOL}
	TEMPERATURE_RN_1 = {NUMBER_REAL_NEGATIVE}{SPACE}{TEMPERATURE_SYMBOL}
	TEMPERATURE_IP_2 = {NUMBER_INTEGER_POSITIVE}{TEMPERATURE_SYMBOL}
	TEMPERATURE_IN_2 = {NUMBER_INTEGER_NEGATIVE}{TEMPERATURE_SYMBOL}
	TEMPERATURE_RP_2 = {NUMBER_REAL_POSITIVE}{TEMPERATURE_SYMBOL}
	TEMPERATURE_RN_2 = {NUMBER_REAL_NEGATIVE}{TEMPERATURE_SYMBOL}
	TEMPERATURE = {TEMPERATURE_IP_1}|{TEMPERATURE_IN_1}|{TEMPERATURE_RP_1}|{TEMPERATURE_RN_1}|{TEMPERATURE_IP_2}|{TEMPERATURE_IN_2}|{TEMPERATURE_RP_2}|{TEMPERATURE_RN_2}

	// LETTERS
	LETTERS_RUS_CAP = [A-ЯЁ][а-яё]+
	LETTERS_RUS_LOW = [а-яё]+
	LETTERS_RUS_UPP = [А-ЯЁ]+
	LETTERS_RUS_MIX = [А-Яа-яёЁ]+
	LETTERS_ENG_CAP = [A-Z][a-z]+
	LETTERS_ENG_LOW = [a-z]+
	LETTERS_ENG_UPP = [A-Z]+
	LETTERS_ENG_MIX = [A-Za-z]+
	LETTERS_RUS = {LETTERS_RUS_CAP}|{LETTERS_RUS_LOW}|{LETTERS_RUS_UPP}|{LETTERS_RUS_MIX}
	LETTERS_ENG = {LETTERS_ENG_CAP}|{LETTERS_ENG_LOW}|{LETTERS_ENG_UPP}|{LETTERS_ENG_MIX}
	LETTERS = {LETTERS_RUS}|{LETTERS_ENG}

	COMPLEX_LETTERS = {LETTERS}("-"|"—"|"'"){LETTERS}
    COMPLEX_LETTERS_RUS = {LETTERS_RUS}("-"|"—"){LETTERS_RUS}
    COMPLEX_LETTERS_ENG = {LETTERS_ENG}("-"|"—"|"'"){LETTERS_ENG}

	//DATE
	YYYY = [1-9][0-9][0-9][0-9]
	MM = ([0][0-9])|([1][0-2])
	DD = ([0-2][0-9])|([3][0-1])
	M = ([1-9])|([1][0-2])	
	D = ([1-9])|([1-2][0-9])|([3][0-1])
	DATE_YYYY_MM_DD_DOT_SEP = {YYYY}"."{SPACE}{MM}"."{SPACE}{DD}
	DATE_YYYY_MM_DD_DASH = {YYYY}[-−]{MM}[-−]{DD}
	DATE_YYYY_MM_DD_SLASH = {YYYY}"/"{MM}"/"{DD}
	DATE_YYYY_M_D_DASH = {YYYY}[-−]{M}[-−]{D}
	DATE_YYYY_M_D_SLASH = {YYYY}"/"{M}"/"{D}
	DATE_D_M_YYYY_DOT = {D}"."{M}"."{YYYY}
	DATE_D_M_YYYY_DASH = {D}[-−]{M}[-−]{YYYY}
	DATE_D_M_YYYY_SLASH = {D}"/"{M}"/"{YYYY}
	DATE_DD_MM_YYYY_DOT = {DD}"."{MM}"."{YYYY}
	DATE_DD_MM_YYYY_DASH = {DD}[-−]{MM}[-−]{YYYY}]
	DATE_DD_MM_YYYY_SLASH = {DD}"/"{MM}"/"{YYYY}
	DATE_M_D_YYYY_SLASH = {M}"/"{D}"/"{YYYY}
	DATE_YMD = {DATE_YYYY_MM_DD_DOT_SEP}|{DATE_YYYY_MM_DD_DASH}|{DATE_YYYY_MM_DD_SLASH}|{DATE_YYYY_M_D_DASH}|{DATE_YYYY_M_D_SLASH}
	DATE_DMY = {DATE_D_M_YYYY_DOT}|{DATE_D_M_YYYY_DASH}|{DATE_D_M_YYYY_SLASH}|{DATE_DD_MM_YYYY_DOT}|{DATE_DD_MM_YYYY_DASH}|{DATE_DD_MM_YYYY_SLASH}

	//TIME
	AM_PM = "a.m."|"p.m"|"AM"|"PM"
	//TIME_12 = ([1-9])|([1][0-2])":"([0-5][0-9])|("60")){SPACE}{AM_PM}
	TIME_24 = (([0][0-9])|([1][0-9])|([2][0-3]))":"(([0-5][0-9])|("60"))
	
	// MAIL
	E_MAIL = ([a-zA-Z0-9!#$%*+/=\?\^_\x2D\{\|\}~\.\x26]+)"@"([a-zA-Z0-9\._-]+[a-zA-Z]{2,4})
	
	RANGE = {NUMBER}("-"|"—"){NUMBER} 

	BRACKET = [\[\]\(\)\{\}]

	PM = [\.,:;!\?…——-]

	SYMBOLS = [«»$£¥₣€°@#\%\^&\*№<>\+=]|("\"")

	ABBREVATION_RUS = "ж/д"|"т.д."|"т. д."|"т.п."|"т. п."|"пр."|"см."|"др."|"гг."|"б/y"|"и.о."|"и. о."|"т.е."|"т. e."|([Пп]"римеч.")|([Сс]"ост.")|("Спб.")
	ABBREVATION_ENG = "anon."|"etc."|"a.m."|"p.m"|"AM"|"PM"
	//MEASUREMET = "кг"|"м"|"м/c"|"см"|"км/ч"
	//UINIT_OF_MEASUREMENT = {NUMBER}{SPACE}{MEASUREMENT}
//------------------------------------------------
%%
<YYINITIAL> 
{
		{NUMBER_REAL_POSITIVE}		{ yybegin(YYINITIAL); return getNumberToken("Real","Positive"); } 
		{NUMBER_REAL_NEGATIVE}		{ yybegin(YYINITIAL); return getNumberToken("Real","Negative"); } 
		{NUMBER_INTEGER_POSITIVE}	{ yybegin(YYINITIAL); return getNumberToken("Integer","Positive"); } 
		{NUMBER_INTEGER_NEGATIVE}	{ yybegin(YYINITIAL); return getNumberToken("Integer","Negative"); }
		
		{LETTERS_RUS_CAP}			{ yybegin(YYINITIAL); return getLettersToken(RUSSIAN,"Capital"); }
		{LETTERS_RUS_LOW}			{ yybegin(YYINITIAL); return getLettersToken(RUSSIAN,"Lower"); }
		{LETTERS_RUS_UPP}			{ yybegin(YYINITIAL); return getLettersToken(RUSSIAN,"Upper"); }
		{LETTERS_RUS_MIX}			{ yybegin(YYINITIAL); return getLettersToken(RUSSIAN,"Mixed"); }
		{LETTERS_ENG_CAP}			{ yybegin(YYINITIAL); return getLettersToken(ENGLISH,"Capital"); }
		{LETTERS_ENG_LOW}			{ yybegin(YYINITIAL); return getLettersToken(ENGLISH,"Lower"); }
		{LETTERS_ENG_UPP}			{ yybegin(YYINITIAL); return getLettersToken(ENGLISH,"Upper"); }
		{LETTERS_ENG_MIX}			{ yybegin(YYINITIAL); return getLettersToken(ENGLISH,"Mixed"); }

		{NEW_LINE}					{ yybegin(YYINITIAL); return getSeparatorToken("New line"); }
		{CAR_RET}					{ yybegin(YYINITIAL); return getSeparatorToken("Carrige return"); }
		{TAB}						{ yybegin(YYINITIAL); return getSeparatorToken("Tabulation"); }
		{SPACE} 					{ yybegin(YYINITIAL); return getSeparatorToken("Space"); }
		{FORM_FEED}					{ yybegin(YYINITIAL); return getSeparatorToken("Form feed"); }
		
		{BRACKET}					{ yybegin(YYINITIAL); return getBracketToken(); }
		
		{SYMBOLS}					{ yybegin(YYINITIAL); return getSymbolToken(); }
		
		{PM}						{ yybegin(YYINITIAL); return getPmToken(); }
		
		{ABBREVATION_RUS}   		{ yybegin(YYINITIAL); return getAbbrevationToken(RUSSIAN); }
		{ABBREVATION_ENG}   		{ yybegin(YYINITIAL); return getAbbrevationToken(ENGLISH); }

		{RANGE} {			
			currentRange = getRangeToken(null, null);
			yybegin(IN_RANGE);
			back();
		}

		{COMPLEX_LETTERS} {
			currentComplexWord = getComplexLettersToken(null, null, null, null);
			yybegin(IN_COMPLEX);
			back();
		}

		{CURRENCY} {
			currentCurrency = getCurrencyToken(null, null);
			yybegin(IN_CURRENCY);
			back();
		}

		{DATE_YMD} {
			currentDate = getDateToken(null,null,null);
			yybegin(IN_DATE_YMD);
			back();
		}

		{DATE_DMY} {
			currentDate = getDateToken(null,null,null);
			yybegin(IN_DATE_DMY);
			back();
		}

		{E_MAIL} {
			return getEmailToken();
		}
}

<IN_RANGE>
{
		{NUMBER_REAL_POSITIVE}	{ 
			if (inRange) {
				yybegin(YYINITIAL); 
				Number numberRight = getNumberToken("Real","Positive");
				currentRange.setRight(numberRight.getText());
				inRange = false;
				return currentRange;
			} else {
				yybegin(IN_RANGE);
				Number numberLeft = getNumberToken("Real","Positive");
				currentRange.setLeft(numberLeft.getText());
				inRange = true;
			}
		}

		{NUMBER_INTEGER_POSITIVE} {
			if (inRange) {
				yybegin(YYINITIAL); 
				Number numberRight = getNumberToken("Integer","Positive");
				currentRange.setRight(numberRight.getText());
				inRange = false;
				return currentRange;
			} else {
				yybegin(IN_RANGE);
				Number numberLeft = getNumberToken("Integer","Positive");
				currentRange.setLeft(numberLeft.getText());
				inRange = true;
			}
		}
}

<IN_CURRENCY>	
{
		{NUMBER_REAL_POSITIVE} {			 
			Number numberValue = getNumberToken("Real","Positive");
			currentCurrency.setValue(numberValue.getText());
			if (currencyEnd) {
				yybegin(YYINITIAL);
				currencyEnd = false;
				return currentCurrency;
			} else {
				yybegin(IN_CURRENCY);				
				currencyEnd = true;
			}
		}

		{NUMBER_REAL_NEGATIVE} {
			Number numberValue = getNumberToken("Real","Negative");
			currentCurrency.setValue(numberValue.getText());
			if (currencyEnd) {
				yybegin(YYINITIAL);
				currencyEnd = false;
				return currentCurrency;
			} else {
				yybegin(IN_CURRENCY);
				currencyEnd = true;
			}
		}

		{NUMBER_INTEGER_POSITIVE} {
			Number numberValue = getNumberToken("Integer","Positive");
			currentCurrency.setValue(numberValue.getText());
			if (currencyEnd) {
				yybegin(YYINITIAL);
				currencyEnd = false;
				return currentCurrency;
			} else {
				yybegin(IN_CURRENCY);			
				currencyEnd = true;
			}
		}

		{NUMBER_INTEGER_NEGATIVE} {
			Number numberValue = getNumberToken("Integer","Negative");
			currentCurrency.setValue(numberValue.getText());
			if (currencyEnd) {
				yybegin(YYINITIAL);
				currencyEnd = false;
				return currentCurrency;
			} else {
				yybegin(IN_CURRENCY);			
				currencyEnd = true;
			}
		}

		{CURRENCY_SYMBOL} {
			Symbol currencySymbol = getSymbolToken();
			currentCurrency.setCurrencySymbol(currencySymbol.getText());
			if (currencyEnd) {
				yybegin(YYINITIAL);
				currencyEnd = false;
				return currentCurrency;
			} else {
				yybegin(IN_CURRENCY);				
				currencyEnd = true;
			}
		}	
}

<IN_COMPLEX> 
{
	    {LETTERS_RUS} {
	    	if (inComplex) {
	    		yybegin(YYINITIAL);
	    		Letters lettersRight = getLettersToken(RUSSIAN, null);
	    		currentComplexWord = addToComplexLetters(currentComplexWord, lettersRight.getNorm(), RUSSIAN, false);
	    		inComplex = false;
	    		return currentComplexWord;
	    	} else {
	    		yybegin(IN_COMPLEX);
	    		Letters lettersLeft = getLettersToken(RUSSIAN, null);
	    		currentComplexWord = addToComplexLetters(currentComplexWord, lettersLeft.getNorm(), RUSSIAN, false);
	    		inComplex = true;
	    	}
	    }

	   	{LETTERS_ENG} {
	   		if (inComplex) {
                yybegin(YYINITIAL);
                Letters lettersRight = getLettersToken(ENGLISH, null);
                currentComplexWord = addToComplexLetters(currentComplexWord, lettersRight.getNorm(), ENGLISH, false);
                inComplex = false;
                return currentComplexWord;
            } else {
                yybegin(IN_COMPLEX);
                Letters lettersLeft = getLettersToken(ENGLISH, null);
                currentComplexWord = addToComplexLetters(currentComplexWord, lettersLeft.getNorm(), ENGLISH, false);
                inComplex = true;
            }
	   	}

	   	("-"|"—"|"'") {
            currentComplexWord = addToComplexLetters(currentComplexWord, yytext(), ENGLISH, true);
	   	}
}

<IN_DATE_YMD> 
{
	{YYYY} {
		Number number = getNumberToken("Integer","Positive");
		currentDate.setYear(number.getText());
	}

	{M}|{D} {
		Number number = getNumberToken("Integer","Positive");
		if (inDate) {
			currentDate.setDay(number.getText());
			yybegin(YYINITIAL);
			inDate = false;
			return currentDate;
		}
		else {
			currentDate.setMonth(number.getText());
			yybegin(IN_DATE_YMD);
			inDate = true;
		}
	}

	{MM}|{DD} {
		Number number = getNumberToken("Integer","Positive");
		if (inDate) {
			currentDate.setDay(number.getText());
			yybegin(YYINITIAL);
			inDate = false;
			return currentDate;
		}
		else {
			currentDate.setMonth(number.getText());
			yybegin(IN_DATE_YMD);
			inDate = true;
		}
	}
}

<IN_DATE_DMY> {
	{YYYY} {
		Number number = getNumberToken("Integer","Positive");
		currentDate.setYear(number.getText());
		yybegin(YYINITIAL);
		return currentDate;
	}

	{M}|{D} {
		Number number = getNumberToken("Integer","Positive");
		if (inDate) {
			currentDate.setMonth(number.getText());
			inDate = false;
		}
		else {
			currentDate.setDay(number.getText());			
			inDate = true;
		}
		yybegin(IN_DATE_DMY);
	}

	{MM}|{DD} {
		Number number = getNumberToken("Integer","Positive");
		if (inDate) {
			currentDate.setMonth(number.getText());			
			inDate = false;
		}
		else {
			currentDate.setDay(number.getText());			
			inDate = true;
		}
		yybegin(IN_DATE_DMY);
	}
}
