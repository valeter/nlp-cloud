package ru.misis.asu.nlp.tokenization;

import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.cas.text.Language;
import org.apache.uima.jcas.JCas;
import ru.misis.asu.nlp.tokenization.types.ComplexLettersEng;
import ru.misis.asu.nlp.tokenization.types.LettersEng;
import ru.misis.asu.nlp.tokenization.types.Token;

import java.io.CharArrayReader;
import java.io.IOException;

/**
 * @author Marsel Sidikov, KFU
 */
public class TokenizationAnnotator extends JCasAnnotator_ImplBase {
    @Override
    public void process(JCas cas) {
        String docText = cas.getDocumentText();
        docText = docText.replaceAll("`", "'");
        char[] text = docText.toCharArray();
        CharArrayReader reader = new CharArrayReader(text);
        JFlexTokenizer scanner = new JFlexTokenizer(reader, cas);
        while (!scanner.isEof()) {
            try {
                Token token = scanner.yylex();
                if (token != null) {
                    if (token instanceof ComplexLettersEng) {
                        ComplexLettersEng letters = (ComplexLettersEng) token;
                        if ("'".equals(letters.getSeparator())) {
                            LettersEng first = new LettersEng(cas), second = new LettersEng(cas);
                            int beginFirst = token.getBegin(), endFirst, endSecond = token.getEnd();
                            if (letters.getLeft().endsWith("n") && "t".equals(letters.getRight())) {
                                endFirst = beginFirst + letters.getLeft().length() - 1;
                            } else {
                                endFirst = beginFirst + letters.getLeft().length();
                            }
                            first.setLanguage("English");
                            first.setBegin(beginFirst);
                            first.setEnd(endFirst);
                            first.setNorm(first.getCoveredText().trim().toLowerCase());

                            second.setLanguage("English");
                            second.setBegin(endFirst);
                            second.setEnd(endSecond);
                            second.setNorm(second.getCoveredText().trim().toLowerCase());

                            first.addToIndexes(); second.addToIndexes();
                        }
                    } else {
                        token.addToIndexes();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
