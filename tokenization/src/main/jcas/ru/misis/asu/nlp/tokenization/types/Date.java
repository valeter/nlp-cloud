

/* First created by JCasGen Sun Sep 04 11:26:23 MSK 2016 */
package ru.misis.asu.nlp.tokenization.types;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Sun Sep 04 11:26:23 MSK 2016
 * XML source: /home/valter/projects/nlp-cloud/tokenization/src/main/resources/uima_xml/tokenization-ts.uima_xml
 * @generated */
public class Date extends Token {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Date.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Date() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public Date(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public Date(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public Date(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: Year

  /** getter for Year - gets 
   * @generated
   * @return value of the feature 
   */
  public String getYear() {
    if (Date_Type.featOkTst && ((Date_Type)jcasType).casFeat_Year == null)
      jcasType.jcas.throwFeatMissing("Year", "ru.misis.asu.nlp.tokenization.types.Date");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Date_Type)jcasType).casFeatCode_Year);}
    
  /** setter for Year - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setYear(String v) {
    if (Date_Type.featOkTst && ((Date_Type)jcasType).casFeat_Year == null)
      jcasType.jcas.throwFeatMissing("Year", "ru.misis.asu.nlp.tokenization.types.Date");
    jcasType.ll_cas.ll_setStringValue(addr, ((Date_Type)jcasType).casFeatCode_Year, v);}    
   
    
  //*--------------*
  //* Feature: Month

  /** getter for Month - gets 
   * @generated
   * @return value of the feature 
   */
  public String getMonth() {
    if (Date_Type.featOkTst && ((Date_Type)jcasType).casFeat_Month == null)
      jcasType.jcas.throwFeatMissing("Month", "ru.misis.asu.nlp.tokenization.types.Date");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Date_Type)jcasType).casFeatCode_Month);}
    
  /** setter for Month - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setMonth(String v) {
    if (Date_Type.featOkTst && ((Date_Type)jcasType).casFeat_Month == null)
      jcasType.jcas.throwFeatMissing("Month", "ru.misis.asu.nlp.tokenization.types.Date");
    jcasType.ll_cas.ll_setStringValue(addr, ((Date_Type)jcasType).casFeatCode_Month, v);}    
   
    
  //*--------------*
  //* Feature: Day

  /** getter for Day - gets 
   * @generated
   * @return value of the feature 
   */
  public String getDay() {
    if (Date_Type.featOkTst && ((Date_Type)jcasType).casFeat_Day == null)
      jcasType.jcas.throwFeatMissing("Day", "ru.misis.asu.nlp.tokenization.types.Date");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Date_Type)jcasType).casFeatCode_Day);}
    
  /** setter for Day - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setDay(String v) {
    if (Date_Type.featOkTst && ((Date_Type)jcasType).casFeat_Day == null)
      jcasType.jcas.throwFeatMissing("Day", "ru.misis.asu.nlp.tokenization.types.Date");
    jcasType.ll_cas.ll_setStringValue(addr, ((Date_Type)jcasType).casFeatCode_Day, v);}    
  }

    