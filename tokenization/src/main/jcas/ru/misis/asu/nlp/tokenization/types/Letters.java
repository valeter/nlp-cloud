

/* First created by JCasGen Sun Sep 04 11:26:23 MSK 2016 */
package ru.misis.asu.nlp.tokenization.types;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Sun Sep 04 11:26:23 MSK 2016
 * XML source: /home/valter/projects/nlp-cloud/tokenization/src/main/resources/uima_xml/tokenization-ts.uima_xml
 * @generated */
public class Letters extends Token {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Letters.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Letters() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public Letters(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public Letters(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public Letters(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: LetterCase

  /** getter for LetterCase - gets 
   * @generated
   * @return value of the feature 
   */
  public String getLetterCase() {
    if (Letters_Type.featOkTst && ((Letters_Type)jcasType).casFeat_LetterCase == null)
      jcasType.jcas.throwFeatMissing("LetterCase", "ru.misis.asu.nlp.tokenization.types.Letters");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Letters_Type)jcasType).casFeatCode_LetterCase);}
    
  /** setter for LetterCase - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setLetterCase(String v) {
    if (Letters_Type.featOkTst && ((Letters_Type)jcasType).casFeat_LetterCase == null)
      jcasType.jcas.throwFeatMissing("LetterCase", "ru.misis.asu.nlp.tokenization.types.Letters");
    jcasType.ll_cas.ll_setStringValue(addr, ((Letters_Type)jcasType).casFeatCode_LetterCase, v);}    
   
    
  //*--------------*
  //* Feature: Language

  /** getter for Language - gets 
   * @generated
   * @return value of the feature 
   */
  public String getLanguage() {
    if (Letters_Type.featOkTst && ((Letters_Type)jcasType).casFeat_Language == null)
      jcasType.jcas.throwFeatMissing("Language", "ru.misis.asu.nlp.tokenization.types.Letters");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Letters_Type)jcasType).casFeatCode_Language);}
    
  /** setter for Language - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setLanguage(String v) {
    if (Letters_Type.featOkTst && ((Letters_Type)jcasType).casFeat_Language == null)
      jcasType.jcas.throwFeatMissing("Language", "ru.misis.asu.nlp.tokenization.types.Letters");
    jcasType.ll_cas.ll_setStringValue(addr, ((Letters_Type)jcasType).casFeatCode_Language, v);}    
  }

    