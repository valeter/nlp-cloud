
/* First created by JCasGen Sun Sep 04 11:26:23 MSK 2016 */
package ru.misis.asu.nlp.tokenization.types;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Sun Sep 04 11:26:23 MSK 2016
 * @generated */
public class ComplexLetters_Type extends Letters_Type {
  /** @generated 
   * @return the generator for this type
   */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (ComplexLetters_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = ComplexLetters_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new ComplexLetters(addr, ComplexLetters_Type.this);
  			   ComplexLetters_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new ComplexLetters(addr, ComplexLetters_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = ComplexLetters.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("ru.misis.asu.nlp.tokenization.types.ComplexLetters");
 
  /** @generated */
  final Feature casFeat_Left;
  /** @generated */
  final int     casFeatCode_Left;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getLeft(int addr) {
        if (featOkTst && casFeat_Left == null)
      jcas.throwFeatMissing("Left", "ru.misis.asu.nlp.tokenization.types.ComplexLetters");
    return ll_cas.ll_getStringValue(addr, casFeatCode_Left);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setLeft(int addr, String v) {
        if (featOkTst && casFeat_Left == null)
      jcas.throwFeatMissing("Left", "ru.misis.asu.nlp.tokenization.types.ComplexLetters");
    ll_cas.ll_setStringValue(addr, casFeatCode_Left, v);}
    
  
 
  /** @generated */
  final Feature casFeat_Right;
  /** @generated */
  final int     casFeatCode_Right;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getRight(int addr) {
        if (featOkTst && casFeat_Right == null)
      jcas.throwFeatMissing("Right", "ru.misis.asu.nlp.tokenization.types.ComplexLetters");
    return ll_cas.ll_getStringValue(addr, casFeatCode_Right);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setRight(int addr, String v) {
        if (featOkTst && casFeat_Right == null)
      jcas.throwFeatMissing("Right", "ru.misis.asu.nlp.tokenization.types.ComplexLetters");
    ll_cas.ll_setStringValue(addr, casFeatCode_Right, v);}
    
  
 
  /** @generated */
  final Feature casFeat_Separator;
  /** @generated */
  final int     casFeatCode_Separator;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getSeparator(int addr) {
        if (featOkTst && casFeat_Separator == null)
      jcas.throwFeatMissing("Separator", "ru.misis.asu.nlp.tokenization.types.ComplexLetters");
    return ll_cas.ll_getStringValue(addr, casFeatCode_Separator);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setSeparator(int addr, String v) {
        if (featOkTst && casFeat_Separator == null)
      jcas.throwFeatMissing("Separator", "ru.misis.asu.nlp.tokenization.types.ComplexLetters");
    ll_cas.ll_setStringValue(addr, casFeatCode_Separator, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	 * @generated
	 * @param jcas JCas
	 * @param casType Type 
	 */
  public ComplexLetters_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_Left = jcas.getRequiredFeatureDE(casType, "Left", "uima.cas.String", featOkTst);
    casFeatCode_Left  = (null == casFeat_Left) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_Left).getCode();

 
    casFeat_Right = jcas.getRequiredFeatureDE(casType, "Right", "uima.cas.String", featOkTst);
    casFeatCode_Right  = (null == casFeat_Right) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_Right).getCode();

 
    casFeat_Separator = jcas.getRequiredFeatureDE(casType, "Separator", "uima.cas.String", featOkTst);
    casFeatCode_Separator  = (null == casFeat_Separator) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_Separator).getCode();

  }
}



    