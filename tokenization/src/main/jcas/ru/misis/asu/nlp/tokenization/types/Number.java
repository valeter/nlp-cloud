

/* First created by JCasGen Sun Sep 04 11:26:23 MSK 2016 */
package ru.misis.asu.nlp.tokenization.types;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Sun Sep 04 11:26:23 MSK 2016
 * XML source: /home/valter/projects/nlp-cloud/tokenization/src/main/resources/uima_xml/tokenization-ts.uima_xml
 * @generated */
public class Number extends Token {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Number.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Number() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public Number(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public Number(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public Number(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: Kind

  /** getter for Kind - gets 
   * @generated
   * @return value of the feature 
   */
  public String getKind() {
    if (Number_Type.featOkTst && ((Number_Type)jcasType).casFeat_Kind == null)
      jcasType.jcas.throwFeatMissing("Kind", "ru.misis.asu.nlp.tokenization.types.Number");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Number_Type)jcasType).casFeatCode_Kind);}
    
  /** setter for Kind - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setKind(String v) {
    if (Number_Type.featOkTst && ((Number_Type)jcasType).casFeat_Kind == null)
      jcasType.jcas.throwFeatMissing("Kind", "ru.misis.asu.nlp.tokenization.types.Number");
    jcasType.ll_cas.ll_setStringValue(addr, ((Number_Type)jcasType).casFeatCode_Kind, v);}    
   
    
  //*--------------*
  //* Feature: Sign

  /** getter for Sign - gets 
   * @generated
   * @return value of the feature 
   */
  public String getSign() {
    if (Number_Type.featOkTst && ((Number_Type)jcasType).casFeat_Sign == null)
      jcasType.jcas.throwFeatMissing("Sign", "ru.misis.asu.nlp.tokenization.types.Number");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Number_Type)jcasType).casFeatCode_Sign);}
    
  /** setter for Sign - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setSign(String v) {
    if (Number_Type.featOkTst && ((Number_Type)jcasType).casFeat_Sign == null)
      jcasType.jcas.throwFeatMissing("Sign", "ru.misis.asu.nlp.tokenization.types.Number");
    jcasType.ll_cas.ll_setStringValue(addr, ((Number_Type)jcasType).casFeatCode_Sign, v);}    
  }

    