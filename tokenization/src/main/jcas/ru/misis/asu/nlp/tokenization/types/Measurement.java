

/* First created by JCasGen Sun Sep 04 11:26:23 MSK 2016 */
package ru.misis.asu.nlp.tokenization.types;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Sun Sep 04 11:26:23 MSK 2016
 * XML source: /home/valter/projects/nlp-cloud/tokenization/src/main/resources/uima_xml/tokenization-ts.uima_xml
 * @generated */
public class Measurement extends Token {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Measurement.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Measurement() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public Measurement(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public Measurement(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public Measurement(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: UnitName

  /** getter for UnitName - gets 
   * @generated
   * @return value of the feature 
   */
  public String getUnitName() {
    if (Measurement_Type.featOkTst && ((Measurement_Type)jcasType).casFeat_UnitName == null)
      jcasType.jcas.throwFeatMissing("UnitName", "ru.misis.asu.nlp.tokenization.types.Measurement");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Measurement_Type)jcasType).casFeatCode_UnitName);}
    
  /** setter for UnitName - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setUnitName(String v) {
    if (Measurement_Type.featOkTst && ((Measurement_Type)jcasType).casFeat_UnitName == null)
      jcasType.jcas.throwFeatMissing("UnitName", "ru.misis.asu.nlp.tokenization.types.Measurement");
    jcasType.ll_cas.ll_setStringValue(addr, ((Measurement_Type)jcasType).casFeatCode_UnitName, v);}    
   
    
  //*--------------*
  //* Feature: Value

  /** getter for Value - gets 
   * @generated
   * @return value of the feature 
   */
  public Number getValue() {
    if (Measurement_Type.featOkTst && ((Measurement_Type)jcasType).casFeat_Value == null)
      jcasType.jcas.throwFeatMissing("Value", "ru.misis.asu.nlp.tokenization.types.Measurement");
    return (Number)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((Measurement_Type)jcasType).casFeatCode_Value)));}
    
  /** setter for Value - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setValue(Number v) {
    if (Measurement_Type.featOkTst && ((Measurement_Type)jcasType).casFeat_Value == null)
      jcasType.jcas.throwFeatMissing("Value", "ru.misis.asu.nlp.tokenization.types.Measurement");
    jcasType.ll_cas.ll_setRefValue(addr, ((Measurement_Type)jcasType).casFeatCode_Value, jcasType.ll_cas.ll_getFSRef(v));}    
  }

    