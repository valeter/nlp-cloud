

/* First created by JCasGen Sun Sep 04 11:26:23 MSK 2016 */
package ru.misis.asu.nlp.tokenization.types;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Sun Sep 04 11:26:23 MSK 2016
 * XML source: /home/valter/projects/nlp-cloud/tokenization/src/main/resources/uima_xml/tokenization-ts.uima_xml
 * @generated */
public class ComplexLetters extends Letters {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(ComplexLetters.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected ComplexLetters() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public ComplexLetters(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public ComplexLetters(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public ComplexLetters(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: Left

  /** getter for Left - gets 
   * @generated
   * @return value of the feature 
   */
  public String getLeft() {
    if (ComplexLetters_Type.featOkTst && ((ComplexLetters_Type)jcasType).casFeat_Left == null)
      jcasType.jcas.throwFeatMissing("Left", "ru.misis.asu.nlp.tokenization.types.ComplexLetters");
    return jcasType.ll_cas.ll_getStringValue(addr, ((ComplexLetters_Type)jcasType).casFeatCode_Left);}
    
  /** setter for Left - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setLeft(String v) {
    if (ComplexLetters_Type.featOkTst && ((ComplexLetters_Type)jcasType).casFeat_Left == null)
      jcasType.jcas.throwFeatMissing("Left", "ru.misis.asu.nlp.tokenization.types.ComplexLetters");
    jcasType.ll_cas.ll_setStringValue(addr, ((ComplexLetters_Type)jcasType).casFeatCode_Left, v);}    
   
    
  //*--------------*
  //* Feature: Right

  /** getter for Right - gets 
   * @generated
   * @return value of the feature 
   */
  public String getRight() {
    if (ComplexLetters_Type.featOkTst && ((ComplexLetters_Type)jcasType).casFeat_Right == null)
      jcasType.jcas.throwFeatMissing("Right", "ru.misis.asu.nlp.tokenization.types.ComplexLetters");
    return jcasType.ll_cas.ll_getStringValue(addr, ((ComplexLetters_Type)jcasType).casFeatCode_Right);}
    
  /** setter for Right - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setRight(String v) {
    if (ComplexLetters_Type.featOkTst && ((ComplexLetters_Type)jcasType).casFeat_Right == null)
      jcasType.jcas.throwFeatMissing("Right", "ru.misis.asu.nlp.tokenization.types.ComplexLetters");
    jcasType.ll_cas.ll_setStringValue(addr, ((ComplexLetters_Type)jcasType).casFeatCode_Right, v);}    
   
    
  //*--------------*
  //* Feature: Separator

  /** getter for Separator - gets 
   * @generated
   * @return value of the feature 
   */
  public String getSeparator() {
    if (ComplexLetters_Type.featOkTst && ((ComplexLetters_Type)jcasType).casFeat_Separator == null)
      jcasType.jcas.throwFeatMissing("Separator", "ru.misis.asu.nlp.tokenization.types.ComplexLetters");
    return jcasType.ll_cas.ll_getStringValue(addr, ((ComplexLetters_Type)jcasType).casFeatCode_Separator);}
    
  /** setter for Separator - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setSeparator(String v) {
    if (ComplexLetters_Type.featOkTst && ((ComplexLetters_Type)jcasType).casFeat_Separator == null)
      jcasType.jcas.throwFeatMissing("Separator", "ru.misis.asu.nlp.tokenization.types.ComplexLetters");
    jcasType.ll_cas.ll_setStringValue(addr, ((ComplexLetters_Type)jcasType).casFeatCode_Separator, v);}    
  }

    