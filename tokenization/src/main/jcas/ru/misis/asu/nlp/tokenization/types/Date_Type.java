
/* First created by JCasGen Sun Sep 04 11:26:23 MSK 2016 */
package ru.misis.asu.nlp.tokenization.types;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Sun Sep 04 11:26:23 MSK 2016
 * @generated */
public class Date_Type extends Token_Type {
  /** @generated 
   * @return the generator for this type
   */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Date_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Date_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Date(addr, Date_Type.this);
  			   Date_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Date(addr, Date_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Date.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("ru.misis.asu.nlp.tokenization.types.Date");
 
  /** @generated */
  final Feature casFeat_Year;
  /** @generated */
  final int     casFeatCode_Year;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getYear(int addr) {
        if (featOkTst && casFeat_Year == null)
      jcas.throwFeatMissing("Year", "ru.misis.asu.nlp.tokenization.types.Date");
    return ll_cas.ll_getStringValue(addr, casFeatCode_Year);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setYear(int addr, String v) {
        if (featOkTst && casFeat_Year == null)
      jcas.throwFeatMissing("Year", "ru.misis.asu.nlp.tokenization.types.Date");
    ll_cas.ll_setStringValue(addr, casFeatCode_Year, v);}
    
  
 
  /** @generated */
  final Feature casFeat_Month;
  /** @generated */
  final int     casFeatCode_Month;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getMonth(int addr) {
        if (featOkTst && casFeat_Month == null)
      jcas.throwFeatMissing("Month", "ru.misis.asu.nlp.tokenization.types.Date");
    return ll_cas.ll_getStringValue(addr, casFeatCode_Month);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setMonth(int addr, String v) {
        if (featOkTst && casFeat_Month == null)
      jcas.throwFeatMissing("Month", "ru.misis.asu.nlp.tokenization.types.Date");
    ll_cas.ll_setStringValue(addr, casFeatCode_Month, v);}
    
  
 
  /** @generated */
  final Feature casFeat_Day;
  /** @generated */
  final int     casFeatCode_Day;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getDay(int addr) {
        if (featOkTst && casFeat_Day == null)
      jcas.throwFeatMissing("Day", "ru.misis.asu.nlp.tokenization.types.Date");
    return ll_cas.ll_getStringValue(addr, casFeatCode_Day);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setDay(int addr, String v) {
        if (featOkTst && casFeat_Day == null)
      jcas.throwFeatMissing("Day", "ru.misis.asu.nlp.tokenization.types.Date");
    ll_cas.ll_setStringValue(addr, casFeatCode_Day, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	 * @generated
	 * @param jcas JCas
	 * @param casType Type 
	 */
  public Date_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_Year = jcas.getRequiredFeatureDE(casType, "Year", "uima.cas.String", featOkTst);
    casFeatCode_Year  = (null == casFeat_Year) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_Year).getCode();

 
    casFeat_Month = jcas.getRequiredFeatureDE(casType, "Month", "uima.cas.String", featOkTst);
    casFeatCode_Month  = (null == casFeat_Month) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_Month).getCode();

 
    casFeat_Day = jcas.getRequiredFeatureDE(casType, "Day", "uima.cas.String", featOkTst);
    casFeatCode_Day  = (null == casFeat_Day) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_Day).getCode();

  }
}



    