

/* First created by JCasGen Sun Sep 04 11:26:23 MSK 2016 */
package ru.misis.asu.nlp.tokenization.types;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Sun Sep 04 11:26:23 MSK 2016
 * XML source: /home/valter/projects/nlp-cloud/tokenization/src/main/resources/uima_xml/tokenization-ts.uima_xml
 * @generated */
public class Currency extends Token {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Currency.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Currency() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public Currency(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public Currency(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public Currency(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: Value

  /** getter for Value - gets 
   * @generated
   * @return value of the feature 
   */
  public String getValue() {
    if (Currency_Type.featOkTst && ((Currency_Type)jcasType).casFeat_Value == null)
      jcasType.jcas.throwFeatMissing("Value", "ru.misis.asu.nlp.tokenization.types.Currency");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Currency_Type)jcasType).casFeatCode_Value);}
    
  /** setter for Value - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setValue(String v) {
    if (Currency_Type.featOkTst && ((Currency_Type)jcasType).casFeat_Value == null)
      jcasType.jcas.throwFeatMissing("Value", "ru.misis.asu.nlp.tokenization.types.Currency");
    jcasType.ll_cas.ll_setStringValue(addr, ((Currency_Type)jcasType).casFeatCode_Value, v);}    
   
    
  //*--------------*
  //* Feature: CurrencySymbol

  /** getter for CurrencySymbol - gets 
   * @generated
   * @return value of the feature 
   */
  public String getCurrencySymbol() {
    if (Currency_Type.featOkTst && ((Currency_Type)jcasType).casFeat_CurrencySymbol == null)
      jcasType.jcas.throwFeatMissing("CurrencySymbol", "ru.misis.asu.nlp.tokenization.types.Currency");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Currency_Type)jcasType).casFeatCode_CurrencySymbol);}
    
  /** setter for CurrencySymbol - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setCurrencySymbol(String v) {
    if (Currency_Type.featOkTst && ((Currency_Type)jcasType).casFeat_CurrencySymbol == null)
      jcasType.jcas.throwFeatMissing("CurrencySymbol", "ru.misis.asu.nlp.tokenization.types.Currency");
    jcasType.ll_cas.ll_setStringValue(addr, ((Currency_Type)jcasType).casFeatCode_CurrencySymbol, v);}    
  }

    